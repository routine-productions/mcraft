<?php


DB::listen(function ($sql, $bindings, $time) {
    //var_dump($sql);
});


//
Route::get('pay-yandex', ['as' => 'pay-yandex', 'uses' => 'MSQ\Controllers\Home@postYandex']);


// Base Routes
Route::get('news', ['as' => 'posts', 'uses' => 'MSQ\Controllers\Home@getPosts']);
Route::get('news/page/{page?}', ['as' => 'posts_list', 'uses' => 'MSQ\Controllers\Home@getPosts']);
Route::get('news/{alias}', ['as' => 'post', 'uses' => 'MSQ\Controllers\Home@getPost']);

// Filter
Route::get('servera-{params}', ['as' => 'filter', 'uses' => 'MSQ\Controllers\Home@filter']);
Route::get('servera-{params}/page/{page?}', ['as' => 'filter-page', 'uses' => 'MSQ\Controllers\Home@getHome']);

Route::get('servera', function () {
    return Redirect::route('home');
});

Route::get('/servera/', function () {
    return Redirect::route('home');
});

// Home Page (GET)
Route::get('/', ['as' => 'home', 'uses' => 'MSQ\Controllers\Home@getHome']);
Route::get('page/{page?}', ['as' => 'servers', 'uses' => 'MSQ\Controllers\Home@getHome']);
Route::get('top', ['as' => 'top', 'uses' => 'MSQ\Controllers\Home@getTopServers']);
Route::get('top/page/{page?}', ['as' => 'top-page', 'uses' => 'MSQ\Controllers\Home@getTopServers']);

Route::get('new-servers', ['as' => 'home', 'uses' => 'MSQ\Controllers\Home@getNewServers']);
Route::get('new-servers/page/{page?}', ['as' => 'new-servers-page', 'uses' => 'MSQ\Controllers\Home@getNewServers']);


// Payments
Route::post('/pay', ['as' => 'pay', 'uses' => 'MSQ\Controllers\Home@setPayment']);
Route::get('/pay/{id}', ['as' => 'pay_id', 'uses' => 'MSQ\Controllers\Home@getPayment']);

Route::any('/pay-success', ['as' => 'pay_success', 'uses' => 'MSQ\Controllers\Home@paySuccess']);
Route::any('/pay-error', ['as' => 'pay_error', 'uses' => 'MSQ\Controllers\Home@payError']);
Route::any('/webmoney-result', [ 'uses' => 'MSQ\Controllers\Home@webmoneyResult']);
Route::any('/unitpay-result', [ 'uses' => 'MSQ\Controllers\Home@unitpayResult']);


Route::get('faq', ['as' => 'faq', 'uses' => 'MSQ\Controllers\Home@faq']);


// User Profile (GET)
Route::get('profile/{username}', ['as' => 'profile', 'uses' => 'MSQ\Controllers\Home@getProfile']);

// Server Banner (GET)
Route::get('banner/{id}/{type?}/{no?}', ['as' => 'server-banner', 'uses' => 'MSQ\Controllers\Server@getBanner']);

// Servers by tag (GET)
Route::get('tag/{slug}/{page?}', ['as' => 'tag', 'uses' => 'MSQ\Controllers\Home@getServersByTag']);

// Servers by mode (GET)
Route::get('mode/{slug}/{page?}', ['as' => 'mode', 'uses' => 'MSQ\Controllers\Home@getServersByMode']);

// Servers by game (GET)
Route::get('game/{slug}/{page?}', ['as' => 'game', 'uses' => 'MSQ\Controllers\Home@getServersByGame']);

// Servers by country (GET)
Route::get('country/{code}/{page?}', ['as' => 'country', 'uses' => 'MSQ\Controllers\Home@getServersByCountry']);

// Servers by version (GET)
Route::get('version/{name}/{page?}', ['as' => 'version', 'uses' => 'MSQ\Controllers\Home@getServersByVersion']);

Route::group(['prefix' => 'cron'], function () {

    Route::get('servers/statistics/key={key}', ['as' => 'cron-statistics', 'uses' => 'MSQ\Controllers\Cron@getServersStatistics']);

    Route::get('servers/rank/key={key}', ['as' => 'cron-rank', 'uses' => 'MSQ\Controllers\Cron@getServersRank']);
});

Route::group(['prefix' => 'search'], function () {
    // Search (GET)
    Route::get('{term}', ['as' => 'search', 'uses' => 'MSQ\Controllers\Home@getSearch']);

    Route::post('', ['as' => 'search-post', 'uses' => 'MSQ\Controllers\Home@postSearch']);

    Route::get('{term}/{page?}', ['as' => 'search-servers', 'uses' => 'MSQ\Controllers\Home@getSearch']);
});

// Contact (GET)
Route::get('contacts', ['as' => 'contact', 'uses' => 'MSQ\Controllers\Home@getContact']);

// Contact (POST)
Route::post('contacts', ['as' => 'contact-post', 'uses' => 'MSQ\Controllers\Home@postContact']);

// Server group
Route::group(['prefix' => 'server'], function () {

    // CSRF protection group
    Route::group(['before' => 'csrf'], function () {
        // Server Comments (POST)
        Route::post('{id}/comments', ['as' => 'server-comments-post', 'uses' => 'MSQ\Controllers\Server@postServerComments']);

        // Server Vote (POST)
        Route::post('{id}/vote', ['as' => 'server-vote-post', 'uses' => 'MSQ\Controllers\Server@postServerVote']);

        // Server edit (POST)
        Route::post('{id}/edit', ['as' => 'server-edit-post', 'uses' => 'MSQ\Controllers\Server@postEditServer']);
    });

    // Server Banners (GET)
    Route::get('{id}/banners', ['as' => 'server-banners', 'uses' => 'MSQ\Controllers\Server@getServerBanners']);

    // Server Comments (GET)
    Route::get('{id}/comments', ['as' => 'server-comments', 'uses' => 'MSQ\Controllers\Server@getServerComments']);

    // Server Vote (GET)
    Route::get('{id}/vote', ['as' => 'server-vote', 'uses' => 'MSQ\Controllers\Server@getServerVote']);

    // Server page (GET)
    Route::get('{id}', ['as' => 'server-page', 'uses' => 'MSQ\Controllers\Server@getServerPage'])->where('id', '[0-9]+');

});

Route::post('/success-vip', ['as' => 'success-vip', 'uses' => 'MSQ\Controllers\Home@postIpn']);
Route::any('/thank-you', ['as' => 'thank-you', 'uses' => 'MSQ\Controllers\Home@anyThankYou']);

// Authenticated group
Route::group(['before' => 'auth'], function () {

    // Vip (GET)
    Route::get('/vip', ['as' => 'vip', 'uses' => 'MSQ\Controllers\Home@getVip']);

    // View payment (GET)
    Route::get('/view-payment/{txn}', ['as' => 'view-payment', 'uses' => 'MSQ\Controllers\Home@getViewPayment']);

    // Confirm VIP (POST)
    Route::post('/vip', ['as' => 'vip-post', 'uses' => 'MSQ\Controllers\Home@postVip']);

    // CSRF protection group
    Route::group(['before' => 'csrf'], function () {
        // Add server (POST)
        Route::post('server/add', ['as' => 'server-add-post', 'uses' => 'MSQ\Controllers\Server@postServerAdd']);


    });

    // Add server (GET)
    Route::get('server/add', ['as' => 'server-add', 'uses' => 'MSQ\Controllers\Server@getServerAdd']);

    // Server edit (GET)
    Route::get('server/{id}/edit', ['as' => 'server-edit', 'uses' => 'MSQ\Controllers\Server@getEditServer'])->where('id', '[0-9]+');

    // Server delete (GET)
    Route::get('server/{id}/delete', ['as' => 'server-delete', 'uses' => 'MSQ\Controllers\Server@deleteServer'])->where('id', '[0-9]+');


    // Servers group
    Route::group(['prefix' => 'servers'], function () {

        // Manage servers (GET)
        Route::get('manage/{page?}', ['as' => 'servers-manage', 'uses' => 'MSQ\Controllers\Server@getManageServers']);
    });
});

// Json group
Route::group(['before' => 'ajax', 'prefix' => 'json'], function () {

    // Check connection server (POST)
    Route::post('check-connection', ['as' => 'check-connection', 'uses' => 'MSQ\Controllers\Ajax\ServerController@postCheckConnection']);
});

Route::get('account', ['as' => 'account', 'uses' => 'MSQ\Controllers\Account@getAccount']);

// Authenticated group
Route::group(['before' => 'auth', 'prefix' => 'account'], function () {

    // CSRF protection group
    Route::group(['before' => 'csrf'], function () {
        // Change passwotd (POST)
        Route::post('change-password', ['as' => 'change-password-post', 'uses' => 'MSQ\Controllers\Account@postChangePassword']);

        // Edit account (POST)
        Route::post('edit', ['as' => 'edit-post', 'uses' => 'MSQ\Controllers\Account@postEditAccount']);
    });

    // Logout (GET)
    Route::get('logout', ['as' => 'logout', 'uses' => 'MSQ\Controllers\Account@getLogOut']);

    // Change passwotd (GET)
    Route::get('change-password', ['as' => 'change-password', 'uses' => 'MSQ\Controllers\Account@getChangePassword']);

    // Dashboard (GET)
    Route::get('dashboard', ['as' => 'dashboard', 'uses' => 'MSQ\Controllers\Account@getDashboard']);

    // Edit account (GET)
    Route::get('edit', ['as' => 'edit', 'uses' => 'MSQ\Controllers\Account@getEditAccount']);
});

Route::group(['before' => 'admin'], function () {
    Route::group(['prefix' => 'admin'], function () {
        Route::get('dashboard', ['as' => 'admin-dashboard', 'uses' => 'MSQ\Controllers\Admin@getDashboard']);

        Route::get('', ['as' => 'admin', 'uses' => 'MSQ\Controllers\Admin@getAdmin']);

        Route::get('settings', ['as' => 'settings', 'uses' => 'MSQ\Controllers\Admin@getSettings']);

        Route::get('settings/configuration', ['as' => 'settings-configuration', 'uses' => 'MSQ\Controllers\Admin@getSettingsConfiguration']);
        Route::post('settings/configuration', ['as' => 'settings-configuration-post', 'uses' => 'MSQ\Controllers\Admin@postSettingsConfiguration']);

        Route::get('settings/seo', ['as' => 'settings-seo', 'uses' => 'MSQ\Controllers\Admin@getSettingsSeo']);
        Route::post('settings/seo', ['as' => 'settings-seo-post', 'uses' => 'MSQ\Controllers\Admin@postSettingsSeo']);

        Route::get('settings/vip', ['as' => 'settings-vip', 'uses' => 'MSQ\Controllers\Admin@getSettingsVip']);
        Route::post('settings/vip', ['as' => 'settings-vip-post', 'uses' => 'MSQ\Controllers\Admin@postSettingsVip']);

        Route::get('ads', ['as' => 'settings-ads', 'uses' => 'MSQ\Controllers\Admin@getAds']);
        Route::post('ads', ['as' => 'settings-ads-post', 'uses' => 'MSQ\Controllers\Admin@postAds']);

        Route::get('users/create', ['as' => 'users-create', 'uses' => 'MSQ\Controllers\Admin@getUsersCreate']);
        Route::post('users/create', ['as' => 'users-create-post', 'uses' => 'MSQ\Controllers\Admin@postUsersCreate']);

        Route::get('users/all', ['as' => 'users-all', 'uses' => 'MSQ\Controllers\Admin@getUsersAll']);

        Route::get('users/edit/{id}', ['as' => 'users-edit', 'uses' => 'MSQ\Controllers\Admin@getEditUser']);
        Route::post('users/edit/{id}', ['as' => 'users-edit-post', 'uses' => 'MSQ\Controllers\Admin@postEditUser']);

        Route::get('servers/all', ['as' => 'servers-all', 'uses' => 'MSQ\Controllers\Admin@getServersAll']);

        Route::get('servers/edit/{id}', ['as' => 'servers-edit', 'uses' => 'MSQ\Controllers\Admin@getEditServer']);
        Route::post('servers/edit/{id}', ['as' => 'servers-edit-post', 'uses' => 'MSQ\Controllers\Admin@postEditServer']);

        Route::get('comments', ['as' => 'comments', 'uses' => 'MSQ\Controllers\Admin@getComments']);

        Route::get('vip', ['as' => 'vip-admin', 'uses' => 'MSQ\Controllers\Admin@getVip']);
        Route::get('vip/edit/{id}', ['as' => 'vip-edit', 'uses' => 'MSQ\Controllers\Admin@getEditVip']);
        Route::post('vip/edit/{id}', ['as' => 'vip-edit-post', 'uses' => 'MSQ\Controllers\Admin@postEditVip']);
        Route::get('vip/view/{id}', ['as' => 'vip-view', 'uses' => 'MSQ\Controllers\Admin@getViewVip']);

        Route::get('delete/{option}/{id}', ['as' => 'delete', 'uses' => 'MSQ\Controllers\Admin@getDelete']);

        Route::get('tools/cron', ['as' => 'tools-cron', 'uses' => 'MSQ\Controllers\Admin@getToolsCron']);

        Route::post('tools/cron', ['as' => 'tools-cron-post', 'uses' => 'MSQ\Controllers\Admin@postToolsCron']);

        // versions
        Route::get('versions/create', ['as' => 'versions-create', 'uses' => 'MSQ\Controllers\Admin@getVersionsCreate']);
        Route::post('versions/create', ['as' => 'versions-create', 'uses' => 'MSQ\Controllers\Admin@postVersionsCreate']);

        Route::get('versions/all', ['as' => 'versions-all', 'uses' => 'MSQ\Controllers\Admin@getVersionsAll']);

        Route::get('versions/edit/{id}', ['as' => 'versions-edit', 'uses' => 'MSQ\Controllers\Admin@getEditVersions']);
        Route::post('versions/edit/{id}', ['as' => 'versions-edit', 'uses' => 'MSQ\Controllers\Admin@postEditVersions']);

        // tags
        Route::get('tags/create', ['as' => 'tags-create', 'uses' => 'MSQ\Controllers\Admin@getTagsCreate']);
        Route::post('tags/create', ['as' => 'tags-create', 'uses' => 'MSQ\Controllers\Admin@postTagsCreate']);

        Route::get('tags/all', ['as' => 'tags-all', 'uses' => 'MSQ\Controllers\Admin@getTagsAll']);

        Route::get('tags/edit/{id}', ['as' => 'tags-edit', 'uses' => 'MSQ\Controllers\Admin@getEditTags']);
        Route::post('tags/edit/{id}', ['as' => 'tags-edit', 'uses' => 'MSQ\Controllers\Admin@postEditTags']);

        // modes
        Route::get('modes/all', ['as' => 'modes-all', 'uses' => 'MSQ\Controllers\Admin@getModesAll']);

        Route::get('modes/create', ['as' => 'modes-create', 'uses' => 'MSQ\Controllers\Admin@getModesCreate']);
        Route::post('modes/create', ['as' => 'modes-create', 'uses' => 'MSQ\Controllers\Admin@postModesCreate']);

        Route::get('modes/edit/{id}', ['as' => 'modes-edit', 'uses' => 'MSQ\Controllers\Admin@getModesEdit']);
        Route::post('modes/edit/{id}', ['as' => 'modes-edit', 'uses' => 'MSQ\Controllers\Admin@postModesEdit']);

        // games
        Route::get('games/create', ['as' => 'games-create', 'uses' => 'MSQ\Controllers\Admin@getGamesCreate']);
        Route::post('games/create', ['as' => 'games-create', 'uses' => 'MSQ\Controllers\Admin@postGamesCreate']);

        Route::get('games/all', ['as' => 'games-all', 'uses' => 'MSQ\Controllers\Admin@getGamesAll']);

        Route::get('games/edit/{id}', ['as' => 'games-edit', 'uses' => 'MSQ\Controllers\Admin@getGamesEdit']);
        Route::post('games/edit/{id}', ['as' => 'games-edit', 'uses' => 'MSQ\Controllers\Admin@postGamesEdit']);

        // news
        Route::get('news/create', ['as' => 'news-create', 'uses' => 'MSQ\Controllers\Admin@getNewsCreate']);
        Route::post('news/create', ['as' => 'news-create', 'uses' => 'MSQ\Controllers\Admin@postNewsCreate']);

        Route::get('news/all', ['as' => 'news-all', 'uses' => 'MSQ\Controllers\Admin@getNewsAll']);

        Route::get('news/edit/{id}', ['as' => 'news-edit', 'uses' => 'MSQ\Controllers\Admin@getNewsEdit']);
        Route::post('news/edit/{id}', ['as' => 'news-edit', 'uses' => 'MSQ\Controllers\Admin@postNewsEdit']);


        // purchases
        Route::get('purchases/create', ['as' => 'purchases-create', 'uses' => 'MSQ\Controllers\Admin@getPurchasesCreate']);
        Route::post('purchases/create', ['as' => 'purchases-create', 'uses' => 'MSQ\Controllers\Admin@postPurchasesCreate']);

        Route::get('purchases/all', ['as' => 'purchases-all', 'uses' => 'MSQ\Controllers\Admin@getPurchasesAll']);

        Route::get('purchases/edit/{id}', ['as' => 'purchases-edit', 'uses' => 'MSQ\Controllers\Admin@getPurchasesEdit']);
        Route::post('purchases/edit/{id}', ['as' => 'purchases-edit', 'uses' => 'MSQ\Controllers\Admin@postPurchasesEdit']);

        // pay_types
        Route::get('pay_types/create', ['as' => 'pay_types-create', 'uses' => 'MSQ\Controllers\Admin@getPayTypesCreate']);
        Route::post('pay_types/create', ['as' => 'pay_types-create', 'uses' => 'MSQ\Controllers\Admin@postPayTypesCreate']);

        Route::get('pay_types/all', ['as' => 'pay_types-all', 'uses' => 'MSQ\Controllers\Admin@getPayTypesAll']);

        Route::get('pay_types/edit/{id}', ['as' => 'pay_types-edit', 'uses' => 'MSQ\Controllers\Admin@getPayTypesEdit']);
        Route::post('pay_types/edit/{id}', ['as' => 'pay_types-edit', 'uses' => 'MSQ\Controllers\Admin@postPayTypesEdit']);
    });
});

Route::group(['before' => 'guest', 'prefix' => 'account'], function () {

    // CSRF protection group
    Route::group(['before' => 'csrf'], function () {

        // Register (POST)
        Route::post('register', ['as' => 'register-post', 'uses' => 'MSQ\Controllers\Account@postRegister']);

        // Login (POST)
        Route::post('login', ['as' => 'login-post', 'uses' => 'MSQ\Controllers\Account@postLogin']);

        // Forgot password (POST)
        Route::post('forgot-password', ['as' => 'forgot-password-post', 'uses' => 'MSQ\Controllers\Account@postForgotPassword']);
    });

    // Register (GET)
    Route::get('register', ['as' => 'register', 'uses' => 'MSQ\Controllers\Account@getRegister']);

    // Login (GET)
    Route::get('login', ['as' => 'login', 'uses' => 'MSQ\Controllers\Account@getLogin']);

    // Activate (GET)
    Route::get('activate/{code}', ['as' => 'activate', 'uses' => 'MSQ\Controllers\Account@getActivate']);

    // Forgot password (GET)
    Route::get('forgot-password', ['as' => 'forgot-password', 'uses' => 'MSQ\Controllers\Account@getForgotPassword']);

    // Forgot password (GET)
    Route::get('recover/{code}', ['as' => 'recover', 'uses' => 'MSQ\Controllers\Account@getRecover']);
    // Nothing found
//    Route::get('dsfesf897897dfs', ['as' => 'not_found', 'uses' => 'MSQ\Controllers\Home@nothing_found']);
});


