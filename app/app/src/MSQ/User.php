<?php namespace MSQ;

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;
use Illuminate\Database\Eloquent\Model;

class User extends Model implements UserInterface, RemindableInterface {

	protected $table = 'users';

	protected $hidden = array('password');

	protected $fillable = array('email', 'username', 'password', 'role', 'code', 'active', 'website', 'facebook_page', 'google_page', 'youtube_url', 'steam_url', 'twitter_username', 'skype_username', 'minecraft_username');

	/**
	 * Count number of servers for an user account
	 */
	public function getTotalServersAttribute()
	{
		return $this->hasMany('MSQ\Server')->whereUserId($this->id)->count();
	}

	/**
	 * Count number of comments for an user account
	 * @return [type] [description]
	 */
	public function getTotalCommentsAttribute()
	{
		return $this->hasMany('MSQ\Comment')->whereUserId($this->id)->count();
	}

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the token value for the "remember me" session.
	 *
	 * @return string
	 */
	public function getRememberToken()
	{
		return $this->remember_token;
	}

	/**
	 * Set the token value for the "remember me" session.
	 *
	 * @param  string  $value
	 * @return void
	 */
	public function setRememberToken($value)
	{
		$this->remember_token = $value;
	}

	/**
	 * Get the column name for the "remember me" token.
	 *
	 * @return string
	 */
	public function getRememberTokenName()
	{
		return 'remember_token';
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

	/**
	 * Check if an user is admin
	 * @return boolean
	 */
	public function isAdmin()
	{
		return ($this->role == 'admin') ? true : false;
	}

	/**
	 * Check if an user is member
	 * @return boolean
	 */
	public function isMember()
	{
		return ($this->role == 'member') ? true : false;
	}

}