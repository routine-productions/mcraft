<?php namespace MSQ;

use Illuminate\Database\Eloquent\Model;

class Post extends Model {

	public $table = 'posts';
	public $timestamps = false;


//	protected $fillable = array('title', 'post');
    protected $guarded = ['id'];
}