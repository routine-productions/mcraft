<?php namespace MSQ;

use Illuminate\Database\Eloquent\Model;

class Purchases extends Model {

	protected $table = 'purchases';
	public $timestamps = true;

	protected $guarded = ['id'];

	public function server()
	{
		return $this->belongsTo('MSQ\Server');
	}

//    public function paytype()
//    {
//        return $this->hasOne('MSQ\Paytypes');
//    }
}