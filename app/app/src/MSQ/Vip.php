<?php namespace MSQ;

use Illuminate\Database\Eloquent\Model;

class Vip extends Model {

	protected $table = 'vips';

	public $timestamps = false;

	protected $fillable = array('server_id', 'start', 'end', 'days', 'txn_id', 'mc_gross', 'payment_status', 'payer_first_name', 'payer_last_name', 'payer_email', 'active');

	public function server()
	{
		return $this->belongsTo('MSQ\Server');
	}

	public function statistics()
	{
		return $this->belongsTo('MSQ\Statistics', 'server_id');
	}

	public function version()
	{
		return $this->belongsTo('MSQ\Version', 'server_id');
	}

	public function country()
	{
		return $this->belongsTo('MSQ\Country', 'server_id');
	}

}