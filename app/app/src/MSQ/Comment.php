<?php namespace MSQ;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model {

	protected $table = 'comments';

	protected $fillable = array('server_id', 'user_id', 'body');

	/**
	 * Get owner of server
	 */
	public function user()
	{
		return $this->belongsTo('MSQ\User');
	}

	public function server()
	{
		return $this->belongsTo('MSQ\Server');
	}

	/**
	 * Settings null value for created_at column
	 */
	public function setUpdatedAtAttribute($value)
	{
	    // Do nothing.
	}

}