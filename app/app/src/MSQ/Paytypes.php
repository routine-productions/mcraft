<?php namespace MSQ;

use Illuminate\Database\Eloquent\Model;

class Paytypes extends Model {

	protected $table = 'pay_types';

	protected $guarded = ['id'];
}