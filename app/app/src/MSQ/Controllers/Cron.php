<?php namespace MSQ\Controllers;

use Illuminate\Routing\Controller;
use MSQ\Repositories\Server\ServerInterface;
use MSQ\Classes\Minecraft\MinecraftQuery;
use MSQ\Repositories\Tag\TagInterface;
use MSQ\Repositories\Statistics\StatisticsInterface;
use MSQ\Repositories\Post\PostInterface;
use Config, App, DB;

class Cron extends Controller
{

    protected $server;
    protected $version;
    protected $statistics;
    protected $post;

    public function __construct(ServerInterface $server, TagInterface $version, StatisticsInterface $statistics, PostInterface $post)
    {
        $this->server = $server;
        $this->version = $version;
        $this->statistics = $statistics;
        $this->post = $post;
        View::share('news', $this->post->getLastNews(4));
    }

    public function getServersStatistics($key)
    {
        if ($key == Config::get('app.cron_key')) {
            $servers = $this->server->all();

            foreach ($servers as $server) {
                $mc = new MinecraftQuery();
                $version = $this->version->find($server->version_id);
                if($version){
                    $version =  $version->name;
                }else{
                    $version = '1.7';
                }

				$response = $mc->getStatus($server->address, $server->port, $version);

				if ($response) {
					$this->server->find($server->id)->update(array(
						'online_time'   => $server->online_time + 1,
						'verified_time' => $server->verified_time + 1,
						'status'        => 'online',
						'points'        => $server->points + 1
					));

					$statistics = $this->statistics->getFirstBy('server_id', $server->id);

					if ($this->statistics->getCountFirstBy('server_id', $server->id)) {
						$statistics->create(array(
							'server_id'   => $server->id,
							'players'     => $response['players'],
							'max_players' => $response['maxplayers'],
							'motd'        => $response['motd'],
							'favicon'	  => $response['favicon']
						));
					} else {
						$this->statistics->create(array(
							'server_id'   => $server->id,
							'players'     => $response['players'],
							'max_players' => $response['maxplayers'],
							'motd'        => $response['motd'],
							'favicon'	  => $response['favicon']
						));
					}


				} else {
					$this->server->find($server->id)->update(array(
						'verified_time' => $server->verified_time + 1,
						'status'        => 'offline',
						'points'        => $server->points > 0 ? $server->points - 1 : 0
					));
				}
            }
        } else {
            return App::abort(403, 'Unauthorized action.');
        }
    }

    public function getServersRank($key)
    {
        if ($key == Config::get('app.cron_key')) {
            DB::statement(DB::raw("SET @r=0"));
            DB::statement('UPDATE servers SET rank= @r:= (@r+1) ORDER BY points DESC;');
        } else {
            return App::abort(403, 'Unauthorized action.');
        }
    }

    public function purchasesNotifications($key)
    {

    }
}