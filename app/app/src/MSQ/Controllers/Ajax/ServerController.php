<?php namespace MSQ\Controllers\Ajax;

use Illuminate\Routing\Controller;
use MSQ\Ajax\ServerAjax;
use Input;

class ServerController extends Controller {

	protected $ajax;

	public function __construct(ServerAjax $ajax)
	{
		$this->ajax = $ajax;
	}

	public function postCheckConnection()
	{
		$port = Input::has('port') ? Input::get('port') : 25565;

		if ($exists = $this->ajax->checkIfExists(Input::get('address'), $port)) {
			return $exists;
		} else {
			if ($status = $this->ajax->checkServerStatus(Input::get('address'), $port, Input::get('version'))) {
				return $status;
			}
		}
	}

}