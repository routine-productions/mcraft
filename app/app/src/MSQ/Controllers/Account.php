<?php namespace MSQ\Controllers;

use Illuminate\Routing\Controller;
use View, Validator, Input, Redirect, Hash, Mail, URL, Auth, Config;
use MSQ\Repositories\User\UserInterface;
use MSQ\Repositories\Post\PostInterface;

use MSQ\Repositories\Tag\TagInterface;


class Account extends Controller
{

    protected $user;
    protected $post;

    public function __construct(UserInterface $user, TagInterface $tag, PostInterface $post)
    {
        $this->user = $user;
        $this->post = $post;
        $this->tag = $tag;

        View::share('tags', $this->tag->getAllTags());
        View::share('games', $this->tag->getAllGames());
        View::share('modes', $this->tag->getAllModes());
        View::share('versions', $this->tag->getAllVersions());
        View::share('news', $this->post->getLastNews(4));
    }

    public function getLogin()
    {
        return Redirect::route('home');
    }

    public function postLogin()
    {
        $validator = Validator::make(Input::all(), Config::get('validation.login'));

        if ($validator->fails()) {
            echo json_encode([
                'status'   => 'error',
                'messages' => $validator->errors()->all()
            ]);
        } else {


            if (Auth::attempt([
                'email'    => Input::get('email'),
                'password' => Input::get('password'),
                'active'   => 1
            ])
            ) {
                echo json_encode([
                    'status'  => 'success',
                    'message' => []
                ]);
            } else {
                echo json_encode([
                    'status'   => 'error',
                    'messages' => [0 => 'Неверный логин или пароль. Повторите попытку.']
                ]);
            }
        }
    }

    public function getLogOut()
    {
        Auth::logout();
        return Redirect::route('home');
    }

    public function postRegister()
    {
        $validator = Validator::make(Input::all(), Config::get('validation.register'));

        if ($validator->fails()) {
            echo json_encode([
                'status'   => 'error',
                'messages' => $validator->errors()->all()
            ]);
        } else {
            $username = Input::get('username');
            $email    = Input::get('email');
            $password = Input::get('password');


            $user = $this->user->create([
                'username' => $username,
                'email'    => $email,
                'password' => Hash::make($password),
                'active'   => 1
            ]);

            if ($user) {
                Auth::attempt([
                    'email'    => $email,
                    'password' => $password,
                    'active'   => 1
                ]);

                echo json_encode([
                    'status'   => 'success',
                    'messages' => []
                ]);
            }

        }
    }

    public function postForgotPassword()
    {
        $validator = Validator::make(Input::all(), Config::get('validation.forgot_password'));

        if ($validator->fails()) {
            echo json_encode([
                'status'   => 'error',
                'messages' => $validator->errors()->all()
            ]);
        } else {
            if ($this->user->forgotPassword(Input::get('email'))) {
                echo json_encode([
                    'status'   => 'success-password',
                    'messages' => [trans('general.forgot_password')]
                ]);
            }
        }
    }


    public function getChangePassword()
    {
        return View::make('account.change-password', [
            'title' => trans('general.change_password_title', ['title' => Config::get('app.seo_meta_title')])
        ]);
    }

    public function postChangePassword()
    {
        $validator = Validator::make(Input::all(), Config::get('validation.change_password'));

        if ($validator->fails()) {
            return Redirect::route('change-password')
                           ->withErrors($validator);
        } else {
            $user = $this->user->find(Auth::user()->id);

            $old_password = Input::get('old_password');
            $password     = Input::get('password');

            if (Hash::check($old_password, $user->getAuthPassword())) {
                $user->password = Hash::make($password);

                if ($user->save()) {
                    return Redirect::route('home')
                                   ->with('global', trans('general.password_changed'))
                                   ->with('type', 'success');
                }
            } else {
                return Redirect::route('change-password')
                               ->with('global', trans('general.old_password_incorrect'));
            }
        }

        return Redirect::route('change-password')
                       ->with('global', trans('general.change_password_problem'));
    }

    public function getActivate($code)
    {
        if ($this->user->activate($code)) {
            return Redirect::route('home')
                           ->with('global', trans('general.account_activated'))
                           ->with('type', 'success');
        }

        return Redirect::route('home')
                       ->with('global', trans('general.account_activate_problem'));
    }

    public function getRecover($code)
    {
        if ($this->user->recover($code)) {
            return Redirect::route('home')
                           ->with('global', trans('general.account_recover'))
                           ->with('type', 'success');
        }

        return Redirect::route('home')
                       ->with('global', trans('general.account_recover_problem'));
    }

    public function getDashboard()
    {
        return View::make('account.dashboard', [
            'title' => trans('general.account_dashboard_title', ['title' => Config::get('app.seo_meta_title')])
        ]);
    }

    public function getEditAccount()
    {
        $user = $this->user->getFirstBy('id', Auth::user()->id);

        return View::make('account.edit', [
            'title' => trans('general.edit_account_title', ['title' => Config::get('app.seo_meta_title')]),
            'user'  => $user
        ]);
    }

    public function postEditAccount()
    {
        $validator = Validator::make(Input::all(), Config::get('validation.edit_account'));

        if ($validator->fails()) {
            return Redirect::route('edit')
                           ->withErrors($validator);
        } else {
            $user = $this->user->find(Auth::user()->id);

            $user->update([
                'website'            => Input::get('website'),
                'facebook_page'      => Input::get('facebook'),
                'google_page'        => Input::get('google'),
                'youtube_url'        => Input::get('youtube'),
                'steam_url'          => Input::get('steam'),
                'twitter_username'   => Input::get('twitter'),
                'skype_username'     => Input::get('skype'),
                'minecraft_username' => Input::get('minecraft'),
            ]);

            return Redirect::route('edit')
                           ->with('global', trans('general.edit_account'))
                           ->with('type', 'success');
        }
    }

    public function getAccount()
    {
        return Redirect::route('dashboard');
    }

}
