<?php namespace MSQ\Controllers;

use Auth;
use Config;
use File;
use Illuminate\Routing\Controller;
use Input;
use MSQ\Classes\Minecraft\Banner;
use MSQ\Classes\Minecraft\MinecraftQuery;
use MSQ\Classes\Minecraft\MinecraftVotifier;
use MSQ\Purchases;
use MSQ\Repositories\Comment\CommentInterface;
use MSQ\Repositories\Country\CountryInterface;
use MSQ\Repositories\Server\ServerInterface;
use MSQ\Repositories\Statistics\StatisticsInterface;
use MSQ\Repositories\Tag\TagInterface;
use MSQ\Repositories\Vip\VipInterface;
use MSQ\Repositories\Vote\VoteInterface;
use MSQ\Repositories\Post\PostInterface;
use Redirect;
use Request;
use Validator;
use View;
use Carbon\Carbon;
use MSQ\Paytypes;


class Server extends Controller
{
    protected $server;
    protected $country;
    protected $version;
    protected $tag;
    protected $mode;
    protected $game;
    protected $statstics;
    protected $comment;
    protected $vote;
    protected $vip;
    protected $post;

    public function __construct(ServerInterface $server,
                                CountryInterface $country,
                                TagInterface $tag,
                                StatisticsInterface $statistics,
                                CommentInterface $comment,
                                VoteInterface $vote,
                                VipInterface $vip,
                                PostInterface $post)
    {
        $this->server = $server;
        $this->country = $country;
        $this->tag = $tag;
        $this->statistics = $statistics;
        $this->comment = $comment;
        $this->vote = $vote;
        $this->vip = $vip;
        $this->post = $post;

        View::share('tags', $this->tag->getAllTags());
        View::share('games', $this->tag->getAllGames());
        View::share('modes', $this->tag->getAllModes());
        View::share('versions', $this->tag->getAllVersions());

        View::share('type_banner', Paytypes::where('type', 'banner')->get());
        View::share('type_diamond', Paytypes::where('type', 'diamond')->get());
        View::share('type_color', Paytypes::where('type', 'color')->get());
        View::share('news', $this->post->getLastNews(4));
    }

    public function getServerAdd()
    {
        return View::make('server.add', [
            'title' => trans('general.add_server_title', ['title' => Config::get('app.seo_meta_title')]),
            'countries' => $this->country->all()
        ]);
    }

    public function postServerAdd()
    {
        $validator = Validator::make(Input::all(), Config::get('validation.server_add'));

        if ($validator->fails()) {
            return Redirect::route('server-add')
                ->withErrors($validator)
                ->withInput();
        } else {
            $mc = new MinecraftQuery();
            $response = $mc->getStatus(Input::get('address'), Input::has('port') ? Input::get('port') : 25565, $this->tag->find(Input::get('version'))->name);

            // Check server if online or offline
            if ($response) {
                $status = 'online';
            } else {
                $status = 'offline';
            }

            if (Input::hasFile('banner')) {
                // Upload banner if exists
                $banner = Input::file('banner');
                $destination = public_path() . '/uploads/images/banners/';
                $extension = $banner->getClientOriginalExtension();
                $filename = 'banner-' . mt_rand(11111, 99999) . '.' . $extension;
                $banner->move($destination, $filename);
            } else {
                $filename = '';
            }

            $server = $this->server->create([
                'name' => trim(Input::get('name')),
                'description' => e(Input::get('description')),
                'short_description' => e(Input::get('short_description')),
                'address' => Input::get('address'),
                'port' => Input::has('port') ? Input::get('port') : 25565,
                'website' => Input::get('website'),
                'country_id' => Input::get('country'),
                'version_id' => Input::get('version'),
                'banner' => $filename,
                'youtube' => Input::get('youtube'),
                'votes' => 0,
                'status' => $status,
                'user_id' => Auth::user()->id,
                'online_time' => 1,
                'verified_time' => 1,
                'rank' => $this->server->lastRank(),
                // Votifer
                'votifier_active' => Input::has('votifier_active') ? 'true' : 'false',
                'votifier_address' => Input::has('votifier_address') ? Input::get('votifier_address') : Input::get('address'),
                'votifier_port' => Input::has('votifier_port') ? Input::get('votifier_port') : 25565,
                'votifier_key' => Input::get('votifier_key'),

                // Meta
                'meta_title' => Input::get('meta_title'),
                'meta_description' => Input::get('meta_description'),
                'meta_keywords' => Input::get('meta_keywords'),
                'seo_text' => Input::get('seo_text')
            ]);


            if ($server) {

                $server->version_belongs()->detach();
                $server->version_belongs()->attach(Input::get('version'));

                // Add tags to server
                $tags = Input::get('tags');

                if ($tags) {
                    foreach ($tags as $key => $value) {
                        $server->tags()->attach($value);
                    }
                }

                // Add games to server
                $games = Input::get('games');

                if ($games) {
                    foreach ($games as $key => $value) {
                        $server->games()->attach($value);
                    }
                }


                // Add modes to server
                $modes = Input::get('modes');

                if ($modes) {
                    foreach ($modes as $key => $value) {
                        $server->modes()->attach($value);
                    }
                }

                // Add statistics for server
                if ($response) {
                    $this->statistics->create([
                        'server_id' => $server->id,
                        'players' => $response['players'],
                        'max_players' => $response['maxplayers'],
                        'motd' => $response['motd'],
                        'favicon' => $response['favicon']
                    ]);
                }

                return Redirect::route('dashboard')
                    ->with('global', trans('general.server_added'))
                    ->with('type', 'success');
            }
        }
    }

    public function getServerPage($id)
    {
        $Country = json_decode(file_get_contents('http://freegeoip.net/json/' . $_SERVER['REMOTE_ADDR']), true);

        if (!empty($Country['country_code'])) {
            $Country = strtolower($Country['country_code']);
        } else {
            $Country = 'ru';
        }

        $server = $this->server->getFirstBy('id', $id, ['statistics', 'version', 'user', 'statistics_list', 'purchases_banner', 'purchases_color', 'purchases_diamonds']);
        try {
            $server->already_voted = $this->vote->checkAlreadyVoted($server->id);
        } catch (\ErrorException $error) {
            return View::make('errors.error', [
                'h1' => 'NOT FOUND',
                'description' => "Такого сервера не найденно"
            ]);
        }
        $purchases = Purchases::where('server_id', $id)->where('end_period', '>', Carbon::now())->where('paid', '>', '0')->get();
        $purchases_non_paid = Purchases::where('server_id', $id)->where('end_period', '>', Carbon::now())->where('paid', '=', '0')->get();

        $old_purchases = Purchases::where('server_id', $id)->where('end_period', '<', Carbon::now())->where('paid', '>', '0')->get();
        if ($this->server->getCountFirstBy('id', $id)) {
            if ($this->statistics->getCountFirstBy('server_id', $id)) {
                $uptime = @($server->online_time / $server->verified_time) * 100;
                $uptime = substr($uptime, 0, 5);
                $uptime = $server->online_time > 0 ? $uptime . '%' : 'N/A';
            } else {
                $uptime = 'N/A';
            }

            $server->description = "<p>" . str_replace("\n", '</p><p>', $server->description) . "</p>";

            return View::make('server.page', [
                'title' => Config::get('app.seo_server_prefix') . $server->name . Config::get('app.seo_server_suffix'),
                'meta_description' => $server->meta_description,
                'meta_keywords' => $server->meta_keywords,
                'seo_text' => $server->seo_text,
                'server' => $server,
                'id' => $id,
                'uptime' => $uptime,
                'vip' => $this->vip,
                'country' => $Country,
                'checked_time' => $this->updatedTime($server->statistics->updated_at),
                'purchases' => $purchases,
                'old_purchases' => $old_purchases,
                'purchases_non_paid' => $purchases_non_paid,
            ]);
        }

        return Redirect::route('home');
    }


    public function updatedTime($updated)
    {
        $text = '';
        $time = time() - strtotime($updated);
        if ($time < 60) {
            $text .= 'Менее минуты назад';
        } else if ($time < 3600) {
            if (gmdate("i", $time) % 10 == 1 && gmdate("i", $time) != 11) {
                $min = 'минуту';
            } elseif ((gmdate("i", $time) % 10 > 1 && gmdate("i", $time) % 10 < 5) && (gmdate("i", $time) < 10 || gmdate("i", $time) > 20)) {
                $min = 'минуты';
            } else {
                $min = 'минут';
            }
            $text .= intval(gmdate("i", $time)) . ' ' . $min . ' назад';
        } else if ($time < 7200) {
            $text .= 'Час назад';
        } else if ($time < 86400) {
            $text .= gmdate("В H:i", $time);
        } else {
            $text .= gmdate("Y.m.d в H:i", $time);
        }

        return $text;
    }

    public function getBanner($id, $type = '468x60', $no = '1')
    {
        $server = $this->server->getFirstBy('id', $id, ['statistics', 'version']);
//        print_r($server->toArray());exit;

        if ($this->server->getCountFirstBy('id', $id)) {
            $image = "assets/images/banners/{$type}/banner_{$no}.jpg";

            if (!File::exists($image)) $image = $image = "assets/images/banners/{$type}/banner_1.jpg";

            $banner = new Banner('jpeg', $image);

            switch ($type) {
                case '234x60':
                    $banner->createText($server->address, ['size' => 11, 'angle' => 0, 'x' => 5, 'y' => 22], 'assets/fonts/ARIBLK.ttf', 'white')
                        ->createText($server->statistics['max_players'] ? 'Players: ' . $server->statistics['players'] . '/' . $server->statistics['max_players'] : 'N\A', ['size' => 13, 'angle' => 0, 'x' => 8, 'y' => 45], 'assets/fonts/accid.ttf', 'white')
                        ->createText(ucfirst($server->status), ['size' => 15, 'angle' => 0, 'x' => 180, 'y' => 55], 'assets/fonts/accid.ttf', $server->status == 'online' ? 'green' : 'red')
                        ->render();
                    break;
                case '468x60':
                    $banner->createText($server->name, ['size' => 12, 'angle' => 0, 'x' => 5, 'y' => 19], 'assets/fonts/black.ttf', 'coral')
                        ->createText($server->address . ':' . $server->port, ['size' => 11, 'angle' => 0, 'x' => 3, 'y' => 38], 'assets/fonts/tahoma.ttf', 'white')
                        ->createText('Version: ' . $server->version->name, ['size' => 12, 'angle' => 0, 'x' => 5, 'y' => 55], 'assets/fonts/black.ttf', 'white')
                        ->createText($server->version['name'] ? 'Players: ' . $server->statistics['players'] . '/' . $server->statistics['max_players'] : 'N\A', ['size' => 12, 'angle' => 0, 'x' => 150, 'y' => 55], 'assets/fonts/black.ttf', 'white')
                        ->createText(ucfirst($server->status), ['size' => 13, 'angle' => 0, 'x' => 410, 'y' => 55], 'assets/fonts/black.ttf', $server->status == 'online' ? 'green' : 'red')
                        ->render();
                case '600x100':
                    $banner->createText($server->name, ['size' => 18, 'angle' => 0, 'x' => 6, 'y' => 29], 'assets/fonts/accid.ttf', 'coral')
                        ->createText($server->address . ':' . $server->port, ['size' => 14, 'angle' => 0, 'x' => 10, 'y' => 55], 'assets/fonts/tahoma.ttf', 'white')
                        ->createText('Version: ' . $server->version->name, ['size' => 14, 'angle' => 0, 'x' => 12, 'y' => 80], 'assets/fonts/black.ttf', 'white')
                        ->createText($server->version['name'] ? 'Players: ' . $server->statistics['players'] . '/' . $server->statistics['max_players'] : 'N\A', ['size' => 14, 'angle' => 0, 'x' => 150, 'y' => 80], 'assets/fonts/black.ttf', 'white')
                        ->createText(ucfirst($server->status), ['size' => 15, 'angle' => 0, 'x' => 535, 'y' => 90], 'assets/fonts/accid.ttf', $server->status == 'online' ? 'green' : 'red')
                        ->render();
                case '728x90':
                    $banner->createText($server->name, ['size' => 15, 'angle' => 0, 'x' => 10, 'y' => 29], 'assets/fonts/black.ttf', 'coral')
                        ->createText($server->address . ':' . $server->port, ['size' => 14, 'angle' => 0, 'x' => 10, 'y' => 55], 'assets/fonts/tahoma.ttf', 'white')
                        ->createText('Version: ' . $server->version->name, ['size' => 14, 'angle' => 0, 'x' => 12, 'y' => 80], 'assets/fonts/black.ttf', 'white')
                        ->createText($server->version['name'] ? 'Players: ' . $server->statistics['players'] . '/' . $server->statistics['max_players'] : 'N\A', ['size' => 14, 'angle' => 0, 'x' => 150, 'y' => 80], 'assets/fonts/black.ttf', 'white')
                        ->createText(ucfirst($server->status), ['size' => 13, 'angle' => 0, 'x' => 650, 'y' => 80], 'assets/fonts/black.ttf', $server->status == 'online' ? 'green' : 'red')
                        ->render();
            }

            return $banner;
        }

        return Redirect::route('home');

    }

    public function getServerBanners($id)
    {
        $server = $this->server->getFirstBy('id', $id);

        if ($this->server->getCountFirstBy('id', $id)) {
            return View::make('server.banners', [
                'title' => trans('general.server_banners_title', ['server' => $server->name, 'title' => Config::get('app.seo_meta_title')]),
                'server' => $server,
            ]);
        }

        return Redirect::route('home');
    }

    public function getServerComments($id)
    {
        $server = $this->server->getFirstBy('id', $id);

        $comments = $this->comment->comments($id);

        if ($this->server->getCountFirstBy('id', $id)) {
            return View::make('server.comments', [
                'title' => trans('general.server_comments_title', ['server' => $server->name, 'title' => Config::get('app.seo_meta_title')]),
                'id' => $id,
                'server' => $server,
                'comments' => $comments
            ]);
        }

        return Redirect::route('home');
    }

    public function postServerComments($id)
    {
        $validator = Validator::make(Input::all(), Config::get('validation.server_comment'));

        if ($validator->fails()) {
            return Redirect::route('server-comments', $id)
                ->withErrors($validator)
                ->withInput();
        } else {
            $comment = $this->comment->create([
                'server_id' => $id,
                'user_id' => Auth::user()->id,
                'body' => e(Input::get('comment')),
            ]);

            if ($comment) {
                return Redirect::route('server-comments', $id)
                    ->with('global', trans('general.comment_added'))
                    ->with('type', 'success');
            }
        }
    }

    public function getServerVote($id)
    {
        $server = $this->server->getFirstBy('id', $id);

        if ($this->server->getCountFirstBy('id', $id)) {
            return View::make('server.vote', [
                'title' => trans('general.server_vote_title', ['server' => $server->name, 'title' => Config::get('app.seo_meta_title')]),
                'id' => $id,
                'server' => $server,
                'votes' => $this->vote->allVotes($id),
                'already_voted' => $this->vote->checkAlreadyVoted($id)
            ]);
        }

        return Redirect::route('home');
    }

    public function postServerVote($id)
    {
        $validator = Validator::make(Input::all(), Config::get('validation.server_vote'));

        if ($validator->fails()) {
            return Redirect::route('server-vote', $id)
                ->withErrors($validator)
                ->withInput();
        } else {
            $user_id = Auth::check() ? Auth::user()->id : null;

            if ($this->vote->giveVote($id, Input::get('username'), $user_id) === true) {
                $vote = $this->server->getFirstBy('id', $id);

                if ($vote->votifier_active == 'true' && strlen($vote->votifier_key) > 5) {
                    $votifier = new MinecraftVotifier($vote->votifier_key, $vote->votifier_address, $vote->votifier_port);
                    $votifier->sendVote(Input::get('username'));
                }

                // Add point for rank system
                $vote->points = $vote->points + 1;
                $vote->votes = $vote->votes + 1;

                $vote->save();

                return Redirect::route('server-vote', $id)
                    ->with('global', trans('general.server_voted'))
                    ->with('type', 'success');
            }

            return Redirect::route('server-vote', $id)
                ->with('global', trans('general.server_already_voted'));

        }
    }

    public function getManageServers()
    {
        $servers = $this->server->getMyServersByPage(Auth::user()->id, Config::get('app.servers_per_manage'));

        return View::make('servers.manage', [
            'title' => trans('general.manage_servers_title', ['title' => Config::get('app.seo_meta_title')]),
            'servers' => $servers,
            'vip' => $this->vip
        ]);
    }

    public function getEditServer($id)
    {
        $server = $this->server->editServer($id, Auth::user()->id);

//        print_r( $this->mode->all()->toArray());
//        exit;
        if ($server) {
            return View::make('server.edit', [
                'title' => trans('general.edit_server_title', ['title' => Config::get('app.seo_meta_title')]),
                'server' => $server,
                'id' => $id,
                'countries' => $this->country->all(),
                'object' => $this->server
            ]);
        }

        return Redirect::route('home');
    }

    public function postEditServer($id)
    {
        $validator = Validator::make(Input::all(), Config::get('validation.edit_server'));

        if ($validator->fails()) {
            return Redirect::route('server-edit', $id)
                ->withErrors($validator)
                ->withInput();
        } else {
            if (Input::hasFile('banner')) {
                // Upload banner if exists
                $banner = Input::file('banner');
                $destination = public_path() . '/uploads/images/banners/';
                $extension = $banner->getClientOriginalExtension();
                $filename = 'banner-' . mt_rand(11111, 99999) . '.' . $extension;
                $banner->move($destination, $filename);
            } else {
                $filename = $this->server->find($id)->banner;
            }

            $server = $this->server->find($id);

            $server->update([
                'name' => trim(Input::get('name')),
                'description' => e(Input::get('description')),
                'short_description' => e(Input::get('short_description')),
                'address' => Input::get('address'),
                'port' => Input::has('port') ? Input::get('port') : 25565,
                'website' => Input::get('website'),
                'version_id' => Input::get('version'),
                'banner' => $filename,
                'youtube' => Input::get('youtube'),
                'votifier_active' => Input::has('votifier_active') ? 'true' : 'false',
                'votifier_address' => Input::has('votifier_address') ? Input::get('votifier_address') : Input::get('address'),
                'votifier_port' => Input::has('votifier_port') ? Input::get('votifier_port') : 25565,
                'votifier_key' => Input::get('votifier_key'),

                // Meta
                'meta_title' => Input::get('meta_title'),
                'meta_description' => Input::get('meta_description'),
                'meta_keywords' => Input::get('meta_keywords'),
                'seo_text' => Input::get('seo_text')
            ]);


            // Add version to server

            $server->version_belongs()->detach();
            $server->version_belongs()->attach(Input::get('version'));


            // Add tags to server
            $tags = Input::get('tags');

            if ($tags) {
                $server->tags()->detach();

                foreach ($tags as $key => $value) {
                    $server->tags()->attach($value);
                }
            }

            // Add games to server
            $games = Input::get('games');

            if ($games) {
                $server->games()->detach();

                foreach ($games as $key => $value) {
                    $server->games()->attach($value);
                }
            }

            // Add tags to server
            $modes = Input::get('modes');

            if ($modes) {
                $server->modes()->detach();

                foreach ($modes as $key => $value) {
                    $server->modes()->attach($value);
                }
            }

            return Redirect::route('server-edit', $id)
                ->with('global', trans('general.edit_server'))
                ->with('type', 'success');
        }
    }

    public function deleteServer($id)
    {
        $this->server->deleteMyServer($id);

        return Redirect::back()->with('global', trans('general.record_deleted'));
    }
}