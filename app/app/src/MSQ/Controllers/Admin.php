<?php namespace MSQ\Controllers;

use Illuminate\Routing\Controller;
use MSQ\Paytypes;
use MSQ\Purchases;
use View, Redirect, Config, Input, Validator, Hash;
use MSQ\Repositories\User\UserInterface;
use MSQ\Repositories\Server\ServerInterface;
use MSQ\Repositories\Country\CountryInterface;
use MSQ\Repositories\Tag\TagInterface;
use MSQ\Repositories\Comment\CommentInterface;
use MSQ\Repositories\Statistics\StatisticsInterface;
use MSQ\Repositories\Vip\VipInterface;


use MSQ\Repositories\Post\PostInterface;

class Admin extends Controller
{

    protected $user;
    protected $server;
    protected $country;
    protected $version;
    protected $tag;
    protected $game;
    protected $mode;
    protected $comment;
    protected $statistics;
    protected $vip;
    protected $post;

    public function __construct(UserInterface $user,
                                ServerInterface $server,
                                CountryInterface $country,
                                TagInterface $tag,
                                CommentInterface $comment,
                                StatisticsInterface $statistics,
                                VipInterface $vip,
                                PostInterface $post)
    {
        $this->user = $user;
        $this->server = $server;
        $this->country = $country;
        $this->tag = $tag;

        /*
         * 1 - tags
         * 2 - games
         * 3 - modes
         * 4 - versions
         */

        $this->comment = $comment;
        $this->statistics = $statistics;
        $this->vip = $vip;
        $this->post = $post;
        View::share('news', $this->post->getLastNews(4));
    }

    public function getAdmin()
    {
        return Redirect::route('admin-dashboard');
    }

    public function getDashboard()
    {
        return View::make('admin.dashboard', [
            'title' => trans('general.admin_dashboard_title'),
            'count_users' => $this->user->all()->count(),
            'count_servers' => $this->server->all()->count(),
            'count_comments' => $this->comment->all()->count()
        ]);
    }

    public function getSettingsConfiguration()
    {
        return View::make('admin.settings.configuration', [
            'title' => trans('general.settings_configuration_title')
        ]);
    }

    public function postSettingsConfiguration()
    {
        $validator = Validator::make(Input::all(), Config::get('validation.settings_configuration'));

        if ($validator->fails()) {
            return Redirect::route('settings-configuration')
                ->withErrors($validator)
                ->withInput();
        } else {
            $environment = 'production';

            Config::store('app.sitename', Input::get('name'), $environment);
            Config::store('app.language', Input::get('language'), $environment);
            Config::store('app.facebook_link', Input::get('facebook'), $environment);
            Config::store('app.twitter_link', Input::get('twitter'), $environment);
            Config::store('app.googleplus_link', Input::get('google'), $environment);
            Config::store('app.tracking', Input::get('tracking'), $environment);
            Config::store('app.copyright', Input::get('copyright'), $environment);
            Config::store('app.servers_per_home', Input::get('sph'), $environment);
            Config::store('app.servers_per_tags', Input::get('spt'), $environment);
            Config::store('app.servers_per_manage', Input::get('spm'), $environment);
            Config::store('app.servers_per_search', Input::get('sps'), $environment);
            Config::store('app.servers_per_country', Input::get('spc'), $environment);
            Config::store('app.servers_per_version', Input::get('spv'), $environment);
            Config::store('app.allowed_tags', Input::get('tags'), $environment);
            Config::store('app.confirm_account', Input::get('confirm'), $environment);
            Config::store('app.owner_email', Input::get('owner_email'), $environment);
            Config::store('app.news_message', Input::get('news'), $environment);
            Config::store('app.recaptcha_public_key', Input::get('public_key'), $environment);
            Config::store('app.recaptcha_private_key', Input::get('private_key'), $environment);
            Config::store('app.webmoney_wallet', Input::get('webmoney_wallet'), $environment);
            Config::store('app.webmoney_secret', Input::get('webmoney_secret'), $environment);


            Config::store('app.text_adverts', Input::get('text_adverts'), $environment);
            Config::store('app.faq', Input::get('faq'), $environment);

            return Redirect::route('settings-configuration')
                ->with('global', trans('general.settings_configuration'))
                ->with('type', 'success');
        }
    }

    public function getSettingsSeo()
    {
        return View::make('admin.settings.seo', [
            'title' => trans('general.settings_seo_title')
        ]);
    }

    public function postSettingsSeo()
    {
        $validator = Validator::make(Input::all(), Config::get('validation.settings_seo'));

        if ($validator->fails()) {
            return Redirect::route('settings-seo')
                ->withErrors($validator)
                ->withInput();
        } else {
            $environment = 'production';

            Config::store('app.seo_meta_title', Input::get('meta_title'), $environment);
            Config::store('app.seo_meta_description', Input::get('meta_description'), $environment);
            Config::store('app.seo_profile_prefix', Input::get('profile_prefix'), $environment);
            Config::store('app.seo_profile_suffix', Input::get('profile_suffix'), $environment);
            Config::store('app.seo_server_prefix', Input::get('server_prefix'), $environment);
            Config::store('app.seo_server_suffix', Input::get('server_suffix'), $environment);
            Config::store('app.seo_tag_prefix', Input::get('tag_prefix'), $environment);
            Config::store('app.seo_tag_suffix', Input::get('tag_suffix'), $environment);
            Config::store('app.seo_country_prefix', Input::get('country_prefix'), $environment);
            Config::store('app.seo_country_suffix', Input::get('country_suffix'), $environment);
            Config::store('app.seo_version_prefix', Input::get('version_prefix'), $environment);
            Config::store('app.seo_version_suffix', Input::get('version_suffix'), $environment);

            return Redirect::route('settings-seo')
                ->with('global', trans('general.settings_seo'))
                ->with('type', 'success');
        }
    }

    public function getAds()
    {
        return View::make('admin.ads', [
            'title' => trans('general.ads_title')
        ]);
    }

    public function postAds()
    {
        $environment = 'production';

        Config::store('app.ads_top', Input::get('ads_top'), $environment);
        Config::store('app.ads_bottom', Input::get('ads_bottom'), $environment);

        return Redirect::route('settings-ads')
            ->with('global', trans('general.ads'))
            ->with('type', 'success');
    }

    public function getUsersCreate()
    {
        return View::make('admin.users.create', [
            'title' => trans('general.users_create_title')
        ]);
    }

    public function postUsersCreate()
    {
        $validator = Validator::make(Input::all(), Config::get('validation.users_create'));

        if ($validator->fails()) {
            return Redirect::route('users-create')
                ->withErrors($validator)
                ->withInput();
        } else {
            $username = Input::get('username');
            $email = Input::get('email');
            $password = Input::get('password');

            $user = $this->user->create([
                'username' => $username,
                'email' => $email,
                'password' => Hash::make($password),
                'active' => 1
            ]);

            if ($user) {
                return Redirect::route('users-create')
                    ->with('global', trans('general.users_create'))
                    ->with('type', 'success');
            }

        }
    }

    public function getUsersAll()
    {
        return View::make('admin.users.all', [
            'title' => trans('general.users_all_title'),
            'users' => $this->user->all()
        ]);
    }

    public function getEditUser($id)
    {
        $user = $this->user->getFirstBy('id', $id);

        if ($this->user->getCountFirstBy('id', $id)) {
            return View::make('admin.users.edit', [
                'title' => trans('general.edit_user_title'),
                'user' => $user,
                'id' => $id
            ]);
        }

        return Redirect::route('home');
    }


    public function getServersAll()
    {
        return View::make('admin.servers.all', [
            'title' => trans('general.servers_all_title'),
            'servers' => $this->server->all()
        ]);
    }

    public function postEditUser($id)
    {
        $validator = Validator::make(Input::all(), Config::get('validation.users_edit'));

        if ($validator->fails()) {
            return Redirect::route('users-edit', $id)
                ->withErrors($validator)
                ->withInput();
        } else {
            $password = Input::get('password') ? Hash::make(Input::get('password')) : $this->user->find($id)->password;
            $user = $this->user->find($id);

            $update_user = $user->update([
                'email' => Input::get('email'),
                'username' => Input::get('username'),
                'password' => $password,
                'role' => Input::get('role'),
                'active' => Input::get('status')
            ]);

            if ($update_user) {
                return Redirect::route('users-edit', $id)
                    ->with('global', trans('general.edit_user'))
                    ->with('type', 'success');
            }
        }
    }

    public function getEditServer($id)
    {
        $server = $this->server->getFirstBy('id', $id);

        if ($this->server->getCountFirstBy('id', $id)) {
            return View::make('admin.servers.edit', [
                'title' => trans('general.edit_server_title'),
                'server' => $server,
                'id' => $id,
                'countries' => $this->country->all(),
                'versions' => $this->tag->getAllVersions(),
                'tags' => $this->tag->getAllTags(),
                'modes' => $this->tag->getAllModes(),
                'games' => $this->tag->getAllGames(),
                'object' => $this->server,
            ]);
        }

        return Redirect::route('home');
    }

    public function postEditServer($id)
    {
        $validator = Validator::make(Input::all(), Config::get('validation.servers_edit'));

        if ($validator->fails()) {
            return Redirect::route('servers-edit', $id)
                ->withErrors($validator)
                ->withInput();
        } else {
            if (Input::hasFile('banner')) {
                // Upload banner if exists
                $banner = Input::file('banner');
                $destination = public_path() . '/uploads/images/banners/';
                $extension = $banner->getClientOriginalExtension();
                $filename = 'banner-' . mt_rand(11111, 99999) . '.' . $extension;
                $banner->move($destination, $filename);
            } else {
                $filename = $this->server->find($id)->banner;
            }

            $server = $this->server->find($id);

            $server->update([
                'name' => trim(Input::get('name')),
                'description' => e(Input::get('description')),
                'short_description' => e(Input::get('short_description')),
                'address' => Input::get('address'),
                'port' => Input::has('port') ? Input::get('port') : 25565,
                'website' => Input::get('website'),
                'version_id' => Input::get('version'),
                'banner' => $filename,
                'youtube' => Input::get('youtube'),
                'diamonds' => Input::get('diamonds'),
                'votifier_active' => Input::has('votifier_active') ? 'true' : 'false',
                'votifier_address' => Input::has('votifier_address') ? Input::get('votifier_address') : Input::get('address'),
                'votifier_port' => Input::has('votifier_port') ? Input::get('votifier_port') : 25565,
                'votifier_key' => Input::get('votifier_key'),

                // Meta
                'meta_title' => Input::get('meta_title'),
                'meta_description' => Input::get('meta_description'),
                'meta_keywords' => Input::get('meta_keywords'),
                'seo_text' => Input::get('seo_text'),
            ]);

            // Add version to server
            $version = Input::get('version');
            $server->version_belongs()->detach();

            if ($version) {
                $server->version_belongs()->attach(Input::get('version'));
            }

            // Add tags to server
            $tags = Input::get('tags');

            if ($tags) {
                foreach ($tags as $key => $value) {
                    $server->tags()->attach($value);
                }
            }

            // Add modes to server
            $modes = Input::get('modes');

            if ($modes) {
                foreach ($modes as $key => $value) {
                    $server->modes()->attach($value);
                }
            }

            // Add games to server
            $games = Input::get('games');

            if ($games) {
                foreach ($games as $key => $value) {
                    $server->games()->attach($value);
                }
            }

            return Redirect::route('servers-edit', $id)
                ->with('global', trans('general.edit_server'))
                ->with('type', 'success');

        }
    }

    public function getComments()
    {
        return View::make('admin.comments', [
            'title' => trans('general.comments_title'),
            'comments' => $this->comment->allComments()
        ]);
    }

    public function getDelete($option, $id)
    {
        switch ($option) {
            case 'user':
                $this->user->find($id)->delete();
                $this->comment->deleteAllByUsername($id);
                $this->server->deleteAllByUsername($id);
                break;

            case 'server':
                $this->server->find($id)->delete();
                $this->comment->deleteAllByServer($id);
                $this->statistics->deleteAllByServer($id);
                break;

            case 'comment':
                $this->comment->find($id)->delete();
                break;

            case 'vip':
                $this->vip->find($id)->delete();
                break;
            case 'tags':
                $this->tag->find($id)->servers()->detach();
                $this->tag->find($id)->delete();
                break;
            case 'purchases':
                Purchases::find($id)->delete();
                break;
            case 'pay_types':
                Paytypes::find($id)->delete();
                break;
        }

        return Redirect::back()->with('global', trans('general.record_deleted'));
    }

    public function getToolsCron()
    {
        return View::make('admin.tools.cron', [
            'title' => trans('general.tools_cron_title')
        ]);
    }

    public function postToolsCron()
    {
        $environment = 'production';

        Config::store('app.cron_key', Input::get('key'), $environment);

        return Redirect::route('tools-cron')
            ->with('global', trans('general.tools_cron'))
            ->with('type', 'success');
    }

    public function getSettingsVip()
    {
        return View::make('admin.settings.vip', [
            'title' => trans('general.settings_vip_title')
        ]);
    }

    public function postSettingsVip()
    {
        $environment = 'production';

        Config::store('app.business_mail', Input::get('business'), $environment);
        Config::store('app.amount_day', Input::get('amount'), $environment);
        Config::store('app.vip_currency', Input::get('currency'), $environment);

        return Redirect::route('settings-vip')
            ->with('global', trans('general.settings_vip'))
            ->with('type', 'success');
    }

    public function getVip()
    {
        return View::make('admin.vip.all', [
            'title' => trans('general.admin_vip_title'),
            'vips' => $this->vip->allVips()
        ]);
    }

    public function getEditVip($id)
    {
        $vip = $this->vip->getFirstBy('id', $id);

        if ($this->vip->getCountFirstBy('id', $id)) {
            return View::make('admin.vip.edit', [
                'title' => trans('general.edit_vip_title'),
                'vip' => $vip,
                'id' => $id,
            ]);
        }

        return Redirect::route('home');
    }

    public function postEditVip($id)
    {
        $validator = Validator::make(Input::all(), Config::get('validation.vip_edit'));

        if ($validator->fails()) {
            return Redirect::route('vip-edit', $id)
                ->withErrors($validator)
                ->withInput();
        } else {
            $vip = $this->vip->find($id);

            $update_vip = $vip->update([
                'start' => Input::get('start'),
                'end' => Input::get('end'),
                'mc_gross' => Input::get('gross'),
                'payment_status' => Input::get('status'),
                'payer_first_name' => Input::get('first_name'),
                'payer_last_name' => Input::get('last_name'),
                'payer_email' => Input::get('email'),
            ]);

            if ($update_vip) {
                return Redirect::route('vip-edit', $id)
                    ->with('global', trasn('general.edit_vip'))
                    ->with('type', 'success');
            }
        }
    }

    public function getViewVip($id)
    {
        $vip = $this->vip->getFirstBy('id', $id);

        if ($this->vip->getCountFirstBy('id', $id)) {
            return View::make('admin.vip.view', [
                'title' => trans('general.view_vip_title'),
                'vip' => $vip,
            ]);
        }

        return Redirect::route('home');
    }


    // Tags
    public function getTagsAll()
    {
        return View::make('admin.tags.all', [
            'title' => 'Теги',
            'tags' => $this->tag->getAllTags(),
            'type_id' => 1,
            'type' => 'tags'
        ]);
    }


    public function getEditTags($id)
    {
        $tag = $this->tag->getFirstBy('id', $id);

        if ($this->tag->getCountFirstBy('id', $id)) {
            return View::make('admin.tags.edit', [
                'title' => trans('general.edit_server_title'),
                'tag' => $tag,
                'id' => $id,
                'type' => 'tags',
                'type_id' => '1',
            ]);
        }
        return Redirect::route('home');
    }


    public function postEditTags($id)
    {
        $validator = Validator::make(Input::all(), Config::get('validation.tags_edit'));

        if ($validator->fails()) {
            return Redirect::route('tags-edit', $id)
                ->withErrors($validator)
                ->withInput();
        } else {
            $tag = $this->tag->find($id);

            $tag->slug = Input::get('slug');
            $tag->name = Input::get('name');
            // Meta
            $tag->meta_title = Input::get('meta_title');
            $tag->meta_description = Input::get('meta_description');
            $tag->meta_keywords = Input::get('meta_keywords');
            $tag->seo_text = Input::get('seo_text');
            $tag->h1 = Input::get('h1');

            $tag->save();
            return Redirect::route('tags-edit', $id)
                ->with('global', trans('general.edit_server'))
                ->with('type', 'success');

        }
    }

    public function getTagsCreate()
    {
        return View::make('admin.tags.create', [
            'title' => 'Добавить тег',
            'type' => 'tags'
        ]);
    }


    public function postTagsCreate()
    {
        $validator = Validator::make(Input::all(), Config::get('validation.tags_create'));

        if ($validator->fails()) {
            return Redirect::route('tags-create')
                ->withErrors($validator)
                ->withInput();
        } else {
            $tag = $this->tag->create([]);
            $tag->slug = Input::get('slug');
            $tag->name = Input::get('name');

            // Meta
            $tag->meta_title = Input::get('meta_title');
            $tag->meta_description = Input::get('meta_description');
            $tag->meta_keywords = Input::get('meta_keywords');
            $tag->seo_text = Input::get('seo_text');
            $tag->h1 = Input::get('h1');
            $tag->type = 1;

            $tag->save();

            return Redirect::route('tags-all', $tag->id)
                ->with('global', trans('general.edit_server'))
                ->with('type', 'success');
        }
    }

    // Versions
    public function getVersionsAll()
    {
        return View::make('admin.tags.all', [
            'title' => 'Версии minecraft',
            'tags' => $this->tag->getAllVersions(),
            'type' => 'versions'
        ]);
    }


    public function getEditVersions($id)
    {
        $version = $this->tag->getFirstBy('id', $id);

        return View::make('admin.tags.edit', [
            'title' => trans('general.edit_server_title'),
            'tag' => $version,
            'id' => $id,
            'type' => 'versions',
            'type_id' => '1',
        ]);

        return Redirect::route('home');
    }


    public function postEditVersions($id)
    {
        $validator = Validator::make(Input::all(), Config::get('validation.versions'));

        if ($validator->fails()) {
            return Redirect::route('versions-edit', $id)
                ->withErrors($validator)
                ->withInput();
        } else {
            $version = $this->tag->find($id);

            $version->name = Input::get('name');
            $version->slug = Input::get('slug');

            // Meta
            $version->meta_title = Input::get('meta_title');
            $version->meta_description = Input::get('meta_description');
            $version->meta_keywords = Input::get('meta_keywords');
            $version->seo_text = Input::get('seo_text');
            $version->h1 = Input::get('h1');

            $version->save();

            return Redirect::route('versions-edit', $id)
                ->with('global', trans('general.edit_server'))
                ->with('type', 'success');
        }
    }

    public function getVersionsCreate()
    {
        return View::make('admin.tags.create', [
            'title' => 'Добавить версию',
            'type' => 'versions'
        ]);
    }


    public function postVersionsCreate()
    {
        $validator = Validator::make(Input::all(), Config::get('validation.versions'));

        if ($validator->fails()) {
            return Redirect::route('versions-create')
                ->withErrors($validator)
                ->withInput();
        } else {
            $version = $this->tag->create([]);
            $version->name = Input::get('name');
            $version->slug = Input::get('slug');

            // Meta
            $version->meta_title = Input::get('meta_title');
            $version->meta_description = Input::get('meta_description');
            $version->meta_keywords = Input::get('meta_keywords');
            $version->seo_text = Input::get('seo_text');
            $version->h1 = Input::get('h1');
            $version->type = 4;

            $version->save();

            return Redirect::route('versions-all', $version->id)
                ->with('global', trans('general.edit_server'))
                ->with('type', 'success');
        }
    }

    // Modes
    public function getModesAll()
    {
        return View::make('admin.tags.all', [
            'title' => 'Моды minecraft',
            'tags' => $this->tag->getAllModes(),
            'type' => 'modes'
        ]);
    }

    public function getModesEdit($id)
    {
        if ($mode = $this->tag->getFirstBy('id', $id)) {
            return View::make('admin.tags.edit', [
                'title' => trans('general.edit_server_title'),
                'tag' => $mode,
                'id' => $id,
                'type' => 'modes'
            ]);
        }
        return Redirect::route('home');
    }

    public function postModesEdit($id)
    {
        $validator = Validator::make(Input::all(), Config::get('validation.modes_edit'));

        if ($validator->fails()) {
            return Redirect::route('modes-edit', $id)
                ->withErrors($validator)
                ->withInput();
        } else {
            $mode = $this->tag->find($id);

            $mode->slug = Input::get('slug');
            $mode->name = Input::get('name');

            // Meta
            $mode->meta_title = Input::get('meta_title');
            $mode->meta_description = Input::get('meta_description');
            $mode->meta_keywords = Input::get('meta_keywords');
            $mode->seo_text = Input::get('seo_text');
            $mode->h1 = Input::get('h1');

            $mode->save();

            return Redirect::route('modes-edit', $id)
                ->with('global', trans('general.edit_server'))
                ->with('type', 'success');

        }
    }

    public function getModesCreate()
    {
        return View::make('admin.tags.create', [
            'title' => 'Добавить мод',
            'type' => 'modes'
        ]);
    }

    public function postModesCreate()
    {
        $validator = Validator::make(Input::all(), Config::get('validation.tags_create'));

        if ($validator->fails()) {
            return Redirect::route('modes-create')
                ->withErrors($validator)
                ->withInput();
        } else {
            $mode = $this->tag->create([]);
            $mode->slug = Input::get('slug');
            $mode->name = Input::get('name');

            // Meta
            $mode->meta_title = Input::get('meta_title');
            $mode->meta_description = Input::get('meta_description');
            $mode->meta_keywords = Input::get('meta_keywords');
            $mode->seo_text = Input::get('seo_text');
            $mode->h1 = Input::get('h1');
            $mode->type = 3;
            $mode->save();

            return Redirect::route('modes-all', $mode->id)
                ->with('global', trans('general.edit_server'))
                ->with('type', 'success');
        }
    }

    // Games
    public function getGamesAll()
    {
        return View::make('admin.tags.all', [
            'title' => 'Мини-игры',
            'tags' => $this->tag->getAllGames(),
            'type' => 'games'
        ]);
    }

    public function getGamesEdit($id)
    {
        if ($game = $this->tag->getFirstBy('id', $id)) {
            return View::make('admin.tags.edit', [
                'title' => trans('general.edit_server_title'),
                'tag' => $game,
                'id' => $id,
                'type' => 'games'
            ]);
        }
        return Redirect::route('home');
    }

    public function postGamesEdit($id)
    {
        $validator = Validator::make(Input::all(), Config::get('validation.games_edit'));

        if ($validator->fails()) {
            return Redirect::route('games-edit', $id)
                ->withErrors($validator)
                ->withInput();
        } else {
            $game = $this->tag->find($id);

            $game->slug = Input::get('slug');
            $game->name = Input::get('name');

            // Meta
            $game->meta_title = Input::get('meta_title');
            $game->meta_description = Input::get('meta_description');
            $game->meta_keywords = Input::get('meta_keywords');
            $game->seo_text = Input::get('seo_text');
            $game->h1 = Input::get('h1');

            $game->save();

            return Redirect::route('games-edit', $id)
                ->with('global', trans('general.edit_server'))
                ->with('type', 'success');

        }
    }

    public function getGamesCreate()
    {
        return View::make('admin.tags.create', [
            'title' => 'Добавить мини-игру',
            'type' => 'games'
        ]);
    }

    public function postGamesCreate()
    {
        $validator = Validator::make(Input::all(), Config::get('validation.tags_create'));

        if ($validator->fails()) {
            return Redirect::route('games-create')
                ->withErrors($validator)
                ->withInput();
        } else {
            $mode = $this->tag->create([]);
            $mode->slug = Input::get('slug');
            $mode->name = Input::get('name');

            // Meta
            $mode->meta_title = Input::get('meta_title');
            $mode->meta_description = Input::get('meta_description');
            $mode->meta_keywords = Input::get('meta_keywords');
            $mode->seo_text = Input::get('seo_text');
            $mode->h1 = Input::get('h1');
            $mode->type = 2;

            $mode->save();

            return Redirect::route('games-all', $mode->id)
                ->with('global', trans('general.edit_server'))
                ->with('type', 'success');
        }
    }


    // Games
    public function getNewsAll()
    {
        return View::make('admin.posts.all', [
            'title' => 'Новости',
            'posts' => $this->post->all()
        ]);
    }

    public function getNewsEdit($id)
    {
        if ($post = $this->post->getFirstBy('id', $id)) {
            return View::make('admin.posts.edit', [
                'title' => trans('general.edit_server_title'),
                'post' => $post,
                'id' => $id,

            ]);
        }
        return Redirect::route('home');
    }


    public function postNewsEdit($id)
    {
        $validator = Validator::make(Input::all(), Config::get('validation.news_edit'));

        if ($validator->fails()) {
            return Redirect::route('news-edit', $id)
                ->withErrors($validator)
                ->withInput();
        } else {
            $post = $this->post->find($id);

            if (Input::hasFile('img')) {
                $img = Input::file('img');
                $destination = public_path() . '/uploads/images/news/';
                $extension = $img->getClientOriginalExtension();
                $filename = 'img-' . mt_rand(11111, 99999) . '.' . $extension;
                $img->move($destination, $filename);

                $post->img = '/uploads/images/news/' . $filename;
            }

            $post->title = Input::get('title');
            $post->alias = Input::get('alias');
            $post->text = Input::get('text');
            $post->intro = Input::get('intro');

            // Meta
            $post->meta_title = Input::get('meta_title');
            $post->meta_description = Input::get('meta_description');
            $post->meta_keywords = Input::get('meta_keywords');
            $post->seo_text = Input::get('seo_text');

            $post->save();

            return Redirect::route('news-edit', $id)
                ->with('global', trans('general.edit_server'))
                ->with('type', 'success');

        }
    }

    public function getNewsCreate()
    {
        return View::make('admin.posts.create', [
            'title' => 'Добавить новость'
        ]);
    }

    public function postNewsCreate()
    {
        $validator = Validator::make(Input::all(), Config::get('validation.news_create'));

        if ($validator->fails()) {
            return Redirect::route('news-create')
                ->withErrors($validator)
                ->withInput();
        } else {
            $post = $this->post->create([]);

            if (Input::hasFile('img')) {
                $img = Input::file('img');
                $destination = public_path() . '/uploads/images/news/';
                $extension = $img->getClientOriginalExtension();
                $filename = 'img-' . mt_rand(11111, 99999) . '.' . $extension;
                $img->move($destination, $filename);

                $post->img = '/uploads/images/news/' . $filename;
            }

            $post->title = Input::get('title');
            $post->alias = Input::get('alias');
            $post->text = Input::get('text');
            $post->intro = Input::get('intro');

            // Meta
            $post->meta_title = Input::get('meta_title');
            $post->meta_description = Input::get('meta_description');
            $post->meta_keywords = Input::get('meta_keywords');
            $post->seo_text = Input::get('seo_text');


            $post->save();

            return Redirect::route('news-edit', $post->id)
                ->with('global', trans('general.edit_server'))
                ->with('type', 'success');

        }
    }


    /*
     * *** Purchases
     */
    public function getPurchasesAll()
    {
        return View::make('admin.purchases.all', [
            'title' => 'Все покупки',
            'purchases' => Purchases::all()
        ]);
    }

    public function getPurchasesEdit($id)
    {
        if ($purchase = Purchases::where('id', $id)->first()) {
            return View::make('admin.purchases.edit', [
                'title' => 'Редактировать покупку',
                'purchase' => $purchase,
                'id' => $id,
                'servers' => $this->server->all(),
            ]);
        }
        return Redirect::route('home');
    }

    public function postPurchasesEdit($id)
    {
        $validator = Validator::make(Input::all(), Config::get('validation.purchases'));
        if ($validator->fails()) {
            return Redirect::route('purchases-edit', $id)
                ->withErrors($validator)
                ->withInput();

        } else {
            $purchase = Purchases::find($id);

            // calculate end period
            $end_period = date('Y-m-d H:i:s', strtotime('+' . Input::get('days') . ' days', strtotime($purchase->created_at)));

            $purchase = $purchase->update([
                'server_id' => Input::get('server_id'),
                'type' => Input::get('type'),
                'days' => Input::get('days'),
                'cost' => Input::get('cost'),
                'color' => Input::get('color'),
                'diamonds' => Input::get('diamonds'),
                'end_period' => $end_period,
                'paid' => Input::get('paid')
            ]);

            if ($purchase) {
                return Redirect::route('purchases-edit', $id)
                    ->with('global', trans('general.edit_user'))
                    ->with('type', 'success');
            }
        }
    }

    public function getPurchasesCreate()
    {
        return View::make('admin.purchases.create', [
            'title' => 'Добавить покупку',
            'servers' => $this->server->all()
        ]);
    }

    public function postPurchasesCreate()
    {
        $validator = Validator::make(Input::all(), Config::get('validation.purchases'));
        if ($validator->fails()) {
            return Redirect::route('purchases-create')
                ->withErrors($validator)
                ->withInput();
        } else {
            // calculate end period
            $end_period = date('Y-m-d H:i:s', strtotime('+' . Input::get('days') . ' days', time()));
            $purchase = new Purchases();

            $purchase = $purchase->create([
                'server_id' => Input::get('server_id'),
                'type' => Input::get('type'),
                'days' => Input::get('days'),
                'cost' => Input::get('cost'),
                'color' => Input::get('color'),
                'diamonds' => Input::get('diamonds'),
                'end_period' => $end_period,
                'paid' => Input::get('paid')
            ]);

            if ($purchase) {
                return Redirect::route('purchases-edit', $purchase->id)
                    ->with('global', trans('general.edit_user'))
                    ->with('type', 'success');
            }
        }
    }

    /*
     * *** Pay types
     */

    public function getPayTypesAll()
    {
        return View::make('admin.pay_types.all', [
            'title' => 'Все типы покупок',
            'pay_types' => Paytypes::orderBy('type')->get()
        ]);
    }

    public function getPayTypesEdit($id)
    {
        if ($paytype = Paytypes::where('id', $id)->first()) {
            return View::make('admin.pay_types.edit', [
                'title' => 'Редактировать тип оплаты',
                'paytype' => $paytype,
                'id' => $id
            ]);
        }
        return Redirect::route('home');
    }

    public function postPayTypesEdit($id)
    {
        $validator = Validator::make(Input::all(), Config::get('validation.pay_types'));
        if ($validator->fails()) {
            return Redirect::route('pay_types-edit', $id)
                ->withErrors($validator)
                ->withInput();

        } else {
            $paytypes = Paytypes::find($id);
            $paytypes = $paytypes->update([
                'type' => Input::get('type'),
                'days' => Input::get('days'),
                'name' => Input::get('name'),
                'cost' => Input::get('cost'),
                'sale' => Input::get('sale'),
            ]);

            if ($paytypes) {
                return Redirect::route('pay_types-edit', $id)
                    ->with('global', trans('general.edit_user'))
                    ->with('type', 'success');
            }
        }
    }

    public function getPayTypesCreate()
    {
        return View::make('admin.pay_types.create', [
            'title' => 'Создать тип оплаты'
        ]);
    }

    public function postPayTypesCreate()
    {
        $validator = Validator::make(Input::all(), Config::get('validation.pay_types'));
        if ($validator->fails()) {
            return Redirect::route('pay_types-create')
                ->withErrors($validator)
                ->withInput();

        } else {
            $paytypes = new Paytypes();
            $paytypes = $paytypes->create([
                'type' => Input::get('type'),
                'days' => Input::get('days'),
                'name' => Input::get('name'),
                'cost' => Input::get('cost'),
                'sale' => Input::get('sale')
            ]);

            if ($paytypes) {
                return Redirect::route('pay_types-edit', $paytypes->id)
                    ->with('global', trans('general.edit_user'))
                    ->with('type', 'success');
            }
        }
    }
}