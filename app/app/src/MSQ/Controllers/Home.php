<?php namespace MSQ\Controllers;

use Illuminate\Routing\Controller;
use MSQ\Paytypes;
use MSQ\Purchases;
use MSQ\Repositories\Game\GameInterface;
use MSQ\Repositories\Mode\ModeInterface;
use MSQ\Repositories\Server\ServerInterface;
use MSQ\Repositories\User\UserInterface;
use MSQ\Repositories\Tag\TagInterface;
use MSQ\Repositories\Country\CountryInterface;
use MSQ\Repositories\Version\VersionInterface;
use MSQ\Repositories\Vip\VipInterface;
use MSQ\Repositories\Post\PostInterface;
use MSQ\Repositories\Vote\VoteInterface;

use MSQ\Server;
use View, Redirect, Config, DB, Response, Validator, Input, Mail, Gateway, URL, Auth;
use Carbon\Carbon;
use PayPal\Ipn\Listener;
use PayPal\Ipn\Message;
use PayPal\Ipn\Verifier\CurlVerifier;

use YandexMoney\API;
use UnitPay;

class Home extends Controller
{

    protected $server;
    protected $user;
    protected $tag;
    protected $game;
    protected $mode;
    protected $country;
    protected $version;
    protected $vip;
    protected $vote;
    protected $post;

    public function __construct(VoteInterface $vote, ServerInterface $server, UserInterface $user, TagInterface $tag, CountryInterface $country, VipInterface $vip, PostInterface $post)
    {
        $this->server = $server;
        $this->user = $user;

        $this->tag = $tag;

        $this->country = $country;
        $this->vip = $vip;
        $this->post = $post;
        $this->vote = $vote;

        View::share('tags', $this->tag->getAllTags());
        View::share('games', $this->tag->getAllGames());
        View::share('modes', $this->tag->getAllModes());
        View::share('versions', $this->tag->getAllVersions());
        View::share('news', $this->post->getLastNews(4));
    }

    public function getHome($page = 1)
    {
        $servers = $this->server->getByPageServerDiamond(Config::get('app.servers_per_home'));

        foreach ($servers as &$server) {
            $server->already_voted = $this->vote->checkAlreadyVoted($server->id);
        }

//        View::share('news', $this->post->getNews(4));

        return View::make('home', [
            'title' => Config::get('app.seo_meta_title'),
            'servers' => $servers,
            'countries' => $this->country->all(),
            'page_title' => 'Рейтинг серверов Minecraft',
            'page' => $page
        ]);
    }

    public function getTopServers($page = 1)
    {
        $servers = $this->server->getByPageServerTop(Config::get('app.servers_per_home'));
        foreach ($servers as &$server) {
            $server->already_voted = $this->vote->checkAlreadyVoted($server->id);
        }

        return View::make('home', [
            'title' => Config::get('app.seo_meta_title'),
            'servers' => $servers,
            'countries' => $this->country->all(),
            'page_title' => 'Топ серверов Minecraft',
            'page' => $page
        ]);
    }


    public function faq()
    {
        return View::make('faq', [
            'title' => 'FAQ'
        ]);
    }

    public function setPayment()
    {
        $cost = 0;
        $pay_type = Paytypes::find(Input::get('pay_type'));

        if (Input::get('type') == 'diamond') {
            $pay_type = Paytypes::find(Input::get('pay_type'));
            $cost = (Input::get('count') ? Input::get('count') : 1) * ($pay_type->cost * (100 - $pay_type->sale) / 100);
        } else if (Input::get('type') == 'banner' || Input::get('type') == 'color') {
            $cost = $pay_type->cost * (100 - $pay_type->sale) / 100;
        } else {
            return Redirect::route('home');
        }


        $end_period = date('Y-m-d H:i:s', strtotime('+' . $pay_type->days . ' days', time()));

        $purchase = new Purchases();
        $purchase = $purchase->create([
            'server_id' => Input::get('server_id'),
            'type' => Input::get('type'),
            'days' => $pay_type->days,
            'cost' => $cost,
            'color' => Input::get('color') ? Input::get('color') : '',
            'diamonds' => Input::get('count') ? Input::get('count') : 0,
            'end_period' => $end_period,
            'paid' => 0
        ]);

        return Redirect::route('pay_id', ['id' => $purchase->id]);
    }

    public function getPayment($id)
    {
        $purchase = Purchases::find($id);

        // Payment Created
        return View::make('pay.pay', [
            'title' => 'Pay',
            'purchase' => $purchase
        ]);
    }

    public function webmoneyResult()
    {
        $purchase = Purchases::find(Input::get('LMI_PAYMENT_NO'));

        if (Input::get('LMI_PREREQUEST') == 1) {
            if (!count($purchase)) {
                echo 'Ошибка 1';
                exit;
            }
//
            if ($purchase->cost != intval(Input::get('LMI_PAYMENT_AMOUNT'))) {
                echo 'Ошибка 2';
                exit;
            }

            if (Input::get('LMI_PAYEE_PURSE') != Config::get('app.webmoney_wallet')) {
                echo 'Ошибка 3';
                exit;
            }

            echo 'YES';
        } else {
            $secret_key = Config::get('app.webmoney_secret');
            $common_string = Input::get('LMI_PAYEE_PURSE') . Input::get('LMI_PAYMENT_AMOUNT') . Input::get('LMI_PAYMENT_NO') .
                Input::get('LMI_MODE') . Input::get('LMI_SYS_INVS_NO') . Input::get('LMI_SYS_TRANS_NO') .
                Input::get('LMI_SYS_TRANS_DATE') . $secret_key . Input::get('LMI_PAYER_PURSE') . Input::get('LMI_PAYER_WM');
            $hash = strtoupper(hash("sha256", $common_string));

            if ($hash != Input::get('LMI_HASH')) {
                exit;
            }

            $purchase->paid = 1;
            $purchase->end_period = date('Y-m-d H:i:s', strtotime('+' . $purchase->days . ' days', time()));
            $purchase->save();
        }
    }

    public function unitpayResult()
    {



        $projectId = 67711;
        $secretKey = 'de0cf0876b4d2fe67d129a04d3670e36';
        $orderCurrency = 'RUB';

        $unitPay = new UnitPay($secretKey);

        try {
            // Validate request (check ip address, signature and etc)
            $unitPay->checkHandlerRequest();

            list($method, $params) = array($_GET['method'], $_GET['params']);

            $purchase = Purchases::find($params['account']);


            // Very important! Validate request with your order data, before complete order
            if (
                $params['orderSum'] != $purchase->cost ||
                $params['orderCurrency'] != $orderCurrency ||
                $params['projectId'] != $projectId ||
                !count($purchase)
            ) {
                // logging data and throw exception
              echo 'Error logged data!';
            }
            switch ($method) {
                // Just check order (check server status, check order in DB and etc)
                case 'check':
                    print $unitPay->getSuccessHandlerResponse('Check Success. Ready to pay.');
                    break;
                // Method Pay means that the money received
                case 'pay':
                    // Please complete order
                    $purchase->paid = 1;
                    $purchase->end_period = date('Y-m-d H:i:s', strtotime('+' . $purchase->days . ' days', time()));
                    $purchase->save();
                    print $unitPay->getSuccessHandlerResponse('Pay Success');
                    break;
                // Method Error means that an error has occurred.
                case 'error':
                    // Please log error text.
                    print $unitPay->getSuccessHandlerResponse('Error logged');
                    break;
            }
        } catch (Exception $e) {
            print $unitPay->getErrorHandlerResponse($e->getMessage());
        }
        exit;
    }


    public function paySuccess()
    {
        return View::make('pay.success', [
            'title' => 'Оплата завершена успешно'
        ]);
    }

    public function payError()
    {
        return View::make('pay.error', [
            'title' => 'Ошибка оплаты'
        ]);
    }


    public function getNewServers($page = 1)
    {
        $servers = $this->server->getByPageServerNew(Config::get('app.servers_per_home'));
        foreach ($servers as &$server) {
            $server->already_voted = $this->vote->checkAlreadyVoted($server->id);
        }

        return View::make('home', [
            'title' => Config::get('app.seo_meta_title'),
            'servers' => $servers,
            'countries' => $this->country->all(),
            'page_title' => 'Новые сервера Minecraft',
            'page' => $page
        ]);
    }


    public function getProfile($username)
    {
        $user = $this->user->getFirstBy('username', $username);

        if ($this->user->getCountFirstBy('username', $username)) {
            return View::make('profile', [
                'title' => Config::get('app.seo_profile_prefix') . $user->username . Config::get('app.seo_profile_suffix'),
                'user' => $user,
                'servers' => $this->server->getMyServers($username)
            ]);
        }

        return Redirect::route('home');
    }

    public function getServersByTag($slug)
    {
        $tag = $this->tag->getFirstBy('slug', $slug);

        if ($this->tag->getCountFirstBy('slug', $slug)) {
            $servers = $this->server->searchByTag($slug, Config::get('app.servers_per_tags'));
            foreach ($servers as &$server) {
                $server->already_voted = $this->vote->checkAlreadyVoted($server->id);
            }

            $message = '';
            if (!count($servers)) {
                $message = 'Ничего не найдено!';
            }

            return View::make('home', [
                'title' => Config::get('app.seo_tag_prefix') . $tag->meta_title . Config::get('app.seo_tag_suffix'),
                'meta_description' => $tag->meta_description,
                'meta_keywords' => $tag->meta_keywords,
                'seo_text' => $tag->seo_text,
                'tag' => $tag,
                'servers' => $servers,
                'page_title' => 'Сервера Minecraft c тегом "' . $tag->name . '"',
                'message' => $message,
            ]);
        }
        return Redirect::route('home');
    }


    public function getServersByMode($slug)
    {
        $tag = $this->mode->getFirstBy('slug', $slug);

        if ($this->mode->getCountFirstBy('slug', $slug)) {
            $servers = $this->server->searchByMode($slug, Config::get('app.servers_per_tags'));
            foreach ($servers as &$server) {
                $server->already_voted = $this->vote->checkAlreadyVoted($server->id);
            }

            $message = '';
            if (!count($servers)) {
                $message = 'Ничего не найдено!';
            }

            return View::make('home', [
                'title' => Config::get('app.seo_tag_prefix') . $tag->name . Config::get('app.seo_tag_suffix'),
                'meta_description' => $tag->meta_description,
                'meta_keywords' => $tag->meta_keywords,
                'seo_text' => $tag->seo_text,
                'tag' => $tag,
                'servers' => $servers,
                'page_title' => 'Сервера Minecraft c модом "' . $tag->name . '"',
                'message' => $message,
            ]);
        }
        return Redirect::route('home');
    }

    public function getServersByGame($slug)
    {
        $tag = $this->game->getFirstBy('slug', $slug);

        if ($this->game->getCountFirstBy('slug', $slug)) {
            $servers = $this->server->searchByGame($slug, Config::get('app.servers_per_tags'));
            foreach ($servers as &$server) {
                $server->already_voted = $this->vote->checkAlreadyVoted($server->id);
            }

            $message = '';
            if (!count($servers)) {
                $message = 'Ничего не найдено!';
            }

            return View::make('home', [
                'title' => Config::get('app.seo_tag_prefix') . $tag->name . Config::get('app.seo_tag_suffix'),
                'meta_description' => $tag->meta_description,
                'meta_keywords' => $tag->meta_keywords,
                'seo_text' => $tag->seo_text,
                'tag' => $tag,
                'servers' => $servers,
                'page_title' => 'Сервера Minecraft c мини игрой "' . $tag->name . '"',
                'message' => $message
            ]);
        }
        return Redirect::route('home');
    }


    public function getServersByCountry($code)
    {
        $country = $this->country->getFirstBy('code', strtoupper($code));

        if ($this->country->getCountFirstBy('code', strtoupper($code))) {
            return View::make('home', [
                'title' => Config::get('app.seo_country_prefix') . $country->name . Config::get('app.seo_country_suffix'),
                'country' => $country,
                'servers' => $this->server->searchByCountry($code, Config::get('app.servers_per_country'))
            ]);
        }

        return Redirect::route('home');
    }

    public function getServersByVersion($name)
    {
        $version = $this->version->getFirstBy('slug', $name);


        if ($this->version->getCountFirstBy('slug', $name)) {
            $servers = $this->server->searchByVersion($name, Config::get('app.servers_per_version'));
            foreach ($servers as &$server) {
                $server->already_voted = $this->vote->checkAlreadyVoted($server->id);
            }

            $message = '';
            if (!count($servers)) {
                $message = 'Ничего не найдено!';
            }

            return View::make('home', [
                'title' => Config::get('app.seo_version_prefix') . $version->meta_title . Config::get('app.seo_version_suffix'),
                'meta_description' => $version->meta_description,
                'meta_keywords' => $version->meta_keywords,
                'seo_text' => $version->seo_text,
                'version' => $version,
                'servers' => $servers,
                'page_title' => 'Сервера Minecraft версии "' . $version->name . '"',
                'message' => $message
            ]);
        }

        return Redirect::route('home');
    }

    public function getSearch($term, $page = 1)
    {
        $servers = $this->server->getByPageSearch(e(urldecode($term)), Config::get('app.servers_per_search'));
        foreach ($servers as &$server) {
            $server->already_voted = $this->vote->checkAlreadyVoted($server->id);
        }


        $message = '';
        if (!count($servers)) {
            $message = 'Ничего не найдено!';
        }

        return View::make('home', [
            'title' => trans('general.search_title', ['title' => Config::get('app.seo_meta_title')]),
            'servers' => $servers,
            'term' => $term,
            'page_title' => 'Результаты поиска по запросу: "' . $term . '"',
            'message' => $message,
            'page' => $page,
        ]);

        return View::make('search');
    }

    public function postSearch()
    {
        if (strlen(Input::get('term')) < 3) {
            return Redirect::route('home');
        }

        return Redirect::route('search', e(urlencode(Input::get('term'))));
    }

    public function getContact()
    {
        return View::make('contact', [
            'title' => trans('general.contact_title', ['title' => Config::get('app.seo_meta_title')])
        ]);
    }

    public function postContact()
    {
        $validator = Validator::make(Input::all(), Config::get('validation.contact'));

        if ($validator->fails()) {
            return Redirect::route('contact')
                ->withErrors($validator)
                ->withInput();
        } else {
            $email = Input::get('email');
            $subject = Input::get('subject');
            $msg = Input::get('message');

            Mail::send('emails.contact', ['email' => $email, 'subject' => $subject, 'msg' => $msg], function ($message) use ($email, $subject) {
                $message->from($email)->to(Config::get('app.owner_email'))->subject($subject);
            });

            return Redirect::route('contact')
                ->with('global', trans('general.message_sent'))
                ->with('type', 'success');
        }
    }

    public function getVip()
    {
        return View::make('vip', [
            'title' => trans('general.vip_title', ['title' => Config::get('app.seo_meta_title')]),
            'vip' => $this->vip,
            'servers' => $this->server->getMyServers(Auth::user()->username)
        ]);
    }

    public function postVip()
    {
        $validator = Validator::make(Input::all(), Config::get('validation.vip'));

        if ($validator->fails()) {
            return Redirect::route('vip')
                ->withErrors($validator)
                ->withInput();
        } else {
            $adapter = Gateway::driver('Paypal')->initialize([
                'sandboxMode' => false,
                'successUrl' => URL::route('thank-you'),
                'cancelUrl' => URL::route('thank-you'),
                'backendUrl' => URL::route('success-vip'),
                'merchantAccount' => Config::get('app.business_mail'),
                'language' => 'EN',
                'currency' => Config::get('app.vip_currency'),
                'purpose' => 'VIP Membership ' . Config::get('app.seo_meta_title'),
                'amount' => Input::get('amount') * Config::get('app.amount_day'),
                'remark' => Input::get('server') . ':' . Input::get('amount'),
            ]);

            $pay = $adapter->includeSubmitBtn()->render([
                'rm' => 2
            ]);

            return View::make('confirm-vip', [
                'title' => trans('general.confirm_vip_title', ['title' => Config::get('app.seo_meta_title')]),
                'server' => $this->server->find(Input::get('server')),
                'days' => Input::get('amount'),
                'end' => Carbon::now()->addDays(Input::get('amount')),
                'pay' => $pay
            ]);
        }
    }

    public function postIpn()
    {
        $listener = new Listener;
        $verifier = new CurlVerifier;
        $ipnMessage = Message::createFromGlobals(); // uses php://input

        $verifier->setIpnMessage($ipnMessage);
        $verifier->setEnvironment('production'); // can either be sandbox or production

        $listener->setVerifier($verifier);

        $listener->listen(function () use ($listener) {
            $custom = explode(':', Input::get('custom'));

            if (Input::get('receiver_email') != Config::get('app.business_mail')) {
                return false;
            }

            if (Input::get('mc_gross') != Config::get('app.amount_day') * $custom[1]) {
                return false;
            }

            if ($this->vip->getCountFirstBy('txn_id', Input::get('txn_id')) < 1) {
                $this->vip->create([
                    'server_id' => $custom[0],
                    'start' => Carbon::now(),
                    'end' => Carbon::now()->addDays($custom[1]),
                    'days' => $custom[1],
                    'txn_id' => Input::get('txn_id'),
                    'mc_gross' => Input::get('mc_gross'),
                    'payment_status' => 'pending',
                    'payer_first_name' => Input::get('first_name'),
                    'payer_last_name' => Input::get('last_name'),
                    'payer_email' => Input::get('payer_email')
                ]);
            }


            if (Input::get('payment_status') == 'Completed') {
                /*Mail::send('emails.vip', array('page' => URL::route('servers-manage')), function($message)
                {
                    $message->from(Config::get('app.owner_email'))->to(Auth::user()->email)->subject('Vip Membership');
                });*/

                return $this->vip->updateVip(Input::get('txn_id'), [
                    'payment_status' => 'completed'
                ]);
            }

            if (Input::get('payment_status') == 'Pending') {
                return $this->vip->updateVip(Input::get('txn_id'), [
                    'payment_status' => 'pending'
                ]);
            }

            if (Input::get('payment_status') == 'Refunded') {
                return $this->vip->updateVip(Input::get('txn_id'), [
                    'payment_status' => 'refunded'
                ]);
            }
        }, function () use ($listener) {
            // on invalid IPN (somethings not right!)
            $report = $listener->getReport();
            $resp = $listener->getVerifier()->getVerificationResponse();
        });
    }

    public function anyThankYou()
    {
        return View::make('thank-you', [
            'title' => trans('general.thank_you_title', ['title' => Config::get('app.seo_meta_title')])
        ]);
    }

    public function getViewPayment($txn)
    {
        $txn_id = $this->vip->getFirstBy('txn_id', $txn);

        if ($this->vip->getCountFirstBy('txn_id', $txn)) {
            return View::make('view-payment', [
                'title' => trans('general.view_payment_title', ['title' => Config::get('app.seo_meta_title')]),
                'payment' => $txn_id,
            ]);
        }

        return Redirect::route('home');
    }

    public function getPosts()
    {
        $posts = $this->post->getNews(10);

        return View::make('posts.list', [
            'title' => 'Новости',
            'posts' => $posts,
            'page_title' => 'Рейтинг серверов Minecraft'
        ]);
    }

    public function getPost($Alias)
    {
        $post = $this->post->getPost($Alias);

        if (!$post) {
            return View::make('errors.error', [
                'h1' => 'ERROR 404',
                'description' => 'Новость с алиасом "' . $Alias . '" не найдена.'
            ]);
        }

        return View::make('posts.item', [
            'title' => 'Новости',
            'Post' => $post,
            'page_title' => $post->title
        ]);
    }


    /*
     * !!! Filter
     */


    public function filter($Params, $page = 1)
    {

        // Parse Uri
        $Filter = explode('-', $Params);


        $Servers = $this->server->getByPageFilter($Filter, Config::get('app.servers_per_home'));
        $Types = $this->tag->detectTypes($Filter);

        // Test if voted
        $H1 = 'Серверы Minecraft ';
        $Title = 'Серверы Minecraft ';
        $Keywords = '';
        $Description = 'Серверы Minecraft ';
        $Seo_Text = 'Рейтинг серверов Майнкрафт (Minecraft) ';


        // Get Data

        foreach ($Types['version'] as $item) {
            $H1 .= 'версии ' . $item->h1 . ', ';
            $Title .= 'версии ' . $item->meta_title . ', ';
            $Keywords .= 'Версия ' . $item->meta_keywords . ', ';
            $Description .= 'с версией ' . $item->meta_description . ', ';
            $Seo_Text .= 'с версией ' . $item->seo_text . ', ';
        }

        if (count($Types['tags']) == 0) {
            $Text = '';
        } else if (count($Types['tags']) == 1) {
            $Text = ' с тегом ';
        } else if (count($Types['tags']) > 1) {
            $Text = ' с тегами ';
        } else {
            $Text = '';
        }

        $H1 .= $Text;
        $Title .= $Text;
        $Description .= $Text;
        $Seo_Text .= $Text;

        $Index = count($Types['tags']);
        foreach ($Types['tags'] as $key => $item) {
            $Index--;
            if ($Index == 1) {
                $separate = ' и ';
            } else {
                $separate = ', ';
            }

            $H1 .= '' . $item->h1 . $separate;
            $Title .= '' . $item->meta_title . $separate;
            $Keywords .= $item->meta_keywords . ', ';
            $Description .= $item->meta_description . $separate;
            $Seo_Text .= '' . $item->seo_text . $separate;
        }

        if (count($Types['modes']) == 0) {
            $Text = '';
        } else if (count($Types['modes']) == 1) {
            $Text = ' с модом ';
        } else if (count($Types['modes']) > 1) {
            $Text = ' с модами ';
        } else {
            $Text = '';
        }

        $H1 .= $Text;
        $Title .= $Text;
        $Description .= $Text;
        $Seo_Text .= $Text;

        $Index = count($Types['modes']);
        foreach ($Types['modes'] as $item) {
            $Index--;
            if ($Index == 1) {
                $separate = ' и ';
            } else {
                $separate = ', ';
            }

            $H1 .= '' . $item->h1 . $separate;
            $Title .= '' . $item->meta_title . $separate;
            $Keywords .= $item->meta_keywords . ', ';
            $Description .= '' . $item->meta_description . $separate;
            $Seo_Text .= '' . $item->seo_text . $separate;
        }


        if (count($Types['games']) == 0) {
            $Text = '';
        } else if (count($Types['games']) == 1) {
            $Text = ' с мини-игрой ';
        } else if (count($Types['games']) > 1) {
            $Text = ' с мини-играми ';
        } else {
            $Text = '';
        }

        $H1 .= $Text;
        $Title .= $Text;
        $Description .= $Text;
        $Seo_Text .= $Text;

        $Index = count($Types['games']);
        foreach ($Types['games'] as $item) {
            $Index--;
            if ($Index == 1) {
                $separate = ' и ';
            } else {
                $separate = ', ';
            }
            $H1 .= '' . $item->h1 . $separate;
            $Title .= '' . $item->meta_title . $separate;
            $Keywords .= $item->meta_keywords . ', ';
            $Description .= '' . $item->meta_description . $separate;
            $Seo_Text .= '' . $item->seo_text . $separate;
        }


        $H1 = substr($H1, 0, -2);
        $Title = substr($Title, 0, -2);
        $Keywords = substr($Keywords, 0, -2);
        $Description = substr($Description, 0, -2) . '.';
        $Seo_Text = substr($Seo_Text, 0, -2) . '.';

        $Tags = $this->tag->getAllTags();
        $Games = $this->tag->getAllGames();
        $Modes = $this->tag->getAllModes();
        $Versions = $this->tag->getAllVersions();

        if (isset($Types['tags'])) {
            foreach ($Tags as &$Tag) {
                foreach ($Types['tags'] as $Filter_Tag) {
                    if ($Tag->slug == $Filter_Tag->slug) {
                        $Tag->active = true;
                    }
                }
            }
        }
        if (isset($Types['games'])) {
            foreach ($Games as &$Game) {
                foreach ($Types['games'] as $Filter_Game) {
                    if ($Game->slug == $Filter_Game->slug) {
                        $Game->active = true;
                    }
                }
            }
        }

        if (isset($Types['modes'])) {
            foreach ($Modes as &$Mode) {
                foreach ($Types['modes'] as $Filter_Mode) {
                    if ($Mode->slug == $Filter_Mode->slug) {
                        $Mode->active = true;
                    }
                }
            }
        }

        if (isset($Types['version'])) {
            foreach ($Versions as &$Version) {
                foreach ($Types['version'] as $Filter_Version) {
                    if ($Version->slug == $Filter_Version->slug) {
                        $Version->active = true;
                    }
                }
            }
        }

        View::share('tags', $Tags);
        View::share('games', $Games);
        View::share('modes', $Modes);
        View::share('versions', $Versions);


        $Filter_Version = '';
        $Filter_Tags = '';
        $Filter_Games = '';
        $Filter_Modes = '';

        foreach ($Types['version'] as $Item) {
            $Filter_Version .= '-' . $Item->slug;
        }
        foreach ($Types['tags'] as $Item) {
            $Filter_Tags .= '-' . $Item->slug;
        }
        foreach ($Types['games'] as $Item) {
            $Filter_Games .= '-' . $Item->slug;
        }
        foreach ($Types['modes'] as $Item) {
            $Filter_Modes .= '-' . $Item->slug;
        }


        foreach ($Servers as &$server) {
            $server->already_voted = $this->vote->checkAlreadyVoted($server->id);
        }

        return View::make('home', [
            'title' => $Title . ' ' . Config::get('app.seo_meta_title'),
            'meta_description' => $Keywords,
            'meta_keywords' => $Description,
            'seo_text' => $Seo_Text,
            'h1' => $H1,
            'servers' => $Servers,
            'countries' => $this->country->all(),
            'page_title' => $Title,

            'filter_version' => $Filter_Version,
            'filter_tags' => $Filter_Tags,
            'filter_games' => $Filter_Games,
            'filter_modes' => $Filter_Modes,

            'filter' => $Filter,
            'types' => $Types,
            'page' => $page,
        ]);
    }

    public function nothing_found()
    {
        return View::make('errors.error', [
            'h1' => 'НИЧЕГО НЕ НАЙДЕНО',
            'description' => 'По данному запросу ничего не найдено'
        ]);
    }

    public function postYandex()
    {
        $client_id = '212474DB617386B00CC5C8D11D6661C7F2102EE0C9C5AD9E9E66F80D330CA34A';
        $redirect_uri = 'https://minecraft-server.ru/pay-yandex';
        $scope = ['client_secret' => 'C07168CDDC5C00A6D6E04512C74EEC78C0E5C229BD24BBAEEB727721E5FF55677894F4E90704D11567BB32808F62ED014BE962C3D461858EDCFAE88E554192D1s'];
        $auth_url = API::buildObtainTokenUrl($client_id, $redirect_uri, $scope);
        print_r($auth_url);
//        print_r(Input::all());
//        exit;
    }
}