<?php namespace MSQ;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{

    protected $table = 'votes';

    protected $fillable = ['server_id', 'user_id', 'mc_username', 'ip'];

    /**
     * Settings null value for created_at column
     */
    public function setUpdatedAtAttribute($value)
    {
        // Do nothing.
    }


    public function checkAlreadyVoted($server)
    {

        return false;
    }
}