<?php namespace MSQ\Providers;

use Illuminate\Support\ServiceProvider;

class MsqServiceProvider extends ServiceProvider {

	public function boot()
	{

	}

	public function register()
	{
		$this->app->bind('MSQ\Repositories\Table\TableInterface', 'MSQ\Repositories\Table\EloquentTable');
		$this->app->bind('MSQ\Repositories\Server\ServerInterface', 'MSQ\Repositories\Server\EloquentServer');
		$this->app->bind('MSQ\Repositories\User\UserInterface', 'MSQ\Repositories\User\EloquentUser');
		$this->app->bind('MSQ\Repositories\Country\CountryInterface', 'MSQ\Repositories\Country\EloquentCountry');
		$this->app->bind('MSQ\Repositories\Version\VersionInterface', 'MSQ\Repositories\Version\EloquentVersion');
		$this->app->bind('MSQ\Repositories\Tag\TagInterface', 'MSQ\Repositories\Tag\EloquentTag');
		$this->app->bind('MSQ\Repositories\Mode\ModeInterface', 'MSQ\Repositories\Mode\EloquentMode');
		$this->app->bind('MSQ\Repositories\Game\GameInterface', 'MSQ\Repositories\Game\EloquentGame');
		$this->app->bind('MSQ\Repositories\Statistics\StatisticsInterface', 'MSQ\Repositories\Statistics\EloquentStatistics');
		$this->app->bind('MSQ\Repositories\Comment\CommentInterface', 'MSQ\Repositories\Comment\EloquentComment');
		$this->app->bind('MSQ\Repositories\Vote\VoteInterface', 'MSQ\Repositories\Vote\EloquentVote');
		$this->app->bind('MSQ\Repositories\Vip\VipInterface', 'MSQ\Repositories\Vip\EloquentVip');
		$this->app->bind('MSQ\Repositories\Post\PostInterface', 'MSQ\Repositories\Post\EloquentPost');
	}

}