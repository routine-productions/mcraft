<?php namespace MSQ;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model {

	public $table = 'tags';
//	public $fillable = array('name');
	public $timestamps = false;
    protected $guarded = ['id'];

	public function servers()
	{
		return $this->belongsToMany('MSQ\Server','server_tag');
	}
}