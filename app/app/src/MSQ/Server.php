<?php namespace MSQ;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Server extends Model
{

    protected $table = 'servers';

//    protected $fillable = ['name', 'description', 'short_description', 'address', 'port', 'website', 'country_id', 'version_id', 'banner', 'youtube', 'votifier_active', 'votifier_address', 'votifier_port', 'votifier_key', 'status', 'user_id', 'online_time', 'verified_time', 'points', 'votes', 'rank', 'diamonds'];
    protected $guarded = ['id'];

    /**
     * Get all statistics for a server
     */
    public function statistics()
    {
        return $this->hasOne('MSQ\Statistics')->orderBy('updated_at', 'DESC');
    }

    public function statistics_list()
    {
        return $this->hasMany('MSQ\Statistics')->orderBy('updated_at', 'DESC')->take(24);
    }


    /**
     * Get version of server
     */
    public function version()
    {
//        return $this->belongsTo('MSQ\Tag')->where('type', 4);
        return $this->hasOne('MSQ\Tag', 'id', 'version_id');
    }

    public function version_belongs()
    {
        return $this->belongsToMany('MSQ\Tag')->where('type', 4);
//        return $this->hasOne('MSQ\Tag', 'id', 'version_id');
    }

    /**
     * Get tags of server
     */
    public function tags()
    {
        return $this->belongsToMany('MSQ\Tag', 'server_tag')->where('type', 1);
    }

    /**
     * Get games of server
     */
    public function games()
    {
        return $this->belongsToMany('MSQ\Tag', 'server_tag')->where('type', 2);
    }


    /**
     * Get modes of server
     */
    public function modes()
    {
        return $this->belongsToMany('MSQ\Tag', 'server_tag')->where('type', 3);
    }

    /**
     * Get filter tags, games, versions, modex
     */
    public function filter()
    {
        return $this->belongsToMany('MSQ\Tag', 'server_tag');
    }


    /**
     * Get country of server
     */
    public function country()
    {
        return $this->belongsTo('MSQ\Country');
    }

    /**
     * Get owner of server
     */
    public function user()
    {
        return $this->belongsTo('MSQ\User');
    }

    /**
     * Count number of comment for a server
     */
    public function getTotalCommentsAttribute()
    {
        return $this->hasMany('MSQ\Comment')->whereServerId($this->id)->count();
    }

    /**
     * Purchases Banner
     */
    public function purchases_banner()
    {
        return $this->hasOne('MSQ\Purchases')->where('end_period', '>', Carbon::now())->where('type', 'banner')->where('paid', '>', 0);
    }

    /**
     * Purchases Color
     */
    public function purchases_color()
    {
        return $this->hasOne('MSQ\Purchases')->where('end_period', '>', Carbon::now())->where('type', 'color')->where('paid', '>', 0);
    }

    /**
     * Purchases Color
     */
    public function purchases_diamonds()
    {
        return $this->hasMany('MSQ\Purchases')->where('end_period', '>', Carbon::now())->where('type', 'diamond')->where('paid', '>', 0);
    }

    public function purchases_diamonds_count()
    {
        return $this->hasMany('MSQ\Purchases')->where('end_period', '>', Carbon::now())->where('type', 'diamond')->where('paid', '>', 0)->sum('diamonds');
    }

}