<?php namespace MSQ;

use Illuminate\Database\Eloquent\Model;

class Statistics extends Model {

	protected $table = 'statistics';

	protected $fillable = array('server_id', 'players', 'max_players', 'motd', 'favicon');

	/**
	 * Settings null value for created_at column
	 */
	public function setCreatedAtAttribute($value)
	{
	    // Do nothing.
	}

}