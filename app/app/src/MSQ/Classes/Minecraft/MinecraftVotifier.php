<?php namespace MSQ\Classes\Minecraft;

class MinecraftVotifier {
	const VOTE_FORMAT       = "VOTE\n%s\n%s\n%s\n%d\n";
	const PUBLIC_KEY_FORMAT = "-----BEGIN PUBLIC KEY-----\n%s\n-----END PUBLIC KEY-----";

	private $public_key;
	private $server_ip;
	private $port;
	private $service_name;

	public function __construct($public_key = null, $server_ip = null, $port = 8192, $service_name = null) {
		$this->public_key   = $this->formatPublicKey($public_key);
		$this->server_ip    = $server_ip;
		$this->port         = $port;
		$this->service_name = $service_name;
	}

	public function __get($name) {
		return isset($this->$name) ? $this->$name : null;
	}

	public function __set($name, $value) {
		if ($name == 'public_key')
			$this->public_key = $this->formatPublicKey($value);
		else
			$this->$name = $value;
	}

	private function formatPublicKey($public_key) {
		$public_key = wordwrap($public_key, 65, "\n", true);
		$public_key = sprintf(self::PUBLIC_KEY_FORMAT, $public_key);

		return $public_key;
	}

	public function sendVote($username) {
		$address = $_SERVER['REMOTE_ADDR']; //FIXME: Is it a good idea to hardcode this? Does not work from CLI.
		$vote = sprintf(self::VOTE_FORMAT, $this->service_name, $username, $address, time());

		openssl_public_encrypt($vote, $data, $this->public_key);

		$socket = @fsockopen($this->server_ip, $this->port);

		if ($socket) {
			if (fwrite($socket, $data))
				return true;
		}

		return false;
	}
}
