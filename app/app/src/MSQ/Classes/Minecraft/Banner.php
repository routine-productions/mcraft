<?php namespace MSQ\Classes\Minecraft;

class Banner
{
	private $data = array();

	private $colors = array(
		'white' => array(
			'r' => 255,
			'g' => 255,
			'b' => 255
		),
		'red' => array(
			'r' => 255,
			'g' => 0,
			'b' => 0
		),
		'green' => array(
			'r' => 60,
			'g' => 201,
			'b' => 44
		),
		'coral' => array(
			'r' => 255,
			'g' => 119,
			'b' => 0
		),
	);

	/**
	 * Class construct
	 * @param  string $type
	 * @param  string $background
	 * @return mixed
	 */
	public function __construct($type, $background)
	{
		// Image type, e.g: png, jpeg
		$this->data['type'] = $type;

		// Image background with extension = type
		$this->data['background'] = $background;

		$this->imageCreateFrom();
	}

	/**
	 * Create image from type
	 * @return mixed
	 */
	private function imageCreateFrom()
	{
		switch ($this->data['type']) {
			case 'png':
				$this->data['image_create'] = imagecreatefrompng($this->data['background']);
				break;
			case 'jpeg':
				$this->data['image_create'] = imagecreatefromjpeg($this->data['background']);
				break;
		}

		return $this;
	}

	/**
	 * Create image text
	 * @param  string $text
	 * @param  array $settings
	 * @param  string $font
	 * @param  string $color
	 * @return mixed
	 */
	public function createText($text, $settings = array(), $font, $color = 'white')
	{
		// Create image text
		$this->data['text'] = imagettftext($this->data['image_create'], $settings['size'], $settings['angle'], $settings['x'], $settings['y'], ImageColorAllocate($this->data['image_create'], $this->colors[$color]['r'], $this->colors[$color]['g'], $this->colors[$color]['b']), $font, $text);
		return $this;
	}

	/**
	 * Create image header
	 * @return function
	 */
	private function createHeader()
	{
		return header("Content-Type: image/{$this->data['type']}");
	}

	/**
	 * Destroy image
	 * @return mixed
	 */
	private function destroyImage()
	{
		switch ($this->data['type']) {
			case 'png':
				imagepng($this->data['image_create']);
				imagedestroy($this->data['image_create']);
				break;
			case 'jpeg':
				imagejpeg($this->data['image_create'], NULL, 100);
				imagedestroy($this->data['image_create']);
				break;
		}
	}

	/**
	 * Render banner
	 * @return mixed
	 */
	public function render()
	{
		// Create image header
		$this->createHeader();

		// Set image text
		$this->data['text'];

		// Destroy image
		$this->destroyImage();
	}

	
}