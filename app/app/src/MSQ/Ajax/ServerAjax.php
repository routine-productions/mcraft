<?php namespace MSQ\Ajax;

use MSQ\Repositories\Server\ServerInterface;
use Response;
use MSQ\Classes\Minecraft\MinecraftQuery;

class ServerAjax {

	protected $server;

	public function __construct(ServerInterface $server)
	{
		$this->server = $server;
	}

	public function checkIfExists($address, $port = 25565)
	{
		$server = $this->server->getFirstBy('address', $address);

		if ($this->server->getCountFirstBy('address', $address)) {
			return Response::json(array('state' => 'error', 'message' => 'Address already exists'));
		}
	}

	public function checkServerStatus($address, $port = 25565, $version)
	{
		$status = new MinecraftQuery();

		$response = $status->getStatus($address, $port, $version);

		if ($response) {
			return Response::json(array('state' => 'good'));
		} else {
			return Response::json(array('state' => 'error', 'message' => 'Connection problem. Check your server settings'));
		}
	}

}