<?php namespace MSQ\Repositories\Vote;

use MSQ\Vote;
use MSQ\Repositories\AbstractEloquentRepository;
use Request, Carbon\Carbon, DB;

class EloquentVote extends AbstractEloquentRepository implements VoteInterface
{

    protected $model;

    /**
     * Vote Constructor
     *
     * @param Vote $model IoC for Vote
     */
    public function __construct(Vote $model)
    {
        $this->model = $model;
    }

    /**
     * Create method for create a new user
     *
     * @param  array $data User data
     *
     * @return boolean      True if successful
     */
    public function create(array $data)
    {
        return $this->model->create($data);
    }

    public function giveVote($server, $minecraft)
    {
        if ($this->checkAlreadyVoted($server) == false) {
            $vote = $this->create([
                'server_id'   => $server,
                'mc_username' => $minecraft,
                'ip'          => Request::getClientIp()
            ]);

            if ($vote) {
                return true;
            }
        }
    }

    public function checkAlreadyVoted($server)
    {
        $vote = $this->model
            ->where('server_id', '=', $server)
            ->where('ip', '=', Request::getClientIp())
            ->where('created_at', '<', Carbon::today()->addHours(24));
        if ($vote->count()) {
            return true;
        }
        return false;
    }

    public function allVotes($server)
    {
        return $this->model->select(DB::raw('count(*) as votes, mc_username'))->where('server_id', $server)->orderBy('votes', 'DESC')->groupBy('mc_username')->get();
    }
}