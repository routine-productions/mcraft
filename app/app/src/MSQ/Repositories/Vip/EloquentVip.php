<?php namespace MSQ\Repositories\Vip;

use MSQ\Vip;
use MSQ\Repositories\AbstractEloquentRepository;
use Carbon\Carbon;

class EloquentVip extends AbstractEloquentRepository implements VipInterface {

	protected $model;

	/**
	 * Vip Constructor
	 * @param Vip $model IoC for Vip
	 */
	public function __construct(Vip $model)
	{
		$this->model = $model;
	}

	/**
	 * Create method for create a new user
	 * @param  array  $data Vip data
	 * @return boolean      True if successful
	 */
	public function create(array $data)
	{
		return $this->model->create($data);
	}


	public function getServers()
	{
		$servers = $this->model->where('end', '>', Carbon::now())
		                       ->where('payment_status', 'completed');

		return $servers->get();
	}

	public function updateVip($txn_id, array $data)
	{
		return $this->model->where('txn_id', $txn_id)->update($data);
	}

	public function checkVip($id)
	{
		return ($this->model->where('server_id', $id)->where('payment_status', 'completed')->where('end', '>', Carbon::now())->count()) ? true : false;
	}

	public function allVips()
	{
		$vips = $this->model->all();

		return $vips;
	}
	

}