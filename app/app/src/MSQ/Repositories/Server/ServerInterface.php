<?php namespace MSQ\Repositories\Server;

interface ServerInterface {
	public function searchByTag($tag, $limit);
}