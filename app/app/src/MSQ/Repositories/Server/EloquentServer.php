<?php namespace MSQ\Repositories\Server;

use MSQ\Server;
use MSQ\Repositories\AbstractEloquentRepository;
use DB, Auth;

class EloquentServer extends AbstractEloquentRepository implements ServerInterface
{

    protected $model;

    /**
     * Server Constructor
     *
     * @param Server $model IoC for Server
     */
    public function __construct(Server $model)
    {
        $this->model = $model;
    }


    /**
     * Get servers by limit
     *
     * @param  int $limit
     *
     * @return object
     */
    public function getByPageServer($limit)
    {
        return $this->model->orderBy('rank', 'asc')->with('statistics', 'version')->paginate($limit)->route('servers');
    }

    /**
     * Get servers by diamond
     *
     * @param  int $limit
     *
     * @return object
     */
    public function getByPageServerDiamond($limit)
    {
        return $this->model
            ->select(DB::raw('servers.*, sum(diamonds) as diamonds_sum'))
            ->leftjoin('purchases AS pr', 'servers.id', '=', 'pr.server_id')
            ->groupBy('servers.id')
            ->orderBy('diamonds_sum', 'desc')
            ->with(['statistics', 'version', 'purchases_banner', 'purchases_color', 'purchases_diamonds'])
            ->paginate($limit)->route('servers');
    }

    /**
     * Get servers by date
     *
     * @param  int $limit
     *
     * @return object
     */
    public function getByPageServerNew($limit)
    {
        return $this->model->orderBy('created_at', 'desc')->with('statistics', 'version', 'purchases_banner', 'purchases_color', 'purchases_diamonds')->paginate($limit)->route('new-servers-page');
    }

    /**
     * Get servers by date
     *
     * @param  int $limit
     *
     * @return object
     */
    public function getByPageServerTop($limit)
    {
        return $this->model->orderBy('votes', 'desc')->with('statistics', 'version', 'purchases_banner', 'purchases_color', 'purchases_diamonds')->paginate($limit)->route('top-page');
    }

    /**
     * Get servers by limit
     *
     * @param  int $limit
     *
     * @return object
     */
    public function getByPageSearch($term, $limit)
    {
        return $this->model->where('name', 'like', '%' . $term . '%')->orderBy('votes', 'desc')->with('statistics', 'version', 'purchases_banner', 'purchases_color', 'purchases_diamonds')->paginate($limit)->route('search-servers', [$term]);
    }

    public function getMyServersByPage($user_id, $limit)
    {
        return $this->model->where('user_id', $user_id)->with('statistics', 'version', 'purchases_banner', 'purchases_color', 'purchases_diamonds')->paginate($limit)->route('servers-manage');
    }

    /**
     * Create a data in table
     *
     * @param  array $data [description]
     *
     * @return object
     */
    public function create(array $data)
    {
        return $this->model->create($data);
    }

    public function tags()
    {
        return $this->model->tags();
    }

    public function games()
    {
        return $this->model->games();
    }

    public function modes()
    {
        return $this->model->modes();
    }

    public function searchByTag($slug, $limit)
    {
        return $this->model->with('statistics', 'version', 'purchases_banner', 'purchases_color', 'purchases_diamonds')->whereHas('tags', function ($q) use ($slug) {
            $q->where('slug', $slug);
        })->orderBy('diamonds', 'desc')->paginate($limit)->route('tag', [$slug]);
    }

    public function searchByMode($slug, $limit)
    {
        return $this->model->with('statistics', 'version', 'purchases_banner', 'purchases_color', 'purchases_diamonds')->whereHas('modes', function ($q) use ($slug) {
            $q->where('slug', $slug);
        })->orderBy('diamonds', 'desc')->paginate($limit)->route('mode', [$slug]);
    }

    public function searchByGame($slug, $limit)
    {
        return $this->model->with('statistics', 'version', 'purchases_banner', 'purchases_color', 'purchases_diamonds')->whereHas('games', function ($q) use ($slug) {
            $q->where('slug', $slug);
        })->orderBy('diamonds', 'desc')->paginate($limit)->route('game', [$slug]);
    }

    public function searchByCountry($code, $limit)
    {
        return $this->model->with('statistics', 'version', 'purchases_banner', 'purchases_color', 'purchases_diamonds')->whereHas('country', function ($q) use ($code) {
            $q->where('code', $code);
        })->orderBy('diamonds', 'desc')->paginate($limit)->route('country', [$code]);
    }

    public function searchByVersion($name, $limit)
    {
        return $this->model->with('statistics', 'version', 'purchases_banner', 'purchases_color', 'purchases_diamonds')->whereHas('version', function ($q) use ($name) {
            $q->where('slug', $name);
        })->orderBy('diamonds', 'desc')->paginate($limit)->route('version', [$name]);
    }


    public function editServer($server_id, $user_id)
    {
        return $this->model->where('id', $server_id)->where('user_id', $user_id)->first();
    }

    public function haveTag($server_id, $tag_id)
    {
        $tags = $this->model->find($server_id)->tags;

        if ($tags->count()) {
            foreach ($tags as $tag) {
                if ($tag->id == $tag_id) return true;
            }
        }

        return false;
    }

    public function haveGame($server_id, $game_id)
    {
        $tags = $this->model->find($server_id)->games;

        if ($tags->count()) {
            foreach ($tags as $tag) {
                if ($tag->id == $game_id) return true;
            }
        }

        return false;
    }

    public function haveMode($server_id, $mode_id)
    {
        $tags = $this->model->find($server_id)->modes;

        if ($tags->count()) {
            foreach ($tags as $tag) {
                if ($tag->id == $mode_id) return true;
            }
        }

        return false;
    }

    public function deleteAllByUsername($id)
    {
        return $this->model->where('user_id', '=', $id)->delete();
    }

    public function lastRank()
    {
        return DB::table('servers')->max('rank') + 1;
    }

    public function deleteMyServer($id)
    {
        if ($delete = $this->model->where('user_id', '=', Auth::user()->id)->where('id', '=', $id)) {
            return $delete->delete();
        }

        return false;
    }

    public function getMyServers($username)
    {
        return $this->model->with('statistics', 'version')->whereHas('user', function ($q) use ($username) {
            $q->where('username', $username);
        })->get();
    }


    /**
     * Get servers by  !filter
     */
    public function getByPageFilter($Filter, $Limit)
    {
        $Model = $this->model;

//        print_r($Filter);
//        exit;
        if (isset($Filter)) {
            foreach ($Filter as $Tag) {
                $Model = $Model->whereHas('filter', function ($Query) use ($Tag) {
                    $Query->where('slug', $Tag);
                });
            }
        }

        return $Model->with('statistics', 'version', 'tags', 'games', 'modes', 'purchases_banner', 'purchases_color', 'purchases_diamonds')->orderBy('votes', 'desc')   ->paginate(10000);
//        return $Model->with('statistics', 'version', 'tags', 'games', 'modes')->paginate(2)->route('filter-page');
    }
}