<?php namespace MSQ\Repositories\Country;

use MSQ\Country;
use MSQ\Repositories\AbstractEloquentRepository;

class EloquentCountry extends AbstractEloquentRepository implements CountryInterface {

	protected $model;

	/**
	 * Country constructor
	 * @param Country $model IoC for Country
	 */
	public function __construct(Country $model)
	{
		$this->model = $model;
	}

}