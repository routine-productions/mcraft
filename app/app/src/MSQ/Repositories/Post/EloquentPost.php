<?php namespace MSQ\Repositories\Post;

use MSQ\Post;
use MSQ\Repositories\AbstractEloquentRepository;

class EloquentPost extends AbstractEloquentRepository implements PostInterface {

	protected $model;

	/**
	 * Tag Constructor
	 * @param Post $model IoC for Post
	 */
	public function __construct(Post $model)
	{
		$this->model = $model;
	}

	/**
	 * Create a data in table
	 * @param  array  $data [description]
	 * @return object
	 */
	public function create(array $data)
	{
		return $this->model->create($data);
	}

	/**
	 * Get servers by date
	 * @param  int $limit
	 * @return object
	 */
	public function getNews($limit)
	{
		return $this->model->orderBy('created_at', 'desc')->paginate($limit)->route('posts_list');
	}

	public function getLastNews($count)
	{
		return $this->model->orderBy('created_at', 'desc')->take($count)->get();
	}


    /**
     * Get servers by date
     * @param  int $limit
     * @return object
     */
    public function getPost($Alias)
    {
        return $this->model->where('alias', $Alias)->first();
    }
}