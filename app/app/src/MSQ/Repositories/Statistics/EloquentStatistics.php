<?php namespace MSQ\Repositories\Statistics;

use MSQ\Statistics;
use MSQ\Repositories\AbstractEloquentRepository;

class EloquentStatistics extends AbstractEloquentRepository implements StatisticsInterface {

	protected $model;

	/**
	 * Statistics Constructor
	 * @param Statistics $model [description]
	 */
	public function __construct(Statistics $model)
	{
		$this->model = $model;
	}

	/**
	 * Create a data in table
	 * @param  array  $data [description]
	 * @return object
	 */
	public function create(array $data)
	{
		return $this->model->create($data);
	}

	public function deleteAllByServer($id)
	{
		return $this->model->where('server_id', '=', $id)->delete();
	}
}