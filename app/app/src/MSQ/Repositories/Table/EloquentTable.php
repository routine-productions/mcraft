<?php namespace MSQ\Repositories\Table;

use MSQ\Table;

class EloquentTable implements TableInterface {

	protected $model;

	public function __construct(Table $model)
	{
		$this->model = $model;
	}

	public function test()
	{
		return 'test';
	}

}