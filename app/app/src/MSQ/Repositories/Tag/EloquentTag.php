<?php namespace MSQ\Repositories\Tag;

use MSQ\Tag;
use MSQ\Repositories\AbstractEloquentRepository;

class EloquentTag extends AbstractEloquentRepository implements TagInterface
{

    protected $model;

    public function __construct(Tag $model)
    {
        $this->model = $model;
    }

    public function create(array $data)
    {
        return $this->model->create($data);
    }


    public function getAllTags()
    {
        return $this->model->where('type', 1)->get();
    }

    public function getAllGames()
    {
        return $this->model->where('type', 2)->get();
    }

    public function getAllModes()
    {
        return $this->model->where('type', 3)->get();
    }

    public function getAllVersions()
    {
        return $this->model->where('type', 4)->get();
    }


    public function detectTypes($Filter)
    {
        $Model = $this->model;
        $Types = [
            'tags'    => [],
            'games'   => [],
            'modes'   => [],
            'version' => []
        ];
        $Assoc = [
            1 => 'tags',
            2 => 'games',
            3 => 'modes',
            4 => 'version'
        ];

        foreach ($Filter as $Tag) {
            $Data                                    = $Model->where('slug', $Tag)->first();
            $Types[$Assoc[$Data->type]][$Data->slug] = $Data;
        }
        return $Types;
    }
}