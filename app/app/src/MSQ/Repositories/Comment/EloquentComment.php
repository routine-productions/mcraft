<?php namespace MSQ\Repositories\Comment;

use MSQ\Comment;
use MSQ\Repositories\AbstractEloquentRepository;

class EloquentComment extends AbstractEloquentRepository implements CommentInterface {

	protected $model;

	/**
	 * Comment Constructor
	 * @param Comment $model
	 */
	public function __construct(Comment $model)
	{
		$this->model = $model;
	}

	/**
	 * Get all comments for a server_id
	 * @param  int $id Server id
	 * @return Object
	 */
	public function comments($id)
	{
		$comments = $this->model->with('user')->where('server_id', '=', $id)->get();

		return $comments;
	}

	public function allComments()
	{
		$comments = $this->model->with('user', 'server')->get();

		return $comments;
	}

	/**
	 * Create method for create a new user
	 * @param  array  $data User data
	 * @return boolean      True if successful
	 */
	public function create(array $data)
	{
		return $this->model->create($data);
	}

	public function deleteAllByUsername($id)
	{
		return $this->model->where('user_id', '=', $id)->delete();
	}

	public function deleteAllByServer($id)
	{
		return $this->model->where('server_id', '=', $id)->delete();
	}

}