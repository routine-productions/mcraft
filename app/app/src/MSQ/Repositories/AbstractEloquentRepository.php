<?php namespace MSQ\Repositories;

use StdClass;
use Illuminate\Support\MessageBag;
use Cribbb\Validation\Validable;

abstract class AbstractEloquentRepository {

	/**
	 * @var Illuminate\Database\Eloquent\Model
	 */
	protected $model;

	/**
	 * @var Illuminate\Support\MessageBag
	 */
	protected $errors;

	/**
	 * Construct
	 *
	 * @param Illuminate\Support\MessageBag $errors
	 */
	public function __construct(MessageBag $errors)
	{
		$this->errors = $errors;
	}

	/**
	 * Make a new instance of the entity to query on
	 *
	 * @param array $with
	 */
	public function make(array $with = array())
	{
		return $this->model->with($with);
	}


	/**
	 * Retrieve all entities
	 *
	 * @param array $with
	 * @return Illuminate\Database\Eloquent\Collection
	 */
	public function all(array $with = array())
	{
		$entity = $this->make($with);

		return $entity->get();
	}

	/**
	 * Find a single entity
	 *
	 * @param int $id
	 * @param array $with
	 * @return Illuminate\Database\Eloquent\Model
	 */
	public function find($id, array $with = array())
	{
		$entity = $this->make($with);

		return $entity->find($id);
	}

	/**
	* Get Results by Page
	*
	* @param int $page
	* @param int $limit
	* @param array $with
	* @return StdClass Object with $items and $totalItems for pagination
	*/
	public function getByPage($page = 1, $limit = 10, $with = array())
	{
		$result = new StdClass;
		$result->page = $page;
		$result->limit= $limit;
		$result->totalItems = 0;
		$result->items= array();
		 
		$query = $this->make($with);
		 
		$model = $query->skip($limit * ($page - 1))
		               ->take($limit)
		               ->get();
		 
		$result->totalItems = $this->model->count();
		$result->items = $model->all();
		 
		return $result;
	}

	/**
	 * Search for many results by key and value
	 *
	 * @param string $key
	 * @param mixed $value
	 * @param array $with
	 * @return Illuminate\Database\Query\Builders
	 */
	public function getManyBy($key, $value, array $with = array())
	{
		return $this->make($with)->where($key, '=', $value)->get();
	}

	/**
	 * Search a single result by key and value
	 *
	 * @param string $key
	 * @param mixed $value
	 * @param array $with
	 * @return Illuminate\Database\Query\Builders
	 */
	public function getFirstBy($key, $value, array $with = array())
	{
		return $this->make($with)->where($key, '=', $value)->first();
	}

	/**
	 * Search a single result and count
	 *
	 * @param string $key
	 * @param mixed $value
	 * @param array $with
	 * @return Illuminate\Database\Query\Builders
	 */
	public function getCountFirstBy($key, $value)
	{
		return $this->model->where($key, '=', $value)->count();
	}

	public function take($limit = null)
	{
		return $this->model->take($limit);
	}

	/**
	 * Return the errors
	 *
	 * @return Illuminate\Support\MessageBag
	 */
	public function errors()
	{
		return $this->errors;
	}

}
