<?php

return [
    'register' => [
        'username' => 'required|unique:users',
        'email' => 'required|email|unique:users',
        'password' => 'required|min:6',
        'recaptcha_response_field' => 'required|recaptcha'
    ],

    'login' => [
        'email' => 'required|email',
        'password' => 'required'
    ],

    'change_password' => [
        'old_password' => 'required',
        'password' => 'required|min:6',
        'password_again' => 'required|same:password',
    ],
    'forgot_password' => [
        'email' => 'required|email',
    ],
    'server_add' => [
        'name' => 'required|unique:servers',
        'banner' => 'image|image_size:468,60',
        'address' => 'required|unique:servers',
        'port' => 'integer',
        'website' => 'url',
        'version' => 'required|exists:tags,id',
        'votifier_port' => 'integer',
    ],
    'server_comment' => [
        'comment' => 'required'
    ],
    'server_vote' => [
        'username' => 'required',
        'recaptcha_response_field' => 'required|recaptcha'
    ],
    'edit_server' => [
        'name' => 'required',
        'banner' => 'image|image_size:468,60',
        'address' => 'required',
        'port' => 'integer',
        'website' => 'url',
        'version' => 'required|exists:tags,id',
        'votifier_port' => 'integer',
    ],
    'contact' => [
        'email' => 'required|email',
        'subject' => 'required',
        'message' => 'required'
    ],
    'edit_account' => [
        'website' => 'url',
        'facebook' => 'url',
        'google' => 'url',
        'youtube' => 'url',
        'steam' => 'url',
    ],
    'settings_configuration' => [
        'language' => 'required:max:2',
        'facebook' => 'url',
        'twitter' => 'url',
        'google' => 'url',
        'sph' => 'required|integer',
        'spt' => 'required|integer',
        'spm' => 'required|integer',
        'sps' => 'required|integer',
        'spc' => 'required|integer',
        'tags' => 'required|integer',
        'owner_email' => 'required|email',
    ],
    'settings_seo' => [
        ''
    ],
    'users_edit' => [
        'email' => 'required:email',
        'username' => 'required',
        'password' => 'required|min:6',
    ],
    'users_create' => [
        'username' => 'required',
        'email' => 'required|email|unique:users',
        'password' => 'required|min:6',
    ],
    'servers_edit' => [
        'name' => 'required',
        'banner' => 'image|image_size:468,60',
        'address' => 'required',
        'port' => 'integer',
        'website' => 'url',
        'country' => 'required|exists:countries,id',
        'version' => 'required|exists:tags,id',
        'votifier_port' => 'integer',
    ],
    'vip' => [
        'amount' => 'required|integer',
        'server' => 'required|integer'
    ],
    'vip_edit' => [
        'start' => 'required|date',
        'end' => 'required|date',
        'gross' => 'required',
        'status' => 'required',
        'first_name' => 'required',
        'last_name' => 'required',
        'email' => 'required|email'

    ],
    'tags_edit' => [
        'name' => 'required',
        'slug' => 'required'
    ],
    'tags_create' => [
        'name' => 'required',
        'slug' => 'required|unique:tags'
    ],
    'versions' => [
        'name' => 'required',
        'slug' => 'required',
    ],
    'modes_edit' => [
        'name' => 'required',
        'slug' => 'required'
    ],
    'modes_create' => [
        'name' => 'required',
        'slug' => 'required|unique:modes'
    ],
    'games_edit' => [
        'name' => 'required',
        'slug' => 'required'
    ],
    'games_create' => [
        'name' => 'required',
        'slug' => 'required|unique:games'
    ],
    'news_edit' => [
        'title' => 'required',
        'alias' => 'required',
        'intro' => 'required',
        'text' => 'required',
    ],
    'news_create' => [
        'title' => 'required',
        'alias' => 'required|unique:posts',
        'intro' => 'required',
        'text' => 'required',
    ],
    'purchases' => [
        'server_id' => 'required|exists:servers,id',
        'cost' => 'required|numeric',
        'type' => 'required',
        'days' => 'required|numeric'
    ],
    'pay_types' => [
        'cost' => 'required|numeric',
        'type' => 'required',
        'days' => 'required|numeric',
        'name' => 'required',
        'sale' => 'required|numeric',
    ]
];