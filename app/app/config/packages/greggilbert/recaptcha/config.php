<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| API Keys
	|--------------------------------------------------------------------------
	|
	| Set the public and private API keys as provided by reCAPTCHA.
	|
	*/
	'public_key'	=> '6LdIoSQTAAAAAMGLFw5L1ty0UtavMqTCX4tNdOwL',
	'private_key'	=> '6LdIoSQTAAAAAIXFHEtV5B10w3ZxgGZ3WZtQ1R5v',
	
	/*
	|--------------------------------------------------------------------------
	| Template
	|--------------------------------------------------------------------------
	|
	| Set a template to use if you don't want to use the standard one.
	|
	*/
	'template'		=> 'white'
);