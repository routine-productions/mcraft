<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Password Reminder Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are the default lines which match reasons
	| that are given by the password broker for a password update attempt
	| has failed, such as for an invalid token or invalid new password.
	|
	*/

	"password" => "Пароли должены иметь не менее шести символов и совпадать.",

	"user" => "Мы не можем найти пользователя с таким адресом электронной почты.",

	"token" => "Данный идентификатор сброса пароля является недействительным.",

	"sent" => "Восстановление пароля отправлено!",

);
