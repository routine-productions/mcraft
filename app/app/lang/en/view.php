<?php

return array(
	// Breadcrumbs
	'home_breadcrumb' => 'Главная',
	'change_password_breadcrumb' => 'Изменить пароль',
	'dashboard_breadcrumb' => 'Моя страница',
	'your_account_breadcrumb' => 'Ваша учетная запись',
	'forgot_password_breadcrumb' => 'Забыли пароль?',
	'login_breadcrumb' => 'Логин',
	'register_breadcrumb' => 'Регистрация',
	'add_server_breadcrumb' => 'Добавить сервер',
	'banners_breadcrumb' => 'Баннеры',
	'comments_breadcrumb' => 'Комментарии',
	'your_servers_breadcrumb' => 'Ваши серверы',
	'edit_your_server_breadcrumb' => 'Редактировать сервер',
	'contact_breadcrumb' => 'Контакты',
	'buy_vip_breadcrumb' => 'Купить VIP',
	'confirm_vip_breadcrumb' => 'Подтвердить VIP',

	// Texts

	# change password
	'change_password' => 'Изменить пароль',
	'old_password_label' => 'Старый пароль',
	'old_password_input' => 'Введите старый пароль',
	'new_password_label' => 'Новый пароль',
	'new_password_input' => 'Введите новый пароль',
	'new_password_again_label' => 'Введите новый пароль еще раз',
	'new_password_again_input' => 'Введите новый пароль еще раз',
	'change_password_button' => 'Изменить пароль',

	# user dashboard
	'dashboard' => 'Учетная запись пользователя',
	'servers' => 'Серверы',
	'register_server_button' => 'Зарегистрация сервера',
	'manage_servers_button' => 'Управление серверами',
	'account' => 'Аккаунт',
	'your_profile_button' => 'Ваш профиль',
	'edit_account_button' => 'Редактировать аккаунт',
	'logout_button' => 'Выйти',

	# edit account
	'edit_account' => 'Редактировать аккаунт',
	'current_values' => 'Текущие значения',
	'current_username' => 'Имя пользователя',
	'current_email' => 'Текущая электронная почта',
	'update_profile' => 'Обновить профиль',
	'website_label' => 'Веб-сайт',
	'website_input' => 'Введите URL',
	'facebook_page_label' => 'Страница на Facebook',
	'facebook_page_input' => 'Введите Facebook URL',
	'google_page_label' => 'Страница Google+',
	'google_page_input' => 'Введите Google+ URL',
	'youtube_url_label' => 'Youtube URL',
	'youtube_url_input' => 'Введите Youtube URL',
	'steam_url_label' => 'Поточный URL',
	'steam_url_input' => 'Введите поточный URL',
	'twitter_username_label' => 'Имя пользователя в Твиттере',
	'twitter_username_input' => 'Введите имя пользователя Twitter',
	'skype_username_label' => ' Имя пользователя Skype',
	'skype_username_input' => 'Введите имя пользователя Skype',
	'minecraft_username_label' => 'Имя пользователя Minecraft',
	'minecraft_username_input' => 'Введите имя пользователя Minecraft',
	'update_profile_button' => 'Обновите профиль',

	# forgot password
	'forgot_password' => 'Забыли пароль?',
	'email_label' => 'Email',
	'email_input' => 'Введите ваш адрес электронной почты',
	'recover_button' => 'Отправить новый пароль',

	# login
	'login' => 'Логин',
	'password_label' => 'Пароль',
	'password_input' => 'Введите ваш пароль',
	'login_button' => 'Логин',

	# register
	'register' => 'Регистрация',
	'username_label' => 'Имя пользователя',
	'username_input' => 'Введите имя пользователя',
	'email_input_register' => 'Введите e-mail',
	'confirm_password_label' => 'Подтвердите пароль',
	'captcha_label' => 'Анти-бот Captcha',
	'register_button' => 'Регистрация',

	# admin
	'user_profile' => 'Профиль пользователя',
	'settings' => 'Настройки',
	'logout' => 'Выйти',
	'preview_site' => 'Просмотр сайта',
	'admin_dashboard' => 'Учетная запись',
	'settings' => 'Настройки',
	'configuration' => 'Конфигурация',
	'seo' => 'Seo',
	'ads' => 'Объявления',
	'users' => 'Пользователи',
	'create_user' => 'Создать пользователя',
	'all_users' => 'Все пользователи',
	'all_servers' => 'Все серверы',
	'comments' => 'Комментарии',
	'tools' => 'Инструменты',
	'cron' => 'CRON',

	# all servers
	'servers_information' => 'Инфонрация о сервере',
	'name_thead' => 'Имя',
	'address_thead' => 'Адрес',
	'port_thead' => 'Порт',
	'settings_thead' => '',
	'edit' => 'Редактировать',
	'delete' => 'Удалить',

	# edit server
	'edit_server' => 'Редактировать сервер',
	'basic_informations' => 'Основные сведения',
	'server_name_label' => 'Имя сервера',
	'server_name_help' => 'Имя сервера.',
	'description_label' => 'Описание',
	'description_help' => 'Вы можете использовать следующие коды BB в описании: [i] / [b] / [url] / [img] / [u]',
	'banner_label' => 'Баннер',
	'banner_help' => 'Максимальное разрешение должно составляеть 468х60 пикселей. Максимальный размер файла должен составляеть 1 МБ. Разрешенные типы файла: jpg / png / gif',
	'server_address_label' => 'Адрес сервера / порт',
	'server_address_help' => 'Адрес сервера.',
	'server_port_help' => 'Порт сервера. Вы можете оставить порт пустым, если вы используете запись SRV или порт по умолчанию (25565)',
	'website_help' => 'Сайт сервера.',
	'country_label' => 'Страна',
	'minecraft_version_label' => 'Версия Minecraft',
	'tags_label' => 'Теги',
	'tags_help' => 'Выберите между 1 и :max тегов, чтобы наилучшим образом описать свой сервер.',
	'votifier' => 'Votifier <i><small>(Опционально)</small></i>',
	'use_votifier_label' => 'Используйте votifier',
	'use_votifier_help' => 'верно для да и ложно для нет.',
	'votifier_address_label' => 'Votifier адрес',
	'votifier_address_help' => 'Введите адрес для votifier.',
	'votifier_port_label' => 'Votifier порт',
	'votifier_port_help' => 'Введите порт для votifier.',
	'votifier_key_label' => 'Открытый ключ Votifier',
	'votifier_key_help' => 'Введите адрес для votifier.',
	'additional_information' => 'Additional information <i><small>(Optional)</small></i>',
	'youtube_video_label' => 'Youtube видео',
	'youtube_video_help' => 'Only insert the unique Youtube ID like that this: m_yqOoUMHPg (http://youtu.be/<strong>m_yqOoUMHPg</strong>).',
	'update_server_button' => 'Обновить',
	'vip_label' => 'Vip',

	# configuration
	'configuration' => 'Конфигурация',
	'website_settings' => 'Настройкиастройки веб-сайта',
	'website_name_label' => 'Названиеазвание сайта',
	'website_name_help' => 'Also shown on top of site, in header as text logo.',
	'default_language_label' => 'Язык по умолчанию',
	'default_language_help' => 'Два коротких символа существующего языка. Например: en',
	'facebook_page_label' => 'Страница Facebook',
	'facebook_page_help' => 'Полный URL страницы Facebook, ex: <strong>https://www.facebook.com/MinecraftServers</strong>',
	'twitter_page_label' => 'Страница Twitter',
	'twitter_page_help' => 'Полный URL страницы Твиттера, ex: <strong>https://twitter.com/MinecraftServers</strong>',
	'google_page_help' => 'Полный URL страницы google+, ex: <strong>https://plus.google.com/MinecraftServers</strong>',
	'tracking_code_label' => 'Код отслеживания',
	'tracking_code_help' => 'Вставьте ваш полный код отслеживания. Например: Google Analytics',
	'copyright_label' => 'Авторские права',
	'website_pagination' => 'Нумерация веб-сайта',
	'servers_home_page_label' => 'Домашняя страница',
	'servers_home_page_help' => 'Количество серверов на странице',
	'servers_tag_page_label' => 'Страница тегов',
	'servers_tag_page_help' => 'Количество серверов на странице',
	'servers_manage_page_label' => 'Управление страницой',
	'servers_manage_page_help' => 'Количество серверов на странице',
	'servers_search_page_label' => 'Страница поиска',
	'servers_country_page_label' => 'Страна',
	'servers_version_page_label' => 'Версия',
	'servers_search_page_help' => 'Количество серверов на странице',
	'servers_country_page_help' => 'Количество серверов на странице',
	'servers_version_page_help' => 'Количество серверов на странице',
	'settings_server' => 'Настройки для серверов',
	'allowed_tags_label' => 'Допустимые теги',
	'allowed_tags_help' => 'Количество допустимых тегов для каждого сервера',
	'update_settings_button' => 'Обновить настройки',
	'confirm_account_label' => 'Подтвердите учетную запись',
	'confirm_account_help' => 'Пользователю необходимо подтвердить свой счет по электронной почте?',
	'owner_email_label' => 'Владелец электронной почты',
	'news_label' => 'Новые сообщения',
	'recaptcha' => 'Recaptcha',
	'recaptcha_public_key_label' => 'Открытый ключ',
	'recaptcha_private_key_label' => 'Секретный ключ',

	# seo
	'search_engine' => 'Поисковая оптимизация',
	'meta_title_label' => 'Главная / глобальная метка страницы',
	'meta_title_help' => 'Название в строке браузера / индекса Google.',
	'meta_description_label' => 'Главная / глобальная метка описания',
	'meta_description_help' => 'Описание меток по умолчанию. Они отображаются в поисковых системах.',
	'user_profile_label' => 'Профиль пользователя',
	'user_profile_prefix_help' => 'Приставка / Перед заголовком',
	'user_profile_suffix_help' => 'Суффикс / После заголовка',
	'server_page_label' => 'Страница сервера',
	'server_page_prefix_help' => 'Приставка / Перед заголовком',
	'server_page_suffix_help' => 'Суффикс / После заголовка',
	'tag_page_label' => 'Страница тегов',
	'tag_page_prefix_help' => 'Приставка / Перед заголовком',
	'tag_page_suffix_help' => 'Суффикс / После заголовка',
	'country_page_label' => 'Страна',
	'country_page_prefix_help' => 'Приставка / Перед заголовком',
	'country_page_suffix_help' => 'Суффикс / После заголовка',
	'version_page_label' => 'Версия',
	'version_page_prefix_help' => 'Приставка / Перед заголовком',
	'version_page_suffix_help' => 'Суффикс / После заголовка',

	# cron
	'cron_secret_key' => 'Cron секретный ключ',
	'cron_key_label' => 'Cron ключ',
	'random_key_button' => 'Генерация случайного ключа',
	'cron_key_help' => 'Этот ключ используется для частных Cron URL.',
	'cron_urls' => 'Cron URL',
	'servers_statistics_label' => 'Статистика сервером',
	'servers_statistics_help' => 'Cron используется для сохранения статистики серверов в базе данных.',
	'servers_rank_label' => 'Оценка серверов',
	'servers_rank_help' => 'Cron используется для преобразования точек ранжирования для каждого сервера.',
	'update_key_button' => 'Обновление ключа',
	'help' => 'Помощь',
	'help_text' => '<h6>Как использовать CRON URL:</h6>
		            <p>Вы можете использовать опцию Cron с вашего веб-хостинга, или вы можете использовать сервис online-Cron для доступа к URL.</p>
		            <p>Например: </p>  
		            <p class="text-primary">Servers statistics</p>
		            <p><small><code><strong>wget</strong> :statistics</code></small></p>
		            <p><small><code><strong>curl</strong> :statistics</code></small></p>

		            <p class="text-primary">Оценка серверов</p>
		            <p><small><code><strong>wget</strong> :rank</code></small></p>
		            <p><small><code><strong>curl</strong> :rank</code></small></p>',
	
	# all users
	'users_informations' => 'Информация о пользователе',
	'username_thead' => 'Имя пользователя',
	'email_thead' => 'Email',
	'role_thead' => 'Роль',
	'active_thead' => 'Активный',

	# create user
	'informations' => 'Информация',
	'username_help' => 'Введите имя пользователя.',
	'email_help' => 'Введите адрес электронной почты.',
	'password_help' => 'Введите пароль.',
	'role_label' => 'Роль',
	'role_help' => 'Роль для имени пользователя, администратора или участника.',
	'create_user_button' => 'Создать пользователя',
	'edit_user' => 'Редактировать пользователя',

	# edit user
	'status_label' => 'Состояние',
	'status_help' => 'Состояние пользователя: активный или неактивный',
	'password' => 'Пароль',
	'update_user_button' => 'Обновить пользователя',

	# ads
	'ads_top' => 'Топ объявлений',
	'content_label' => 'Содержание',
	'content_top_help' => 'Код объявления для первого места.',
	'content_bottom_help' => 'Код объявления для нижней точки.',
	'ads_bottom' => 'Кнопка объявления',
	'update_ads' => 'Обновление объявлений',
	'all_comments' => 'Все комментарии',
	'users_comments' => 'Комментарии пользователей',
	'server_thead' => 'Сервер',

	# dashboard
	'view_all_comments' => 'Показать все комментарии',
	'view_all_servers' => 'Показать все серверы',
	'view_all_users' => 'Просмотр всех пользователей',
	'tickets' => 'Поддержка билетов!',
	'view_all_tickets' => 'Показать все билеты',
	
	# footer
	'home_footer' => 'Главная',
	'add_server_footer' => 'Добавить сервер',
	'contact_footer' => 'Контакты',

	# navigation
	'add_server_nav' => 'Добавить сервер',
	'dashboard_nav' => 'Учетная запись',
	'your_dashboard_nav' => 'Ваша учетная запись',
	'manage_servers_nav' => 'Управление серверами',
	'your_profile_nav' => 'Ваш профиль',
	'edit_account_nav' => 'Изменение аккаунта',
	'change_password_nav' => 'Изменить пароль',
	'logout_nav' => 'Выйти',
	'admin_nav' => 'Администратор',
	'login_register_nav' => 'Войти / Регистрация',
	'register_nav' => 'Регистрация',
	'login_nav' => 'Войти',
	'forgot_password_nav' => 'Забыли пароль?',
	'contact_nav' => 'Контакты',

	# add server
	'add_server' => 'Добавить сервер',
	'add_server_info' => '<div class="Warning"><strong>Ваш сервер должен быть online, чтобы быть принятым.</strong> Ваш сервер должен принять протокол TCP соединения на порт сервера (по умолчанию <strong>25565</strong>).</div>
			              <div class="Warning"><strong>Невозможно зарегистрировать сервер, если у вас нет разрешения от его владельца / администратора.</strong> Пользователи, которые не соблюдают это, возможно, закрыли свои счета.</div>',

	'server_address_port_label' => 'Server Address <span class="required">*</span> / Server Port',
	'server_address_input' => 'Введите адрес сервера или IP',
	'server_port_input' => 'Порт сервера',
	'add_server_port_help' => 'Вы можете оставить порт пустым, если вы используете запись SRV или порт по умолчанию (25565)',
	'check_connection_button' => 'Проверьте соединение',
	'connection_status' => 'Состояние соединения',
	'add_server_website_input' => 'Введите URL веб-сайта  или оставить его пустым',
	'votifier_help' => '<small>Используйте ли Вы сервер votifier? Более подробная информация здесь : <a target="_blank" href="http://dev.bukkit.org/server-mods/votifier/">http://dev.bukkit.org/server-mods/votifier/</a></small>',
	'select_tag_input' => 'Выберите тег...',
	'votifier_port_input' => 'Введите Ваш Votifier порт',
	'server_votifier_help' => 'Оставьте адрес пустым, чтобы использовать один и тот же адрес, что и сервер',
	'register_server_button' => 'Зарегистрировать сервер',

	# server banners
	'banners_of' => 'Баннер ":server"',
	'banners' => 'Баннеры',
	'vote_button' => 'Голос',
	'server_details_button' => 'Информация о сервере',
	'comments_button' => 'Комментарии',
	'report_button' => 'Отчет',
	'600_100_tab' => 'Баннер (600x100px)',
	'728_90_tab' => 'Лайтбокс (728x90px)',
	'468_60_tab' => 'Полноразмерный баннер (468x60px)',
	'234_60_tab' => 'Половинный баннер (234x60px)',
	'show_html_button' => 'Показать HTML код',
	'show_bbcode_button' => 'Показать BBCODE',
	'banners_button' => 'Баннеры',

	# server comments
	'comments_of' => 'Комментировать ":server"',
	'add_comment_label' => '<h4>Добавить комментарий</h4>',
	'add_comment_button' => 'Добавить комментарий',
	'add_comment' => "Добавить комментарий",
	'login_to_comment' => 'Вы должны быть зарегистрированы, чтобы отставить комментарий!',

	# edit server
	'edit_your_server' => 'Редактирование сервера',

	# server page
	'about' => 'О нас',
	'address' => 'Адрес',
	'motd' => 'MOTD',
	'server_status' => 'Статус сервера',
	'online' => 'Online',
	'offline' => 'Offline',
	'checked' => 'Проверено ',
	'players' => 'Игроки',
	'location' => 'Расположение',
	'minecraft_version' => 'Version',
	'website' => 'Веб-сайт',
	'visit_website' => 'Посетить домашнюю страницу сервера',
	'registered_by' => 'Зарегистрировано',
	'registered_since' => 'Зарегистрирован',
	'last_update' => 'Последнее обновление',
	'tags' => 'Теги',
	'statistics' => 'Статистика',
	'uptime' => 'Время',
	'votes' => 'Голос(ов)',
	'rank' => 'Ранг',
	'points' => 'Точки',
	'comments_server' => 'Комментарии',
	'share_server' => 'Поделиться этим сервером',
	'preview' => 'Предварительный просмотр',
	'description' => 'Описание',

	# vote server
	'vote_for' => 'Голосовать за ":server"',
	'support_votifier' => 'Этот сервер поддерживает Votifier.',
	'vote_server' => 'Голосуйте за этот сервер',

	# manage servers
	'register_server' => 'Зарегистрировать сервер',
	'manage_your_servers' => 'Управления серверами',
	'rank_thead' => 'Ранг',
	'players_thead' => 'Игроки',
	'uptime_thead' => 'Время',
	'since_thead' => 'Поскольку',
	'options_thead' => 'Опции',
	'edit_your_server_title' => 'Редактировать сервер',
	'delete_your_server_title' => 'Удалить сервер',
	'vote_server_title' => 'Голос',
	'details_server_title' => 'Детали',

	# contact
	'contact' => 'Контакты',
	'subject_label' => 'Тема',
	'subject_input' => 'Введите тему',
	'message_label' => 'Сообщение',
	'send_button' => 'Отправить',

	# home
	'votes_thead' => 'Голоса',
	'premium_server_thead' => 'Премиум-сервер',
	'countries' => 'Страны',
	'versions' => 'Версии',

	# profile
	'username_profile' => ':username Профиль',
	'details' => 'Подробнее',
	'username_tbody' => 'Имя пользователя',
	'registration_date_tbody' => 'Дата регистрации',
	'server_tbody' => 'Сервер',
	'comment_tbody' => 'Комментарии',
	'servers_of' => 'Серверы :username',

	# search
	'search_result' => 'Результаты поиска ":term"',

	# tag
	'tag_servers' => 'Minecraft :tag Servers',

	# country
	'country_servers' => 'Minecraft :country Servers',

	# version
	'version_servers' => 'Minecraft :version Servers',

	// Vip
	'vip_server' => 'Vip-сервер',
	'buy_vip' => 'Купить Vip-членство',
	'number_of_days' => 'Количество дней (1 день = :amount)',
	'select_server_label' => 'Выберите сервер',
	'select_server_option' => 'Выберите сервер',
	'final_price' => 'Final price:',
	'submit_vip_button' => 'Отправить',
	'confirm_vip' => 'Подтвердите членство VIP Подробнее',
	'vip_server_thead' => 'Сервер',
	'vip_days_thead' => 'Дней',
	'vip_end_date_thead' => 'Дата окончания',
	'vip_price_thead' => 'Цена',

	// Admin setting vip
	'vip' => 'VIP',
	'payment_settings' => 'Настройки платежей',
	'business_email_label' => 'Бизнес Email',
	'business_email_help' => 'Добавьте ваш Paypal-email для получения денег',
	'amount_day_label' => 'Количество в день',
	'amount_day_help' => 'Добавить количество в день',
	'currency_label' => 'Валюта',
	'currency_help' => 'Сумма',
	'all_vip_servers' => 'Все серверы VIP',
	'vip_servers' => 'Vip-серверы',
	'vip_start_date_thead' => 'Дата начала',
	'payment_status' => 'Статус платежа',
	'view_button' => 'Посмотреть',
	'edit_vip' => 'Редактировать VIP',
	'informations' => 'Информация',
	'vip_start_label' => 'Дата начала',
	'vip_end_label' => 'Дата окончания',
	'amount_paid' => 'Оплаченная сумма',
	'payer_first_name_label' => 'Имя игрока',
	'payer_last_name_label' => 'Фамилия игрока',
	'payer_email' => 'Email игрока',
	'update_vip_button' => 'Обновить VIP',
	'view_vip' => 'Посмотреть VIP',
	'txn_id_td' => 'TXN Id',
	'server_td' => 'Сервер',
	'date_start_td' => 'Дата начала',
	'date_end_td' => 'Дата окончания',
	'amount_paid_td' => 'Оплаченная сумма',
	'payment_status_td' => 'Статус платежа',
	'payer_first_name_td' => 'Имя игрока',
	'payer_last_name_td' => 'Фамилия игрока',
	'payer_email_td' => 'Email игрока',

	// Thank you
	'thank_you' => 'Спасибо вам за ваш платеж',
	'paypal_response' => 'Ожидание ответа Paypal. Это может занять несколько минут',
	'payment_canceled' => 'Платеж был отменен! <a href=":url">Вернуться на VIP страницу',

	// View payment
	'payment_details' => 'Детали оплаты',
	'payer_name_td' => 'Имя плательщик',
	'amount_sent_td' => 'Отправленная сумма',
	'amount_expected' => 'Платеж ожидается',

	// Admin vip
	'vip_admin' => 'Vip',

);