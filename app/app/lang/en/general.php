<?php

return array(
	// Page titles

	# home
	'search_title' => 'Поиск серверов - :title',
	'contact_title' => 'Контакт - :title',
	'vip_title' => 'VIP-членство - :title',
	'confirm_vip_title' => 'Подтвердить VIP-членство  - :title',
	'thank_you_title' => 'Спасибо! - :title',
	'view_payment_title' => 'Просмотр оплаты - :title',

	# account
	'register_title' => 'Регистрация - :title',
	'login_title' => 'Логин - :title',
	'change_password_title' => 'Изменить пароль - :title',
	'forgot_password_title' => 'Забыли пароль? - :title',
	'account_dashboard_title' => 'Моя страния - :title',
	'edit_account_title' => 'Редактировать учетную запись - :title',

	# admin
	'admin_dashboard_title' => 'Моя страница',
	'settings_configuration_title' => 'Настройки | Конфигурация',
	'settings_seo_title' => 'Настройки | Seo',
	'ads_title' => 'Объявления',
	'users_create_title' => 'Пользователи | Создать пользователя',
	'users_all_title' => 'Пользователи | Все пользователи',
	'edit_user_title' => 'Пользователи | Редактировать пользователя',
	'servers_all_title' => 'Серверы | Все серверы',
	'edit_server_title' => 'Серверы | Редактировать сервер',
	'comments_title' => 'Комментарии',
	'tools_cron_title' => 'Инструменты | Cron',
	'settings_vip_title' => 'Настройки | VIP',
	'admin_vip_title' => 'Vip',
	'edit_vip_title' => 'Редактировать Vip',
	'view_vip_title' => 'Просмотр Vip',

	# server
	'add_server_title' => 'Добавление сервера Minecraft - :title',
	'server_banners_title' => 'Баннеры :server',
	'server_comments_title' => 'Комментировать :server',
	'server_vote_title' => 'Голосовать за :server',
	'manage_servers_title' => 'Управление серверами',
	'edit_server_title' => 'Редактировать сервер Minecraft',

	// Global messages

	# account
	'message_sent' => 'Сообщение успешно отправлено.',
	'activate_account' => 'Ваш аккаунт был создан. На указанный при регистрации адрес электронной почты была отправлена ссылка для её активации.',
	'login_with_account' => 'Ваша учетная запись была успешно создана и готова к использованию.',
	'account_wrong' => 'E-mail / пароль неверный, или учетная запись не активирована.',
	'account_problem' => 'There was a problem signing you in.',
	'password_changed' => 'Ваш пароль был изменен.',
	'old_password_incorrect' => 'Ваш активный пароль неверен.',
	'change_password_problem' => 'Ваш пароль не может быть изменен.',
	'account_activated' => 'Ваша учетная запись была активирована! Вы можете войти в систему прямо сейчас!',
	'account_activate_problem' => 'Мы не смогли активировать Ваш аккаунт. Попробуйте позже.',
	'forgot_password' => 'Мы отправили новый пароль на Вашу электронную почту.',
	'forgot_password_problem' => 'Не удалось запросить новый пароль.',
	'account_recover' => 'Ваш аккаунт восстановлен. Вы можете войти в систему с новыми данными.',
	'account_recover_problem' => 'Не удалось восстановить Ваш аккаунт.',
	'edit_account' => 'Ваш профиль обновлен.',

	# admin
	'settings_configuration' => 'Параметры конфигурации обновлены.',
	'settings_seo' => 'Конфигурация SEO обновляется.',
	'ads' => 'Настройки объявлений обновляються.',
	'users_create' => 'Пользователь создан.',
	'edit_user' => 'Данные пользователя обновлены!',
	'edit_server' => 'Ваш сервер обновлен.',
	'record_deleted' => 'Запись удалена.',
	'tools_cron' => 'Ключ Cron обновлен.',
	'settings_vip' => 'Настройки VIP обновлены.',
	'edit_vip' => 'VIP отредактирован.',

	# server
	'server_added' => 'Ваш сервер зарегистрирован. Спасибо!',
	'comment_added' => 'Ваш комментарий добавлен.',
	'server_voted' => 'Ваш голос добавлен.',
	'server_already_voted' => 'Сегодня Вы уже голосовали.',
	'edit_server' => 'Ваш сервер обновлен.',

	// Mail
	'activate_account_subject' => 'Активируйте Вашу учетную запись',

	// Vip
	'vip'
	
);
