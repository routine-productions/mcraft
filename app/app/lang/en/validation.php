<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines contain the default error messages used by
	| the validator class. Some of these rules have multiple versions such
	| as the size rules. Feel free to tweak each of these messages here.
	|
	*/

	"accepted"             => ":attribute должны быть приняты.",
	"active_url"           => ":attribute не допустимый URL.",
	"after"                => ":attribute должна быть дата, после :date.",
	"alpha"                => ":attribute может содержать только буквы.",
	"alpha_dash"           => ":attribute может содержать только буквы, цифры и дефис.",
	"alpha_num"            => ":attribute может содержать только буквы и цифры.",
	"array"                => ":attribute должен быть массивом.",
	"before"               => ":attribute должна быть дата перед тем :date.",
	"between"              => array(
		"numeric" => ":attribute должен быть между :min и :max.",
		"file"    => ":attribute должен быть между :min и :max килобайт.",
		"string"  => ":attribute должен быть между :min и :max персонажей.",
		"array"   => ":attribute должен иметь между :min и :max предметов.",
	),
	"confirmed"            => ":attribute подтверждение не совпадает.",
	"date"                 => ":attribute не является действительной датой.",
	"date_format"          => ":attribute не соответствует формату :format.",
	"different"            => ":attribute и :other должны быть разными.",
	"digits"               => ":attribute должно быть :digits цифры.",
	"digits_between"       => ":attribute должен быть между :min и :max цифры.",
	"email"                => ":attribute E-Mail должен быть активным.",
	"exists"               => "selected :attribute не активный.",
	"image"                => ":attribute должно быть изображение.",
	"in"                   => "Выбранный :attribute должен быть активным.",
	"integer"              => ":attribute должно быть целым числом.",
	"ip"                   => ":attribute должен быть действительный IP-адрес.",
	"max"                  => array(
		"numeric" => ":attribute не может быть больше, чем :max.",
		"file"    => ":attribute не может быть больше, чем :max килобайт.",
		"string"  => ":attribute не может быть больше, чем :max персонажей.",
		"array"   => ":attribute не может иметь более :max предметов.",
	),
	"mimes"                => ":attribute должен быть файл типа: :values.",
	"min"                  => array(
		"numeric" => ":attribute должен быть не менее :min.",
		"file"    => ":attribute должен быть не менее :min килобайт.",
		"string"  => ":attribute должен быть не менее :min персонажей.",
		"array"   => ":attribute должны иметь не менее :min предметов.",
	),
	"not_in"               => "Выбранный :attribute не активный.",
	"numeric"              => ":attribute должен быть числом.",
	"regex"                => ":attribute неверного формата.",
	"required"             => "Поле :attribute, обязательное для заполнения.",
	"required_if"          => "Поле :attribute обязательно для заполнения, когда :other является :value.",
	"required_with"        => "Поле :attribute обязательно для заполнения, когда :values настоящее.",
	"required_with_all"    => "Поле :attribute обязательно для заполнения, когда :values настоящее.",
	"required_without"     => "Поле :attribute обязательно для заполнения, когда :values не настоящее.",
	"required_without_all" => "Поле :attribute обязательно для заполнения, если ни один из :values присутствуют.",
	"same"                 => "Поле :attribute и :other должены соответствовать.",
	"size"                 => array(
		"numeric" => ":attribute должен быть :size.",
		"file"    => ":attribute должен быть :size килобайт.",
		"string"  => ":attribute должен быть :size персонажей.",
		"array"   => ":attribute должен содержать :size предметов.",
	),
	"unique"               => ":attribute уже используется.",
	"url"                  => ":attribute неверного формата.",

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| Here you may specify custom validation messages for attributes using the
	| convention "attribute.rule" to name the lines. This makes it quick to
	| specify a specific custom language line for a given attribute rule.
	|
	*/

	'custom' => array(
		'recaptcha_response_field' => array(
			'recaptcha' => 'Captcha введена не верно.',
			'required'  => 'Требуется captcha.',
		),
		'username' => array(
			'required' => 'Требуется имя пользователя.',
			'unique'   => 'Имя пользователя уже использовано.'
		),
		'email' => array(
			'required' => 'Требуется адрес электронной почты.',
			'email'    => 'Адрес электронной почты недействительный.',
			'unique'   => 'Адрес электронной почты, уже использован.'
		),
		'password' => array(
			'required' => 'Требуется пароль.',
			'min'      => 'Пароль должен быть не менее :min.',
		),
		'confirm' => array(
			'required' => 'Требуется подтверждение пароля.',
			'min'      => 'Пароль должен быть не менее :min.',
			'same'     => 'Пароль и подтверждение пароля должны совпадать.'
		),
		'old_password' => array(
			'required' => 'Требуется старый пароль.'
		),
		'password_again' => array(
			'required' => 'Требуется пароль.',
			'same'     => 'Пароль и подтверждение пароля должны совпадать.'
		),
		'name' => array(
			'required' => 'Требуется имя.'
		),
		'banners' => array(
			'image'      => 'Баннер должен быть изображением.',
			'image_size' => 'Баннер должен быть :width ширины и :height высоты.'
		),
		'address' => array(
			'required' => 'Требуется адрес.',
			'unique'   => 'Адрес уже занят.'
		),
		'port' => array(
			'integer' => 'Порт должен быть целым числом.'
		),
		'website' => array(
			'url' => 'URL формат веб-сайта - недопустим.'
		),
		'country' => array(
			'required' => 'Требуется страна.',
			'exists'   => 'Страна недопустима.'
		),
		'version' => array(
			'required' => 'Требуется версия.',
			'exists'   => 'Версия недопустима.'
		),
		'votifier_port' => array(
			'integer' => 'Порт votifier недопустим.'
		),
		'comment' => array(
			'required' => 'Требуется комментарий.'
		),
		'subject' => array(
			'required' => 'Требуется тема.'
		),
		'message' => array(
			'required' => 'Требуется сообщения.'
		),
		'facebook' => array(
			'url' => 'Ссылка URL Facebook - недопустима.'
		),
		'twitter' => array(
			'url' => 'Ссылка URL Twitter - недопустима.'
		),
		'google' => array(
			'url' => 'Ссылка URL Google+ - недопустима.'
		),
		'youtube' => array(
			'url' => 'Ссылка URL Youtube - недопустима.'
		),
		'steam' => array(
			'url' => 'Ссылка URL Steam - недопустима.'
		),
		'language' => array(
			'max'      => 'Язык не может быть больше, чем :max.',
			'required' => 'Поле обязательно для заполнения.'
		),
		'sph' => array(
			'required' => 'Поле обязательно для заполнения.',
			'integer'  => 'Поле должно быть целым числом.'
		),
		'spt' => array(
			'required' => 'Поле обязательно для заполнения.',
			'integer'  => 'Поле должно быть целым числом.'
		),
		'spm' => array(
			'required' => 'Поле обязательно для заполнения.',
			'integer'  => 'Поле должно быть целым числом.'
		),
		'sps' => array(
			'required' => 'Поле обязательно для заполнения.',
			'integer' => 'Поле должно быть целым числом.'
		),
		'spc' => array(
			'required' => 'Поле обязательно для заполнения.',
			'integer'  => 'Поле должно быть целым числом.'
		),
		'tags' => array(
			'required' => 'Требуется теги.',
			'integer'  => 'Теги должны быть целыми числами.'
		),
		'owner_email' => array(
			'required' => 'Требуется адрес электронной почты',
			'email'    => 'Поле должно быть действительным адресом электронной почты.'
		),
		'amount' => array(
			'required' => 'Пожалуйста, добавьте количество дней',
			'integer'  => 'Поле должно быть целым числом.'
		),
		'server' => array(
			'required' => 'Требуется сервер',
			'integer'  => 'Поле сервера должно быть целым числом.'
		)
	),

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Attributes
	|--------------------------------------------------------------------------
	|
	| The following language lines are used to swap attribute place-holders
	| with something more reader friendly such as E-Mail Address instead
	| of "email". This simply helps us make messages a little cleaner.
	|
	*/

	'attributes' => array(),

	'image_size'         => 'The :attribute должен быть :width в ширину и :height в высоту.',

	'between'            => 'между :size1 и :size2 пикселей',
	'lessthan'           => 'меньше, чем :size пикселей',
	'lessthanorequal'    => 'меньше или равно :size пикселей',
	'greaterthan'        => 'больше чем :size пикселей',
	'greaterthanorequal' => 'больше или равно :size пикселей',
	'equal'              => ':size пикселей',
	'anysize'            => 'любого размера',

	'image_aspect'       => 'The :attribute соотношение сторон должен быть :aspect.',

);
