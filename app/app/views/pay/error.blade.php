@extends('layouts.base')

@section('content')
    <div class="Card-Item">
        <article>
            <div class="Card-Wrapper">
                <div class="Card-Head">
                    <div class="Card-Path">
                        <h3 class="Card-Title">Ошибка!</h3>
                    </div>
                </div>

                <div class="Card-Content">
                    <div class="Card-Path">
                       <p>Платеж не был совершен. Попробуйте повторить позже.</p>
                    </div>
                </div>
            </div>
        </article>
    </div>
@stop
