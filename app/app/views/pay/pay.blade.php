@extends('layouts.base')

@section('content')
    <div class="Card-Item">
        <article>
            <div class="Card-Wrapper">
                <div class="Card-Head">
                    <div class="Card-Path">
                        <h3 class="Card-Title">Оплата</h3>
                    </div>
                </div>

                <div class="Card-Content">
                    <div class="Card-Path">
                        @if($purchase && $purchase->paid == 0)
                            <div class="Payments Banner">
                                <p>Id вашего сервера: <strong>{{$purchase->server_id}}</strong></p>
                                <p>Услуга: <strong>
                                        @if($purchase->type == 'diamond')
                                            Покупка кристалов ({{$purchase->diamonds}} шт.)
                                        @elseif($purchase->type == 'banner')
                                            Размещение банера
                                        @elseif($purchase->type == 'color')
                                            Выделение сервера цветом
                                        @endif
                                    </strong>
                                </p>

                                <p>Стоимость: <strong>{{$purchase->cost}} р.</strong></p>

                                <div class="Web-Money">
                                    <form id=pay name=pay method="POST"
                                          action="https://merchant.webmoney.ru/lmi/payment.asp">
                                        <input type="hidden" name="LMI_PAYMENT_AMOUNT" value="{{$purchase->cost}}">
                                        <input type="hidden" name="LMI_PAYMENT_DESC_BASE64"
                                               value="<?= base64_encode("Оплата за сервер minecraft-server.ru №" .
                                                       $purchase->server_id . ' (' . Config::get('app.pay_types.' . $purchase->type) . ')')?> ">
                                        <input type="hidden" name="LMI_PAYMENT_NO" value="{{$purchase->id}}">
                                        <input type="hidden" name="LMI_PAYEE_PURSE"
                                               value="{{Config::get('app.webmoney_wallet')}}">

                                        <br>

                                        <button class="Btn Primary" type="submit" value="submit">Оплатить с помощь
                                            Webmoney
                                        </button>

                                    </form>
                                </div>

                                <div style="margin-top: 20px">
                                    <form action="https://unitpay.ru/pay/67711-dfff5">
                                        <input type="hidden" name="account" value="{{$purchase->id}}">
                                        <input type="hidden" name="sum" value="{{$purchase->cost}}">
                                        <input type="hidden" name="desc"
                                               value="Оплата за сервер minecraft-server.ru №{{$purchase->server_id}} ({{Config::get('app.pay_types.' . $purchase->type)}})">

                                        <button class="Btn Primary" type="submit" value="submit">Оплатить с помощь
                                            UNITPAY
                                        </button>
                                    </form>
                                </div>

                            </div>
                        @else
                            <p>Операция не найдена или платеж уже выполнен!</p>
                        @endif
                    </div>
                </div>
            </div>
        </article>
    </div>
@stop
