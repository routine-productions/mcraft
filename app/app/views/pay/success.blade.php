@extends('layouts.base')

@section('content')
    <div class="Card-Item">
        <article>
            <div class="Card-Wrapper">
                <div class="Card-Head">
                    <div class="Card-Path">
                        <h3 class="Card-Title">Платеж успешно проведен!</h3>
                    </div>
                </div>

                <div class="Card-Content">
                    <div class="Card-Path">
                        <p>В ближайшее время Ваша услуга активируется. В противном случае напишите администратору.</p>
                    </div>
                </div>
            </div>
        </article>
    </div>
@stop
