@extends('layouts.base')

@section('content')
    <section class="Card">
        <header class="Card-Heading">
            <h2>Управление серверами</h2>
        </header>

        <div class="Card-Content">
                <table class="Servers-List">

                <thead>
                <tr>
                    <th>Ранг</th>
                    <th>Сервер</th>
                    <th>Версия</th>
                    <th>Игроков</th>
                    <th>В сети</th>
                    <th>Добавлен</th>
                    <th>Статус</th>
                    <th>Действия</th>
                </tr>
                </thead>

                <tbody>
                @foreach ($servers as $server)
                    <?php
                    $uptime = substr(($server->online_time / $server->verified_time) * 100, 0, 5);
                    $uptime = strlen($uptime) > 1 ? $uptime . '%' : 'N/A';
                    ?>

                    <tr>
                        <td>#{{ $server->rank }}</td>
                        <td>
                            <a href="{{ URL::route('server-page', $server->id) }}"><strong>{{ $server['name'] }}</strong></a>
                            <span>{{ $server['address'] }}:{{ $server['port'] }}</span>
                        </td>
                        <td>{{ $server['version']['name'] }}</td>
                        <td>
                            @if ($server->statistics['max_players'])
                                {{ $server->statistics['players'] }}/{{ $server->statistics['max_players'] }}
                            @else
                                N/A
                            @endif
                        </td>
                        <td>{{ $uptime }}</td>
                        <td>{{ date('M dS, Y', strtotime($server->created_at)) }}</td>
                        <td>{{ $server['status'] }}</td>

                        <td>
                            <a class="Btn Small" href="{{ URL::route('server-edit', $server->id) }}">Редактировать</a>
                            <a class="Btn Small Remove"
                               href="{{ URL::route('server-delete', $server->id) }}">Удалить</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            {{ $servers->links() }}
        </div>
    </section>
@stop


@section('old')
    <div class="container reduce-top">
        <div class="page-header">
            <ol class="breadcrumb">
                <li><a href="{{ URL::route('dashboard') }}">{{ trans('view.dashboard_breadcrumb') }}</a></li>
                <li class="active">{{ trans('view.your_servers_breadcrumb') }}</li>
            </ol>
            <h1>{{ trans('view.manage_your_servers') }}</h1>
        </div>
        <p class="server-action">
            <a href="{{ URL::route('server-add') }}" class="btn btn-primary"><i
                        class="glyphicon glyphicon-plus-sign"></i> {{ trans('view.register_server') }}</a>
        </p>

        <table class="table table-bordered home-servers">
            <thead>
            <tr>
                <th width="7%"><i class="glyphicon glyphicon-list"></i> {{ trans('view.rank_thead') }}</th>
                <th width="53%"><i class="glyphicon glyphicon-hdd"></i> {{ trans('view.server_thead') }}</th>
                <th width="10%"><i class="glyphicon glyphicon-user"></i> {{ trans('view.players_thead') }}</th>
                <th width="10%"><i class="glyphicon glyphicon-time"></i> {{ trans('view.uptime_thead') }}</th>
                <th width="10%"><i class="glyphicon glyphicon-dashboard"></i> {{ trans('view.since_thead') }}</th>
                <th width="10%"><i class="glyphicon glyphicon-cog"></i> {{ trans('view.options_thead') }}</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($servers as $server)
                <tr>
                    <td align="center">#{{ $server->rank }} @if ($vip->checkVip($server->id)) <br><img
                                src="{{ asset('assets/images/star.png') }}" alt="{{ trans('view.vip_server') }}"
                                title="{{ trans('view.vip_server') }}"> @endif</td>
                    <td>
                        <h3 class="server-name shadow-box"><a
                                    href="{{ URL::route('server-page', $server->id) }}">{{ $server->name }} @if ($vip->checkVip($server->id)) </a> @endif
                        </h3>
                        <div class="server-details shadow-box"><span
                                    class="label label-{{ ($server['status'] == 'online') ? 'success' : 'danger' }}">{{ strtoupper($server['status']) }}</span>&nbsp;<a
                                    href="{{ URL::route('version', $server['version']['name']) }}"
                                    class="label label-info">{{ $server['version']['name'] }}</a>&nbsp;<a
                                    href="{{ URL::route('country', strtolower($server->country['code'])) }}">
                                <div class="flag flag-{{ strtolower($server->country['code']) }} left"
                                     style="margin-top: 5px; margin-right: 5px;"></div>
                            </a> &nbsp;<strong>{{ $server['address'] }}:{{ $server['port'] }}</strong></div>
                    </td>
                    <td align="center"><span
                                class="label label-success label-padding">@if ($server->statistics['max_players']) {{ $server->statistics['players'] }}
                            / {{ $server->statistics['max_players'] }} @else N/A @endif</span></td>
                    <?php
                    // Calculate fucking uptime
                    $uptime = ($server->online_time / $server->verified_time) * 100;
                    $uptime = substr($uptime, 0, 5);
                    $uptime = strlen($uptime) > 1 ? $uptime . '%' : 'N/A';
                    ?>
                    <td align="center"><span
                                class="label label-{{ $uptime == 'N/A' ? 'danger' : 'success' }} label-padding">{{ $uptime }}</span>
                    </td>
                    <td align="center">{{ date('M dS, Y', strtotime($server->created_at)) }}</td>
                    <td align="center"><a href="{{ URL::route('server-edit', $server->id) }}" data-toggle="tooltip"
                                          data-placement="top" title="{{ trans('view.edit_your_server_title') }}"><span
                                    class="glyphicon glyphicon-edit size-up"></span></a> <a
                                href="{{ URL::route('server-delete', $server->id) }}"
                                title="{{ trans('view.delete_your_server_title') }}" data-toggle="tooltip"
                                data-placement="top"><span class="glyphicon glyphicon-remove size-up"></span></a> <a
                                href="{{ URL::route('server-vote', $server->id) }}"
                                title="{{ trans('view.vote_server_title') }}" data-toggle="tooltip"
                                data-placement="top"><span class="glyphicon glyphicon-thumbs-up size-up"></span></a> <a
                                href="{{ URL::route('server-page', $server->id) }}"
                                title="{{ trans('view.details_server_title') }}" data-toggle="tooltip"
                                data-placement="top"><span class="glyphicon glyphicon-eye-open size-up"></span></a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $servers->links() }}
    </div>
@stop