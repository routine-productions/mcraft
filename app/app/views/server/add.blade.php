@extends('layouts.base')

@section('head')
    @parent
    <link rel="stylesheet" href="{{ asset('assets/css/selectize.bootstrap3.css') }}">
@stop

@section('content')

    <section class="Card">
        <header class="Card-Heading">
            <h2>{{ trans('view.add_server') }}</h2>
        </header>
        <div class="Card-Content">
            <!-- Default Messages -->
            {{ trans('view.add_server_info') }}

            <form id="form-register-server" class="Form" role="form" action="{{ URL::route('server-add-post') }}" method="post"
                  class="bottom-space"
                  enctype="multipart/form-data">


                <!-- Title -->
                <p class="Row @if ($errors->has('name')) has-error @endif">
                    <label for="name">{{ trans('view.server_name_label') }} <span class="required">*</span></label>
                    <input type="text" name="name" class="form-control" id="name"
                           placeholder="Имя сервера" {{ (Input::old('name')) ? ' value="' . Input::old('name') . '"' : '' }}>
                    @if ($errors->has('name'))
                        <span class="Error">{{ $errors->first('name') }} </span>
                    @endif
                </p>


                {{-- Meta --}}
                <hr>
                <div class="Row">
                    <label>Meta: Title</label>
                    <input class="form-control" name="meta_title" value="{{ Input::old('meta_title') }}">
                </div>
                <div class="Row">
                    <label>Meta: Description</label>
                    <textarea class="form-control"
                              name="meta_description">{{ Input::old('meta_description') }}</textarea>
                </div>
                <div class="Row">
                    <label>Meta: Keywords</label>
                    <textarea class="form-control"
                              name="meta_keywords">{{ Input::old('meta_keywords') }}</textarea>
                </div>
                <div class="Row">
                    <label>Seo Text</label>
                    <textarea class="form-control" name="seo_text">{{ Input::old('seo_text') }}</textarea>
                </div>
                <hr>
            {{-- Meta --}}



            <!-- Version -->
                <p class="Row form-group @if ($errors->has('version')) has-error @endif">
                    <label for="version">{{ trans('view.minecraft_version_label') }}</label>
                    <select name="version" id="version" class="form-control">
                        @foreach ($versions as $version)
                            <option value="{{ $version->id }}">{{ $version->name }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('version'))
                        <span class="Error">{{ $errors->first('version') }} </span>
                    @endif
                </p>

                <!-- Tags -->
                <p class="Row">
                    <label for="select-tags">Теги</label>
                    <select id="select-tags" name="tags[]" multiple placeholder="Выберите тег...">
                        @foreach ($tags as $tag)
                            <option value="{{ $tag->id }}">{{ $tag->name }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('tags'))
                        <span class="Error">{{ $errors->first('tags') }} </span>
                    @endif
                </p>

                <!-- Games -->
                <p class="Row">
                    <label for="select-games">Мини игры</label>
                    <select id="select-games" name="games[]" multiple placeholder="Выберите игру...">
                        @foreach ($games as $game)
                            <option value="{{ $game->id }}">{{ $game->name }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('games'))
                        <span class="Error">{{ $errors->first('games') }} </span>
                    @endif
                </p>

                <!-- Modes -->
                <p class="Row">
                    <label for="select-modes">Моды</label>
                    <select id="select-modes" name="modes[]" multiple placeholder="Выберите моды...">
                        @foreach ($modes as $mode)
                            <option value="{{ $mode->id }}">{{ $mode->name }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('modes'))
                        <span class="Error">{{ $errors->first('modes') }} </span>
                    @endif
                </p>


                <!-- Short Description -->
                <p class="Row">
                    <label for="short_description">Короткое описание</label>
                    <textarea maxlength="200" name="short_description" id="short_description" rows="2"
                              class="form-control">{{ (Input::old('short_description')) ? Input::old('short_description') : '' }}</textarea>
                    <span class="Control-Description">{{ trans('view.description_help') }}</span>
                    @if ($errors->has('short_description'))
                        <span class="Error">{{ $errors->first('short_description') }} </span>
                    @endif
                </p>

                <!-- Full Description -->
                <p class="Row">
                    <label for="description">{{ trans('view.description_label') }}</label>
                    <textarea maxlength="4500" name="description" id="description" rows="6"
                              class="form-control">{{ (Input::old('description')) ? Input::old('description') : '' }}</textarea>
                    <span class="Control-Description">{{ trans('view.description_help') }}</span>
                    @if ($errors->has('description'))
                        <span class="Error">{{ $errors->first('description') }} </span>
                    @endif
                </p>

                <!-- Banner -->
                <p class="Row">
                    <label for="banner">{{ trans('view.banner_label') }}</label>
                    <input type="file" name="banner" id="banner">
                    <span class="Control-Description">{{ trans('view.banner_help') }}</span>
                    @if ($errors->has('banner'))
                        <span class="Error">{{ $errors->first('banner') }} </span>
                    @endif
                </p>

                <!-- Server -->
                <!--Здесь были поля Server address Server port-->

                <!--p class="Row Right">
                    <a class="Btn Primary" id="check">{{ trans('view.check_connection_button') }}</a>
                </p-->


                <!-- Website -->
                <p class="Row @if ($errors->has('website')) has-error @endif">
                    <label for="name">{{ trans('view.website_label') }}</label>
                    <input type="text" name="website" class="form-control" id="website"
                           placeholder="{{ trans('view.add_server_website_input') }}"{{ (Input::old('website')) ? ' value="' . Input::old('website') . '"' : '' }}>
                    @if ($errors->has('website'))
                        <span class="Error">{{ $errors->first('website') }} </span>
                    @endif
                </p>

                <!-- Video -->
                <p class="Row">
                    <label for="video">{{ trans('view.youtube_video_label') }}</label>
                    <input type="text" name="youtube" class="form-control"
                           placeholder="Вставьте ссылку на YouTube видео"
                           id="video" {{ (Input::old('youtube')) ? ' value="' . Input::old('youtube') . '"' : '' }}>
                    @if ($errors->has('youtube'))
                        <span class="Error">{{ $errors->first('youtube  ') }} </span>
                    @endif
                </p>

                <hr>
                <!-- Votifer -->
                <h2>{{ trans('view.votifier') }}</h2>
                <div class="Row">
                    <div class="Error">
                        {{ trans('view.votifier_help') }}
                    </div>
                </div>
                <div class="Row">
                    <input type="checkbox" checked="checked" name="votifier_active" style="visibility: hidden;">
                </div>


                <div class="Row">
                    <label for="votifier_address">{{ trans('view.votifier_address_label') }}</label>
                    <input type="text" value="" name="votifier_address"
                           {{ (Input::old('votifier_address')) ? ' value="' . Input::old('votifier_address') . '"' : '' }} id="votifier_address"
                           class="form-control">

                    @if ($errors->has('votifier_address'))
                        <span class="Error">{{ $errors->first('votifier_address') }}</span>
                    @endif

                    <p class="Error"><i class="fa fa-info-circle"></i> {{ trans('view.server_votifier_help') }}</p>
                </div>


                <div class="Row" class="form-group @if ($errors->has('votifier_port')) has-error @endif">
                    <label for="votifier_port">{{ trans('view.votifier_port_label') }}</label>
                    <input type="text" value="" placeholder="{{ trans('view.votifier_port_input') }}"
                           name="votifier_port" id="votifier_port" class="form-control">
                    @if ($errors->has('votifier_port'))
                        <span class="Error">{{ $errors->first('votifier_port') }}</span>
                    @endif
                </div>

                <!--Server Address * / Server Port-->
                <p class="Row Double">
                    <label for="address">{{ trans('view.server_address_port_label') }}</label>

                    <input type="text" placeholder="{{ trans('view.server_address_input') }}"
                           {{ (Input::old('address')) ? ' value="' . Input::old('address') . '"' : '' }} name="address"
                           id="address" class="form-control">

                    <input type="text" placeholder="{{ trans('view.server_port_input') }}" name="port"
                           id="port"
                           class="form-control" {{ (Input::old('port')) ? ' value="' . Input::old('port') . '"' : '' }}>

                    @if ($errors->has('address'))
                        <span class="Error">{{ $errors->first('address') }} </span>
                    @endif
                    @if ($errors->has('port'))
                        <span class="Error">{{ $errors->first('port') }} </span>
                    @endif

                    <span class="Control-Description" id="connection_notice"></span>
                </p>

                <div class="Row form-group @if ($errors->has('votifier_key')) has-error @endif">
                    <label for="votifier_key">{{ trans('view.votifier_key_label') }}</label>

                    <textarea name="votifier_key" id="votifier_key" rows="7"
                              class="form-control">{{ (Input::old('votifier_key')) ? Input::old('votifier_key') : '' }}</textarea>
                    @if ($errors->has('votifier_key'))
                        <span class="help-block">{{ $errors->first('votifier_key') }}</span>
                    @endif
                </div>


                <!-- Button -->

                <p class="Row ">
                    <button class="Btn Primary"
                            id="register-server">{{ trans('view.register_server_button') }}</button>
                    {{ Form::token() }}
                </p>
            </form>
        </div>
    </section>
@stop

@section('js')
    <script src="{{ asset('assets/js/selectize.js') }}"></script>
    <script>
        $('#select-tags').selectize();
        $('#select-games').selectize();
        $('#select-modes').selectize();

        $('#register-server').prop('disabled', true);
    </script>
@stop