@extends('layouts.base')

@section('content')
        <div class="container reduce-top">
            <div class="page-header">
                <ol class="breadcrumb">
                    <li><a href="{{ URL::route('home') }}">{{ trans('view.home_breadcrumb') }}</a></li>
                    <li><a href="{{ URL::route('server-page', $server->id) }}">{{{ $server->name }}}</a></li>
                    <li class="active">{{ trans('view.comments_breadcrumb') }}</li>
                </ol>
                <h1>{{{ trans('view.comments_of', array('server' => $server->name)) }}}</h1>
            </div>

            <p class="server-action">
                <a href="{{ URL::route('server-vote', $server->id) }}" class="btn btn-success"><i class="glyphicon glyphicon-thumbs-up"></i> {{ trans('view.vote_button') }}</a>
                <a href="{{ URL::route('server-page', $server->id) }}" class="btn btn-primary"><i class="glyphicon glyphicon-picture"></i> {{ trans('view.server_details_button') }}</a>
                <a href="{{ URL::route('server-banners', $server->id) }}" class="btn btn-primary"><i class="glyphicon glyphicon-comment"></i> {{ trans('view.banners_button') }}</a>
            </p>

            @foreach ($comments as $comment)
                <div class="well flat">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tbody>
                            <tr>
                                <td width="220" valign="top">
                                    <p>
                                        <strong><i class="fa fa-user"></i> <a href="{{ URL::route('profile', $comment->user['username']) }}">{{ $comment->user['username'] }}</a></strong><br>
                                        <small><i class="fa fa-clock-o"></i> {{ date('M dS, Y', strtotime($comment->created_at)) }}</small>
                                    </p>
                                </td>
                                <td valign="top">
                                    {{{ $comment->body }}}
                                </td>
                                <td width="20" valign="top">&nbsp;</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            @endforeach

            @if (Auth::check())
                <form role="form" action="{{ URL::route('server-comments-post', $id) }}" method="post">
                    <div class="form-group @if ($errors->has('comment')) has-error @endif">
                        <label for="comment">{{ trans('view.add_comment_label') }}</label>
                        <textarea name="comment" id="comment" rows="12" class="form-control">{{ (Input::old('comment')) ? Input::old('comment') : '' }}</textarea>
                        @if ($errors->has('comment'))
                            <span class="help-block">{{ $errors->first('comment') }} </span>
                        @endif
                    </div>

                    <button type="submit" class="btn btn-default">{{ trans('view.add_comment_button') }}</button>
                    {{ Form::token() }}
                </form>
            @else
                <div class="page-header">
                    <h4>{{ trans('view.add_comment') }}</h4>
                </div>
                <a class="label label-info" href="{{ URL::route('login') }}"><i class="glyphicon glyphicon-info-sign"></i> {{ trans('view.login_to_comment') }}</a>
            @endif

            <div class="bottom-space"></div>
        </div>
@stop