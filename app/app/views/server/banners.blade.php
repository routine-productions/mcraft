@extends('layouts.base')

@section('content')
		<div class="container reduce-top">
			<div class="page-header">
				<ol class="breadcrumb">
					<li><a href="{{ URL::route('home') }}">{{ trans('view.home_breadcrumb') }}</a></li>
					<li><a href="{{ URL::route('server-page', $server->id) }}">{{{ $server->name }}}</a></li>
					<li class="active">{{ trans('view.banners_breadcrumb') }}</li>
				</ol>
				<h1>{{{ trans('view.banners_of', array('server' => $server->name)) }}}</h1>
			</div>
			<p class="server-action">
				<a href="{{ URL::route('server-vote', $server->id) }}" class="btn btn-success"><i class="glyphicon glyphicon-thumbs-up"></i> {{ trans('view.vote_button') }}</a>
				<a href="{{ URL::route('server-page', $server->id) }}" class="btn btn-primary"><i class="glyphicon glyphicon-picture"></i> {{ trans('view.server_details_button') }}</a>
				<a href="{{ URL::route('server-comments', $server->id) }}" class="btn btn-primary"><i class="glyphicon glyphicon-comment"></i> {{ trans('view.comments_button') }}</a>
			</p>

			<div class="page-header">
				<h2>{{ trans('view.banners') }}</h2>
			</div>

			<ul class="nav nav-tabs" id="banners">
				<li class="active"><a data-toggle="tab" href="#600x100">{{ trans('view.600_100_tab') }}</a></li>
				<li><a data-toggle="tab" href="#728x90">{{ trans('view.728_90_tab') }}</a></li>
				<li><a data-toggle="tab" href="#468x60">{{ trans('view.468_60_tab') }}</a></li>
				<li><a data-toggle="tab" href="#234x60">{{ trans('view.234_60_tab') }}</a></li>
			</ul>
			<p>
				<div class="tab-content">
					<div id="600x100" class="tab-pane active">
						<p><img src="{{ URL::route('server-banner', array($server->id, '600x100', 1)) }}" alt="600x100"></p>
						<button data-target="#600x100html_1" data-toggle="collapse" class="btn btn-default btn-xs" type="button">{{ trans('view.show_html_button') }} <i class="glyphicon glyphicon-info-sign"></i></button>
						<p>
							<div class="collapse" id="600x100html_1">
								<textarea cols="10" rows="2" class="form-control" style="width: 600px;"><a href="{{ URL::route('server-page', $server->id) }}" target="_blank"><img src="{{ URL::route('server-banner', array($server->id, '600x100', 1)) }}" border="0"></a></textarea>
							</div>
						</p>
						<button data-target="#600x100bbcode_1" data-toggle="collapse" class="btn btn-default btn-xs" type="button">{{ trans('view.show_bbcode_button') }} <i class="glyphicon glyphicon-info-sign"></i></button>
						<p>
							<div class="collapse" id="600x100bbcode_1">
								<textarea cols="10" rows="2" class="form-control" style="width: 600px;">[url={{ URL::route('server-page', $server->id) }}][img]{{ URL::route('server-banner', array($server->id, '600x100', 1)) }}[/img][/url]</textarea>
							</div>
						</p>

						<p><img src="{{ URL::route('server-banner', array($server->id, '600x100', 2)) }}" alt="600x100"></p>
						<button data-target="#600x100html_2" data-toggle="collapse" class="btn btn-default btn-xs" type="button">{{ trans('view.show_html_button') }} <i class="glyphicon glyphicon-info-sign"></i></button>
						<p>
							<div class="collapse" id="600x100html_2">
								<textarea cols="10" rows="2" class="form-control" style="width: 600px;"><a href="{{ URL::route('server-page', $server->id) }}" target="_blank"><img src="{{ URL::route('server-banner', array($server->id, '600x100', 2)) }}" border="0"></a></textarea>
							</div>
						</p>
						<button data-target="#600x100bbcode_2" data-toggle="collapse" class="btn btn-default btn-xs" type="button">{{ trans('view.show_bbcode_button') }} <i class="glyphicon glyphicon-info-sign"></i></button>
						<p>
							<div class="collapse" id="600x100bbcode_2">
								<textarea cols="10" rows="2" class="form-control" style="width: 600px;">[url={{ URL::route('server-page', $server->id) }}][img]{{ URL::route('server-banner', array($server->id, '600x100', 2)) }}[/img][/url]</textarea>
							</div>
						</p>

						<p><img src="{{ URL::route('server-banner', array($server->id, '600x100', 3)) }}" alt="600x100"></p>
						<button data-target="#600x100html_3" data-toggle="collapse" class="btn btn-default btn-xs" type="button">{{ trans('view.show_html_button') }} <i class="glyphicon glyphicon-info-sign"></i></button>
						<p>
							<div class="collapse" id="600x100html_3">
								<textarea cols="10" rows="2" class="form-control" style="width: 600px;"><a href="{{ URL::route('server-page', $server->id) }}" target="_blank"><img src="{{ URL::route('server-banner', array($server->id, '600x100', 3)) }}" border="0"></a></textarea>
							</div>
						</p>
						<button data-target="#600x100bbcode_3" data-toggle="collapse" class="btn btn-default btn-xs" type="button">{{ trans('view.show_bbcode_button') }} <i class="glyphicon glyphicon-info-sign"></i></button>
						<p>
							<div class="collapse" id="600x100bbcode_3">
								<textarea cols="10" rows="2" class="form-control" style="width: 600px;">[url={{ URL::route('server-page', $server->id) }}][img]{{ URL::route('server-banner', array($server->id, '600x100', 3)) }}[/img][/url]</textarea>
							</div>
						</p>

						<p><img src="{{ URL::route('server-banner', array($server->id, '600x100', 4)) }}" alt="600x100"></p>
						<button data-target="#600x100html_4" data-toggle="collapse" class="btn btn-default btn-xs" type="button">{{ trans('view.show_html_button') }} <i class="glyphicon glyphicon-info-sign"></i></button>
						<p>
							<div class="collapse" id="600x100html_4">
								<textarea cols="10" rows="2" class="form-control" style="width: 600px;"><a href="{{ URL::route('server-page', $server->id) }}" target="_blank"><img src="{{ URL::route('server-banner', array($server->id, '600x100', 4)) }}" border="0"></a></textarea>
							</div>
						</p>
						<button data-target="#600x100bbcode_4" data-toggle="collapse" class="btn btn-default btn-xs" type="button">{{ trans('view.show_bbcode_button') }} <i class="glyphicon glyphicon-info-sign"></i></button>
						<p>
							<div class="collapse" id="600x100bbcode_4">
								<textarea cols="10" rows="2" class="form-control" style="width: 600px;">[url={{ URL::route('server-page', $server->id) }}][img]{{ URL::route('server-banner', array($server->id, '600x100', 4)) }}[/img][/url]</textarea>
							</div>
						</p>

						<p><img src="{{ URL::route('server-banner', array($server->id, '600x100', 5)) }}" alt="600x100"></p>
						<button data-target="#600x100html_5" data-toggle="collapse" class="btn btn-default btn-xs" type="button">{{ trans('view.show_html_button') }} <i class="glyphicon glyphicon-info-sign"></i></button>
						<p>
							<div class="collapse" id="600x100html_5">
								<textarea cols="10" rows="2" class="form-control" style="width: 600px;"><a href="{{ URL::route('server-page', $server->id) }}" target="_blank"><img src="{{ URL::route('server-banner', array($server->id, '600x100', 5)) }}" border="0"></a></textarea>
							</div>
						</p>
						<button data-target="#600x100bbcode_5" data-toggle="collapse" class="btn btn-default btn-xs" type="button">{{ trans('view.show_bbcode_button') }} <i class="glyphicon glyphicon-info-sign"></i></button>
						<p>
							<div class="collapse" id="600x100bbcode_5">
								<textarea cols="10" rows="2" class="form-control" style="width: 600px;">[url={{ URL::route('server-page', $server->id) }}][img]{{ URL::route('server-banner', array($server->id, '600x100', 5)) }}[/img][/url]</textarea>
							</div>
						</p>

						<p><img src="{{ URL::route('server-banner', array($server->id, '600x100', 6)) }}" alt="600x100"></p>
						<button data-target="#600x100html_6" data-toggle="collapse" class="btn btn-default btn-xs" type="button">{{ trans('view.show_html_button') }} <i class="glyphicon glyphicon-info-sign"></i></button>
						<p>
							<div class="collapse" id="600x100html_6">
								<textarea cols="10" rows="2" class="form-control" style="width: 600px;"><a href="{{ URL::route('server-page', $server->id) }}" target="_blank"><img src="{{ URL::route('server-banner', array($server->id, '600x100', 6)) }}" border="0"></a></textarea>
							</div>
						</p>
						<button data-target="#600x100bbcode_6" data-toggle="collapse" class="btn btn-default btn-xs" type="button">{{ trans('view.show_bbcode_button') }} <i class="glyphicon glyphicon-info-sign"></i></button>
						<p>
							<div class="collapse" id="600x100bbcode_6">
								<textarea cols="10" rows="2" class="form-control" style="width: 600px;">[url={{ URL::route('server-page', $server->id) }}][img]{{ URL::route('server-banner', array($server->id, '600x100', 6)) }}[/img][/url]</textarea>
							</div>
						</p>
					</div>

					<div id="728x90" class="tab-pane fade">
						<p><img src="{{ URL::route('server-banner', array($server->id, '728x90', 1)) }}" alt="728x90"></p>
						<button data-target="#728x90html_1" data-toggle="collapse" class="btn btn-default btn-xs" type="button">{{ trans('view.show_html_button') }} <i class="glyphicon glyphicon-info-sign"></i></button>
						<p>
							<div class="collapse" id="728x90html_1">
								<textarea cols="10" rows="2" class="form-control" style="width: 600px;"><a href="{{ URL::route('server-page', $server->id) }}" target="_blank"><img src="{{ URL::route('server-banner', array($server->id, '728x90', 1)) }}" border="0"></a></textarea>
							</div>
						</p>
						<button data-target="#728x90bbcode_1" data-toggle="collapse" class="btn btn-default btn-xs" type="button">{{ trans('view.show_bbcode_button') }} <i class="glyphicon glyphicon-info-sign"></i></button>
						<p>
							<div class="collapse" id="728x90bbcode_1">
								<textarea cols="10" rows="2" class="form-control" style="width: 600px;">[url={{ URL::route('server-page', $server->id) }}][img]{{ URL::route('server-banner', array($server->id, '728x90', 1)) }}[/img][/url]</textarea>
							</div>
						</p>

						<p><img src="{{ URL::route('server-banner', array($server->id, '728x90', 2)) }}" alt="728x90"></p>
						<button data-target="#728x90html_2" data-toggle="collapse" class="btn btn-default btn-xs" type="button">{{ trans('view.show_html_button') }} <i class="glyphicon glyphicon-info-sign"></i></button>
						<p>
							<div class="collapse" id="728x90html_2">
								<textarea cols="10" rows="2" class="form-control" style="width: 600px;"><a href="{{ URL::route('server-page', $server->id) }}" target="_blank"><img src="{{ URL::route('server-banner', array($server->id, '728x90', 2)) }}" border="0"></a></textarea>
							</div>
						</p>
						<button data-target="#728x90bbcode_2" data-toggle="collapse" class="btn btn-default btn-xs" type="button">{{ trans('view.show_bbcode_button') }} <i class="glyphicon glyphicon-info-sign"></i></button>
						<p>
							<div class="collapse" id="728x90bbcode_2">
								<textarea cols="10" rows="2" class="form-control" style="width: 600px;">[url={{ URL::route('server-page', $server->id) }}][img]{{ URL::route('server-banner', array($server->id, '728x90', 2)) }}[/img][/url]</textarea>
							</div>
						</p>

						<p><img src="{{ URL::route('server-banner', array($server->id, '728x90', 3)) }}" alt="728x90"></p>
						<button data-target="#728x90html_3" data-toggle="collapse" class="btn btn-default btn-xs" type="button">{{ trans('view.show_html_button') }} <i class="glyphicon glyphicon-info-sign"></i></button>
						<p>
							<div class="collapse" id="728x90html_3">
								<textarea cols="10" rows="2" class="form-control" style="width: 600px;"><a href="{{ URL::route('server-page', $server->id) }}" target="_blank"><img src="{{ URL::route('server-banner', array($server->id, '728x90', 3)) }}" border="0"></a></textarea>
							</div>
						</p>
						<button data-target="#728x90bbcode_3" data-toggle="collapse" class="btn btn-default btn-xs" type="button">{{ trans('view.show_bbcode_button') }} <i class="glyphicon glyphicon-info-sign"></i></button>
						<p>
							<div class="collapse" id="728x90bbcode_3">
								<textarea cols="10" rows="2" class="form-control" style="width: 600px;">[url={{ URL::route('server-page', $server->id) }}][img]{{ URL::route('server-banner', array($server->id, '728x90', 3)) }}[/img][/url]</textarea>
							</div>
						</p>

						<p><img src="{{ URL::route('server-banner', array($server->id, '728x90', 4)) }}" alt="728x90"></p>
						<button data-target="#728x90html_4" data-toggle="collapse" class="btn btn-default btn-xs" type="button">{{ trans('view.show_html_button') }} <i class="glyphicon glyphicon-info-sign"></i></button>
						<p>
							<div class="collapse" id="728x90html_4">
								<textarea cols="10" rows="2" class="form-control" style="width: 600px;"><a href="{{ URL::route('server-page', $server->id) }}" target="_blank"><img src="{{ URL::route('server-banner', array($server->id, '728x90', 4)) }}" border="0"></a></textarea>
							</div>
						</p>
						<button data-target="#728x90bbcode_4" data-toggle="collapse" class="btn btn-default btn-xs" type="button">{{ trans('view.show_bbcode_button') }} <i class="glyphicon glyphicon-info-sign"></i></button>
						<p>
							<div class="collapse" id="728x90bbcode_4">
								<textarea cols="10" rows="2" class="form-control" style="width: 600px;">[url={{ URL::route('server-page', $server->id) }}][img]{{ URL::route('server-banner', array($server->id, '728x90', 4)) }}[/img][/url]</textarea>
							</div>
						</p>

						<p><img src="{{ URL::route('server-banner', array($server->id, '728x90', 5)) }}" alt="728x90"></p>
						<button data-target="#728x90html_5" data-toggle="collapse" class="btn btn-default btn-xs" type="button">{{ trans('view.show_html_button') }} <i class="glyphicon glyphicon-info-sign"></i></button>
						<p>
							<div class="collapse" id="728x90html_5">
								<textarea cols="10" rows="2" class="form-control" style="width: 600px;"><a href="{{ URL::route('server-page', $server->id) }}" target="_blank"><img src="{{ URL::route('server-banner', array($server->id, '728x90', 5)) }}" border="0"></a></textarea>
							</div>
						</p>
						<button data-target="#728x90bbcode_5" data-toggle="collapse" class="btn btn-default btn-xs" type="button">{{ trans('view.show_bbcode_button') }} <i class="glyphicon glyphicon-info-sign"></i></button>
						<p>
							<div class="collapse" id="728x90bbcode_5">
								<textarea cols="10" rows="2" class="form-control" style="width: 600px;">[url={{ URL::route('server-page', $server->id) }}][img]{{ URL::route('server-banner', array($server->id, '728x90', 5)) }}[/img][/url]</textarea>
							</div>
						</p>

						<p><img src="{{ URL::route('server-banner', array($server->id, '728x90', 6)) }}" alt="728x90"></p>
						<button data-target="#728x90html_6" data-toggle="collapse" class="btn btn-default btn-xs" type="button">{{ trans('view.show_html_button') }} <i class="glyphicon glyphicon-info-sign"></i></button>
						<p>
							<div class="collapse" id="728x90html_6">
								<textarea cols="10" rows="2" class="form-control" style="width: 600px;"><a href="{{ URL::route('server-page', $server->id) }}" target="_blank"><img src="{{ URL::route('server-banner', array($server->id, '728x90', 6)) }}" border="0"></a></textarea>
							</div>
						</p>
						<button data-target="#728x90bbcode_6" data-toggle="collapse" class="btn btn-default btn-xs" type="button">{{ trans('view.show_bbcode_button') }} <i class="glyphicon glyphicon-info-sign"></i></button>
						<p>
							<div class="collapse" id="728x90bbcode_6">
								<textarea cols="10" rows="2" class="form-control" style="width: 600px;">[url={{ URL::route('server-page', $server->id) }}][img]{{ URL::route('server-banner', array($server->id, '728x90', 6)) }}[/img][/url]</textarea>
							</div>
						</p>
					</div>

					<div id="468x60" class="tab-pane fade">
						<p><img src="{{ URL::route('server-banner', array($server->id, '468x60', 1)) }}" alt="468x60"></p>
						<button data-target="#468x60html_1" data-toggle="collapse" class="btn btn-default btn-xs" type="button">{{ trans('view.show_html_button') }} <i class="glyphicon glyphicon-info-sign"></i></button>
						<p>
							<div class="collapse" id="468x60html_1">
								<textarea cols="10" rows="2" class="form-control" style="width: 600px;"><a href="{{ URL::route('server-page', $server->id) }}" target="_blank"><img src="{{ URL::route('server-banner', array($server->id, '468x60', 1)) }}" border="0"></a></textarea>
							</div>
						</p>
						<button data-target="#468x60bbcode_1" data-toggle="collapse" class="btn btn-default btn-xs" type="button">{{ trans('view.show_bbcode_button') }} <i class="glyphicon glyphicon-info-sign"></i></button>
						<p>
							<div class="collapse" id="468x60bbcode_1">
								<textarea cols="10" rows="2" class="form-control" style="width: 600px;">[url={{ URL::route('server-page', $server->id) }}][img]{{ URL::route('server-banner', array($server->id, '468x60', 1)) }}[/img][/url]</textarea>
							</div>
						</p>

						<p><img src="{{ URL::route('server-banner', array($server->id, '468x60', 2)) }}" alt="468x60"></p>
						<button data-target="#468x60html_2" data-toggle="collapse" class="btn btn-default btn-xs" type="button">{{ trans('view.show_html_button') }} <i class="glyphicon glyphicon-info-sign"></i></button>
						<p>
							<div class="collapse" id="468x60html_2">
								<textarea cols="10" rows="2" class="form-control" style="width: 600px;"><a href="{{ URL::route('server-page', $server->id) }}" target="_blank"><img src="{{ URL::route('server-banner', array($server->id, '468x60', 2)) }}" border="0"></a></textarea>
							</div>
						</p>
						<button data-target="#468x60bbcode_2" data-toggle="collapse" class="btn btn-default btn-xs" type="button">{{ trans('view.show_bbcode_button') }} <i class="glyphicon glyphicon-info-sign"></i></button>
						<p>
							<div class="collapse" id="468x60bbcode_2">
								<textarea cols="10" rows="2" class="form-control" style="width: 600px;">[url={{ URL::route('server-page', $server->id) }}][img]{{ URL::route('server-banner', array($server->id, '468x60', 2)) }}[/img][/url]</textarea>
							</div>
						</p>

						<p><img src="{{ URL::route('server-banner', array($server->id, '468x60', 3)) }}" alt="468x60"></p>
						<button data-target="#468x60html_3" data-toggle="collapse" class="btn btn-default btn-xs" type="button">{{ trans('view.show_html_button') }} <i class="glyphicon glyphicon-info-sign"></i></button>
						<p>
							<div class="collapse" id="468x60html_3">
								<textarea cols="10" rows="2" class="form-control" style="width: 600px;"><a href="{{ URL::route('server-page', $server->id) }}" target="_blank"><img src="{{ URL::route('server-banner', array($server->id, '468x60', 3)) }}" border="0"></a></textarea>
							</div>
						</p>
						<button data-target="#468x60bbcode_3" data-toggle="collapse" class="btn btn-default btn-xs" type="button">{{ trans('view.show_bbcode_button') }} <i class="glyphicon glyphicon-info-sign"></i></button>
						<p>
							<div class="collapse" id="468x60bbcode_3">
								<textarea cols="10" rows="2" class="form-control" style="width: 600px;">[url={{ URL::route('server-page', $server->id) }}][img]{{ URL::route('server-banner', array($server->id, '468x60', 3)) }}[/img][/url]</textarea>
							</div>
						</p>

						<p><img src="{{ URL::route('server-banner', array($server->id, '468x60', 4)) }}" alt="468x60"></p>
						<button data-target="#468x60html_4" data-toggle="collapse" class="btn btn-default btn-xs" type="button">{{ trans('view.show_html_button') }} <i class="glyphicon glyphicon-info-sign"></i></button>
						<p>
							<div class="collapse" id="468x60html_4">
								<textarea cols="10" rows="2" class="form-control" style="width: 600px;"><a href="{{ URL::route('server-page', $server->id) }}" target="_blank"><img src="{{ URL::route('server-banner', array($server->id, '468x60', 4)) }}" border="0"></a></textarea>
							</div>
						</p>
						<button data-target="#468x60bbcode_4" data-toggle="collapse" class="btn btn-default btn-xs" type="button">{{ trans('view.show_bbcode_button') }} <i class="glyphicon glyphicon-info-sign"></i></button>
						<p>
							<div class="collapse" id="468x60bbcode_4">
								<textarea cols="10" rows="2" class="form-control" style="width: 600px;">[url={{ URL::route('server-page', $server->id) }}][img]{{ URL::route('server-banner', array($server->id, '468x60', 4)) }}[/img][/url]</textarea>
							</div>
						</p>

						<p><img src="{{ URL::route('server-banner', array($server->id, '468x60', 5)) }}" alt="468x60"></p>
						<button data-target="#468x60html_5" data-toggle="collapse" class="btn btn-default btn-xs" type="button">{{ trans('view.show_html_button') }} <i class="glyphicon glyphicon-info-sign"></i></button>
						<p>
							<div class="collapse" id="468x60html_5">
								<textarea cols="10" rows="2" class="form-control" style="width: 600px;"><a href="{{ URL::route('server-page', $server->id) }}" target="_blank"><img src="{{ URL::route('server-banner', array($server->id, '468x60', 5)) }}" border="0"></a></textarea>
							</div>
						</p>
						<button data-target="#468x60bbcode_5" data-toggle="collapse" class="btn btn-default btn-xs" type="button">{{ trans('view.show_bbcode_button') }} <i class="glyphicon glyphicon-info-sign"></i></button>
						<p>
							<div class="collapse" id="468x60bbcode_5">
								<textarea cols="10" rows="2" class="form-control" style="width: 600px;">[url={{ URL::route('server-page', $server->id) }}][img]{{ URL::route('server-banner', array($server->id, '468x60', 5)) }}[/img][/url]</textarea>
							</div>
						</p>

						<p><img src="{{ URL::route('server-banner', array($server->id, '468x60', 6)) }}" alt="468x60"></p>
						<button data-target="#468x60html_6" data-toggle="collapse" class="btn btn-default btn-xs" type="button">{{ trans('view.show_html_button') }} <i class="glyphicon glyphicon-info-sign"></i></button>
						<p>
							<div class="collapse" id="468x60html_6">
								<textarea cols="10" rows="2" class="form-control" style="width: 600px;"><a href="{{ URL::route('server-page', $server->id) }}" target="_blank"><img src="{{ URL::route('server-banner', array($server->id, '468x60', 6)) }}" border="0"></a></textarea>
							</div>
						</p>
						<button data-target="#468x60bbcode_6" data-toggle="collapse" class="btn btn-default btn-xs" type="button">{{ trans('view.show_bbcode_button') }} <i class="glyphicon glyphicon-info-sign"></i></button>
						<p>
							<div class="collapse" id="468x60bbcode_6">
								<textarea cols="10" rows="2" class="form-control" style="width: 600px;">[url={{ URL::route('server-page', $server->id) }}][img]{{ URL::route('server-banner', array($server->id, '468x60', 6)) }}[/img][/url]</textarea>
							</div>
						</p>
					</div>

					<div id="234x60" class="tab-pane fade">
						<p><img src="{{ URL::route('server-banner', array($server->id, '234x60', 1)) }}" alt="234x60"></p>
						<button data-target="#234x60html_1" data-toggle="collapse" class="btn btn-default btn-xs" type="button">{{ trans('view.show_html_button') }} <i class="glyphicon glyphicon-info-sign"></i></button>
						<p>
							<div class="collapse" id="234x60html_1">
								<textarea cols="10" rows="2" class="form-control" style="width: 600px;"><a href="{{ URL::route('server-page', $server->id) }}" target="_blank"><img src="{{ URL::route('server-banner', array($server->id, '234x60', 1)) }}" border="0"></a></textarea>
							</div>
						</p>
						<button data-target="#234x60bbcode_1" data-toggle="collapse" class="btn btn-default btn-xs" type="button">{{ trans('view.show_bbcode_button') }} <i class="glyphicon glyphicon-info-sign"></i></button>
						<p>
							<div class="collapse" id="234x60bbcode_1">
								<textarea cols="10" rows="2" class="form-control" style="width: 600px;">[url={{ URL::route('server-page', $server->id) }}][img]{{ URL::route('server-banner', array($server->id, '234x60', 1)) }}[/img][/url]</textarea>
							</div>
						</p>

						<p><img src="{{ URL::route('server-banner', array($server->id, '234x60', 2)) }}" alt="234x60"></p>
						<button data-target="#234x60html_2" data-toggle="collapse" class="btn btn-default btn-xs" type="button">{{ trans('view.show_html_button') }} <i class="glyphicon glyphicon-info-sign"></i></button>
						<p>
							<div class="collapse" id="234x60html_2">
								<textarea cols="10" rows="2" class="form-control" style="width: 600px;"><a href="{{ URL::route('server-page', $server->id) }}" target="_blank"><img src="{{ URL::route('server-banner', array($server->id, '234x60', 2)) }}" border="0"></a></textarea>
							</div>
						</p>
						<button data-target="#234x60bbcode_2" data-toggle="collapse" class="btn btn-default btn-xs" type="button">{{ trans('view.show_bbcode_button') }} <i class="glyphicon glyphicon-info-sign"></i></button>
						<p>
							<div class="collapse" id="234x60bbcode_2">
								<textarea cols="10" rows="2" class="form-control" style="width: 600px;">[url={{ URL::route('server-page', $server->id) }}][img]{{ URL::route('server-banner', array($server->id, '234x60', 2)) }}[/img][/url]</textarea>
							</div>
						</p>

						<p><img src="{{ URL::route('server-banner', array($server->id, '234x60', 3)) }}" alt="234x60"></p>
						<button data-target="#234x60html_3" data-toggle="collapse" class="btn btn-default btn-xs" type="button">{{ trans('view.show_html_button') }} <i class="glyphicon glyphicon-info-sign"></i></button>
						<p>
							<div class="collapse" id="234x60html_3">
								<textarea cols="10" rows="2" class="form-control" style="width: 600px;"><a href="{{ URL::route('server-page', $server->id) }}" target="_blank"><img src="{{ URL::route('server-banner', array($server->id, '234x60', 3)) }}" border="0"></a></textarea>
							</div>
						</p>
						<button data-target="#234x60bbcode_3" data-toggle="collapse" class="btn btn-default btn-xs" type="button">{{ trans('view.show_bbcode_button') }} <i class="glyphicon glyphicon-info-sign"></i></button>
						<p>
							<div class="collapse" id="234x60bbcode_3">
								<textarea cols="10" rows="2" class="form-control" style="width: 600px;">[url={{ URL::route('server-page', $server->id) }}][img]{{ URL::route('server-banner', array($server->id, '234x60', 3)) }}[/img][/url]</textarea>
							</div>
						</p>

						<p><img src="{{ URL::route('server-banner', array($server->id, '234x60', 4)) }}" alt="234x60"></p>
						<button data-target="#234x60html_4" data-toggle="collapse" class="btn btn-default btn-xs" type="button">{{ trans('view.show_html_button') }} <i class="glyphicon glyphicon-info-sign"></i></button>
						<p>
							<div class="collapse" id="234x60html_4">
								<textarea cols="10" rows="2" class="form-control" style="width: 600px;"><a href="{{ URL::route('server-page', $server->id) }}" target="_blank"><img src="{{ URL::route('server-banner', array($server->id, '234x60', 4)) }}" border="0"></a></textarea>
							</div>
						</p>
						<button data-target="#234x60bbcode_4" data-toggle="collapse" class="btn btn-default btn-xs" type="button">{{ trans('view.show_bbcode_button') }} <i class="glyphicon glyphicon-info-sign"></i></button>
						<p>
							<div class="collapse" id="234x60bbcode_4">
								<textarea cols="10" rows="2" class="form-control" style="width: 600px;">[url={{ URL::route('server-page', $server->id) }}][img]{{ URL::route('server-banner', array($server->id, '234x60', 4)) }}[/img][/url]</textarea>
							</div>
						</p>

						<p><img src="{{ URL::route('server-banner', array($server->id, '234x60', 5)) }}" alt="234x60"></p>
						<button data-target="#234x60html_5" data-toggle="collapse" class="btn btn-default btn-xs" type="button">{{ trans('view.show_html_button') }} <i class="glyphicon glyphicon-info-sign"></i></button>
						<p>
							<div class="collapse" id="234x60html_5">
								<textarea cols="10" rows="2" class="form-control" style="width: 600px;"><a href="{{ URL::route('server-page', $server->id) }}" target="_blank"><img src="{{ URL::route('server-banner', array($server->id, '234x60', 5)) }}" border="0"></a></textarea>
							</div>
						</p>
						<button data-target="#234x60bbcode_5" data-toggle="collapse" class="btn btn-default btn-xs" type="button">{{ trans('view.show_bbcode_button') }} <i class="glyphicon glyphicon-info-sign"></i></button>
						<p>
							<div class="collapse" id="234x60bbcode_5">
								<textarea cols="10" rows="2" class="form-control" style="width: 600px;">[url={{ URL::route('server-page', $server->id) }}][img]{{ URL::route('server-banner', array($server->id, '234x60', 5)) }}[/img][/url]</textarea>
							</div>
						</p>

						<p><img src="{{ URL::route('server-banner', array($server->id, '234x60', 6)) }}" alt="234x60"></p>
						<button data-target="#234x60html_6" data-toggle="collapse" class="btn btn-default btn-xs" type="button">{{ trans('view.show_html_button') }} <i class="glyphicon glyphicon-info-sign"></i></button>
						<p>
							<div class="collapse" id="234x60html_6">
								<textarea cols="10" rows="2" class="form-control" style="width: 600px;"><a href="{{ URL::route('server-page', $server->id) }}" target="_blank"><img src="{{ URL::route('server-banner', array($server->id, '234x60', 6)) }}" border="0"></a></textarea>
							</div>
						</p>
						<button data-target="#234x60bbcode_6" data-toggle="collapse" class="btn btn-default btn-xs" type="button">{{ trans('view.show_bbcode_button') }} <i class="glyphicon glyphicon-info-sign"></i></button>
						<p>
							<div class="collapse" id="234x60bbcode_6">
								<textarea cols="10" rows="2" class="form-control" style="width: 600px;">[url={{ URL::route('server-page', $server->id) }}][img]{{ URL::route('server-banner', array($server->id, '234x60', 6)) }}[/img][/url]</textarea>
							</div>
						</p>
					</div>
				</div>
			</p>

			<div class="bottom-space"></div>
		</div>
@stop