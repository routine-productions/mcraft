@extends('layouts.base')

@section('head')
    <meta property="og:title" content="{{ $server->name }}"/>
    <meta property="og:url" content="{{ URL::route('server-page', $id) }}"/>

    <!-- Put this script tag to the <head> of your page -->
    <script type="text/javascript" src="//vk.com/js/api/openapi.js?124"></script>

    <script type="text/javascript">
        VK.init({apiId: 5552319, onlyWidgets: true});
    </script>
@stop

@section('content')
    <?php
    $uptime = substr(($server->online_time / $server->verified_time) * 100, 0, 5);
    $uptime = strlen($uptime) > 1 ? $uptime . '%' : 'N/A';


    ?>
    @if(!empty($server))
    <section class="Card-Full @if(!empty($server->purchases_color))Color-{{$server->purchases_color->color}}@endif"
             data-page-id="{{$server->id}}">
        <div class="JS-Tabs">
            <div class="Full-Head">
                <div class="Full-Top">
                    <div class="Card-Path">
                        <h1 class="Full-Title">
                            {{ $server->name }}
                            <span><img src="http://www.geonames.org/flags/x/{{$country}}.gif" width="36px"></span>
                        </h1>
                        <p>{{ BBCode::parse($server->short_description) }}</p>
                    </div>

                    <div class="Card-Path">
                        @if(!$server->already_voted)
                            <div class="Card-Liker">
                                <span class="Like-Count">{{ $server->votes }}</span>
                                <a href="{{URL::route('server-vote',$server->id)}}"
                                   class="Like-Up">Голосовать</a>
                            </div>
                        @else
                            <div class="Card-Liker">
                                <span class="Like-Count">{{$server->votes}}</span>
                                <a href="#" class="Like-Up Like-Warning">Вы уже проголосовали</a>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="Full-Tabs-Nav JS-Tabs-Navigation">
                    <ul>
                        <li class="JS-Tab Active" data-href="Tab-1">О сервере</li>
                        <li class="JS-Tab" data-href="Tab-2">Отзывы</li>
                        <li class="JS-Tab" data-href="Tab-3">Виджеты</li>
                        <li class="JS-Tab" data-href="Tab-4">Статистика</li>
                        @if(Auth::user() && (Auth::user()->isAdmin() || (Auth::user()->isMember() && Auth::user()->id == $server->user_id )))
                            <li class="JS-Tab" data-href="Tab-5">Услуги</li>
                        @endif
                    </ul>
                </div>
            </div>


            <div class="JS-Tabs-Content">
                <div class="Full-Description" data-tab='Tab-1'>
                    <div class="Description-Table">
                        <ul>
                            <li>
                                <table>
                                    <tbody>
                                    <tr>
                                        <td>Голосов</td>
                                        <td>{{ $server->votes }}
                                            @if(!count($server->purchases_color))
                                                @if(Auth::user() && (Auth::user()->isAdmin() || (Auth::user()->isMember() && Auth::user()->id == $server->user_id )))
                                                    <button class="Btn Primary JS-Modal-Button" id="bay-pure"
                                                            href="#Modal-Color">Выделить сервер цветом
                                                    </button>
                                                @endif
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Рейтинг</td>
                                        <td>{{ $server->purchases_diamonds->sum('diamonds') }}
                                            @if(Auth::user() && (Auth::user()->isAdmin() || (Auth::user()->isMember() && Auth::user()->id == $server->user_id )))
                                                <button class="Btn Primary JS-Modal-Button" id="bay-cristal"
                                                        href="#Modal-Diamond">Купить алмазов
                                                </button>
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Статус</td>
                                        <td>{{ $server->status }}</td>
                                    </tr>
                                    <tr>
                                        <td>IP адрес</td>
                                        <td>{{$server->address}}:{{$server->port}}</td>
                                    </tr>
                                    <tr>
                                        <td>Аптайм</td>
                                        <td>{{$uptime}}</td>
                                    </tr>
                                    <tr>
                                        <td>Игроков Онлайн</td>
                                        <td>
                                            @if ($server->statistics['max_players'])
                                                {{ $server->statistics['players'] }}
                                                / {{ $server->statistics['max_players'] }}
                                            @else
                                                N/A
                                            @endif
                                        </td>
                                    </tr>
                                    @if(isset($checked_time))
                                        <tr>
                                            <td>Последняя проверка</td>
                                            <td>
                                                {{$checked_time}}
                                            </td>
                                        </tr>
                                    @endif
                                    <tr>
                                        @if(isset($server->version))
                                            <td>Версия сервера</td>
                                            <td>{{$server->version->name}}</td>
                                        @endif
                                    </tr>
                                    @if($server->website)
                                        <tr>
                                            <td>Сайт сервера</td>
                                            <td><a href="{{$server->website}}">{{$server->website}}</a></td>
                                        </tr>
                                    @endif
                                    @if(count($server->tags))
                                        <tr>
                                            <td>Теги</td>
                                            <td>
                                                @foreach($server->tags  as $tag)
                                                    {{$tag->name}};
                                                @endforeach
                                            </td>
                                        </tr>
                                    @endif
                                    @if(count($server->games))
                                        <tr>
                                            <td>Мини игры</td>
                                            <td>
                                                @foreach($server->games  as $game)
                                                    {{$game->name}};
                                                @endforeach
                                            </td>
                                        </tr>
                                    @endif
                                    @if(count($server->modes))
                                        <tr>
                                            <td>Плагины</td>
                                            <td>
                                                @foreach($server->modes  as $mode)
                                                    {{$mode->name}};
                                                @endforeach
                                            </td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                                <div class="addthis_inline_share_toolbox"></div>
                            </li>
                            <li>
                                <div class="By-Server">
                                    <h3>О сервере</h3>

                                    @if(count($server->purchases_banner))
                                        @if(!$server->banner)
                                            <img src="{{ asset('uploads/images/banners/' . $server->banner) }}"
                                                 alt="">
                                        @else
                                            <img title="{{ $server->name }}" alt="{{ $server->name }}"
                                                 src="{{ URL::route('server-banner', array($server->id, '468x60', $server->id%4+1)) }}">
                                        @endif
                                    @else
                                        @if(Auth::user() && (Auth::user()->isAdmin() || (Auth::user()->isMember() && Auth::user()->id == $server->user_id )))
                                            <button class="Btn Primary JS-Modal-Button" id="bay-banner"
                                                    href="#Modal-Banner">
                                                Купить баннер
                                            </button>
                                        @endif
                                    @endif
                                    {{ BBCode::parse($server->description) }}
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="Full-Description" data-tab='Tab-2'>
                    <h2>Отзывы</h2>

                    <!-- Put this div tag to the place, where the Comments block will be -->
                    <div id="vk_comments"></div>
                    <script type="text/javascript">
                        VK.Widgets.Comments("vk_comments", {redesign: 1, limit: 10, width: "665", attach: "*"});
                    </script>
                </div>

                <div data-tab="Tab-3" class="Full-Description Widgets">
                    <h2>Виджет "Мы на MinecraftRating":</h2>
                    <a href="{{url()}}/server/{{$server->id}}" target="_blank" alt="Мы на MinecraftRating">
                        <img src="{{url()}}/uploads/we_at_minecraft.png">
                    </a>
                    <p>Код html вставки:</p>
                    <input type="text" disabled
                           value='<a href="{{url()}}/server/{{$server->id}}" target="_blank" alt="Мы на MinecraftRating"><img src="{{url()}}/uploads/checked_minecraft.png"></a>'>


                    <h2>Виджет "Проверено MinecraftRating":</h2>
                    <a href="{{url()}}/server/{{$server->id}}" target="_blank" alt="Мы на MinecraftRating">
                        <img src="{{url()}}/uploads/checked_minecraft.png">
                    </a>
                    <p>Код html вставки:</p>
                    <input type="text" disabled
                           value='<a href="{{url()}}/server/{{$server->id}}" target="_blank" alt="Проверено MinecraftRating"><img src="{{url()}}/uploads/checked_minecraft.png"></a>'>


                    <h2>Кнопка голосования за сервер на MinecraftRating:</h2>
                    <a href="{{url()}}/server/{{$server->id}}/vote" target="_blank" alt="Мы на MinecraftRating">
                        <img src="{{url()}}/uploads/vote_for_minecraft.png">
                    </a>
                    <p>Код html вставки:</p>
                    <input type="text" disabled
                           value=' <a href="{{url()}}/server/{{$server->id}}/vote" target="_blank" alt="Мы на MinecraftRating"><img src="{{url()}}/uploads/vote_for_minecraft.png"></a>'>

                </div>

                <div data-tab="Tab-4" class="Full-Description Statistics-Tab">
                    <script>

                        google.charts.load('current', {'packages': ['corechart']});
                        google.charts.setOnLoadCallback(drawChart);

                            function drawChart() {

                            var data = new google.visualization.DataTable();
                            data.addColumn('date', 'Время');
                            data.addColumn('number', 'Игроки');

                            data.addRows(
                                    [
                                            @foreach($server->statistics_list as $stat)
                                            <?php
                                            $updated_time = explode(' ', $stat->updated_at);
                                            $updated_time[0] = explode('-', $updated_time[0]);
                                            $updated_time[1] = explode(':', $updated_time[1]);
                                            ?>
                                        [new Date({{$updated_time[0][0]}},{{$updated_time[0][1]}},{{$updated_time[0][2]}},{{$updated_time[1][0]}}), {{$stat->players}}],
                                        @endforeach
                                    ]);

                            var options = {
                                title: '',
                                date: "M/d/yy hh:mm",
                                width: '1104',
                                height: 500,
                                curveType: 'function',
                                pointSize: 6,
                                legend: {position: 'bottom'},
                                hAxis: {
                                    format: 'M/d/yy hh:mm',
                                    gridlines: {

                                        color: '#f2f2f2',
                                        count: -1,
                                        units: {
                                            days: {format: ['yyyy.M.dd']},
                                            hours   : {format: ['hh:mm', 'ha']},
                                        }
                                    },
                                    textStyle: {
                                        fontSize: 11
                                    }

                                },
                                vAxis: {
                                    gridlines: {
                                        color: '#ececec',
                                        count: 5
                                    },
                                    minValue: 0,
                                    textStyle: {
                                        fontSize: 14
                                    }
                                },
                                timeline: {
                                    groupByRowLabel: true
                                }
                            };
                            var date_formatter = new google.visualization.DateFormat({
                                pattern: "yy.M.d h:mm"
                            });

                            date_formatter.format(data, 0);

                            var chart = new google.visualization.LineChart(document.getElementById('chart_div'));

                            chart.draw(data, options);
                        }
                    </script>
                    <div id="chart_div" width="100%"></div>
                </div>

                @if(Auth::user() && (Auth::user()->isAdmin() || (Auth::user()->isMember() && Auth::user()->id == $server->user_id )))
                    <div data-tab="Tab-5" class="Full-Description Purchases">
                        @if(count($purchases))
                            <h2>Активные услуги</h2>
                            <table>
                                <tr>
                                    <th>Услуга</th>
                                    <th>Стоимость</th>
                                    <th>Количество дней</th>
                                    <th>Начало действия</th>
                                    <th>Окончание действия</th>
                                </tr>

                                @foreach($purchases as $purchase)

                                    <tr>
                                        <td>{{Config::get('app.pay_types.' . $purchase->type)}}
                                            @if($purchase->type =='diamond')
                                                ({{$purchase->diamonds}} шт.)
                                            @endif
                                            @if($purchase->type == 'color')
                                                ({{$purchase->color}})
                                            @endif
                                        </td>


                                        <td>{{ $purchase->cost }} р.</td>
                                        <td>{{ $purchase->days }}</td>
                                        <td>{{ $purchase->created_at }}</td>
                                        <td>{{ $purchase->end_period }}</td>
                                    </tr>
                                @endforeach
                            </table>
                        @endif

                            @if(count($purchases_non_paid))
                                <h2>Ожидающие оплату услуги</h2>
                                <table>
                                    <tr>
                                        <th>Услуга</th>
                                        <th>Стоимость</th>
                                        <th>Количество дней</th>
                                        <th>Начало действия</th>
                                        <th>Окончание действия</th>
                                        <th>Оплатить<th>
                                    </tr>

                                    @foreach($purchases_non_paid as $purchase)
                                        <tr>
                                            <td>{{Config::get('app.pay_types.' . $purchase->type)}}
                                                @if($purchase->type =='diamond')
                                                    ({{$purchase->diamonds}} шт.)
                                                @endif
                                                @if($purchase->type == 'color')
                                                    ({{Config::get('app.colors.'. $purchase->color)}})
                                                @endif
                                            </td>
                                            <td>{{ $purchase->cost }} р.</td>
                                            <td>{{ $purchase->days }}</td>
                                            <td>{{ $purchase->created_at }}</td>
                                            <td>{{ $purchase->end_period }}</td>
                                            <td><a href="/pay/{{$purchase->id}}">Оплатить</a></td>
                                        </tr>
                                    @endforeach
                                </table>
                            @endif


                        @if(count($old_purchases))
                            <h2>Прошедшие услуги</h2>
                            <table>
                                <tr>
                                    <th>Услуга</th>
                                    <th>Стоимость</th>
                                    <th>Количество дней</th>
                                    <th>Начало действия</th>
                                    <th>Окончание действия</th>
                                </tr>

                                @foreach($old_purchases as $purchase)
                                    <tr>
                                        <td>{{Config::get('app.pay_types.' . $purchase->type)}}
                                            @if($purchase->type =='diamond')
                                                ({{$purchase->diamonds}} шт.)
                                            @endif
                                            @if($purchase->type == 'color')
                                                ({{Config::get('app.colors.'. $purchase->color)}})
                                            @endif
                                        </td>
                                        <td>{{ $purchase->cost }} р.</td>
                                        <td>{{ $purchase->days }}</td>
                                        <td>{{ $purchase->created_at }}</td>
                                        <td>{{ $purchase->end_period }}</td>
                                    </tr>
                                @endforeach
                            </table>
                        @endif

                        @if(!count($old_purchases) && !count($purchases) && !count($purchases_non_paid))
                            <p>Нет услуг !</p>
                        @endif

                    </div>
                @endif
            </div>
        </div>

    </section>
    @else
        <section><div><p>UPS!!!</p></div></section>
    @endif
@stop