@extends('layouts.base')


@section('content')
    <section class="Card">
        <header class="Card-Heading">
            <h2>{{ trans('view.edit_your_server') }}</h2>
        </header>
        <div class="Card-Content">
            <form class="Form" role="form" action="{{ URL::route('server-edit-post', $id) }}" method="post"
                  class="bottom-space"
                  enctype="multipart/form-data">


                <!-- Title -->
                <p class="Row @if ($errors->has('name')) has-error @endif">
                    <label for="name">{{ trans('view.server_name_label') }} <span class="required">*</span></label>
                    <input type="text" name="name" class="form-control" id="name"
                           placeholder="Имя сервера" {{ (Input::old('name')) ? ' value="' . Input::old('name') . '"' : ' value="' . $server->name . '"' }}>
                    @if ($errors->has('name'))
                        <span class="Error">{{ $errors->first('name') }} </span>
                    @endif
                </p>


                {{-- Meta --}}
                <hr>
                <div class="Row">
                    <label>Meta: Title</label>
                    <input class="form-control" name="meta_title" value="{{ $server->meta_title }}">
                </div>
                <div class="Row">
                    <label>Meta: Description</label>
                    <textarea class="form-control"
                              name="meta_description">{{ $server->meta_description }}</textarea>
                </div>
                <div class="Row">
                    <label>Meta: Keywords</label>
                    <textarea class="form-control"
                              name="meta_keywords">{{ $server->meta_keywords }}</textarea>
                </div>
                <div class="Row">
                    <label>Seo Text</label>
                    <textarea class="form-control" name="seo_text">{{ $server->seo_text }}</textarea>
                </div>
                <hr>
            {{-- Meta --}}


            <!-- Version -->
                <p class="Row form-group @if ($errors->has('version')) has-error @endif">
                    <label for="version">{{ trans('view.minecraft_version_label') }}</label>
                    <select name="version" id="version" class="form-control">
                        @foreach ($versions as $version)
                            <option value="{{ $version->id }}"{{ ($version->id == $server->version_id) ? ' selected' : '' }}>{{ $version->name }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('version'))
                        <span class="Error">{{ $errors->first('version') }} </span>
                    @endif
                </p>

                <!-- Tags -->
                <p class="Row">
                    <label for="select-tags">Теги</label>
                    <select id="select-tags" name="tags[]" multiple placeholder="Выберите тег...">
                        @foreach ($tags as $tag)
                            <option value="{{ $tag->id }}"
                                    @if ($object->haveTag($server->id, $tag->id) == true) selected @endif>{{ $tag->name }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('tags'))
                        <span class="Error">{{ $errors->first('tags') }} </span>
                    @endif
                </p>

                <!-- Games -->
                <p class="Row">
                    <label for="select-games">Мини игры</label>
                    <select id="select-games" name="games[]" multiple placeholder="Выберите игру...">
                        @foreach ($games as $game)
                            <option value="{{ $game->id }}"
                                    @if ($object->haveGame($server->id, $game->id) == true) selected @endif>{{ $game->name }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('games'))
                        <span class="Error">{{ $errors->first('games') }} </span>
                    @endif
                </p>

                <!-- Modes -->
                <p class="Row">
                    <label for="select-modes">Моды</label>
                    <select id="select-modes" name="modes[]" multiple placeholder="Выберите моды...">
                        @foreach ($modes as $mode)
                            <option value="{{ $mode->id }}"
                                    @if ($object->haveMode($server->id, $mode->id) == true) selected @endif>{{ $mode->name }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('modes'))
                        <span class="Error">{{ $errors->first('modes') }}</span>
                    @endif
                </p>


                <!-- Short Description -->
                <p class="Row">
                    <label for="short_description">Короткое описание</label>
                    <textarea maxlength="200" name="short_description" id="short_description" rows="2"
                              class="form-control">{{ (Input::old('short_description')) ? Input::old('short_description') : $server->short_description }}</textarea>
                    <span class="Control-Description">{{ trans('view.description_help') }}</span>
                    @if ($errors->has('short_description'))
                        <span class="Error">{{ $errors->first('short_description') }} </span>
                    @endif
                </p>

                <!-- Full Description -->
                <p class="Row">
                    <label for="description">{{ trans('view.description_label') }}</label>
                    <textarea maxlength="4500" name="description" id="description" rows="6"
                              class="form-control">{{ (Input::old('description')) ? Input::old('description') : $server->description }}</textarea>
                    <span class="Control-Description">{{ trans('view.description_help') }}</span>
                    @if ($errors->has('description'))
                        <span class="Error">{{ $errors->first('description') }} </span>
                    @endif
                </p>

                <!-- Banner -->
                <p class="Row">
                    <label for="banner">{{ trans('view.banner_label') }}</label>
                    <input type="file" name="banner" id="banner">
                    <span class="Control-Description">{{ trans('view.banner_help') }}</span>
                    @if ($errors->has('banner'))
                        <span class="Error">{{ $errors->first('banner') }} </span>
                    @endif
                </p>
                <p class="Row Right">
                    @if(!empty($server->banner))
                        <img src="/uploads/images/banners/{{$server->banner}}" alt="">
                    @endif
                </p>

                <!-- Server -->
                <p class="Row Double">
                    <label for="address">{{ trans('view.server_address_port_label') }}</label>

                    <input type="text" placeholder="{{ trans('view.server_address_input') }}"
                           {{ (Input::old('address')) ? ' value="' . Input::old('address') . '"' : ' value="' . $server->address . '"' }} name="address"
                           id="address" class="form-control">

                    <input type="text" placeholder="{{ trans('view.server_port_input') }}" name="port"
                           id="port"
                           class="form-control" {{ (Input::old('port')) ? ' value="' . Input::old('port') . '"' : ' value="' . $server->port . '"' }}>

                    @if ($errors->has('address'))
                        <span class="Error">{{ $errors->first('address') }}</span>
                    @endif
                    @if ($errors->has('port'))
                        <span class="Error">{{ $errors->first('port') }}</span>
                    @endif

                    <span class="Control-Description" id="connection_notice"></span>
                </p>

                <!-- Website -->
                <p class="Row @if ($errors->has('website')) has-error @endif">
                    <label for="name">{{ trans('view.website_label') }}</label>
                    <input type="text" name="website" class="form-control" id="website"
                           placeholder="{{ trans('view.add_server_website_input') }}"{{ (Input::old('website')) ? ' value="' . Input::old('website') . '"' : ' value="' . $server->website . '"' }}>
                    @if ($errors->has('website'))
                        <span class="Error">{{ $errors->first('website') }} </span>
                    @endif
                </p>

                <!-- Video -->
                <p class="Row">
                    <label for="video">{{ trans('view.youtube_video_label') }}</label>
                    <input type="text" name="youtube" class="form-control"
                           placeholder="Вставьте ссылку на YouTube видео"
                           id="video" {{ (Input::old('youtube')) ? ' value="' . Input::old('youtube') . '"' : ' value="' . $server->youtube . '"' }}>
                    @if ($errors->has('youtube'))
                        <span class="Error">{{ $errors->first('youtube  ') }} </span>
                    @endif
                </p>




                <hr>
                <!-- Votofer -->
                <h2>{{ trans('view.votifier') }}</h2>
                <div class="Row">
                    <div class="Error">
                        {{ trans('view.votifier_help') }}
                    </div>
                </div>

                <div class="Row">
                    <input type="checkbox" checked="checked" name="votifier_active" style="visibility: hidden;">
                </div>


                <div class="Row">
                    <label for="votifier_address">{{ trans('view.votifier_address_label') }}</label>
                    <input type="text" value="" name="votifier_address"
                           {{ (Input::old('votifier_address')) ? ' value="' . Input::old('votifier_address') . '"' : $server->votifier_address }} id="votifier_address"
                           class="form-control">

                    @if ($errors->has('votifier_address'))
                        <span class="Error">{{ $errors->first('votifier_address') }}</span>
                    @endif

                    <p class="Error"><i class="fa fa-info-circle"></i> {{ trans('view.server_votifier_help') }}</p>
                </div>


                <div class="Row" class="form-group @if ($errors->has('votifier_port')) has-error @endif">
                    <label for="votifier_port">{{ trans('view.votifier_port_label') }}</label>
                    <input type="text" value="" placeholder="{{ trans('view.votifier_port_input') }}"
                           name="votifier_port" id="votifier_port" class="form-control"
                            {{ (Input::old('votifier_address')) ? ' value="' . Input::old('votifier_port') . '"' : $server->votifier_port }}>
                    @if ($errors->has('votifier_port'))
                        <span class="Error">{{ $errors->first('votifier_port') }}</span>
                    @endif
                </div>

                <div class="Row form-group @if ($errors->has('votifier_key')) has-error @endif">
                    <label for="votifier_key">{{ trans('view.votifier_key_label') }}</label>

                    <textarea name="votifier_key" id="votifier_key" rows="7"
                              class="form-control">{{ (Input::old('votifier_key')) ? Input::old('votifier_key') : $server->votifier_key }}</textarea>
                    @if ($errors->has('votifier_key'))
                        <span class="help-block">{{ $errors->first('votifier_key') }}</span>
                    @endif
                </div>

                <!-- Button -->
                <p class="Row">
                    <button type="submit" class="Btn Primary" id="register-server">Редактировать сервер</button>
                    {{ Form::token() }}
                </p>
            </form>
        </div>
    </section>
@stop


































@section('asdf')
    <div class="container reduce-top">
        <div class="page-header">
            <ol class="breadcrumb">
                <li><a href="{{ URL::route('home') }}">{{ trans('view.home_breadcrumb') }}</a></li>
                <li><a href="{{ URL::route('servers-manage') }}">{{ trans('view.your_servers_breadcrumb') }}</a></li>
                <li class="active">{{ trans('view.edit_your_server_breadcrumb') }}</li>
            </ol>
            <h1>{{ trans('view.edit_your_server') }}</h1>
        </div>

        <div class="page-header">
            <h3>{{ trans('view.basic_informations') }}</h3>
        </div>

        <form role="form" action="{{ URL::route('server-edit-post', $id) }}" method="post" class="bottom-space"
              enctype="multipart/form-data">
            <div class="form-group @if ($errors->has('name')) has-error @endif">
                <label for="name">{{ trans('view.server_name_label') }} <span class="required">*</span></label>
                <input type="text" name="name" class="form-control" id="name"
                       placeholder="{{ trans('view.server_name_help') }}"{{ (Input::old('name')) ? ' value="' . Input::old('name') . '"' : ' value="' . $server->name . '"' }}>
                @if ($errors->has('name'))
                    <span class="help-block">{{ $errors->first('name') }} </span>
                @endif
            </div>

            <div class="form-group">
                <label for="description">{{ trans('view.description_label') }}</label>
                <textarea maxlength="4500" name="description" id="description" rows="12"
                          class="form-control">{{ (Input::old('description')) ? Input::old('description') : $server->description }}</textarea>
                <p class="help-block"><i class="glyphicon glyphicon-info-sign"></i> {{ trans('view.description_help') }}
                </p>
            </div>

            <div class="form-group">
                <label for="banner">{{ trans('view.banner_label') }}</label>
                <input type="file" name="banner" id="banner">
                <p class="help-block"><i class="glyphicon glyphicon-info-sign"></i> {{ trans('view.banner_help') }}</p>
                @if ($errors->has('banner'))
                    <span class="help-block"><div class="error-val">{{ $errors->first('banner') }}</div></span>
                @endif
            </div>

            <div class="form-group">
                <div class="well">
                    <label for="address">{{ trans('view.server_address_port_label') }}</label>
                    <div class="row">
                        <div class="col-xs-5">
                            <input type="text" placeholder="{{ trans('view.server_address_input') }}"
                                   {{ (Input::old('address')) ? ' value="' . Input::old('address') . '"' : ' value="' . $server->address . '"' }} name="address"
                                   id="address" class="form-control">
                            @if ($errors->has('address'))
                                <span class="help-block"><div
                                            class="error-val">{{ $errors->first('address') }}</div></span>
                            @endif
                        </div>
                        <div class="col-xs-2">
                            <input type="text" placeholder="{{ trans('view.server_port_input') }}" name="port" id="port"
                                   value="{{ $server->port }}" class="form-control">
                            @if ($errors->has('port'))
                                <span class="help-block"><div
                                            class="error-val">{{ $errors->first('port') }}</div></span>
                            @endif
                        </div>
                        <div class="col-xs-5">
                            <div id="address_notice"></div>
                        </div>
                    </div>
                    <p class="help-block"><i
                                class="glyphicon glyphicon-info-sign"></i> {{ trans('view.add_server_port_help') }}</p>
                </div>
            </div>

            <div class="form-group @if ($errors->has('website')) has-error @endif">
                <label for="name">{{ trans('view.website_label') }}</label>
                <input type="text" name="website" class="form-control" id="website"
                       placeholder="{{ trans('view.add_server_website_input') }}"{{ (Input::old('website')) ? ' value="' . Input::old('website') . '"' : ' value="' . $server->website . '"' }}>
                @if ($errors->has('website'))
                    <span class="help-block">{{ $errors->first('website') }}</span>
                @endif
            </div>

            <div class="form-group @if ($errors->has('country')) has-error @endif">
                <label for="country">{{ trans('view.country_label') }}</label>
                <select name="country" id="country" class="form-control">
                    @foreach ($countries as $country)
                        <option value="{{ $country->id }}"{{ ($country->id == $server->country_id) ? ' selected' : '' }}>{{ $country->name }}</option>
                    @endforeach
                </select>
                @if ($errors->has('country'))
                    <span class="help-block">{{ $errors->first('country') }}</span>
                @endif
            </div>

            <div class="form-group @if ($errors->has('version')) has-error @endif">
                <label for="version">{{ trans('view.minecraft_version_label') }}</label>
                <select name="version" id="version" class="form-control">
                    @foreach ($versions as $version)
                        <option value="{{ $version->id }}"{{ ($version->id == $server->version_id) ? ' selected' : '' }}>{{ $version->name }}</option>
                    @endforeach
                </select>
                @if ($errors->has('version'))
                    <span class="help-block">{{ $errors->first('version') }}</span>
                @endif
            </div>

            <div class="control-group">
                <label for="select-tags">{{ trans('view.tags_label') }}</label>
                <select id="select-tags" name="tags[]" multiple style="width:50%" placeholder="Select a tag...">
                    @foreach ($tags as $tag)
                        <option value="{{ $tag->id }}"
                                @if ($object->haveTag($server->id, $tag->id) == true) selected @endif>{{ $tag->name }}</option>
                    @endforeach
                </select>
                <span class="help-block">{{ trans('view.tags_help', array('max' => Config::get('app.allowed_tags'))) }}</span>
            </div>

            <div class="page-header">
                <h3>{{ trans('view.votifier') }}</h3>
            </div>
            <p>{{ trans('view.votifier_help') }}</p>

            <div class="row">
                <div class="col-xs-3">
                    <label for="votifier_active">{{ trans('view.use_votifier_label') }}</label>
                </div>
                <div class="col-xs-2"><input type="checkbox" onclick="votifier();" value="1" id="votifier_active"
                                             name="votifier_active"
                                             @if ($errors->has('votifier_key') || $server->votifier_active == 'true') checked @endif>
                </div>
            </div>

            <div class="form-group @if ($errors->has('votifier_address')) has-error @endif">
                <label for="votifier_address">{{ trans('view.votifier_address_label') }}</label>
                <div class="row">
                    <div class="col-xs-5">
                        <input type="text" value="" name="votifier_address"
                               {{ (Input::old('votifier_address')) ? ' value="' . Input::old('votifier_address') . '"' : ' value="' . $server->votifier_address . '"' }} id="votifier_address"
                               class="form-control">
                        @if ($errors->has('votifier_address'))
                            <span class="help-block">{{ $errors->first('votifier_address') }}</span>
                        @endif
                    </div>
                </div>
                <p class="help-block"><i class="fa fa-info-circle"></i> {{ $errors->first('votifier_address') }}</p>
            </div>

            <div class="form-group @if ($errors->has('votifier_port')) has-error @endif">
                <label for="votifier_port">{{ trans('view.votifier_port_label') }}</label>
                <div class="row">
                    <div class="col-xs-3">
                        <input type="text" value="" placeholder="{{ trans('view.votifier_port_input') }}"
                               name="votifier_port" id="votifier_port" value="{{ $server->votifier_port}}"
                               class="form-control">
                        @if ($errors->has('votifier_port'))
                            <span class="help-block">{{ $errors->first('votifier_port') }}</span>
                        @endif
                    </div>
                </div>
            </div>

            <div class="form-group @if ($errors->has('votifier_key')) has-error @endif">
                <label for="votifier_key">{{ trans('view.votifier_key_label') }}</label>
                <div class="row">
                    <div class="col-xs-8">
                            <textarea name="votifier_key" id="votifier_key" rows="7"
                                      class="form-control">{{ (Input::old('votifier_key')) ? Input::old('votifier_key') : '' . $server->votifier_key . '' }}</textarea>
                        @if ($errors->has('votifier_key'))
                            <span class="help-block">{{ $errors->first('votifier_key') }}</span>
                        @endif
                    </div>
                    <div class="col-xs-4"></div>
                </div>
            </div>


            <div class="page-header">
                <h3>{{ trans('view.additional_information') }}</h3>
            </div>

            <div class="form-group">
                <label for="video">{{ trans('view.youtube_video_label') }}</label>
                <input type="text" name="youtube" class="form-control"
                       id="video" {{ (Input::old('youtube')) ? ' value="' . Input::old('youtube') . '"' : ' value="' . $server->youtube . '"' }}>
                @if ($errors->has('youtube'))
                    <span class="help-block"><div class="error-val">{{ $errors->first('youtube') }}</div></span>
                @endif
                <p class="help-block"><i
                            class="glyphicon glyphicon-info-sign"></i> {{ trans('view.youtube_video_help') }}</p>
            </div>
            <button type="submit" class="btn btn-default">{{ trans('view.update_server_button') }}</button>
            {{ Form::token() }}
        </form>

    </div>
@stop

@section('js')
    <script src="{{ asset('assets/js/selectize.js') }}"></script>
    <script>
        $('#select-tags').selectize();
        $('#select-games').selectize();
        $('#select-modes').selectize();
    </script>
@stop