@extends('layouts.base')

@section('content')
    <div class="container reduce-top Card">

        <form action="{{ URL::route('server-vote-post', $id) }}" method="post">
            <div class="row">
                <div class="col-md-8">
                    <div class="panel panel-primary">
                        @if(!$already_voted)
                            <header class="Card-Heading">
                                <h2>{{ trans('view.vote_for', array('server' => $server->name)) }}</h2>
                            </header>

                            <div class="panel-body Card-Content Form">
                                @if ($server->votifier_active == 'true') <span
                                        class="help-block text-center">{{ trans('view.support_votifier') }}</span> @endif
                                <div class="Row form-group @if ($errors->has('username')) has-error @endif">
                                    <label for="captcha">Имя пользователя minecraft</label>
                                    <input type="text" name="username" class="form-control"
                                           placeholder="username">
                                    @if ($errors->has('username'))
                                        <span class="help-block">{{ $errors->first('username') }} </span>
                                    @endif
                                </div>

                                <div class="Row form-group">
                                    <label for="captcha">{{ trans('view.captcha_label') }}</label>
                                    {{ Form::captcha(array('template' => 'captcha')) }}
                                    @if ($errors->has('recaptcha_response_field'))
                                        <span class="help-block"><div
                                                    class="error-val">{{ $errors->first('recaptcha_response_field') }}</div></span>
                                    @endif
                                </div>

                                <button type="submit"
                                        class="Btn Primary btn btn-success btn-lg center-block">{{ trans('view.vote_server') }}</button>
                                {{ Form::token() }}
                            </div>
                        @else
                            <header class="Card-Heading">
                                <h2 style="color:red;">Вы уже голосовали сегодня за этот сервер!</h2>
                            </header>
                        @endif
                    </div>
                </div>
                @if(count($votes)>0)
                    <div class="col-md-4 Card-Content">
                        <div class="panel panel-danger">
                            <h2 class="panel-title">Список голосующих</h2>

                            <div class="Servers-List">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th width="80%">Nickname</th>
                                        <th width="20%">Голоса</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($votes as $vote)
                                        <tr>
                                            <td>{{ $vote->mc_username }}</td>
                                            <td>{{ $vote->votes }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <a href="{{URL::route('server-page',$server->id)}}" style='margin-top: 10rem'
                           class="Btn Primary">Перейти
                            к серверу "{{$server->name}}"</a>
                    </div>
                @endif
            </div>
        </form>
    </div>
@stop