<script> var RecaptchaOptions = {theme: 'clean'};</script>
<script src="//www.google.com/recaptcha/api/challenge?k=<?php echo $public_key ?><?php echo(isset($lang) ? '&hl=' . $lang : '') ?>"></script>
<noscript>
    <iframe src="//www.google.com/recaptcha/api/noscript?k=<?php echo $public_key ?><?php echo(isset($lang) ? '&hl=' . $lang : '') ?>"
            height="300" width="100%" frameborder="0"></iframe>
    <br>
    <textarea class="form-control" name="recaptcha_challenge_field" rows="3" cols="50"></textarea>
    <input type="hidden" name="recaptcha_response_field" value="manual_challenge">
</noscript>