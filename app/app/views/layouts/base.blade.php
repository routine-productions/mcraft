<!DOCTYPE html>
<html lang='ru'>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/favicon.ico">
    <link rel="stylesheet" href="/assets/css/index.min.css" type="text/css"/>
    <title>@if (isset($title)) {{{ $title }}} @endif</title>

    <meta name="unitpay-verification" content="f6a0809b7f72d3310dc58ff04b5fad" />
    <meta name="yandex-verification" content="2d145494b3c96fb1" />


    @if (isset($meta_description))
        <meta name="description" content="{{{ $meta_description }}}">
    @endif
    @if (isset($meta_keywords))
        <meta name="keywords" content="{{{ $meta_keywords }}}">
    @endif


    @yield('meta')
    @yield('head')

    <script>var fullUrl = "{{ URL::route('home') }}";</script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    {{ Config::get('app.tracking') }}
</head>

<body>
<?php require_once ROOT_DIR . '/assets/img/sprite.svg';?>

<header>
    @include('modules/navbar')
</header>


<main>
    <section class="Main-Content">
        <div class="Wrapper">
            @if(Session::has('global'))
                <div class="Global-Message">
                    <div class="Global-Message-Type-{{ (Session::has('type')) ? '' . Session::get('type') . '' : 'danger' }}">{{ Session::get('global') }}</div>
                </div>
            @endif

            @yield('global')
            @yield('content')
            @yield('ads_bottom')
        </div>
    </section>
</main>


@include('modules/footer')


@include('popups/auth')

@if(isset($type_diamond) && isset($server))
    @include('popups/banner')
    @include('popups/diamond')
    @include('popups/color')
    @include('popups/pay')
@endif


<script src="/assets/js/index.min.js"></script>
@yield('js')

{{--<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57c94a94b088bbd3"></script>--}}

</body>
</html>