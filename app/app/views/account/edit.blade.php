@extends('layouts.base')

@section('content')

    <section class="Card">
        <header class="Card-Heading">
            <h2>{{ trans('view.update_profile') }}</h2>
        </header>
        <div class="Card-Content">
            <form role="form" method="post" action="{{ URL::route('edit-post') }}">

                <p class="Row">
                    <label for="username">Имя пользователя</label>
                    <input type="text" id="username" placeholder="{{ trans('view.website_input') }}"
                           value="{{ $user['username'] }}" readonly>
                </p>

                <p class="Row">
                    <label for="email">Email</label>
                    <input type="text" id="email" placeholder="{{ trans('view.website_input') }}"
                           value="{{ $user['email'] }}" readonly>
                </p>

                <p class="Row">
                    <label for="website">{{ trans('view.website_label') }}</label>
                    <input type="text" name="website" id="website" placeholder="{{ trans('view.website_input') }}"
                           value="{{ $user['website'] }}">
                    @if ($errors->has('website'))
                        <span class="help-block">{{ $errors->first('website') }} </span>
                    @endif
                </p>

                <p class="Row">
                    <label for="facebook">{{ trans('view.facebook_page_label') }}</label>
                    <input type="text" name="facebook" class="form-control" id="facebook"
                           placeholder="{{ trans('view.facebook_page_input') }}" value="{{ $user['facebook_page'] }}">
                    @if ($errors->has('facebook'))
                        <span class="help-block">{{ $errors->first('facebook') }} </span>
                    @endif
                </p>

                <p class="Row">
                    <label for="Google">{{ trans('view.google_page_label') }}</label>
                    <input type="text" name="google" id="Google" placeholder="{{ trans('view.google_page_input') }}"
                           value="{{ $user['google_page'] }}">
                    @if ($errors->has('google'))
                        <span class="help-block">{{ $errors->first('google') }} </span>
                    @endif
                </p>

                <p class="Row">
                    <label for="youtube">{{ trans('view.youtube_url_label') }}</label>
                    <input type="text" name="youtube" id="youtube" placeholder="{{ trans('view.youtube_url_input') }}"
                           value="{{ $user['youtube_url'] }}">
                    @if ($errors->has('youtube'))
                        <span class="help-block">{{ $errors->first('youtube') }} </span>
                    @endif
                </p>

                <p class="Row">
                    <label for="steam">{{ trans('view.steam_url_label') }}</label>
                    <input type="text" name="steam" id="steam" placeholder="{{ trans('view.steam_url_input') }}"
                           value="{{ $user['steam_url'] }}">
                    @if ($errors->has('steam'))
                        <span class="help-block">{{ $errors->first('steam') }} </span>
                    @endif
                </p>

                <p class="Row">
                    <label for="twitter">{{ trans('view.twitter_username_label') }}</label>
                    <input type="text" name="twitter" id="twitter"
                           placeholder="{{ trans('view.twitter_username_input') }}"
                           value="{{ $user['twitter_username'] }}">
                    @if ($errors->has('twitter'))
                        <span class="help-block">{{ $errors->first('twitter') }} </span>
                    @endif
                </p>

                <p class="Row">
                    <label for="skype">{{ trans('view.skype_username_label') }}</label>
                    <input type="text" name="skype" id="skype" placeholder="{{ trans('view.skype_username_input') }}"
                           value="{{ $user['skype_username'] }}">
                </p>

                <p class="Row">
                    <label for="minecraft">{{ trans('view.minecraft_username_label') }}</label>
                    <input type="text" name="minecraft" class="form-control" id="minecraft"
                           placeholder="{{ trans('view.minecraft_username_input') }}"
                           value="{{ $user['minecraft_username'] }}">
                </p>

                <p class="Row">
                    <button type="submit" class="Btn Primary">{{ trans('view.update_profile_button') }}</button>
                </p>

                {{ Form::token() }}
            </form>
        </div>
    </section>
@stop