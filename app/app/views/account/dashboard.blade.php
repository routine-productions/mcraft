@extends('layouts.base')

@section('content')
    <section class="Card">
        <header class="Card-Heading">
            <h2>Панель управления</h2>
        </header>
        <div class="Card-Content">

            <div class="Cabinet-Links">
                <h3>Управление серверами</h3>
                <a class="Btn Primary"
                   href="{{ URL::route('server-add') }}">{{ trans('view.register_server_button') }}</a>
                <a class="Btn Primary"
                   href="{{ URL::route('servers-manage') }}">{{ trans('view.manage_servers_button') }}</a>
            </div>

            <div class="Cabinet-Links">
                <h3>Управление акаунтом</h3>
                <a class="Btn Primary"
                   href="{{ URL::route('profile', Auth::user()->username) }}">{{ trans('view.your_profile_button') }}</a>
                <a class="Btn Primary" href="{{ URL::route('edit') }}">{{ trans('view.edit_account_button') }}</a>
                <a class="Btn Primary" href="{{ URL::route('logout') }}">{{ trans('view.logout_button') }}</a>
            </div>
        </div>
    </section>
@stop