@extends('layouts.base')

@section('content')
    <section class="Card">
        <header class="Card-Heading">
            <h2 class="Heading-Secondary">Смена пароля</h2>
        </header>
        <div class="Card-Content">
            <form role="form" method="post" action="{{ URL::route('change-password-post') }}">
                <p class="Row">
                    <label for="old_password">Старый пароль</label>
                    <input id="old_password" name="old_password" type="password" autocomplete="off">
                    @if ($errors->has('old_password'))
                        <span class="Error">{{ $errors->first('old_password') }}</span>
                    @endif
                </p>
                <p class="Row">
                    <label for="password">Новый пароль</label>
                    <input id="password" name="password" type="password" autocomplete="off">
                    @if ($errors->has('password'))
                        <span class="Error">{{ $errors->first('password') }}</span>
                    @endif
                </p>
                <p class="Row">
                    <label for="password_again">Новый пароль ещё раз</label>
                    <input id="password_again" name="password_again" type="password" autocomplete="off">
                    @if ($errors->has('password_again'))
                        <span class="Error">{{ $errors->first('password_again') }}</span>
                    @endif
                </p>
                <p class="Row">
                    <button type="submit" class="Btn Primary">Сменить пароль</button>
                </p>
                {{ Form::token() }}
            </form>
        </div>
    </section>
@stop