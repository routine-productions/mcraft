@extends('layouts.base')

@section('content')
		<div class="container reduce-top">
			<div class="page-header">
				<ol class="breadcrumb">
					<li><a href="{{ URL::route('home') }}">{{ trans('view.home_breadcrumb') }}</a></li>
					<li class="active">{{ trans('view.forgot_password_breadcrumb') }}</li>
				</ol>
				<h1>{{ trans('view.forgot_password') }}</h1>
			</div>
			
			<div class="col-md-4">
				<div class="row">
					<form role="form" method="post" action="{{ URL::route('forgot-password-post') }}" class="bottom-space">
						<div class="form-group @if ($errors->has('email')) has-error @endif">
							<label for="email">{{ trans('view.email_label') }}</label>
							<input type="text" name="email" class="form-control" id="email" placeholder="{{ trans('view.email_input') }}"{{ (Input::old('email')) ? ' value="' . Input::old('email') . '"' : '' }}>
							@if ($errors->has('email'))
			    				<span class="help-block">{{ $errors->first('email') }} </span>
			    			@endif
						</div>
						<button type="submit" class="btn btn-default">{{ trans('view.recover_button') }}</button>
						{{ Form::token() }}
					</form>
				</div>
			</div>
		</div>
@stop