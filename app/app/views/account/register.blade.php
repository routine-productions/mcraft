@extends('layouts.base')

@section('content')
		<div class="container reduce-top">
			<div class="page-header">
				<ol class="breadcrumb">
					<li><a href="{{ URL::route('home') }}">{{ trans('view.home_breadcrumb') }}</a></li>
					<li class="active">{{ trans('view.register_breadcrumb') }}</li>
				</ol>
				<h1>{{ trans('view.register') }}</h1>
			</div>
			<div class="col-md-4">
				<div class="row">
					<form role="form" method="post" action="{{ URL::route('register-post') }}" class="bottom-space">
						<div class="form-group @if ($errors->has('username')) has-error @endif">
							<label for="username">{{ trans('view.username_label') }}</label>
							<input type="text" name="username" class="form-control" id="username" placeholder="{{ trans('view.username_input') }}"{{ (Input::old('username')) ? ' value="' . Input::old('username') . '"' : '' }}>
							@if ($errors->has('username'))
			    				<span class="help-block">{{ $errors->first('username') }} </span>
			    			@endif
						</div>
						<div class="form-group @if ($errors->has('email')) has-error @endif">
							<label for="email">{{ trans('view.email_label') }}</label>
							<input type="text" name="email" class="form-control" id="email" placeholder="{{ trans('view.email_input_register') }}"{{ (Input::old('email')) ? ' value="' . Input::old('email') . '"' : '' }}>
							@if ($errors->has('email'))
			    				<span class="help-block">{{ $errors->first('email') }} </span>
			    			@endif
						</div>
						<div class="form-group @if ($errors->has('password')) has-error @endif">
							<label for="password">{{ trans('view.password_label') }}</label>
							<input type="password" name="password" class="form-control" id="password">
							@if ($errors->has('password'))
			    				<span class="help-block">{{ $errors->first('password') }} </span>
			    			@endif
						</div>
						<div class="form-group @if ($errors->has('confirm')) has-error @endif">
							<label for="confirmPassword">{{ trans('view.confirm_password_label') }}</label>
							<input type="password" name="confirm" class="form-control" id="confirmPassword">
							@if ($errors->has('confirm'))
			    				<span class="help-block">{{ $errors->first('confirm') }} </span>
			    			@endif
						</div>
						<div class="form-group @if ($errors->has('recaptcha_response_field')) has-error @endif">
							<label for="captcha">{{ trans('view.captcha_label') }}</label>
							{{ Form::captcha(array('template' => 'captcha')) }}
							@if ($errors->has('recaptcha_response_field'))
			    				<span class="help-block">{{ $errors->first('recaptcha_response_field') }} </span>
			    			@endif
						</div>
						<button type="submit" class="btn btn-default">{{ trans('view.register_button') }}</button>
						{{ Form::token() }}
					</form>
				</div>
			</div>

		</div>
@stop