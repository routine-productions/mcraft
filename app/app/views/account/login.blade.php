@extends('layouts.base')

@section('content')
		<div class="container reduce-top">
			<div class="page-header">
				<ol class="breadcrumb">
					<li><a href="{{ URL::route('home') }}">{{ trans('view.home_breadcrumb') }}</a></li>
					<li class="active">{{ trans('view.login_breadcrumb') }}</li>
				</ol>
				<h1>{{ trans('view.login') }}</h1>
			</div>
			<div class="col-md-4">
				<div class="row">
					<form role="form" method="post" action="{{ URL::route('login-post') }}" class="bottom-space">
						<div class="form-group @if ($errors->has('email')) has-error @endif">
							<label for="email">{{ trans('view.email_label') }}</label>
							<input type="text" name="email" class="form-control" id="email" placeholder="{{ trans('view.email_input') }}">
							@if ($errors->has('email'))
			    				<span class="help-block">{{ $errors->first('email') }} </span>
			    			@endif
						</div>
						<div class="form-group @if ($errors->has('password')) has-error @endif">
							<label for="password">{{ trans('view.password_label') }}</label>
							<input type="password" name="password" class="form-control" id="password" placeholder="{{ trans('view.password_input') }}">
							@if ($errors->has('password'))
			    				<span class="help-block">{{ $errors->first('password') }} </span>
			    			@endif
						</div>
						<button type="submit" class="btn btn-default">{{ trans('view.login_button') }}</button>
						{{ Form::token() }}
					</form>
				</div>
			</div>
		</div>
@stop