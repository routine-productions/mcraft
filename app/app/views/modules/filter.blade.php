<div class="Filter-Menu">
    <div class="Wrapper">

        <!-- Versions -->
        <div class="Filter-List">
            <span class="Filter-Title">Версия сервера</span>
            <ul>
                @if(isset($versions))
                    @foreach($versions as $version)
                        @if(isset($filter_version))
                            @if(!isset($version->active))
                                <li>
                                    <a href="/servera-{{$version->slug}}{{$filter_tags}}{{$filter_games}}{{$filter_modes}}">{{$version->name}}</a>
                                </li>
                            @else
                                <li class="Active">
                                    <a href="/servera{{$filter_tags}}{{$filter_games}}{{$filter_modes}}">{{$version->name}}</a>
                                </li>
                            @endif
                        @else
                            <li><a href="/servera-{{$version->slug}}">{{$version->name}}</a></li>
                        @endif
                    @endforeach
                @endif
            </ul>
        </div>

        <!-- Tags -->
        <div class="Filter-List">
            <span class="Filter-Title">Теги</span>
            <ul>
                @if(isset($tags))
                    @foreach($tags as $tag)
                        @if(isset($filter_version))
                            @if(!isset($tag->active))
                                <li>
                                    <a href="/servera{{$filter_version}}{{$filter_tags}}-{{$tag->slug}}{{$filter_games}}{{$filter_modes}}">{{$tag->name}}</a>
                                </li>
                            @else
                                <?php
                                $temp = $types['tags'];
                                $temp_tags = '';

                                foreach ($temp as $filter_key => &$filter_item) {
                                    if ($filter_item->slug == $tag->slug) {
                                        unset($temp[$filter_key]);
                                    }
                                }

                                foreach ($temp as $Item) {
                                    $temp_tags .= '-' . $Item->slug;
                                }
                                ?>
                                <li class="Active">
                                    <a href="/servera{{$filter_version}}{{$temp_tags}}{{$filter_games}}{{$filter_modes}}">{{$tag->name}}</a>
                                </li>
                            @endif
                        @else
                            <li><a href="/servera-{{$tag->slug}}">{{$tag->name}}</a></li>
                        @endif
                    @endforeach
                @endif
            </ul>
        </div>


        <!-- Modes -->
        <div class="Filter-List">
            <span class="Filter-Title">Моды</span>
            <ul>
                @if(isset($modes))
                    @foreach($modes as $mode)
                        @if(isset($filter_version))
                            @if(!isset($mode->active))
                                <li>
                                    <a href="/servera{{$filter_version}}{{$filter_tags}}{{$filter_games}}{{$filter_modes}}-{{$mode->slug}}">{{$mode->name}}</a>
                                </li>
                            @else
                                <?php
                                $temp = $types['modes'];
                                $temp_modes = '';
                                foreach ($temp as $filter_key => &$filter_item) {
                                    if ($filter_item->slug == $mode->slug) {
                                        unset($temp[$filter_key]);
                                    }
                                }

                                foreach ($temp as $Item) {
                                    $temp_modes .= '-' . $Item->slug;
                                }
                                ?>
                                <li class="Active">
                                    <a href="/servera{{$filter_version}}{{$filter_tags}}{{$filter_games}}{{$temp_modes}}">{{$mode->name}}</a>
                                </li>
                            @endif
                        @else
                            <li><a href="/servera-{{$mode->slug}}">{{$mode->name}}</a></li>
                        @endif
                    @endforeach
                @endif
            </ul>
        </div>

        <!-- Tags -->
        <div class="Filter-List">
            <span class="Filter-Title">Мини игры</span>
            <ul>
                @if(isset($games))
                    @foreach($games as $game)
                        @if(isset($filter_version))
                            @if(!isset($game->active))
                                <li>
                                    <a href="/servera{{$filter_version}}{{$filter_tags}}{{$filter_games}}-{{$game->slug}}{{$filter_modes}}">{{$game->name}}</a>
                                </li>
                            @else
                                <?php
                                $temp = $types['games'];
                                $temp_games = '';
                                foreach ($temp as $filter_key => &$filter_item) {
                                    if ($filter_item->slug == $game->slug) {
                                        unset($temp[$filter_key]);
                                    }
                                }

                                foreach ($temp as $Item) {
                                    $temp_games .= '-' . $Item->slug;
                                }
                                ?>
                                <li class="Active">
                                    <a href="/servera{{$filter_version}}{{$filter_tags}}{{$temp_games}}{{$filter_modes}}">{{$game->name}}</a>
                                </li>
                            @endif
                        @else
                            <li><a href="/servera-{{$game->slug}}">{{$game->name}}</a></li>
                        @endif
                    @endforeach
                @endif
            </ul>
        </div>
    </div>
</div>