



<div class="navbar navbar-default navbar-static-top" style="margin-bottom: 0;" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ URL::route('home') }}">{{ Config::get('app.sitename') }}</a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                @if (Auth::check())
                    @if (Auth::user()->isMember() || Auth::user()->isAdmin())
                        <li><a href="{{ URL::route('server-add') }}">{{ trans('view.add_server_nav') }}</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ trans('view.dashboard_nav') }}
                                <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="{{ URL::route('dashboard') }}">{{ trans('view.your_dashboard_nav') }}</a>
                                </li>

                                <li class="divider"></li>
                                <li>
                                    <a href="{{ URL::route('servers-manage') }}">{{ trans('view.manage_servers_nav') }}</a>
                                </li>

                                <li class="divider"></li>
                                <li>
                                    <a href="{{ URL::route('profile', Auth::user()->username) }}">{{ trans('view.your_profile_nav') }}</a>
                                </li>
                                <li><a href="{{ URL::route('edit') }}">{{ trans('view.edit_account_nav') }}</a></li>
                                <li>
                                    <a href="{{ URL::route('change-password') }}">{{ trans('view.change_password_nav') }}</a>
                                </li>
                                <li><a href="{{ URL::route('logout') }}">{{ trans('view.logout_nav') }}</a></li>
                            </ul>
                        </li>
                    @endif

                    @if (Auth::user()->isAdmin())
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ trans('view.admin_nav') }} <b
                                        class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="{{ URL::route('admin-dashboard') }}">{{ trans('view.dashboard_nav') }}</a>
                                </li>
                            </ul>
                        </li>
                    @endif
                @else
                    <li><a href="{{ URL::route('server-add') }}">{{ trans('view.add_server_nav') }}</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle"
                           data-toggle="dropdown">{{ trans('view.login_register_nav') }} <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ URL::route('register') }}">{{ trans('view.register_nav') }}</a></li>
                            <li><a href="{{ URL::route('login') }}">{{ trans('view.login_nav') }}</a></li>
                            <li>
                                <a href="{{ URL::route('forgot-password') }}">{{ trans('view.forgot_password_nav') }}</a>
                            </li>
                        </ul>
                    </li>
                @endif

                <li><a href="{{ URL::route('vip') }}">{{ trans('view.vip') }}</a></li>
                <li><a href="{{ URL::route('contact') }}">{{ trans('view.contact_nav') }}</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <ul style="line-height: 50px;" class="social">
                    @if (Config::get('app.facebook_link'))
                        <li><a target="_blank" href="{{ Config::get('app.facebook_link') }}"><img width="32" height="32"
                                                                                                  border="0"
                                                                                                  style="vertical-align:middle"
                                                                                                  alt="Facebook"
                                                                                                  title="Facebook"
                                                                                                  src="{{ asset('assets/images/social/facebook.png') }}"></a>
                        </li> @endif
                    @if (Config::get('app.googleplus_link'))
                        <li><a target="_blank" href="{{ (Config::get('app.googleplus_link')) }}"><img width="32"
                                                                                                      height="32"
                                                                                                      border="0"
                                                                                                      style="vertical-align:middle"
                                                                                                      alt="Google+"
                                                                                                      title="Google+"
                                                                                                      src="{{ asset('assets/images/social/googleplus.png') }}"></a>
                        </li> @endif
                    @if (Config::get('app.twitter_link'))
                        <li><a target="_blank" href="{{ (Config::get('app.twitter_link')) }}"><img width="32"
                                                                                                   height="32"
                                                                                                   border="0"
                                                                                                   style="vertical-align:middle"
                                                                                                   alt="Twitter"
                                                                                                   title="Twitter"
                                                                                                   src="{{ asset('assets/images/social/twitter.png') }}"></a>
                        </li> @endif
                </ul>
            </ul>
        </div><!--/.nav-collapse -->

    </div>
</div>