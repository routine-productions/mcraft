<footer>
    <section class="Footer">
        <div class="Wrapper">
            <div class="Footer-Content">
                <div class="Footer-Block">
                    <h4 class="Footer-Title">Навигация</h4>
                    <ul class="Footer-List">
                        <li><a href="/faq">FAQ</a></li>
                        <li><a href="/contacts">Реклама и контакты</a></li>
                        <li><a href="/news">Блог и новости</a></li>
                        <li class="JS-Modal-Button" href="#Modal-1"><a>Вход / регистрация</a></li>
                    </ul>
                </div>
                <div class="Footer-Block">
                    <h4 class="Footer-Title">Сервера</h4>
                    <ul class="Footer-List">
                        <li><a href="/">Рейтинг серверов</a></li>
                        <li><a href="/new-servers">Новые сервера</a></li>
                        @if (Auth::check())
                            <li><a href="/server/add">Добавить сервер</a></li>
                        @else
                            <li class="JS-Modal-Button" href="#Modal-1"><a>Добавить сервер</a></li>
                        @endif
                        @if (Auth::check())
                            <li><a href="">Раскрутить сервер</a></li>
                        @else
                            <li class="JS-Modal-Button" href="#Modal-1"><a href="">Раскрутить сервер</a></li>
                        @endif
                    </ul>
                </div>

                <div class="Footer-Block">
                    <h4 class="Footer-Title">Последнее в блоге</h4>
                    <ul class="Footer-List">
                        <!--
                            //items[i]->attributes['title']
                            //items[i]->attributes['id']
                         -->
                        {{--<li><a href="/news/jhtjh">76iry6khyejtyjhny</a></li>--}}
                        @if(!empty($news))
                            @foreach($news as $item)
                                <li><a href="/news/{{$item->alias}}">{{$item->title}}</a></li>
                            @endforeach
                        @endif
                    </ul>
                </div>

                <div class="Footer-Block">
                    <h4 class="Footer-Title">Мы в соцсетях</h4>
                    <ul class="Footer-Socials">
                        <li>
                            <a href="">
                            <span>
                                <svg class="Vk-Logo">
                                    <use xlink:href="#vk-logo"></use>
                                </svg>
                            </span>
                            </a>
                        </li>
                        <li>
                            <a href="">
                            <span>
                                <svg class="Fb-Logo">
                                    <use xlink:href="#fb-logo"></use>
                                </svg>
                            </span>
                            </a>
                        </li>
                        <li>
                            <a href="">
                            <span>
                                <svg class="Ok-Logo">
                                    <use xlink:href="#ok-logo"></use>
                                </svg>
                            </span>
                            </a>
                        </li>
                        <li>
                            <a href="">
                            <span>
                                <svg class="Tw-Logo">
                                    <use xlink:href="#tw-logo"></use>
                                </svg>
                            </span>
                            </a>
                        </li>
                        <li>
                            <a href="">
                            <span>
                                 <svg class="Gp-Logo">
                                     <use xlink:href="#gp-logo"></use>
                                 </svg>
                            </span>
                            </a>
                        </li>
                    </ul>
                    <h4 class="Footer-Title">Счетчики</h4>
                    <ul class="Footer-Counter">
                        <li>
                            <img src="/assets/img/counter-01.png" alt="">
                        </li>
                        <li>
                            <img src="/assets/img/counter-02.gif" alt="">
                        </li>
                        <li>
                            <img src="/assets/img/counter-03.gif" alt="">
                        </li>
                        <li>
                            <img src="/assets/img/counter-04.gif" alt="">
                        </li>
                        <li>
                            <img src="/assets/img/counter-05.png" alt="">
                        </li>
                        <li>
                            <img src="/assets/img/counter-06.png" alt="">
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="Footer-Copyright">
            <div class="Wrapper">
                <span>&copy; Minecraft-Servers.ru, 2016 г.</span>
            </div>
        </div>
    </section>
</footer>