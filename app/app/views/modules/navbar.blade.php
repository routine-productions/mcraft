<section class="Header">
    <div class="Wrapper">
        <div class="Header-Menu">
            <div class="Header-Logo">
                <a href="/">
                    <h2>Minecraft<span>Rating</span></h2>
                </a>
            </div>

            <nav>
                <ul>
                    <li><a href="/new-servers">новые сервера</a></li>
                    <li><a href="/top">топ серверов</a></li>
                    <li><a href="/news">новости</a></li>
                    <li><a href="/contacts">реклама и контакты</a></li>

                    @if (!Auth::check())
                        <?php $_SESSION['log_in'] = false; ?>
                        <li class="JS-Modal-Button" href="#Modal-1">войти</li>
                    @endif

                    @if (Auth::check())
                        @if (Auth::user()->isMember() || Auth::user()->isAdmin())
                            <li class="Show-Hidden-Menu">
                                <em>учетная запись<span> &#9660;</span></em>
                                <ul class="Hidden-List">
                                    @if (Auth::user()->isAdmin())
                                        <li><a href="{{ URL::route('admin-dashboard') }}" target="_blank"
                                               class="Admin-Panel">админ панель</a></li>
                                    @endif

                                    <li><a href="{{ URL::route('servers-manage') }}">управление серверами</a></li>
                                    <li><a href="{{ URL::route('server-add') }}">добавить сервер</a></li>
                                    <li><a href="{{ URL::route('profile', Auth::user()->username) }}">профиль</a></li>
                                    <li><a href="{{ URL::route('edit') }}">настройки профиля</a></li>
                                    <li><a href="{{ URL::route('change-password') }}">сменить пароль</a></li>
                                    <li><a href="{{ URL::route('logout') }}">выход</a></li>
                                </ul>
                            </li>
                        @endif
                    @endif
                </ul>
            </nav>

            <button class="Header-Hamburger hamburger--slider hamburger" type="button">
                <span class="hamburger-box"><span class="hamburger-inner"></span></span>
            </button>
        </div>
    </div>

    <div class="Header-Filters">

        @if (Auth::check() && isset($_SESSION['log_in']) && $_SESSION['log_in'] == false)
                <article class="login_info"></article>
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
                <script type="text/javascript" src="/assets/js/loginInfo.js"></script>
            <?php $_SESSION['log_in'] = true; ?>
        @endif


        <div class="Filters-Nav">
            <div class="Wrapper">
                <div class="Show-Filters">

                    <div class="Filter-Block Left">
                        <div class="Filter-Button">
                            {{--<div class="Filter-Open">--}}
                                {{--<svg>--}}
                                    {{--<use xlink:href="#arrow-double"></use>--}}
                                {{--</svg>--}}
                                {{--<span>Показать дополнительные фильтры</span>--}}
                            {{--</div>--}}
                            {{--<div class="Filter-Close">--}}
                                {{--<svg>--}}
                                    {{--<use xlink:href="#delete"></use>--}}
                                {{--</svg>--}}
                                {{--<span>Скрыть дополнительные фильтры</span>--}}
                            {{--</div>--}}
                            <div class="Filter-Open">
                                <span>Дополнительные фильтры</span>
                            </div>
                        </div>
                    </div>

                    <div class="Filter-Block Right">
                        <div class="Header-Search">
                            <svg>
                                <use xlink:href="#magnifier"></use>
                            </svg>
                            <input type="text" placeholder="поиск по назвнию сервера"
                                   value="@if(isset($term)){{$term}}@endif">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('modules/filter')
    </div>
</section>
