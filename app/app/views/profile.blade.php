@extends('layouts.base')

@section('content')
    <article class="Card">
        <header class="Card-Heading">
            <h2>Профиль</h2>
        </header>

        <div class="Card-Content">
            <table class="Adaptive">
                <tbody>
                <tr>
                    <td>{{ trans('view.username_tbody') }}</td>
                    <td>{{ $user->username }}</td>
                </tr>
                <tr>
                    <td>{{ trans('view.registration_date_tbody') }}</td>
                    <td>{{ date('M dS, Y', strtotime($user->created_at)) }}</td>
                </tr>
                <tr>
                    <td>Количество серверов</td>
                    <td>{{ $user->total_servers }}</td>
                </tr>
                </tbody>
            </table>

            <a href="/account/edit" class="Btn Primary">Редактировать профиль</a>
        </div>
    </article>
@stop