@extends('layouts.base')

@section('content')
		<div class="container reduce-top">
			<div class="page-header">
				<h1>{{ trans('view.thank_you') }}</h1>
			</div>

			<div class="col-md-7">
				<div class="row">
					<p>{{ trans('view.paypal_response') }}<span id="dots"></span></p>
					<p><h3>@if (Input::get('state') == 'cancel') {{ trans('view.payment_canceled', array('url' => URL::route('vip'))) }}</a> @endif</h3></p>
				</div>
			</div>
		</div>
@stop

@section('js')
	<script>
		var dots = 0;
	    
		$(document).ready(function()
		{
		    setInterval (type, 600);
		});

		function type()
		{
		    if(dots < 3)
		    {
		        $('#dots').append('.');
		        dots++;
		    }
		    else
		    {
		        $('#dots').html('');
		        dots = 0;
		    }
		}

		@if (Input::get('state') == 'success' && Input::has('txn_id'))
			setTimeout(function () {
			   window.location.href = "{{ URL::route('view-payment', Input::get('txn_id')) }}";
			}, 5000);
		@endif
	</script>
@stop
