@extends('layouts.base')
@section('content')
    <article>
        <div class="Card-Wrapper ">
            <div class="Card-Head ">
                <h1 style="padding: 1rem">{{$h1}}</h1>
            </div>

            <div class="Card-Content">
                <div class="Card-Path">
                <p style="padding: 10px; margin-bottom: 20px;">{{$description}}</p>
                    <a href="/" class="Btn Primary Read-More">
                        <span>На главную</span>
                    </a>
                </div>
            </div>
        </div>
    </article>
@stop

