@extends('layouts.base')

@section('content')
		<div class="container reduce-top">
			<div class="page-header">
				<ol class="breadcrumb">
					<li><a href="{{ URL::route('home') }}">{{ trans('view.home_breadcrumb') }}</a></li>
					<li class="active">{{ trans('view.confirm_vip_breadcrumb') }}</li>
				</ol>
				<h1>{{ trans('view.confirm_vip') }}</h1>
			</div>

			<div class="col-md-5">
				<div class="row">
					<table style="background:white;" class="table table-bordered">
		  			<thead>
						<tr>
							<th>{{ trans('view.vip_server_thead') }}</th>
							<th>{{ trans('view.vip_days_thead') }}</th>
							<th>{{ trans('view.vip_end_date_thead') }}</th>
							<th>{{ trans('view.vip_price_thead') }}</th>
						</tr>
					</thead>
						<tbody>
							<tr>
							<td>{{ $server->address }}</td>
							<td>{{ $days }}</td>
							<td>{{ $end }}</td>
							<td>{{ Config::get('app.vip_currency') == 'USD' ? '$' : '€' }}{{ $days * 1 }}</td>
							</tr>
						</tbody>
					</table>	

					{{ $pay }}
				</div>
			</div>
		</div>
@stop
