@extends('layouts.base')

@section('content')
    <section class="Posts Card">
        <header class="Card-Heading">
            <h1 class="Posts-Header">Новости</h1>
        </header>


        @foreach($posts as $Post)
            <a href="{{URL::route('post', $Post->alias)}}" class="Posts-Item Card-Content">

                <img class="Posts-Image" src="{{$Post->img}}" alt="{{$Post->title}}">
                <div class="Posts-Info">
                    <h2 class="Posts-Title">{{$Post->title}}</h2>
                    <div class="Posts-Introtext" class>{{$Post->intro}}</div>
                    <div class="Posts-Created">{{date('Y.m.d h:i', time($Post->created_at))}}</div>
                </div>
            </a>
        @endforeach
    </section>
@stop