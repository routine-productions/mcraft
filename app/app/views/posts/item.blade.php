@extends('layouts.base')

@section('head')
    <script type="text/javascript" src="//vk.com/js/api/openapi.js?124"></script>
    <script type="text/javascript">
        VK.init({apiId: 5552319, onlyWidgets: true});
    </script>
@stop

@section('content')
    <section class="Post Card">
        <header class="Card-Heading">
            <h1 class="Post-Title">{{$Post->title}}</h1>
        </header>

        <div class="Card-Content">
            <img class="Post-Image" src="{{$Post->img}}" alt="{{$Post->title}}">
            <div class="Post-Info">
                <div class="Post-Fulltext">{{$Post->text}}</div>
                <div class="Post-Created">{{date('Y.m.d h:i', time($Post->created_at))}}</div>
                <div class="Post-Comments">
                    <h2>Отзывы</h2>
                    <div id="vk_comments"></div>
                    <script type="text/javascript">
                        VK.Widgets.Comments("vk_comments", {redesign: 1, limit: 10, width: "665", attach: "*"});
                    </script>
                </div>
            </div>
        </div>
    </section>
@stop