@extends('layouts.base')



@section('content')

    <section class="Card">
        <header class="Card-Heading">
            <h2>Реклама и контакты</h2>
        </header>
        <div class="Card-Content">

            <div class="Card-Text">
                {{ BBCode::parse(Config::get('app.text_adverts')) }}
            </div>
            <h2>Отправить сообщение в тех. поддержку</h2>

            <form action="{{ URL::route('contact-post') }}" method="post" class="bottom-space" role="form">
                <p class="Row">
                    <label for="subject">{{ trans('view.subject_label') }} </label>
                    <input type="text" name="subject" class="form-control" id="subject"
                           placeholder="{{ trans('view.subject_input') }}"{{ (Input::old('subject')) ? ' value="' . Input::old('subject') . '"' : '' }}>
                    @if ($errors->has('subject'))
                        <span class="Error">{{ $errors->first('subject') }} </span>
                    @endif
                </p>

                <p class="Row">
                    <label for="email">{{ trans('view.email_label') }} </label>
                    <input type="text" name="email" class="form-control" id="email"
                           placeholder="{{ trans('view.email_input') }}"{{ (Input::old('email')) ? ' value="' . Input::old('email') . '"' : '' }}>
                    @if ($errors->has('email'))
                        <span class="Error">{{ $errors->first('email') }} </span>
                    @endif
                </p>

                <p class="Row">
                    <label for="message">{{ trans('view.message_label') }}</label>
                        <textarea maxlength="4500" name="message" id="message" rows="12"
                                  class="form-control">{{ (Input::old('message')) ? Input::old('message') : '' }}</textarea>
                    @if ($errors->has('message'))
                        <span class="Error">{{ $errors->first('message') }} </span>
                    @endif
                </p>

                <button type="submit" class="Btn Primary">{{ trans('view.send_button') }}</button>
            </form>
        </div>
    </section>
@stop