<section class="JS-Modal-Blackout" id="Modal-1">
    <div class="Cabinet JS-Modal JS-Tabs">
        <div class="Cabinet-Head">
            <h3 class="Cabinet-Title">Личный кабинет</h3>
            <span class="Cabinet-Close JS-Modal-Close">×</span>
        </div>
        <ul class="Cabinet-Nav JS-Tabs-Navigation">
            <li class="JS-Tab Active" data-href="Tab-1">
                Вход
            </li>
            <li class="JS-Tab" data-href="Tab-2">
                Регистрация
            </li>
        </ul>

        <div class="JS-Tabs-Content">
            <div class="Cabinet-Content" data-tab='Tab-1'>
                <form action="">
                    <ul>
                        <li>
                            <label for="">Email</label>
                            <input type="text">
                        </li>
                        <li>
                            <label for="">Пароль</label>
                            <input type="text">
                        </li>
                    </ul>
                    <div class="Cabinet-Buttons">
                        <button class="Cabinet-Enter">
                            Войти
                        </button>
                        <div class="Restore-Password">
                            <a href="">
                                Восстановить пароль
                            </a>
                        </div>
                    </div>
                </form>
            </div>
            <div class="Cabinet-Content" data-tab='Tab-2'>
                <form action="">
                    <ul>
                        <li>
                            <label for="">Email</label>
                            <input type="text">
                        </li>
                        <li>
                            <label for="">Пароль</label>
                            <input type="text">
                        </li>
                    </ul>
                    <div class="Cabinet-Buttons">
                        <button class="Cabinet-Enter">
                            Войти
                        </button>
                        <div class="Restore-Password">
                            <a href="">
                                Восстановить пароль
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>