<ul class="Article-List">
    @for($l = 0; $l < 4; $l++)
        <li class="Card-Item">
            @include('temporary/card')
        </li>
    @endfor
</ul>

@include('temporary/pagenav')


<div class="Button-Up JS-Scroll-Top">
    <svg><use xlink:href="#arrow"></use></svg>
</div>