<article>
    <div class="Card-Wrapper">
        <div class="Card-Head">
            <div class="Card-Path">
                <span class="Card-Number"># 1</span>

                <h3 class="Card-Title"><a href="card_full">Pure Vanilla Server</a></h3>
            </div>
            <div class="Card-Path">
                <span class="Card-Version">1.9.1</span>

                <div class="Card-Users">
                    <svg>
                        <use xlink:href="#user"></use>
                    </svg>
                    <span>920 / 1000</span>
                </div>
                <div class="Card-Diamond">
                    <img src="/assets/img/Diamond.png" alt="">
                    <span>100</span>
                </div>

                <div class="Card-Liker">
                    <div class="Like-Down">
                        <svg>
                            <use xlink:href="#arrow"></use>
                        </svg>
                    </div>
                <span class="Like-Count">
                    1027
                </span>

                    <div class="Like-Up">
                        <svg>
                            <use xlink:href="#arrow"></use>
                        </svg>
                    </div>
                </div>
            </div>
        </div>
        <div class="Card-Content">
            <div class="Card-Path">
                <p class="Card-Description">
                    Вчера состоялось долгожданное открытие нового игрового сервера - TechnoMagic. Это новый сервер,
                    в
                    котором мы объединили магические и индустриальные модификации.
                </p>
                <a href="card_full" class="Btn Primary Read-More">
                    <span>Подробнее</span>
                    <svg>
                        <use xlink:href="#arrow-direct"></use>
                    </svg>
                </a>
            </div>
            <div class="Card-Path">
                <a class="Card-Advertising" href="">
                    <img src="/assets/img/advertising.jpg" alt="">
                </a>
                <div class="Card-Tags">
                    <svg class="Tags-Logo">
                        <use xlink:href="#tags"></use>
                    </svg>
                    <ul>
                        <?php for ($i = 0; $i < 10; $i++) { ?>
                            <li>
                                <a href="">Bedwars</a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</article>