<nav class="Pagenav">
    <span class="Pagenav-Beginning">
        <a href="">
            <svg>
                <use xlink:href="#arrow-double"></use>
            </svg>
        </a>
    </span>
    <span class="Step-Prev">
        <a href="">
            <svg>
                <use xlink:href="#arrow"></use>
            </svg>
        </a>
    </span>
    <ul>
        @for ($k = 1; $k < 9; $k++)
            <li>
                <a href=""><?= $k ?></a>
            </li>
        @endfor
    </ul>
    <span class="Step-Next">
        <a href="">
            <svg>
                <use xlink:href="#arrow"></use>
            </svg>
        </a>
    </span>
     <span class="Pagenav-End">
         <a href="">
             <svg>
                 <use xlink:href="#arrow-double"></use>
             </svg>
         </a>
    </span>
</nav>