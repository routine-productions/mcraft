@extends('layouts.base')

@section('content')
		<div class="container reduce-top">
			<div class="page-header">
				<ol class="breadcrumb">
					<li><a href="{{ URL::route('home') }}">{{ trans('view.home_breadcrumb') }}</a></li>
					<li class="active">{{ trans('view.buy_vip_breadcrumb') }}</li>
				</ol>
				<h1>{{ trans('view.buy_vip') }}</h1>
			</div>

			<div class="col-md-4">
				<div class="row">
					<form action="{{ URL::route('vip-post') }}" method="post" class="bottom-space" role="form">
						<div class="form-group @if ($errors->has('amount')) has-error @endif">
							<label for="amount">{{ trans('view.number_of_days', array('amount' => Config::get('app.amount_day') . ' ' . Config::get('app.vip_currency'))) }} </label>
							<input type="text" name="amount" class="form-control" id="amount">
							@if ($errors->has('amount'))
				    			<span class="help-block">{{ $errors->first('amount') }} </span>
				    		@endif
						</div>

						<div class="form-group @if ($errors->has('server')) has-error @endif">
							<label for="server">{{ trans('view.select_server_label') }}</label>
							<select name="server" id="server" class="form-control">
								<option value="">{{ trans('view.select_server_option') }}</option>
								@foreach ($servers as $server)
									<option value="{{ $server->id }}" @if ($vip->checkVip($server->id)) disabled="disabled" @endif>&nbsp;&nbsp;{{ $server->address }}</option>
								@endforeach
							</select>
							@if ($errors->has('server'))
				    			<span class="help-block">{{ $errors->first('server') }} </span>
				    		@endif
						</div>

						<div class="form-group">
							<span class="help-block">{{ trans('view.final_price') }} <i id="price"><strong></strong></i></span>
						</div>

						<button type="submit" class="btn btn-primary btn-lg">{{ trans('view.submit_vip_button') }}</button>
					</form>
				</div>
			</div>
		</div>
@stop

@section('js')
	
<script>
	$(document).ready(function(){
		var currency = '{{ Config::get('app.vip_currency') }}' == 'EUR' ? '€' : '$';

		$('i#price strong').html(currency + '0.00');

		$('#amount').on('input propertychange', function(){
	        var days = $(this).val();
	        var price = parseInt(days) * 1;

	        if(isNaN(price)) {
			    $('i#price strong').text('Days can only be numbers!');
	        }
	        else {
	        	

			    $('i#price strong').text(currency + price);
	        }
		});
	});
</script>
@stop