@extends('layouts.base')

@section('content')
		<!-- Begin page content -->
		<div class="container reduce-top">
			<div class="page-header">
				<ol class="breadcrumb">
					<li><a href="{{ URL::route('home') }}">{{ trans('view.home_breadcrumb') }}</a></li>
					<li class="active">{{ trans('view.version_servers', array('version' => $version->name)) }}</li>
				</ol>
				<h1>{{ trans('view.version_servers', array('version' => $version->name)) }}</h1>
			</div>
			<table class="table table-bordered tag-servers">
				<thead>
					<tr>
						<th width="10%"><i class="glyphicon glyphicon-list"></i> {{ trans('view.rank_thead' )}}</th>
						<th width="60%"><i class="glyphicon glyphicon-hdd"></i> {{ trans('view.server_thead') }}</th>
						<th width="10%"><i class="glyphicon glyphicon-user"></i> {{ trans('view.players_thead') }}</th>
						<th width="10%"><i class="glyphicon glyphicon-time"></i> {{ trans('view.uptime_thead')}}</th>
						<th width="10%"><i class="glyphicon glyphicon-thumbs-up"></i> {{ trans('view.votes_thead') }}</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($servers as $server)
					<?php
						// Calculate fucking uptime
						$uptime = @($server->online_time / $server->verified_time) * 100;
						$uptime = substr($uptime, 0, 5);
						$uptime = strlen($uptime) > 1 ? $uptime . '%' : 'N/A';
					?>
					<tr>
						<td align="center"><div style="margin-bottom: 5px;"><a href="{{ URL::route('server-page', $server->id) }}"><img width="64" height="64" border="0" title="{{ $server->name }}" alt="{{ $server->name }}" src="{{ $server->statistics['favicon'] }}"></a></div>#{{ $server->rank }}</td>
						<td>
							<div style="width: 100%; height: 69px;">
								@if ($server->banner) <a href="{{ URL::route('server-page', $server->id) }}"><img border="0" title="{{ $server->name }}" alt="{{ $server->name }}" src="{{ asset('uploads/images/banners/' . $server->banner) }}"></a> @else <a href="{{ URL::route('server-page', $server->id) }}"><h3>{{ $server->name }}</h3></a> @endif
							</div>
							<div class="server-details"><span class="label label-{{ ($server['status'] == 'online') ? 'success' : 'danger' }}">{{ strtoupper($server['status']) }}</span>&nbsp;<a href="{{ URL::route('version', $server['version']['name']) }}" class="label label-info">{{ $server['version']['name'] }}</a>&nbsp;<a href="{{ URL::route('country', strtolower($server->country['code'])) }}"><div class="flag flag-{{ strtolower($server->country['code']) }} left" style="margin-top: 5px; margin-right: 5px;"></div></a> &nbsp;<strong>{{ $server['address'] }}:{{ $server['port'] }}</strong></div>
						</td>
						<td align="center"><span class="label label-success label-padding">@if ($server->statistics['max_players']) {{ $server->statistics['players'] }} / {{ $server->statistics['max_players'] }} @else N/A @endif</span></td>
						<td align="center"><span class="label label-{{ $uptime == 'N/A' ? 'danger' : 'success' }} label-padding">{{ $uptime }}</span></td>
						<td align="center"><span class="label label-info label-padding">{{ $server['votes'] }}</span></td>
					</tr>
					@endforeach
				</tbody>
			</table>
			{{ $servers->links() }}
		</div>
@stop