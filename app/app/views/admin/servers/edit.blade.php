@extends('admin.layouts.base')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/css/selectize.bootstrap3.css') }}">
@stop

@section('content')
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">{{ trans('view.edit_server') }}</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <form role="form" method="post" action="{{ URL::route('servers-edit-post', $id) }}" enctype="multipart/form-data">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                {{ trans('view.basic_informations') }}
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-8">
                                        <div class="form-group">
                                            <label>{{ trans('view.server_name_label') }}</label>
                                            <input class="form-control" name="name" value="{{ $server->name }}">
                                            @if ($errors->has('name'))
                                                <span class="help-block"><div class="error-val">{{ $errors->first('name') }}</div></span>
                                            @endif
                                            <p class="help-block">{{ trans('view.server_name_help') }}</p>
                                        </div>


                                        {{-- Meta --}}
                                        <hr>
                                        <div class="form-group">
                                            <label>Meta: Title</label>
                                            <input class="form-control" name="meta_title" value="{{ $server->meta_title }}">
                                        </div>
                                        <div class="form-group">
                                            <label>Meta: Description</label>
                                            <textarea class="form-control"
                                                      name="meta_description">{{ $server->meta_description }}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Meta: Keywords</label>
                                            <textarea class="form-control"
                                                      name="meta_keywords">{{ $server   ->meta_keywords }}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Seo Text</label>
                                            <textarea class="form-control" name="seo_text">{{ $server->seo_text }}</textarea>
                                        </div>
                                        <hr>
                                        {{-- Meta --}}


                                        <div class="form-group">
                                            <label>Кристалы</label>
                                            <input class="form-control" name="diamonds" value="{{ $server->diamonds }}">
                                            @if ($errors->has('diamonds'))
                                                <span class="help-block"><div class="error-val">{{ $errors->first('diamonds') }}</div></span>
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <label>Короткое описание</label>
                                            <textarea rows="3" name="short_description" class="form-control">{{ $server->short_description }}</textarea>
                                        </div>

                                        <div class="form-group">
                                            <label>{{ trans('view.description_label') }}</label>
                                            <textarea rows="7" name="description" class="form-control">{{ $server->description }}</textarea>
                                            <p class="help-block">{{ trans('view.description_help') }}</p>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label>{{ trans('view.banner_label') }}</label>
                                            <input type="file" id="banner" name="banner">
                                            @if ($errors->has('banner'))
                                                <span class="help-block"><div class="error-val">{{ $errors->first('banner') }}</div></span>
                                            @endif
                                            <p class="help-block">{{ trans('view.banner_help') }}</p>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label>{{ trans('view.server_address_label') }}</label>
                                            <input class="form-control" name="address" value="{{ $server->address }}">
                                            @if ($errors->has('address'))
                                                <span class="help-block"><div class="error-val">{{ $errors->first('address') }}</div></span>
                                            @endif
                                            <p class="help-block">{{ trans('view.server_address_help') }}</p>
                                            <input class="form-control" name="port" value="{{ $server->port }}">
                                            @if ($errors->has('port'))
                                                <span class="help-block"><div class="error-val">{{ $errors->first('port') }}</div></span>
                                            @endif
                                            <p class="help-block">{{ trans('view.server_port_help') }}</p>
                                        </div>

                                        <div class="form-group">
                                            <label>{{ trans('view.website_label') }}</label>
                                            <input class="form-control" name="website" value="{{ $server->website }}">
                                            @if ($errors->has('website'))
                                                <span class="help-block"><div class="error-val">{{ $errors->first('website') }}</div></span>
                                            @endif
                                            <p class="help-block">{{ trans('view.website_help') }}</p>
                                        </div>

                                        <div class="form-group">
                                            <label>{{ trans('view.country_label') }}</label>
                                            <select name="country" id="country" class="form-control">
                                                @foreach ($countries as $country)
                                                    <option value="{{ $country->id }}"{{ ($country->id == $server->country_id) ? ' selected' : '' }}>{{ $country->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>{{ trans('view.minecraft_version_label') }}</label>
                                            <select name="version" id="version" class="form-control">
                                                @foreach ($versions as $version)
                                                    <option value="{{ $version->id }}"{{ ($version->id == $server->version_id) ? ' selected' : '' }}>{{ $version->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="control-group">
                                            <label for="select-tags">{{ trans('view.tags_label') }}</label>
                                            <select id="select-tags" name="tags[]" multiple style="width:50%" placeholder="Select a tag...">
                                                @foreach ($tags as $tag)
                                                    <option value="{{ $tag->id }}"@if ($object->haveTag($server->id, $tag->id) == true) selected @endif>{{ $tag->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="control-group">
                                            <label for="select-modes">Моды</label>
                                            <select id="select-modes" name="modes[]" multiple style="width:50%" placeholder="Select a modes...">
                                                @foreach ($modes as $tag)
                                                    <option value="{{ $tag->id }}"@if ($object->haveMode($server->id, $tag->id) == true) selected @endif>{{ $tag->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="control-group">
                                            <label for="select-games">{{ trans('view.tags_label') }}</label>
                                            <select id="select-games" name="games[]" multiple style="width:50%" placeholder="Select a tag...">
                                                @foreach ($games as $tag)
                                                    <option value="{{ $tag->id }}"@if ($object->haveGame($server->id, $tag->id) == true) selected @endif>{{ $tag->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            <!-- /.row (nested) -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                {{ trans('view.votifier') }}
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-7">
                                        <div class="form-group">
                                            <label>{{ trans('view.use_votifier_label') }}</label>
                                            <select name="votifier_active" class="form-control">
                                                @foreach (array('false', 'true') as $votifier)
                                                    <option value="{{ $votifier }}"{{ ($votifier == $server->votifier_active) ? ' selected' : '' }}>{{ $votifier }}</option>
                                                @endforeach
                                            </select>
                                            <p class="help-block">{{ trans('view.use_votifier_help') }}</p>
                                        </div>

                                        <div class="form-group">
                                            <label>{{ trans('view.votifier_address_label') }}</label>
                                            <input class="form-control" name="votifier_address">
                                            <p class="help-block">{{ trans('view.votifier_address_help') }}</p>
                                        </div>

                                        <div class="form-group">
                                            <label>{{ trans('view.votifier_port_label') }}</label>
                                            <input class="form-control" name="votifier_port">
                                            <p class="help-block">{{ trans('view.votifier_port_help') }}</p>
                                        </div>

                                        <div class="form-group">
                                            <label>{{ trans('view.votifier_key_label') }}</label>
                                            <textarea rows="7" name="votifier_key" class="form-control">{{ $server->votifier_key }}</textarea>
                                            <p class="help-block">{{ trans('view.votifier_key_help') }}</p>
                                        </div>

                                    </div>
                                </div>
                            <!-- /.row (nested) -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                {{ trans('view.additional_information') }}
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-8">
                                        <div class="form-group">
                                            <label>{{ trans('view.youtube_video_label') }}</label>
                                            <input class="form-control" name="youtube" value="{{ $server->youtube }}">
                                            <p class="help-block">{{ trans('view.youtube_video_help') }}</p>
                                        </div>
                                    </div>
                                </div>
                            <!-- /.row (nested) -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->

                        <button type="submit" class="btn btn-default">{{ trans('view.update_server_button') }}</button>
                    </div>
                </form>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
@stop

@section('js')
    <script src="{{ asset('assets/js/selectize.js') }}"></script>
    <script>
        $('#select-tags').selectize();
        $('#select-modes').selectize();
        $('#select-games').selectize();
    </script>
@stop