@extends('admin.layouts.base')

@section('css')
	<!-- DataTables CSS -->
    <link href="{{ asset('assets/backend/css/plugins/dataTables.bootstrap.css') }}" rel="stylesheet">
@stop

@section('content')
			<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">{{ trans('view.all_servers') }}</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
            	<div class="panel panel-default">
                        <div class="panel-heading">
                            {{ trans('view.servers_information') }}
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="servers">
                                    <thead>
                                        <tr>
                                            <th width="60%">{{ trans('view.name_thead') }}</th>
                                            <th width="20%">{{ trans('view.address_thead') }}</th>
                                            <th width="5%">{{ trans('view.port_thead') }}</th>
                                            <th width="15%">{{ trans('view.settings_thead') }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	@foreach ($servers as $server)
	                                        <tr>
	                                            <td><a href="{{ URL::route('server-page', $server->id) }}" target="__blank">{{ $server->name }}</a</td>
	                                            <td>{{ $server->address }}</td>
	                                            <td>{{ $server->port }}</td>
	                                            <td><a href="{{ URL::route('servers-edit', $server->id) }}" class="btn btn-primary">{{ trans('view.edit') }}</a> <a href="{{ URL::route('delete', array('server', $server->id)) }}" class="btn btn-danger">{{ trans('view.delete') }}</a></td>
	                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
 			</div>
            <!-- /.row -->
@stop

@section('js')
	<script src="{{ asset('assets/backend/js/plugins/dataTables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/backend/js/plugins/dataTables/dataTables.bootstrap.js') }}"></script>

    <script>
	    $(document).ready(function() {
	        $('#servers').dataTable({
		      "aoColumnDefs": [
		          { 'bSortable': false, 'aTargets': [ 3 ] }
		       ]
			});
	    });
    </script>
@stop