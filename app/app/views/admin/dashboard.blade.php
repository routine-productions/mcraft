@extends('admin.layouts.base')

@section('css')
	<link href="{{ asset('assets/backend/css/plugins/timeline.css') }}" rel="stylesheet">
	<link href="{{ asset('assets/backend/css/plugins/morris.css') }}" rel="stylesheet">
@stop

@section('content')
			<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">{{ trans('view.admin_dashboard')}}</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
                <div class="col-lg-6 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-tasks fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">{{ $count_servers }}</div>
                                    <div>{{ trans('view.servers') }}</div>
                                </div>
                            </div>
                        </div>
                        <a href="{{ URL::route('servers-all') }}">
                            <div class="panel-footer">
                                <span class="pull-left">{{ trans('view.view_all_servers') }}</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-users fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">{{ $count_users }}</div>
                                    <div>{{ trans('view.users') }}</div>
                                </div>
                            </div>
                        </div>
                        <a href="{{ URL::route('users-all') }}">
                            <div class="panel-footer">
                                <span class="pull-left">{{ trans('view.view_all_users') }}</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-4">
                    
                </div>
            </div>
            <!-- /.row -->
@stop
