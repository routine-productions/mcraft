@extends('admin.layouts.base')

@section('css')
    <link href="{{ asset('assets/backend/css/plugins/dataTables.bootstrap.css') }}" rel="stylesheet">
@stop

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">{{ $title }}</h1>
        </div>
    </div>
    <div class="row">
        <p class="col-lg-12">
            <a href="{{ URL::route('pay_types-create') }}" class="btn btn-primary">Добавить</a>
        </p>
    </div>
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">Все покупки</div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th width="4%">ID</th>

                            <th width="6%">Тип</th>
                            <th width="10%">Имя</th>
                            <th width="10%">Дни</th>

                            <th width="15%">Стоимость без скидки</th>
                            <th width="10%">Скидка %</th>
                            <th width="15%">Стоимость со скидкой</th>

                            <th width="30%">Действия</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($pay_types as $pay_type)
                            <tr>
                                <td><a href="{{ URL::route('pay_types-edit', $pay_type->id) }}">{{ $pay_type->id }}</a>
                                </td>
                                <td>{{ Config::get('app.pay_types.' .$pay_type->type) }}</td>
                                <td>{{ $pay_type->name }}</td>
                                <td>{{ $pay_type->days }}</td>
                                <td>{{ $pay_type->cost }} ₽</td>
                                <td>{{ $pay_type->sale }}%</td>
                                <td>{{ $pay_type->cost * (100 -$pay_type->sale)/100 }} ₽</td>

                                <td>
                                    <a href="{{ URL::route('pay_types-edit', $pay_type->id) }}"
                                       class="btn btn-primary">{{ trans('view.edit') }}</a>
                                    <a href="{{ URL::route('delete', array('pay_types', $pay_type->id)) }}"
                                       class="btn btn-danger">{{ trans('view.delete') }}</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <a href="{{ URL::route('pay_types-create') }}" class="btn btn-primary">Добавить</a>
    </div>

@stop

@section('js')
    <script src="{{ asset('assets/backend/js/plugins/dataTables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/backend/js/plugins/dataTables/dataTables.bootstrap.js') }}"></script>

    <script>
        $(document).ready(function () {
            $('.table').dataTable({
                "pageLength": 50,
                "aoColumnDefs": [
                    {'bSortable': false, 'aTargets': [3]}
                ]
            });
        });
    </script>
@stop