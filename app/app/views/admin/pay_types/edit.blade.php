@extends('admin.layouts.base')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/css/selectize.bootstrap3.css') }}">
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Редактировать</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <p>
                <a href="{{ URL::route('pay_types-all', $id) }}">Вернуться к разделу</a>
            </p>
        </div>
    </div>
    <div class="row">
        <form class='col-lg-12' role="form" method="post" action="{{ URL::route('pay_types-edit', $id) }}">
            <div class="panel panel-default">
                <div class="panel-heading">{{ trans('view.basic_informations') }}</div>
                <div class="panel-body">
                    <div class="row col-lg-8">
                        <div class="form-group">
                            <label>Тип</label>
                            <select class="form-control" name="type">
                                @foreach (Config::get('app.pay_types') as $typeKey => $type)
                                    <option value="{{ $typeKey }}" {{ $typeKey == $paytype->type ? 'selected' : '' }}>{{$type}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('type'))
                                <span class="help-block"><div class="error-val">{{$errors->first('type')}}</div></span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Имя</label>
                            <input class="form-control" name="name" value="{{ $paytype->name }}">
                            @if ($errors->has('days'))
                                <span class="help-block"><div class="error-val">{{$errors->first('name')}}</div></span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Дни</label>
                            <input class="form-control" name="days" value="{{ $paytype->days }}">
                            @if ($errors->has('days'))
                                <span class="help-block"><div class="error-val">{{$errors->first('days')}}</div></span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Стоимоть без скидки, рубли</label>
                            <input class="form-control" name="cost" value="{{ $paytype->cost }}">
                            @if ($errors->has('cost'))
                                <span class="help-block"><div class="error-val">{{$errors->first('cost')}}</div></span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Скидка, %</label>
                            <input class="form-control" name="sale" value="{{ $paytype->sale }}">
                            @if ($errors->has('sale'))
                                <span class="help-block"><div class="error-val">{{$errors->first('sale')}}</div></span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-default">{{ trans('view.update_server_button') }}</button>

        </form>
    </div>

@stop

@section('js')
    <script src="{{ asset('assets/js/selectize.js') }}"></script>
    <script>
        $('#select-tags').selectize();
        $('#select-modes').selectize();
        $('#select-games').selectize();
    </script>
@stop