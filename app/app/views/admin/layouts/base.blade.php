<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@if (isset($title)) {{{ $title }}} @endif</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('assets/backend/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/backend/css/datepicker.css') }}" rel="stylesheet"
          type="text/css">

    <!-- MetisMenu CSS -->
    <link href="{{ asset('assets/backend/css/plugins/metisMenu/metisMenu.min.css') }}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ asset('assets/backend/css/sb-admin-2.css') }}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{{ asset('assets/backend/font-awesome-4.1.0/css/font-awesome.min.css') }}" rel="stylesheet"
          type="text/css">


@yield('css')

<!-- NProgress -->
    <link href="{{ asset('assets/backend/nprogress/nprogress.css') }}" rel="stylesheet">
    <script src="{{ asset('assets/backend/nprogress/nprogress.js') }}"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<script>NProgress.start();
    setTimeout(function () {
        NProgress.done();
        $('.fade').removeClass('out');
    }, 1000);</script>

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ URL::route('admin-dashboard') }}">{{ Config::get('app.sitename') }}</a>
        </div>
        <!-- /.navbar-header -->

        <ul class="nav navbar-top-links navbar-right">
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="{{ URL::route('profile', Auth::user()->username) }}" target="__blank"><i
                                    class="fa fa-user fa-fw"></i> {{ trans('view.user_profile') }}</a>
                    </li>
                    <li><a href="{{ URL::route('edit') }}"><i class="fa fa-gear fa-fw"></i> {{ trans('view.settings') }}
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li><a href="{{ URL::route('logout') }}"><i
                                    class="fa fa-sign-out fa-fw"></i> {{ trans('view.logout') }}</a>
                    </li>
                </ul>
            </li>
        </ul>


        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    <li class="preview">
                        <a href="{{ URL::route('home') }}" target="_blank"
                           class="btn btn-default btn-lg btn-block">Вернуться на сайт</a>
                    </li>


                    {{-- Servers --}}
                    <li>
                        <a href="#"><i class="fa fa-tasks fa-fw"></i> {{ trans('view.servers') }}<span
                                    class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="{{ URL::route('servers-all') }}">Серверы</a></li>
                            <li><a href="{{ URL::route('versions-all') }}">Версии</a></li>
                            <li><a href="{{ URL::route('tags-all') }}">Теги</a></li>
                            <li><a href="{{ URL::route('modes-all') }}">Моды</a></li>
                            <li><a href="{{ URL::route('games-all') }}">Мини-игры</a></li>
                        </ul>
                    </li>

                    {{-- Users --}}
                    <li>
                        <a href="#"><i class="fa fa-user fa-fw"></i>{{ trans('view.users') }}
                            <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="{{ URL::route('users-create') }}">{{ trans('view.create_user') }}</a></li>
                            <li><a href="{{ URL::route('users-all') }}">{{ trans('view.all_users') }}</a></li>
                        </ul>
                    </li>

                    {{-- Purchases --}}
                    <li>
                        <a href="#"><i class="fa fa-credit-card fa-fw"></i>Покупки <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="{{ URL::route('purchases-all') }}">Все покупки</a></li>
                            <li><a href="{{ URL::route('purchases-create') }}">Добавить покупку</a></li>
                            <li><a href="{{ URL::route('pay_types-all') }}">Все стоимости услуг</a></li>
                            <li><a href="{{ URL::route('pay_types-create') }}">Добавить стоимость услуги</a></li>
                        </ul>
                    </li>

                    {{-- News --}}
                    <li>
                        <a href="#"><i class="fa fa-list-alt fa-fw"></i>Новости<span class="fa arrow"></span></a></a>
                        <ul class="nav nav-second-level">
                            <li><a href="{{ URL::route('news-create') }}">Добавить новость</a></li>
                            <li><a href="{{ URL::route('news-all') }}">Все новости</a></li>
                        </ul>
                    </li>


                    {{-- Settings --}}
                    <li>
                        <a href="#"><i class="fa fa-gear fa-fw"></i> {{ trans('view.settings') }}<span
                                    class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="{{ URL::route('settings-configuration') }}">{{ trans('view.configuration') }}</a>
                            </li>
                            <li><a href="{{ URL::route('settings-seo') }}">{{ trans('view.seo') }}</a></li>
                            <li><a href="{{ URL::route('tools-cron') }}">{{ trans('view.cron') }}</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>

    <!-- Page Content -->
    <div id="page-wrapper">
    @if (Session::has('global'))
        <!-- Global messages here -->
            <div class="col-lg-12" style="margin-top: 10px;">
                <div class="row">
                    <div class="alert alert-{{ (Session::has('type')) ? '' . Session::get('type') . '' : 'danger' }}"><i
                                class="fa fa-info fa-fw"></i> {{ Session::get('global') }}</div>
                </div>
            </div>
        @endif

        @yield('content')
    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery Version 1.11.0 -->
<script src="{{ asset('assets/backend/js/jquery-1.11.0.js') }}"></script>

<!-- Bootstrap Core JavaScript -->
<script src="{{ asset('assets/backend/js/bootstrap.min.js') }}"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="{{ asset('assets/backend/js/plugins/metisMenu/metisMenu.min.js') }}"></script>
<script src="{{ asset('assets/backend/js/bootstrap-datepicker.js') }}"></script>


@yield('js')

<!-- Custom Theme JavaScript -->
<script src="{{ asset('assets/backend/js/sb-admin-2.js') }}"></script>

</body>

</html>

