@extends('admin.layouts.base')

@section('css')
	<!-- DataTables CSS -->
    <link href="{{ asset('assets/backend/css/plugins/dataTables.bootstrap.css') }}" rel="stylesheet">
@stop

@section('content')
			<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">{{ trans('view.all_comments') }}</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
            	<div class="panel panel-default">
                        <div class="panel-heading">
                            {{ trans('view.users_comments') }}
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="servers">
                                    <thead>
                                        <tr>
                                            <th width="35%">{{ trans('view.server_thead') }}</th>
                                            <th width="15%">{{ trans('view.username_thead') }}</th>
                                            <th width="40%">Content</th>
                                            <th width="10%"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	@foreach ($comments as $comment)
	                                        <tr>
                                                <td><a href="{{ URL::route('server-page', $comment->server['id']) }}">{{ $comment->server['name'] }}</a></td>
                                                <td><a href="{{ URL::route('profile', $comment->user['username']) }}">{{ $comment->user['username'] }}</a></td>
	                                            <td><a href="{{ URL::route('server-comments', $comment->server['id']) }}">{{ $comment->body }}</a></td>
	                                            <td><a href="{{ URL::route('delete', array('comment', $comment->id)) }}" class="btn btn-danger">{{ trans('view.delete') }}</a></td>
	                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
 			</div>
            <!-- /.row -->
@stop

@section('js')
	<script src="{{ asset('assets/backend/js/plugins/dataTables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/backend/js/plugins/dataTables/dataTables.bootstrap.js') }}"></script>

    <script>
	    $(document).ready(function() {
	        $('#servers').dataTable({
		      "aoColumnDefs": [
		          { 'bSortable': false, 'aTargets': [ 3 ] }
		       ]
			});
	    });
    </script>
@stop