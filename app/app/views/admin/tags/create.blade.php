@extends('admin.layouts.base')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/css/selectize.bootstrap3.css') }}">
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Добавить</h1>
        </div>
    </div>
    <div class="row">
        <form role="form" method="post" action="{{ URL::route($type .'-create') }}">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {{ trans('view.basic_informations') }}
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="form-group">
                                    <label>Название</label>
                                    <input class="form-control" name="name" value="">
                                    @if ($errors->has('name'))
                                        <span class="help-block"><div
                                                    class="error-val">{{ $errors->first('name') }}</div></span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label>Алиас</label>
                                    <input class="form-control" name="slug" value="">
                                    @if ($errors->has('slug'))
                                        <span class="help-block"><div
                                                    class="error-val">{{ $errors->first('slug') }}</div></span>
                                    @endif
                                </div>


                                {{-- Meta --}}
                                <hr>
                                <div class="form-group">
                                    <label>Meta: Title</label>
                                    <input class="form-control" name="meta_title" value="{{Input::old('meta_title')}}">
                                </div>
                                <div class="form-group">
                                    <label>Meta: Description</label>
                                    <textarea class="form-control"
                                              name="meta_description">{{Input::old('meta_description')}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label>Meta: Keywords</label>
                                    <textarea class="form-control"
                                              name="meta_keywords">{{Input::old('meta_keywords')}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label>Seo Text</label>
                                    <textarea class="form-control" name="seo_text">{{Input::old('seo_text')}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label>H1</label>
                                    <textarea class="form-control" name="h1">{{Input::old('h1')}}</textarea>
                                </div>
                                <hr>
                                {{-- Meta --}}


                            </div>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-default">Добавить</button>
            </div>
        </form>
    </div>

@stop

@section('js')
    <script src="{{ asset('assets/js/selectize.js') }}"></script>
    <script>
        $('#select-tags').selectize();
        $('#select-modes').selectize();
        $('#select-games').selectize();
    </script>
@stop