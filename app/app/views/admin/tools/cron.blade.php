@extends('admin.layouts.base')

@section('content')
			<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">{{ trans('view.cron') }}</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
            	<form role="form" method="post" action="{{ URL::route('tools-cron-post') }}" name="cron">
	                <div class="col-lg-12">
	                	<div class="col-lg-7">
		                    <div class="panel panel-default">
		                        <div class="panel-heading">
		                            {{ trans('view.cron_secret_key') }}
		                        </div>
		                        <div class="panel-body">
		                            <div class="row">
		                                <div class="col-lg-7">
		                                    <div class="form-group">
		                                        <label>{{ trans('view.cron_key_label') }}</label>
		                                        <p><input name="key" class="form-control" value="{{ Config::get('app.cron_key') }}"></p>
		                                        <button class="btn btn-outline btn-primary btn-sm" type="button" onClick="randomString();">{{ trans('view.random_key_button') }}</button>
		                                        <p class="help-block">{{ trans('view.cron_key_help') }}</p>
		                                    </div>
		                            	</div>
		                        	</div>
		                        <!-- /.row (nested) -->
		                        </div>
		                        <!-- /.panel-body -->
		                    </div>
		                    <!-- /.panel -->

		                    <div class="panel panel-default">
		                        <div class="panel-heading">
		                            {{ trans('view.cron_urls') }}
		                        </div>
		                        <div class="panel-body">
		                            <div class="row">
		                                <div class="col-lg-12">
		                                    <div class="form-group">
		                                        <label>{{ trans('view.servers_statistics_label') }}</label>
		                                        <input class="form-control" value="{{ URL::route('cron-statistics', Config::get('app.cron_key')) }}" readonly="readonly">
		                                        <p class="help-block">{{ trans('view.servers_statistics_help') }}</p>
		                                    </div>

		                                    <div class="form-group">
		                                        <label>{{ trans('view.servers_rank_label') }}</label>
		                                        <input class="form-control" value="{{ URL::route('cron-rank', Config::get('app.cron_key')) }}" readonly="readonly">
		                                        <p class="help-block">{{ trans('view.servers_rank_help') }}</p>
		                                    </div>
		                            	</div>
		                        	</div>
		                        <!-- /.row (nested) -->
		                        </div>
		                        <!-- /.panel-body -->
		                    </div>
		                    <!-- /.panel -->

		                    <button type="submit" class="btn btn-default">{{ trans('view.update_key_button') }}</button>
		                </div>

		                <div class="col-lg-5">
		                	<div class="panel panel-default">
		                        <div class="panel-heading">
		                            {{ trans('view.help') }}
		                        </div>
		                        <div class="panel-body">
		                            <div class="row">
		                                <div class="col-lg-12">
		                                    {{ trans('view.help_text', array('statistics' => URL::route('cron-statistics', Config::get('app.cron_key')), 'rank' => URL::route('cron-rank', Config::get('app.cron_key'))))}}
		                            	</div>
		                        	</div>
		                        <!-- /.row (nested) -->
		                        </div>
		                        <!-- /.panel-body -->
		                    </div>
		                    <!-- /.panel -->
		                </div>
	                    
	               	</div>
               	</form>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
@stop