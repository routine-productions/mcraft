@extends('admin.layouts.base')

@section('css')
	<!-- DataTables CSS -->
    <link href="{{ asset('assets/backend/css/plugins/dataTables.bootstrap.css') }}" rel="stylesheet">
@stop

@section('content')
			<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">{{ trans('view.all_vip_servers') }}</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
            	<div class="panel panel-default">
                        <div class="panel-heading">
                            {{ trans('view.vip_servers') }}
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="vips">
                                    <thead>
                                        <tr>
                                            <th width="30%">{{ trans('view.vip_server_thead') }}</th>
                                            <th width="15%">{{ trans('view.vip_start_date_thead') }}</th>
                                            <th width="15%">{{ trans('view.vip_end_date_thead') }}</th>
                                            <th width="5%">{{ trans('view.vip_days_thead') }}</th>
                                            <th width="15%">{{ trans('view.payment_status') }}</th>
                                            <th width="20%"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	@foreach ($vips as $vip)
	                                        <tr>
	                                            <td><a href="{{ URL::route('server-page', $vip->server['id']) }}" target="__blank">{{ $vip->server['name'] }}</a</td>
	                                            <td>{{ $vip->start }}</td>
                                                <td>{{ $vip->end }}</td>
                                                <td>{{ $vip->days }}</td>
	                                            <td>{{ ucfirst($vip->payment_status) }}</td>
	                                            <td><a href="{{ URL::route('vip-edit', $vip->id) }}" class="btn btn-primary">{{ trans('view.edit') }}</a> <a href="{{ URL::route('vip-view', $vip->id) }}" class="btn btn-primary">{{ trans('view.view_button') }}</a> <a href="{{ URL::route('delete', array('vip', $vip->id)) }}" class="btn btn-danger">{{ trans('view.delete') }}</a></td>
	                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
 			</div>
            <!-- /.row -->
@stop

@section('js')
	<script src="{{ asset('assets/backend/js/plugins/dataTables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/backend/js/plugins/dataTables/dataTables.bootstrap.js') }}"></script>

    <script>
	    $(document).ready(function() {
	        $('#vips').dataTable({
		      "aoColumnDefs": [
		          { 'bSortable': false, 'aTargets': [ 3 ] }
		       ]
			});
	    });
    </script>
@stop