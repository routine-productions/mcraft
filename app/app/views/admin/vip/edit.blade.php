@extends('admin.layouts.base')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/css/selectize.bootstrap3.css') }}">
@stop

@section('content')
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">{{ trans('view.edit_vip') }}</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <form role="form" method="post" action="{{ URL::route('vip-edit-post', $id) }}" enctype="multipart/form-data">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                {{ trans('view.informations') }}
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group @if ($errors->has('start')) has-error @endif">
                                            <label>{{ trans('view.vip_start_label') }}</label>
                                            <input class="form-control" name="start" value="{{ $vip->start }}">
                                            @if ($errors->has('start'))
                                                <span class="help-block"><div class="error-val">{{ $errors->first('start') }}</div></span>
                                            @endif
                                        </div>

                                        <div class="form-group @if ($errors->has('end')) has-error @endif">
                                            <label>{{ trans('view.vip_end_label') }}</label>
                                            <input class="form-control" name="end" value="{{ $vip->end }}">
                                            @if ($errors->has('end'))
                                                <span class="help-block"><div class="error-val">{{ $errors->first('end') }}</div></span>
                                            @endif
                                        </div>

                                        <div class="form-group @if ($errors->has('gross')) has-error @endif">
                                            <label>{{ trans('view.amount_paid') }}</label>
                                            <input class="form-control" name="gross" value="{{ $vip->mc_gross }}">
                                            @if ($errors->has('gross'))
                                                <span class="help-block"><div class="error-val">{{ $errors->first('gross') }}</div></span>
                                            @endif
                                        </div>

                                        <div class="form-group @if ($errors->has('status')) has-error @endif">
                                            <label>{{ trans('view.payment_status') }}</label>
                                            <select name="status" class="form-control">
                                                @foreach (array('pending', 'completed', 'refunded') as $status)
                                                    <option value="{{ strtolower($status) }}" @if ($vip->payment_status == $status) selected @endif>{{ ucfirst($status) }}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('status'))
                                                <span class="help-block"><div class="error-val">{{ $errors->first('status') }}</div></span>
                                            @endif
                                        </div>

                                        <div class="form-group @if ($errors->has('first_name')) has-error @endif">
                                            <label>{{ trans('view.payer_first_name_label') }}</label>
                                            <input class="form-control" name="first_name" value="{{ $vip->payer_first_name }}">
                                            @if ($errors->has('first_name'))
                                                <span class="help-block"><div class="error-val">{{ $errors->first('first_name') }}</div></span>
                                            @endif
                                        </div>

                                        <div class="form-group @if ($errors->has('last_name')) has-error @endif">
                                            <label>{{ trans('view.payer_last_name_label') }}</label>
                                            <input class="form-control" name="last_name" value="{{ $vip->payer_last_name }}">
                                            @if ($errors->has('last_name'))
                                                <span class="help-block"><div class="error-val">{{ $errors->first('last_name') }}</div></span>
                                            @endif
                                        </div>

                                        <div class="form-group @if ($errors->has('email')) has-error @endif">
                                            <label>{{ trans('view.payer_email') }}</label>
                                            <input class="form-control" name="email" value="{{ $vip->payer_email }}">
                                            @if ($errors->has('email'))
                                                <span class="help-block"><div class="error-val">{{ $errors->first('email') }}</div></span>
                                            @endif
                                        </div>

                                    </div>
                                </div>
                            <!-- /.row (nested) -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->

                        <button type="submit" class="btn btn-default">{{ trans('view.update_vip_button') }}</button>
                    </div>
                </form>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
@stop

@section('js')
    <script src="{{ asset('assets/js/selectize.js') }}"></script>
    <script>
        $('#select-tags').selectize({
            maxItems: {{ Config::get('app.allowed_tags') }},
        });
    </script>
@stop