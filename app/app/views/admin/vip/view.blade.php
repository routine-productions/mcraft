@extends('admin.layouts.base')

@section('content')
			<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">{{ trans('view.view_vip') }}</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
            	<div class="panel panel-default">
                        <div class="panel-heading">
                            {{ trans('view.vip') }}
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <tr>
                                        <td>{{ trans('view.txn_id_td') }}</td>
                                        <td>{{ $vip->txn_id }}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ trans('view.server_td') }}</td>
                                        <td>{{ $vip->server['name'] }}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ trans('view.date_start_td') }}</td>
                                        <td>{{ $vip->start }}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ trans('view.date_end_td') }}</td>
                                        <td>{{ $vip->end }}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ trans('view.amount_paid_td') }}</td>
                                        <td>{{ Config::get('app.vip_currency') == 'USD' ? '$' : '€' }}{{ $vip->mc_gross }}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ trans('view.payment_status_td') }}</td>
                                        <td>{{ ucfirst($vip->payment_status) }}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ trans('view.payer_first_name_td') }}</td>
                                        <td>{{ $vip->payer_first_name }}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ trans('view.payer_last_name_td') }}</td>
                                        <td>{{ $vip->payer_last_name }}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ trans('view.payer_email_td') }}</td>
                                        <td>{{ $vip->payer_email }}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
 			</div>
            <!-- /.row -->
@stop