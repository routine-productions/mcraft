@extends('admin.layouts.base')

@section('content')
			<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">{{ trans('view.configuration') }}</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
            	<form role="form" method="post" action="{{ URL::route('settings-configuration-post') }}">
	                <div class="col-lg-12">
	                    <div class="panel panel-default">
	                        <div class="panel-heading">
	                            {{ trans('view.website_settings') }}
	                        </div>
	                        <div class="panel-body">
	                            <div class="row">
	                                <div class="col-lg-7">
	                                    <div class="form-group">
	                                        <label>{{ trans('view.website_name_label') }}</label>
	                                        <input class="form-control" name="name" value="{{ Config::get('app.sitename') }}">
	                                        <p class="help-block">{{ trans('view.website_name_help') }}</p>
	                                    </div>
	                                    <div class="form-group">
	                                        <label>{{ trans('view.default_language_label') }}</label>
	                                        <input class="form-control" name="language" value="{{ Config::get('app.language') }}">
	                                        @if ($errors->has('language'))
							    				<span class="help-block"><div class="error-val">{{ $errors->first('language') }}</div></span>
							    			@endif
	                                        <p class="help-block">{{ trans('view.default_language_help') }}</p>
	                                    </div>
	                                    <div class="form-group">
	                                        <label>{{ trans('view.facebook_page_label') }}</label>
	                                        <input class="form-control" name="facebook" value="{{ Config::get('app.facebook_link') }}">
	                                        @if ($errors->has('facebook'))
							    				<span class="help-block"><div class="error-val">{{ $errors->first('facebook') }}</div></span>
							    			@endif
	                                        <p class="help-block">{{ trans('view.facebook_page_help') }}</p>
	                                    </div>
	                                    <div class="form-group">
	                                        <label>{{ trans('view.twitter_page_label') }}</label>
	                                        <input class="form-control" name="twitter" value="{{ Config::get('app.twitter_link') }}">
	                                        @if ($errors->has('twitter'))
							    				<span class="help-block"><div class="error-val">{{ $errors->first('twitter') }}</div></span>
							    			@endif
	                                        <p class="help-block">{{ trans('view.twitter_page_help') }}</p>
	                                    </div>
	                                    <div class="form-group">
	                                        <label>{{ trans('view.google_page_label') }}</label>
	                                        <input class="form-control" name="google" value="{{ Config::get('app.googleplus_link') }}">
	                                        @if ($errors->has('google'))
							    				<span class="help-block"><div class="error-val">{{ $errors->first('google') }}</div></span>
							    			@endif
	                                        <p class="help-block">{{ trans('view.google_page_help') }}</p>
	                                    </div>
	                                    <div class="form-group">
	                                        <label>{{ trans('view.tracking_code_label') }}</label>
	                                        <textarea rows="3" name="tracking" class="form-control">{{{ Config::get('app.tracking') }}}</textarea>
	                                        <p class="help-block">{{ trans('view.tracking_code_help') }}</p>
	                                    </div>
	                                    <div class="form-group">
	                                        <label>{{ trans('view.copyright_label') }}</label>
	                                        <input class="form-control" name="copyright" value="{{{ Config::get('app.copyright') }}}">
	                                    </div>
	                                    <div class="form-group">
                                            <label>{{ trans('view.confirm_account_label') }}</label>
                                            <select class="form-control" name="confirm">
                                                @foreach (array('true', 'false') as $confirm)
                                                    <option value="{{ $confirm }}" {{ Config::get('app.confirm_account') == $confirm ? 'selected' : '' }}>{{ $confirm }}</option>
                                                @endforeach
                                            </select>
                                            <p class="help-block">{{ trans('view.confirm_account_help') }}</p>
                                        </div>
                                        <div class="form-group">
	                                        <label>{{ trans('view.owner_email_label') }}</label>
	                                        <input class="form-control" name="owner_email" value="{{{ Config::get('app.owner_email') }}}">
	                                   		@if ($errors->has('owner_email'))
							    				<span class="help-block"><div class="error-val">{{ $errors->first('owner_email') }}</div></span>
							    			@endif
	                                    </div>

	                                    <div class="form-group">
	                                        <label>{{ trans('view.news_label') }}</label>
	                                        <textarea rows="3" name="news" class="form-control">{{{ Config::get('app.news_message') }}}</textarea>
	                                    </div>
	                            	</div>
	                        	</div>
	                        <!-- /.row (nested) -->
	                        </div>
	                        <!-- /.panel-body -->
	                    </div>
	                    <!-- /.panel -->
	                    <div class="panel panel-default">
	                        <div class="panel-heading">
	                            {{ trans('view.recaptcha') }}
	                        </div>
	                        <div class="panel-body">
	                            <div class="row">
	                                <div class="col-lg-6">
										<div class="form-group">
	                                        <label>{{ trans('view.recaptcha_public_key_label') }}</label>
	                                        <input class="form-control" name="public_key" value="{{ Config::get('app.recaptcha_public_key') }}">
	                                    </div>
	                                    <div class="form-group">
	                                        <label>{{ trans('view.recaptcha_private_key_label') }}</label>
	                                        <input class="form-control" name="private_key" value="{{ Config::get('app.recaptcha_private_key') }}">
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>

	                    <div class="panel panel-default">
	                        <div class="panel-heading">
	                            {{ trans('view.website_pagination') }}
	                        </div>
	                        <div class="panel-body">
	                            <div class="row">
	                                <div class="col-lg-6">
	                                    <div class="form-group">
	                                        <label>{{ trans('view.servers_home_page_label') }}</label>
	                                        <input class="form-control" name="sph" value="{{ Config::get('app.servers_per_home') }}">
	                                        @if ($errors->has('sph'))
							    				<span class="help-block"><div class="error-val">{{ $errors->first('sph') }}</div></span>
							    			@endif
	                                        <p class="help-block">{{ trans('view.servers_home_page_help') }}</p>
	                                    </div>
	                                    <div class="form-group">
	                                        <label>{{ trans('view.servers_tag_page_label') }}</label>
	                                        <input class="form-control" name="spt" value="{{ Config::get('app.servers_per_tags') }}">
	                                        @if ($errors->has('spt'))
							    				<span class="help-block"><div class="error-val">{{ $errors->first('spt') }}</div></span>
							    			@endif
	                                        <p class="help-block">{{ trans('view.servers_tag_page_help') }}</p>
	                                    </div>
	                                    <div class="form-group">
	                                        <label>{{ trans('view.servers_manage_page_label') }}</label>
	                                        <input class="form-control" name="spm" value="{{ Config::get('app.servers_per_manage') }}">
	                                        @if ($errors->has('spm'))
							    				<span class="help-block"><div class="error-val">{{ $errors->first('spm') }}</div></span>
							    			@endif
	                                        <p class="help-block">{{ trans('view.servers_manage_page_help') }}</p>
	                                    </div>
	                                    <div class="form-group">
	                                        <label>{{ trans('view.servers_search_page_label') }}</label>
	                                        <input class="form-control" name="sps" value="{{ Config::get('app.servers_per_search') }}">
	                                        @if ($errors->has('sps'))
							    				<span class="help-block"><div class="error-val">{{ $errors->first('sps') }}</div></span>
							    			@endif
	                                        <p class="help-block">{{ trans('view.servers_search_page_help') }}</p>
	                                    </div>
	                                    <div class="form-group">
	                                        <label>{{ trans('view.servers_country_page_label') }}</label>
	                                        <input class="form-control" name="spc" value="{{ Config::get('app.servers_per_country') }}">
	                                        @if ($errors->has('spc'))
							    				<span class="help-block"><div class="error-val">{{ $errors->first('spc') }}</div></span>
							    			@endif
	                                        <p class="help-block">{{ trans('view.servers_country_page_help') }}</p>
	                                    </div>
	                                    <div class="form-group">
	                                        <label>{{ trans('view.servers_version_page_label') }}</label>
	                                        <input class="form-control" name="spv" value="{{ Config::get('app.servers_per_version') }}">
	                                        @if ($errors->has('spv'))
							    				<span class="help-block"><div class="error-val">{{ $errors->first('spv') }}</div></span>
							    			@endif
	                                        <p class="help-block">{{ trans('view.servers_version_page_help') }}</p>
	                                    </div>
	                                </div>

	                            </div>
	                            <!-- /.row (nested) -->
	                        </div>
	                        <!-- /.panel-body -->
	                    </div>
	                    <!-- /.panel -->
	                    <div class="panel panel-default">
	                        <div class="panel-heading">
	                            {{ trans('view.settings_server') }}
	                        </div>
	                        <div class="panel-body">
	                            <div class="row">
	                                <div class="col-lg-6">
	                                    <div class="form-group">
	                                        <label>{{ trans('view.allowed_tags_label') }}</label>
	                                        <input class="form-control" name="tags" value="{{ Config::get('app.allowed_tags') }}">
	                                         @if ($errors->has('tags'))
							    				<span class="help-block"><div class="error-val">{{ $errors->first('tags') }}</div></span>
							    			@endif
	                                        <p class="help-block">{{ trans('view.allowed_tags_help') }}</p>
	                                    </div>
	                                </div>
	                            </div>
	                            <!-- /.row (nested) -->
	                        </div>
	                        <!-- /.panel-body -->
	                    </div>



                        <!-- /.panel -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                 Тексты
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Реклама и контакты</label>
                                            <textarea class="form-control" name="text_adverts" rows="10">{{ Config::get('app.text_adverts') }}</textarea>
                                            @if ($errors->has('app.text_adverts'))
                                                <span class="help-block"><div class="error-val">{{ $errors->first('app.text_adverts') }}</div></span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <!-- /.row (nested) -->

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>FAQ</label>
                                            <textarea class="form-control" name="faq" rows="10">{{ Config::get('app.faq') }}</textarea>
                                            @if ($errors->has('app.faq'))
                                                <span class="help-block"><div class="error-val">{{ $errors->first('app.faq') }}</div></span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <!-- /.row (nested) -->
                            </div>
                            <!-- /.panel-body -->
                        </div>


                        <!-- /.panel -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Платежные данные
                            </div>
                            <div class="panel-body">
                                <div class="row">
									<div class="col-lg-6">
										<div class="form-group">
											<label>Кошелек Webmoney</label>
											<input class="form-control" type="text" name="webmoney_wallet" value="{{Config::get('app.webmoney_wallet') }}">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-6">
										<div class="form-group">
											<label>Webmoney Secret</label>
											<input class="form-control" type="text" name="webmoney_secret" value="{{Config::get('app.webmoney_secret') }}">
										</div>
									</div>
								</div>
                                <!-- /.row (nested) -->
                            </div>
                            <!-- /.panel-body -->
                        </div>



	                    <button type="submit" class="btn btn-default">{{ trans('view.update_settings_button') }}</button>
	               	</div>
               	</form>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
@stop