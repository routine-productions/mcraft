@extends('admin.layouts.base')

@section('content')
			<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">{{ trans('view.seo') }}</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
            	<form role="form" method="post" action="{{ URL::route('settings-seo-post') }}">
	                <div class="col-lg-12">
	                    <div class="panel panel-default">
	                        <div class="panel-heading">
	                            {{ trans('view.search_engine') }}
	                        </div>
	                        <div class="panel-body">
	                            <div class="row">
	                                <div class="col-lg-7">
	                                    <div class="form-group">
	                                        <label>{{ trans('view.meta_title_label') }}</label>
	                                        <input class="form-control" name="meta_title" value="{{ Config::get('app.seo_meta_title') }}">
	                                        <p class="help-block">{{ trans('view.meta_title_help') }}</p>
	                                    </div>
	                                    <div class="form-group">
	                                        <label>{{ trans('view.meta_description_label') }}</label>
	                                        <textarea rows="3" name="meta_description" class="form-control">{{{ Config::get('app.seo_meta_title') }}}</textarea>
	                                        <p class="help-block">{{ trans('view.meta_description_help') }}</p>
	                                    </div>
	                                    <div class="form-group">
	                                        <label>{{ trans('view.user_profile_label') }}</label>
	                                        <input class="form-control" name="profile_prefix" value="{{ Config::get('app.seo_profile_prefix') }}">
	                                        <p class="help-block">{{ trans('view.user_profile_prefix_help') }}</p>
	                                        <input class="form-control" name="profile_suffix" value="{{ Config::get('app.seo_profile_suffix') }}">
	                                        <p class="help-block">{{ trans('view.user_profile_suffix_help') }}</p>
	                                    </div>
	                                    <div class="form-group">
	                                        <label>{{ trans('view.server_page_label') }}</label>
	                                        <input class="form-control" name="server_prefix" value="{{ Config::get('app.seo_server_prefix') }}">
	                                        <p class="help-block">{{ trans('view.server_page_prefix_help') }}</p>
	                                        <input class="form-control" name="server_suffix" value="{{ Config::get('app.seo_server_suffix') }}">
	                                        <p class="help-block">{{ trans('view.server_page_suffix_help') }}</p>
	                                    </div>

	                                    <div class="form-group">
	                                        <label>{{ trans('view.tag_page_label') }}</label>
	                                        <input class="form-control" name="tag_prefix" value="{{ Config::get('app.seo_tag_prefix') }}">
	                                        <p class="help-block">{{ trans('view.tag_page_prefix_help') }}</p>
	                                        <input class="form-control" name="tag_suffix" value="{{ Config::get('app.seo_tag_suffix') }}">
	                                        <p class="help-block">{{ trans('view.tag_page_suffix_help') }}</p>
	                                    </div>

	                                    <div class="form-group">
	                                        <label>{{ trans('view.country_page_label') }}</label>
	                                        <input class="form-control" name="country_prefix" value="{{ Config::get('app.seo_country_prefix') }}">
	                                        <p class="help-block">{{ trans('view.country_page_prefix_help') }}</p>
	                                        <input class="form-control" name="country_suffix" value="{{ Config::get('app.seo_country_suffix') }}">
	                                        <p class="help-block">{{ trans('view.country_page_suffix_help') }}</p>
	                                    </div>

	                                   	<div class="form-group">
	                                        <label>{{ trans('view.version_page_label') }}</label>
	                                        <input class="form-control" name="version_prefix" value="{{ Config::get('app.seo_version_prefix') }}">
	                                        <p class="help-block">{{ trans('view.version_page_prefix_help') }}</p>
	                                        <input class="form-control" name="version_suffix" value="{{ Config::get('app.seo_version_suffix') }}">
	                                        <p class="help-block">{{ trans('view.version_page_suffix_help') }}</p>
	                                    </div>
	                            	</div>
	                        	</div>
	                        <!-- /.row (nested) -->
	                        </div>
	                        <!-- /.panel-body -->
	                    </div>
	                    <!-- /.panel -->
	                    
	                    <button type="submit" class="btn btn-default">{{ trans('view.update_settings_button') }}</button>
	               	</div>
               	</form>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
@stop