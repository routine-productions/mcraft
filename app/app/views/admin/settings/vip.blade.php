@extends('admin.layouts.base')

@section('content')
			<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">{{ trans('view.vip') }}</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
            	<form role="form" method="post" action="{{ URL::route('settings-vip-post') }}">
	                <div class="col-lg-12">
	                    <div class="panel panel-default">
	                        <div class="panel-heading">
	                            {{ trans('view.payment_settings' )}}
	                        </div>
	                        <div class="panel-body">
	                            <div class="row">
	                                <div class="col-lg-7">
	                                    <div class="form-group">
	                                        <label>{{ trans('view.business_email_label') }}</label>
	                                        <input class="form-control" name="business" value="{{ Config::get('app.business_mail') }}">
	                                        <p class="help-block">{{ trans('view.business_email_help') }}</p>
	                                    </div>

	                                    <div class="form-group">
	                                        <label>{{ trans('view.amount_day_label') }}</label>
	                                        <input class="form-control" name="amount" value="{{ Config::get('app.amount_day') }}">
	                                        <p class="help-block">{{ trans('view.amount_day_help') }}</p>
	                                    </div>

	                                    <div class="form-group">
	                                        <label>{{ trans('view.currency_label') }}</label>
	                                        <select name="currency" class="form-control">
	                                        	@foreach (array('USD', 'EUR') as $currency)
	                                        		<option value="{{ $currency }}" @if (Config::get('app.vip_currency') == $currency) selected @endif>{{ $currency }}</option>
	                                        	@endforeach
	                                        </select>
	                                        <p class="help-block">{{ trans('view.currency_help') }}</p>
	                                    </div>
	                            	</div>
	                        	</div>
	                        <!-- /.row (nested) -->
	                        </div>
	                        <!-- /.panel-body -->
	                    </div>
	                    <!-- /.panel -->
	                    
	                    <button type="submit" class="btn btn-default">{{ trans('view.update_settings_button') }}</button>
	               	</div>
               	</form>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
@stop