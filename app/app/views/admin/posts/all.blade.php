@extends('admin.layouts.base')

@section('css')
    <!-- DataTables CSS -->
    <link href="{{ asset('assets/backend/css/plugins/dataTables.bootstrap.css') }}" rel="stylesheet">
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Все новости</h1>
        </div>
    </div>
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                {{ trans('view.servers_information') }}
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="servers">
                        <thead>
                        <tr>
                            <th width="20%">ID</th>
                            <th width="20%">Заголовок</th>
                            <th width="20%">Алиас</th>
                            <th width="40%">Действия</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($posts as $post)
                            <tr>
                                <td><a href="{{ URL::route('news-edit', $post->id) }}" target="__blank">{{ $post->id }}</a>
                                </td>
                                <td>{{ $post->title }}</td>
                                <td>{{ $post->alias }}</td>
                                <td>
                                    <a href="{{ URL::route('news-edit', $post->id) }}"
                                       class="btn btn-primary">{{ trans('view.edit') }}</a>
                                    <a href="{{ URL::route('delete', array('post', $post->id)) }}"
                                       class="btn btn-danger">{{ trans('view.delete') }}</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <a href="{{ URL::route('news-create') }}" class="btn btn-primary">Добавить новость</a>
    </div>

@stop

@section('js')
    <script src="{{ asset('assets/backend/js/plugins/dataTables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/backend/js/plugins/dataTables/dataTables.bootstrap.js') }}"></script>

    <script>
        $(document).ready(function () {
            $('#servers').dataTable({
                "aoColumnDefs": [
                    {'bSortable': false, 'aTargets': [3]}
                ]
            });
        });
    </script>
@stop