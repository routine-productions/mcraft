@extends('admin.layouts.base')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/css/selectize.bootstrap3.css') }}">
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Добавить новость</h1>
        </div>
    </div>
    <div class="row">
        <form role="form" method="post" action="{{ URL::route('news-create') }}" enctype="multipart/form-data">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {{ trans('view.basic_informations') }}
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="form-group">
                                    <label>Название</label>
                                    <input class="form-control" name="title" value="{{Input::old('title')}}">
                                    @if ($errors->has('title'))
                                        <span class="help-block"><div
                                                    class="error-val">{{ $errors->first('title') }}</div></span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label>Алиас</label>
                                    <input class="form-control" name="alias" value="{{Input::old('alias')}}">
                                    @if ($errors->has('alias'))
                                        <span class="help-block"><div
                                                    class="error-val">{{ $errors->first('alias') }}</div></span>
                                    @endif
                                </div>


                                {{-- Meta --}}
                                <hr>
                                <div class="form-group">
                                    <label>Meta: Title</label>
                                    <input class="form-control" name="meta_title" value="{{Input::old('meta_title')}}">
                                </div>
                                <div class="form-group">
                                    <label>Meta: Description</label>
                                    <textarea class="form-control" name="meta_description">{{Input::old('meta_description')}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label>Meta: Keywords</label>
                                    <textarea class="form-control"
                                              name="meta_keywords">{{Input::old('meta_keywords')}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label>Seo Text</label>
                                    <textarea class="form-control" name="seo_text">{{Input::old('seo_text')}}</textarea>
                                </div>
                                <hr>
                                {{-- Meta --}}


                                <div class="form-group">
                                    <label>Короткий текст</label>
                                    <textarea class="form-control" name="intro"
                                              rows="4">{{Input::old('intro')}}</textarea>
                                    @if ($errors->has('intro'))
                                        <span class="help-block"><div
                                                    class="error-val">{{ $errors->first('alias') }}</div></span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label>Полный текст</label>
                                    <textarea class="form-control" name="text"
                                              rows="8">{{Input::old('text')}}</textarea>
                                    @if ($errors->has('text'))
                                        <span class="help-block"><div
                                                    class="error-val">{{ $errors->first('text') }}</div></span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label>Изображение</label>
                                    <input type="file" id="img" name="img">
                                    @if ($errors->has('img'))
                                        <span class="help-block"><div
                                                    class="error-val">{{ $errors->first('img') }}</div></span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-default">Добавить</button>
            </div>
        </form>
    </div>
@stop

@section('js')
    <script src="{{ asset('assets/js/selectize.js') }}"></script>
    <script>
        $('#select-tags').selectize();
        $('#select-modes').selectize();
        $('#select-games').selectize();
    </script>
@stop