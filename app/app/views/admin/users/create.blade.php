@extends('admin.layouts.base')

@section('content')
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">{{ trans('view.create_user') }}</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <form role="form" method="post" action="{{ URL::route('users-create-post') }}">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                {{ trans('view.informations') }}
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-7">
                                        <div class="form-group">
                                            <label>{{ trans('view.username_label') }}</label>
                                            <input class="form-control" name="username">
                                            @if ($errors->has('username'))
                                                <span class="help-block"><div class="error-val">{{ $errors->first('username') }}</div></span>
                                            @endif
                                            <p class="help-block">{{ trans('view.username_help') }}</p>
                                        </div>

                                        <div class="form-group">
                                            <label>{{ trans('view.email_label') }}</label>
                                            <input class="form-control" name="email">
                                            @if ($errors->has('email'))
                                                <span class="help-block"><div class="error-val">{{ $errors->first('email') }}</div></span>
                                            @endif
                                            <p class="help-block">{{ trans('view.email_help') }}</p>
                                        </div>

                                        <div class="form-group">
                                            <label>{{ trans('view.password_label') }}</label>
                                            <input type="password" class="form-control" name="password">
                                            @if ($errors->has('password'))
                                                <span class="help-block"><div class="error-val">{{ $errors->first('password') }}</div></span>
                                            @endif
                                            <p class="help-block">{{ trans('view.password_help') }}</p>
                                        </div>

                                        <div class="form-group">
                                            <label>{{ trans('view.role_label') }}</label>
                                            <select class="form-control" name="role">
                                                @foreach (array('member', 'admin') as $value)
                                                    <option value="{{ $value }}">{{ $value }}</option>
                                                @endforeach
                                            </select>
                                            <p class="help-block">{{ trans('view.role_help') }}</p>
                                        </div>
                                        
                                    </div>
                                </div>
                            <!-- /.row (nested) -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                        
                        <button type="submit" class="btn btn-default">{{ trans('view.create_user_button') }}</button>
                    </div>
                </form>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
@stop