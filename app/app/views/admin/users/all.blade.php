@extends('admin.layouts.base')

@section('css')
	<!-- DataTables CSS -->
    <link href="{{ asset('assets/backend/css/plugins/dataTables.bootstrap.css') }}" rel="stylesheet">
@stop

@section('content')
			<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">{{ trans('view.all_users' )}}</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
            	<div class="panel panel-default">
                        <div class="panel-heading">
                            {{ trans('view.users_informations')}}
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th width="25%">{{ trans('view.username_thead') }}</th>
                                            <th width="35%">{{ trans('view.email_thead') }}</th>
                                            <th width="10%">{{ trans('view.role_thead') }}</th>
                                            <th width="15%">{{ trans('view.active_thead') }}</th>
                                            <th width="15%">{{ trans('view.settings_thead') }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	@foreach ($users as $user)
	                                        <tr>
	                                            <td>{{ $user->username }}</td>
	                                            <td>{{ $user->email }}</td>
	                                            <td>{{ $user->role }}</td>
	                                            <td>@if ($user->active == 1) active @elseif ($user->active == 0) inactive @endif</td>
	                                            <td><a href="{{ URL::route('users-edit', $user->id) }}" class="btn btn-primary">{{ trans('view.edit') }}</a> @if (Auth::user()->id == $user->id) <a class="btn btn-danger" disabled="disabled">{{ trans('view.delete') }}</a> @else <a href="{{ URL::route('delete', array('user', $user->id)) }}" class="btn btn-danger">{{ trans('view.delete') }}</a> @endif</td>
	                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
 			</div>
            <!-- /.row -->
@stop

@section('js')
	<script src="{{ asset('assets/backend/js/plugins/dataTables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/backend/js/plugins/dataTables/dataTables.bootstrap.js') }}"></script>

    <script>
	    $(document).ready(function() {
	        $('#dataTables-example').dataTable({
		      "aoColumnDefs": [
		          { 'bSortable': false, 'aTargets': [ 4 ] }
		       ]
			});
	    });
    </script>
@stop