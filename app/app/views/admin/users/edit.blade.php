@extends('admin.layouts.base')

@section('content')
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">{{ trans('view.edit_user') }}</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <form role="form" method="post" action="{{ URL::route('users-edit-post', $id) }}">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                {{ trans('view.basic_informations') }}
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-7">
                                        <div class="form-group">
                                            <label>{{ trans('view.email_label') }}</label>
                                            <input class="form-control" name="email" value="{{ $user->email }}">
                                            @if ($errors->has('email'))
                                                <span class="help-block"><div class="error-val">{{ $errors->first('email') }}</div></span>
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <label>{{ trans('view.username_label') }}</label>
                                            <input class="form-control" name="username" value="{{ $user->username }}">
                                            @if ($errors->has('username'))
                                                <span class="help-block"><div class="error-val">{{ $errors->first('username') }}</div></span>
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <label>{{ trans('view.role_label') }}</label>
                                            <select class="form-control" name="role">
                                                @foreach (array('admin', 'member') as $value)
                                                    <option value="{{ $value }}" {{ $user->role == $value ? 'selected' : '' }}>{{ $value }}</option>
                                                @endforeach
                                            </select>
                                            <p class="help-block">{{ trans('view.role_help') }}</p>
                                        </div>
                                        <div class="form-group">
                                            <label>{{ trans('view.status_label') }}</label>
                                            <select class="form-control" name="status">
                                                @foreach (array(1 => 'active', 0 => 'inactive') as $key => $active)
                                                    <option value="{{ $key }}" {{ $user->active == $key ? 'selected' : '' }}>{{ $active }}</option>
                                                @endforeach
                                            </select>
                                            <p class="help-block">{{ trans('view.status_help') }}</p>
                                        </div>
                                    </div>
                                </div>
                            <!-- /.row (nested) -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                {{ trans('view.password') }}
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-7">
                                        <div class="form-group">
                                            <label>{{ trans('view.password_label') }}</label>
                                            <input class="form-control" name="password" type="password">
                                            @if ($errors->has('password'))
                                                <span class="help-block"><div class="error-val">{{ $errors->first('password') }}</div></span>
                                            @endif
                                            <p class="help-block">{{ trans('view.password_help') }}</p>
                                        </div>

                                    </div>
                                </div>
                            <!-- /.row (nested) -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                        
                        <button type="submit" class="btn btn-default">{{ trans('view.update_user_button') }}</button>
                    </div>
                </form>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
@stop