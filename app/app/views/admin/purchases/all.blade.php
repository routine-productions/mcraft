@extends('admin.layouts.base')

@section('css')
    <link href="{{ asset('assets/backend/css/plugins/dataTables.bootstrap.css') }}" rel="stylesheet">
@stop

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">{{ $title }}</h1>
        </div>
    </div>
    <div class="row">
        <p class="col-lg-12">
            <a href="{{ URL::route('purchases-create') }}" class="btn btn-primary">Добавить</a>
        </p>
    </div>
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">Все покупки</div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th width="5%">ID</th>
                            <th width="5%">Server ID</th>

                            <th width="10%">Тип</th>
                            <th width="5%">Дни</th>
                            <th width="10%">Стоимость</th>

                            <th width="5%">Оплачено</th>

                            <th width="10%">Начало периода</th>
                            <th width="10%">Конец периода</th>
                            <th width="30%">Действия</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($purchases as $purchase)
                            <tr>
                                <td><a href="{{ URL::route('purchases-edit', $purchase->id) }}">{{ $purchase->id }}</a>
                                </td>
                                <td>{{ $purchase->server_id }}</td>
                                <td>{{ Config::get('app.pay_types.' .$purchase->type)}}</td>
                                <td>{{ $purchase->days }}</td>
                                <td>{{ $purchase->cost }} р.</td>
                                <td>{{ $purchase->paid ? "Да":"Нет" }}</td>

                                <td>{{ $purchase->created_at }}</td>
                                <td>{{ $purchase->end_period }}</td>

                                <td>
                                    <a href="{{ URL::route('purchases-edit', $purchase->id) }}"
                                       class="btn btn-primary">{{ trans('view.edit') }}</a>
                                    <a href="{{ URL::route('delete', array('purchases', $purchase->id)) }}"
                                       class="btn btn-danger">{{ trans('view.delete') }}</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <a href="{{ URL::route('purchases-create') }}" class="btn btn-primary">Добавить</a>
    </div>

@stop

@section('js')
    <script src="{{ asset('assets/backend/js/plugins/dataTables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/backend/js/plugins/dataTables/dataTables.bootstrap.js') }}"></script>

    <script>
        $(document).ready(function () {
            $('.table').dataTable({
                "pageLength": 50,
                "aoColumnDefs": [
                    {'bSortable': false, 'aTargets': [3]}
                ]
            });
        });
    </script>
@stop