@extends('admin.layouts.base')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/css/selectize.bootstrap3.css') }}">
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Добавить</h1>
        </div>
    </div>
    <div class="row">
        <form role="form" method="post" action="{{ URL::route('purchases-create') }}">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {{ trans('view.basic_informations') }}
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="form-group">
                                    <label>Количество дней</label>
                                    <input class="form-control" name="days" value="{{ Input::old('days') }}">
                                    @if ($errors->has('days'))
                                        <span class="help-block"><div class="error-val">{{$errors->first('days')}}</div></span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Стоимоть, р.</label>
                                    <input class="form-control" name="cost" value="{{ Input::old('cost') }}">
                                    @if ($errors->has('cost'))
                                        <span class="help-block"><div class="error-val">{{$errors->first('cost')}}</div></span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Сервер</label>
                                    <select class="form-control" name="server_id">
                                        @foreach ($servers as $server)
                                            <option value="{{ $server->id }}" {{ $server->id == Input::old('server_id') ? 'selected' : '' }}>{{ $server->name }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('server_id'))
                                        <span class="help-block"><div
                                                    class="error-val">{{$errors->first('server_id')}}</div></span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label>Тип</label>
                                    <select class="form-control" name="type">
                                        @foreach (Config::get('app.pay_types') as $typeKey => $type)
                                            <option value="{{ $typeKey }}" {{ $typeKey == Input::old('type') ? 'selected' : '' }}>{{$type}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('type'))
                                        <span class="help-block"><div class="error-val">{{$errors->first('type')}}</div></span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label>Количество кристалов (Если выбран тип Кристалы)</label>
                                    <input class="form-control" name="diamonds" value="{{ Input::old('diamonds') }}">
                                    @if ($errors->has('diamonds'))
                                        <span class="help-block"><div
                                                    class="error-val">{{$errors->first('diamonds')}}</div></span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Цвет (если выбран тип Цвет)</label>
                                    <select class="form-control" name="color">
                                        @foreach (Config::get('app.colors') as $typeKey=> $type)
                                            <option value="{{ $typeKey }}" {{ $typeKey == Input::old('color') ? 'selected' : '' }}>{{$type}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('color'))
                                        <span class="help-block"><div
                                                    class="error-val">{{$errors->first('color')}}</div></span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label>Оплачено</label>
                                    <select class="form-control" name="paid">
                                        <option value="0" selected>Нет</option>
                                        <option value="1">Да</option>
                                    </select>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-default">Добавить</button>
            </div>
        </form>
    </div>

@stop

@section('js')
    <script src="{{ asset('assets/js/selectize.js') }}"></script>
    <script>
        $('#select-tags').selectize();
        $('#select-modes').selectize();
        $('#select-games').selectize();
        $('.datepicker').datepicker();
    </script>
@stop