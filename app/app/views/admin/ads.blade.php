@extends('admin.layouts.base')

@section('content')
			<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">{{ trans('view.ads') }}</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
            	<form role="form" method="post" action="{{ URL::route('settings-ads-post') }}">
	                <div class="col-lg-12">
	                    <div class="panel panel-default">
	                        <div class="panel-heading">
	                            {{ trans('view.ads_top')}}
	                        </div>
	                        <div class="panel-body">
	                            <div class="row">
	                                <div class="col-lg-7">
	                                    <div class="form-group">
	                                        <label>{{ trans('view.content_label') }}</label>
	                                        <textarea rows="10" name="ads_top" class="form-control">{{{ Config::get('app.ads_top') }}}</textarea>
	                                        <p class="help-block">{{ trans('view.content_top_help') }}</p>
	                                    </div>
	                            	</div>
	                        	</div>
	                        <!-- /.row (nested) -->
	                        </div>
	                        <!-- /.panel-body -->
	                    </div>
	                    <!-- /.panel -->

	                    <div class="panel panel-default">
	                        <div class="panel-heading">
	                            {{ trans('view.ads_bottom') }}
	                        </div>
	                        <div class="panel-body">
	                            <div class="row">
	                                <div class="col-lg-7">
	                                    <div class="form-group">
	                                        <label>{{ trans('view.content_label') }}</label>
	                                        <textarea rows="10" name="ads_bottom" class="form-control">{{{ Config::get('app.ads_bottom') }}}</textarea>
	                                        <p class="help-block">{{ trans('view.content_bottom_help') }}</p>
	                                    </div>
	                            	</div>
	                        	</div>
	                        <!-- /.row (nested) -->
	                        </div>
	                        <!-- /.panel-body -->
	                    </div>
	                    <!-- /.panel -->
	                    
	                    <button type="submit" class="btn btn-default">{{ trans('view.update_ads') }}</button>
	               	</div>
               	</form>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
@stop