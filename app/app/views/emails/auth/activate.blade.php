<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tbody><tr>
		<td style="padding:10px">
			<table border="0" bgcolor="#ffffff" cellspacing="0" cellpadding="0" style="border:1px solid #cfcfcf;width:750px;margin-left:auto;margin-right:auto;font-family:arial,helvetica,sans-serif;border-spacing:0">
				<tbody><tr>
					<td width="750" height="85" colspan="2"><div style="width: 750px; height: 95px; background-color: #222222; color: #ffffff; font-size: 20pt; line-height: 95px;"><div style="margin-left: 20px;">{{ $site_name }}</div></div></td>
				</tr>
				<tr>
					<td width="697">
						<table width="100%" border="0" bgcolor="white" cellspacing="0" cellpadding="0" style="color:#3e3e3e">
							<tbody><tr>
								<td style="padding:40px 0 0 0">
								<p style="font-weight:bold;font-size:15px;margin-left:25px">Dear {{ $username }},</p>
								<p style="font-size:14px;margin-left:25px;margin-right:25px">In order to complete your registration at <strong>{{ $site_name }}</strong>, you need to activate your account by clicking on the link below.</p>
								</td>
							</tr>
							<tr>
								<td height="20"></td>
							</tr>
							<tr>
								<td><p style="padding:0;margin:0;margin-left:25px"><a target="_blank" style="text-decoration:none" href="{{ $link }}"><span style="padding:7px 7px;color:#fff;line-height:100%;display:inline-block;background-color:#333333;width:299px;text-align:center;font-weight:bold">Activate your account</span></a></p></td>
							</tr>
							
							<tr>
								<td height="35"></td>
							</tr>
							<tr>
								<td style="padding-bottom:20px"><p style="padding:0;margin:0;font-size:14px;margin-left:25px">Best regards,<br><br><strong>The {{ $site_name }} Staff</strong></p></td>
							</tr>
							<tr>
								<td><p style="padding:0;margin:0;margin-left:25px"><a target="_blank" style="text-decoration:none" href="{{ URL::route('home') }}"><span style="padding:7px 7px;color:#fff;line-height:100%;display:inline-block;background-color:#333333;width:299px;text-align:center;font-weight:bold">Website</span></a></p></td>
							</tr>
							<tr>
								<td height="15">&nbsp;</td>
							</tr>
						</tbody></table>
					</td>
				</tr>
			</tbody></table>
		</td>
	</tr>
</tbody></table>
	</body>
</html>
