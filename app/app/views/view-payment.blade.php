@extends('layouts.base')

@section('content')
		<div class="container reduce-top">
			<div class="page-header">
				<h1>{{ trans('view.payment_details') }}</h1>
			</div>

			<div class="col-md-7">
				<div class="row">
					<table width="100%">
						<tr>
							<td>{{ trans('view.server_td') }}</td>
							<td><a href="{{ URL::route('server-page', $payment->server['id']) }}">{{ $payment->server['address'] }}</a></td>
						</tr>
						<tr>
							<td>{{ trans('view.payer_name_td') }}</td>
							<td>{{ $payment->payer_first_name }} {{ $payment->payer_last_name }}</td>
						</tr>
						<tr>
							<td>{{ trans('view.date_start_td') }}</td>
							<td>{{ $payment->start }}</td>
						</tr>
						<tr>
							<td>{{ trans('view.date_end_td') }}</td>
							<td>{{ $payment->end }}</td>
						</tr>
						<tr>
							<td>{{ trans('view.amount_sent_td') }}</td>
							<td>{{ Config::get('app.vip_currency') == 'USD' ? '$' : '€' }}{{ $payment->mc_gross }}</td>
						</tr>
						<tr>
							<td>{{ trans('view.amount_expected') }}</td>
							<td>{{ Config::get('app.vip_currency') == 'USD' ? '$' : '€' }}{{ Config::get('app.amount_day') * $payment->days }}</td>
						</tr>
						<tr>
							<td>{{ trans('view.payment_status_td') }}</td>
							<td>{{ ucfirst($payment->payment_status) }}</td>
						</tr>
					</table>	
				</div>
			</div>
		</div>
@stop
