<section class="JS-Modal-Blackout" id="Modal-Diamond">
    <div class="JS-Modal Pay-Popup Cabinet">
        <div class="Cabinet-Head">
            <h2>Выделить сервер цветом</h2>
            <span class="Cabinet-Close JS-Modal-Close">×</span>
        </div>

        <div class="Cabinet-Content">
            <form action="/pay" method="post">
                <label for="count">Количество алмазов</label>
                <input type="hidden" name="type" value="diamond">
                <input type="hidden" name="server_id" value="{{$server->id}}">

                <input type="text" name="count" id="count" value="1">

                <label for="pay_type">Срок действия алмазов</label>
                <select name="pay_type" id="pay_type">
                    @foreach($type_diamond as $diamond)
                        <option data-pay_types-id="{{$diamond->id}}"
                                data-sale="{{$diamond->cost * ($diamond->sale)/100}}"
                                data-type="{{Config::get('app.pay_types.' . $diamond->type)}}"
                                data-value="{{$diamond->cost * (100 - $diamond->sale)/100}}"
                                value="{{$diamond->id}}">
                            {{$diamond->name}}
                            @if($diamond->sale)
                                (скидка {{$diamond->sale}}%)
                            @endif
                        </option>
                    @endforeach
                </select>

                <p class="Full-Cost">Общая стоимость:
                    <span>{{$type_diamond[0]->cost * (100 - $type_diamond[0]->sale)/100}}</span> р.</p>

                <p class="Full-Sale"> @if($type_diamond[0]->sale > 0)Скидка:
                    <span>{{$type_diamond[0]->cost * ($type_diamond[0]->sale)/100}}</span> р.@endif</p>

                <button class="Btn Primary">Оплатить</button>
            </form>
        </div>
    </div>
</section>
