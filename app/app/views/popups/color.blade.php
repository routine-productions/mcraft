<section class="JS-Modal-Blackout" id="Modal-Color">
    <div class="JS-Modal Pay-Popup Cabinet">
        <div class="Cabinet-Head">
            <h2>Выделить сервер цветом</h2>
            <span class="Cabinet-Close JS-Modal-Close">×</span>
        </div>

        <div class="Cabinet-Content">
            <form action="/pay" method="post">
                <label for="color">Выберите цвет</label>
                <input type="hidden" name="type" value="color">
                <input type="hidden" name="server_id" value="{{$server->id}}">
                <select name="color" id="color">
                    @foreach(Config::get('app.colors') as $colorKey =>$color)
                        <option value="{{$colorKey}}">{{$color}}</option>
                    @endforeach
                </select>

                <label for="pay_type">Срок выделения цветом</label>
                <select name="pay_type" id="pay_type">
                    @foreach($type_color as $color)
                        <option data-pay_types-id="{{$color->id}}"
                                data-sale="{{$color->cost * ($color->sale)/100}}"
                                data-type="{{Config::get('app.pay_types.' . $color->type)}}"
                                data-value="{{$color->cost * (100 - $color->sale)/100}}"
                                value="{{$color->id}}">
                            {{$color->name}}
                            @if($color->sale)
                                (скидка {{$color->sale}}%)
                            @endif
                        </option>
                    @endforeach
                </select>

                <p class="Full-Cost">Общая стоимость:
                    <span>{{$type_color[0]->cost * (100 - $type_color[0]->sale)/100}}</span> р.</p>

                <p class="Full-Sale">
                    @if($type_color[0]->sale > 0)Скидка:
                    <span>{{$type_color[0]->cost * ($type_color[0]->sale)/100}}</span>
                    р.
                    @endif
                </p>


                <p>После оплаты ваш сервер в списке выделится выбранным цветом.</p>
                <button class="Btn Primary">Оплатить</button>
            </form>
        </div>
    </div>
</section>
