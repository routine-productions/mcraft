<section class="JS-Modal-Blackout" id="Modal-Banner">
    <div class="JS-Modal Pay-Popup Cabinet">
        <div class="Cabinet-Head">
            <h2>Приобрести баннер</h2>
            <span class="Cabinet-Close JS-Modal-Close">×</span>
        </div>

        <div class="Cabinet-Content">
            <form action="/pay" method="post">

                <input type="hidden" name="type" value="banner">
                <input type="hidden" name="server_id" value="{{$server->id}}">
                <label for="pay_type">Срок действия баннера</label>
                <select name="pay_type" id="pay_type">
                    @foreach($type_banner as $banner)
                        <option data-pay_types-id="{{$banner->id}}"
                                data-sale="{{$banner->cost * ($banner->sale)/100}}"
                                data-type="{{Config::get('app.pay_types.' . $banner->type)}}"
                                data-value="{{$banner->cost * (100 - $banner->sale)/100}}"
                                value="{{$banner->id}}"
                        >
                            {{$banner->name}}
                            @if($banner->sale)
                                (скидка {{$banner->sale}}%)
                            @endif
                        </option>
                    @endforeach
                </select>

                <p class="Full-Cost">Общая стоимость:
                    <span>{{$type_banner[0]->cost * (100 - $type_banner[0]->sale)/100}}</span> р.</p>

                <p class="Full-Sale">@if($type_banner[0]->sale > 0)Скидка:
                    <span>{{$type_banner[0]->cost * ($type_banner[0]->sale)/100}}</span> р.@endif</p>

                <p>После оплаты баннер отобразится на сайте.</p>
                <button class="Btn Primary">Оплатить</button>
            </form>
        </div>
    </div>
</section>
