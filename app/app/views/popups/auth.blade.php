<section class="JS-Modal-Blackout" id="Modal-1">
    <div class="Cabinet JS-Modal JS-Tabs">
        <div class="Cabinet-Head">
            <h3 class="Cabinet-Title">Личный кабинет</h3>
            <span class="Cabinet-Close JS-Modal-Close">×</span>
        </div>
        <ul class="Cabinet-Nav JS-Tabs-Navigation">
            <li class="JS-Tab Active" data-href="Tab-1">
                Вход
            </li>
            <li class="JS-Tab" data-href="Tab-2">
                Регистрация
            </li>
        </ul>

        <div class="JS-Tabs-Content">
            <div class="Cabinet-Content" data-tab='Tab-1'>
                <form action="/account/login" method="post" class="Login">
                    <ul>
                        <li>
                            <label for="email">Email</label>
                            <input type="text" id="email" name="email">
                        </li>
                        <li>
                            <label for="password">Пароль</label>
                            <input type="password" id="password" name="password">
                        </li>
                    </ul>
                    <div class="Cabinet-Buttons">
                        <button class="Cabinet-Enter">
                            Войти
                        </button>
                        <div class="Restore-Password">
                            <a href="#">
                                Восстановить пароль
                            </a>
                        </div>
                    </div>
                    <div class="Cabinet-Message"></div>
                    {{ Form::token() }}
                </form>
            </div>

            <div class="Cabinet-Content" data-tab='Tab-2'>
                <form action="/account/register" method="post" class="Registration">
                    <ul>
                        <li>
                            <label for="username">Имя пользователя</label>
                            <input type="text" id="username" name="username">
                        </li>
                        <li>
                            <label for="email">Email</label>
                            <input type="text" id="email" name="email">
                        </li>
                        <li>
                            <label for="password">Пароль</label>
                            <input type="password" id="password" name="password">
                        </li>
                        <li>
                            {{--{{ Form::captcha(array('template' => 'captcha')) }}--}}
                        </li>
                    </ul>
                    <div class="Cabinet-Buttons">
                        <button class="Cabinet-Enter">
                            Зарегистрироваться
                        </button>
                    </div>
                    <div class="Cabinet-Message"></div>
                    {{ Form::token() }}
                </form>
            </div>

            <div class="Cabinet-Content" data-tab='Tab-3'>
                <form action="/account/forgot-password" method="post" class="Reset-Password">
                    <ul>
                        <li>
                            <label for="#email">Email</label>
                            <input type="text" id="email" name="email">
                        </li>
                    </ul>
                    <div class="Cabinet-Buttons">
                        <button class="Cabinet-Enter">
                            Восстановить пароль
                        </button>
                    </div>
                    <div class="Cabinet-Message"></div>
                    {{ Form::token() }}
                </form>
            </div>
        </div>
    </div>
</section>
