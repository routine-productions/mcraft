@extends('layouts.base')
<?php session_start(); ?>
@section('content')
    @if(isset($servers) && count($servers) > 0)
        @if(isset($h1))
            <h1>{{$h1}}</h1>
        @endif
        @if (isset($seo_text))
            <div class="Seo-Text">{{ $seo_text }}</div>
        @endif
        @if(!empty($message))
            <div class="Warning">{{$message}}</div>
        @endif
    @endif

    <ul class="Article-List">
        @if(isset($servers) && count($servers)>0)
            @foreach ($servers as $server_key => $server)
                <li class="Card-Item @if(!empty($server->purchases_color))Color-{{$server->purchases_color->color}}@endif">
                    <article>
                        <div class="Card-Wrapper">
                            <div class="Card-Head">
                                <div class="Card-Path">
                                    <span class="Card-Number"># {{($server_key+1) + ($page-1) * Config::get('app.servers_per_home')}}</span>
                                    <h3 class="Card-Title"><a href="/server/{{$server->id}}">{{$server->name}}</a></h3>
                                </div>
                                <div class="Card-Path">
                                    @if(isset($server->version))
                                        <span class="Card-Version">
                                            <a href="/servera-{{$server->version->slug }}">{{$server->version->name }}</a>
                                        </span>
                                    @endif

                                    <div class="Card-Users">
                                        <svg><use xlink:href="#user"></use></svg>
                                        <span>
                                            @if ($server->statistics['max_players'])
                                                {{$server->statistics['players']}} / {{$server->statistics['max_players']}}
                                            @else
                                                N/A
                                            @endif
                                        </span>
                                    </div>
                                    <div class="Card-Diamond">
                                        <img src="/assets/img/Diamond.png" alt="">
                                        <span>{{ $server->purchases_diamonds->sum('diamonds') }}</span>
                                    </div>
                                    @if(!$server->already_voted)
                                        <div class="Card-Liker">
                                            <span class="Like-Count">{{$server->votes}}</span>
                                            <a href="{{URL::route('server-vote',$server->id)}}"
                                               class="Like-Up">Голосовать</a>
                                        </div>
                                    @else
                                        <div class="Card-Liker">
                                            <span class="Like-Count">{{$server->votes}}</span>
                                            <a href="#" class="Like-Up Like-Warning">Вы уже проголосовали</a>
                                        </div>
                                    @endif
                                </div>
                            </div>

                            <div class="Card-Content">
                                <div class="Card-Path">
                                    <p class="Card-Description">
                                        {{ BBCode::parse($server->short_description) }}
                                    </p>
                                    <a href="/server/{{$server->id}}" class="Btn Primary Read-More">
                                        <span>Подробнее</span>
                                        <svg>
                                            <use xlink:href="#arrow-direct"></use>
                                        </svg>
                                    </a>
                                </div>
                                <div class="Card-Path">
                                    <a class="Card-Advertising" href="/server/{{$server->id}}">
                                        @if($server->banner)
                                            <img src="{{ asset('uploads/images/banners/' . $server->banner) }}  "
                                                 alt="">
                                        @else
                                            <img title="{{ $server->name }}" alt="{{ $server->name }}"
                                                 src="{{ URL::route('server-banner', array($server->id, '468x60', $server->id%4+1)) }}">
                                        @endif
                                    </a>

                                    @if(count($server->tags))
                                        <div class="Card-Tags">
                                            <svg class="Tags-Logo"><use xlink:href="#tags"></use></svg>
                                            <ul>
                                                @foreach($server->tags as $tag)
                                                    <li><a href="/servera-{{$tag->slug}}">{{$tag->name}}</a></li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif

                                    @if(count($server->modes))
                                        <div class="Card-Tags">
                                            <svg class="Tags-Logo"><use xlink:href="#tags"></use></svg>
                                            <ul>
                                                @foreach($server->modes as $mode)
                                                    <li><a href="/servera-{{$mode->slug}}">{{$mode->name}}</a></li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif

                                    @if(count($server->games))
                                        <div class="Card-Tags">
                                            <svg class="Tags-Logo"><use xlink:href="#tags"></use></svg>
                                            <ul>
                                                @foreach($server->games as $game)
                                                    <li><a href="/servera-{{$game->slug}}">{{$game->name}}</a></li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </article>
                </li>
            @endforeach
        @else
            @include('errors.error',['h1'=>'ERROR 404','description'=>'По данному запросу серверов не найдено...'])
        @endif
    </ul>
    {{ $servers->links() }}
@stop

@section('ads_bottom')
    {{ Config::get('app.ads_bottom') }}
@stop