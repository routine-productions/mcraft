@extends('layouts.base')



@section('content')

    <section class="Card">
        <header class="Card-Heading">
            <h2>FAQ</h2>
        </header>
        <div class="Card-Content">
            <div class="Card-Text">
                {{ BBCode::parse(Config::get('app.faq')) }}
            </div>
        </div>
    </section>
@stop