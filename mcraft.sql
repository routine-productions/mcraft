-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Сен 21 2016 г., 14:55
-- Версия сервера: 10.1.13-MariaDB
-- Версия PHP: 7.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `mcraft`
--

-- --------------------------------------------------------

--
-- Структура таблицы `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `server_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `code` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `countries`
--

INSERT INTO `countries` (`id`, `code`, `name`) VALUES
(1, 'US', 'United States'),
(2, 'CA', 'Canada'),
(3, 'AF', 'Afghanistan'),
(4, 'AL', 'Albania'),
(5, 'DZ', 'Algeria'),
(6, 'DS', 'American Samoa'),
(7, 'AD', 'Andorra'),
(8, 'AO', 'Angola'),
(9, 'AI', 'Anguilla'),
(10, 'AQ', 'Antarctica'),
(11, 'AG', 'Antigua and/or Barbuda'),
(12, 'AR', 'Argentina'),
(13, 'AM', 'Armenia'),
(14, 'AW', 'Aruba'),
(15, 'AU', 'Australia'),
(16, 'AT', 'Austria'),
(17, 'AZ', 'Azerbaijan'),
(18, 'BS', 'Bahamas'),
(19, 'BH', 'Bahrain'),
(20, 'BD', 'Bangladesh'),
(21, 'BB', 'Barbados'),
(22, 'BY', 'Belarus'),
(23, 'BE', 'Belgium'),
(24, 'BZ', 'Belize'),
(25, 'BJ', 'Benin'),
(26, 'BM', 'Bermuda'),
(27, 'BT', 'Bhutan'),
(28, 'BO', 'Bolivia'),
(29, 'BA', 'Bosnia and Herzegovina'),
(30, 'BW', 'Botswana'),
(31, 'BV', 'Bouvet Island'),
(32, 'BR', 'Brazil'),
(33, 'IO', 'British lndian Ocean Territory'),
(34, 'BN', 'Brunei Darussalam'),
(35, 'BG', 'Bulgaria'),
(36, 'BF', 'Burkina Faso'),
(37, 'BI', 'Burundi'),
(38, 'KH', 'Cambodia'),
(39, 'CM', 'Cameroon'),
(40, 'CV', 'Cape Verde'),
(41, 'KY', 'Cayman Islands'),
(42, 'CF', 'Central African Republic'),
(43, 'TD', 'Chad'),
(44, 'CL', 'Chile'),
(45, 'CN', 'China'),
(46, 'CX', 'Christmas Island'),
(47, 'CC', 'Cocos (Keeling) Islands'),
(48, 'CO', 'Colombia'),
(49, 'KM', 'Comoros'),
(50, 'CG', 'Congo'),
(51, 'CK', 'Cook Islands'),
(52, 'CR', 'Costa Rica'),
(53, 'HR', 'Croatia (Hrvatska)'),
(54, 'CU', 'Cuba'),
(55, 'CY', 'Cyprus'),
(56, 'CZ', 'Czech Republic'),
(57, 'DK', 'Denmark'),
(58, 'DJ', 'Djibouti'),
(59, 'DM', 'Dominica'),
(60, 'DO', 'Dominican Republic'),
(61, 'TP', 'East Timor'),
(62, 'EC', 'Ecuador'),
(63, 'EG', 'Egypt'),
(64, 'SV', 'El Salvador'),
(65, 'GQ', 'Equatorial Guinea'),
(66, 'ER', 'Eritrea'),
(67, 'EE', 'Estonia'),
(68, 'ET', 'Ethiopia'),
(69, 'FK', 'Falkland Islands (Malvinas)'),
(70, 'FO', 'Faroe Islands'),
(71, 'FJ', 'Fiji'),
(72, 'FI', 'Finland'),
(73, 'FR', 'France'),
(74, 'FX', 'France, Metropolitan'),
(75, 'GF', 'French Guiana'),
(76, 'PF', 'French Polynesia'),
(77, 'TF', 'French Southern Territories'),
(78, 'GA', 'Gabon'),
(79, 'GM', 'Gambia'),
(80, 'GE', 'Georgia'),
(81, 'DE', 'Germany'),
(82, 'GH', 'Ghana'),
(83, 'GI', 'Gibraltar'),
(84, 'GR', 'Greece'),
(85, 'GL', 'Greenland'),
(86, 'GD', 'Grenada'),
(87, 'GP', 'Guadeloupe'),
(88, 'GU', 'Guam'),
(89, 'GT', 'Guatemala'),
(90, 'GN', 'Guinea'),
(91, 'GW', 'Guinea-Bissau'),
(92, 'GY', 'Guyana'),
(93, 'HT', 'Haiti'),
(94, 'HM', 'Heard and Mc Donald Islands'),
(95, 'HN', 'Honduras'),
(96, 'HK', 'Hong Kong'),
(97, 'HU', 'Hungary'),
(98, 'IS', 'Iceland'),
(99, 'IN', 'India'),
(100, 'ID', 'Indonesia'),
(101, 'IR', 'Iran (Islamic Republic of)'),
(102, 'IQ', 'Iraq'),
(103, 'IE', 'Ireland'),
(104, 'IL', 'Israel'),
(105, 'IT', 'Italy'),
(106, 'CI', 'Ivory Coast'),
(107, 'JM', 'Jamaica'),
(108, 'JP', 'Japan'),
(109, 'JO', 'Jordan'),
(110, 'KZ', 'Kazakhstan'),
(111, 'KE', 'Kenya'),
(112, 'KI', 'Kiribati'),
(113, 'KP', 'Korea, Democratic People''s Republic of'),
(114, 'KR', 'Korea, Republic of'),
(115, 'XK', 'Kosovo'),
(116, 'KW', 'Kuwait'),
(117, 'KG', 'Kyrgyzstan'),
(118, 'LA', 'Lao People''s Democratic Republic'),
(119, 'LV', 'Latvia'),
(120, 'LB', 'Lebanon'),
(121, 'LS', 'Lesotho'),
(122, 'LR', 'Liberia'),
(123, 'LY', 'Libyan Arab Jamahiriya'),
(124, 'LI', 'Liechtenstein'),
(125, 'LT', 'Lithuania'),
(126, 'LU', 'Luxembourg'),
(127, 'MO', 'Macau'),
(128, 'MK', 'Macedonia'),
(129, 'MG', 'Madagascar'),
(130, 'MW', 'Malawi'),
(131, 'MY', 'Malaysia'),
(132, 'MV', 'Maldives'),
(133, 'ML', 'Mali'),
(134, 'MT', 'Malta'),
(135, 'MH', 'Marshall Islands'),
(136, 'MQ', 'Martinique'),
(137, 'MR', 'Mauritania'),
(138, 'MU', 'Mauritius'),
(139, 'TY', 'Mayotte'),
(140, 'MX', 'Mexico'),
(141, 'FM', 'Micronesia, Federated States of'),
(142, 'MD', 'Moldova, Republic of'),
(143, 'MC', 'Monaco'),
(144, 'MN', 'Mongolia'),
(145, 'ME', 'Montenegro'),
(146, 'MS', 'Montserrat'),
(147, 'MA', 'Morocco'),
(148, 'MZ', 'Mozambique'),
(149, 'MM', 'Myanmar'),
(150, 'NA', 'Namibia'),
(151, 'NR', 'Nauru'),
(152, 'NP', 'Nepal'),
(153, 'NL', 'Netherlands'),
(154, 'AN', 'Netherlands Antilles'),
(155, 'NC', 'New Caledonia'),
(156, 'NZ', 'New Zealand'),
(157, 'NI', 'Nicaragua'),
(158, 'NE', 'Niger'),
(159, 'NG', 'Nigeria'),
(160, 'NU', 'Niue'),
(161, 'NF', 'Norfork Island'),
(162, 'MP', 'Northern Mariana Islands'),
(163, 'NO', 'Norway'),
(164, 'OM', 'Oman'),
(165, 'PK', 'Pakistan'),
(166, 'PW', 'Palau'),
(167, 'PA', 'Panama'),
(168, 'PG', 'Papua New Guinea'),
(169, 'PY', 'Paraguay'),
(170, 'PE', 'Peru'),
(171, 'PH', 'Philippines'),
(172, 'PN', 'Pitcairn'),
(173, 'PL', 'Poland'),
(174, 'PT', 'Portugal'),
(175, 'PR', 'Puerto Rico'),
(176, 'QA', 'Qatar'),
(177, 'RE', 'Reunion'),
(178, 'RO', 'Romania'),
(179, 'RU', 'Russian Federation'),
(180, 'RW', 'Rwanda'),
(181, 'KN', 'Saint Kitts and Nevis'),
(182, 'LC', 'Saint Lucia'),
(183, 'VC', 'Saint Vincent and the Grenadines'),
(184, 'WS', 'Samoa'),
(185, 'SM', 'San Marino'),
(186, 'ST', 'Sao Tome and Principe'),
(187, 'SA', 'Saudi Arabia'),
(188, 'SN', 'Senegal'),
(189, 'RS', 'Serbia'),
(190, 'SC', 'Seychelles'),
(191, 'SL', 'Sierra Leone'),
(192, 'SG', 'Singapore'),
(193, 'SK', 'Slovakia'),
(194, 'SI', 'Slovenia'),
(195, 'SB', 'Solomon Islands'),
(196, 'SO', 'Somalia'),
(197, 'ZA', 'South Africa'),
(198, 'GS', 'South Georgia South Sandwich Islands'),
(199, 'ES', 'Spain'),
(200, 'LK', 'Sri Lanka'),
(201, 'SH', 'St. Helena'),
(202, 'PM', 'St. Pierre and Miquelon'),
(203, 'SD', 'Sudan'),
(204, 'SR', 'Suriname'),
(205, 'SJ', 'Svalbarn and Jan Mayen Islands'),
(206, 'SZ', 'Swaziland'),
(207, 'SE', 'Sweden'),
(208, 'CH', 'Switzerland'),
(209, 'SY', 'Syrian Arab Republic'),
(210, 'TW', 'Taiwan'),
(211, 'TJ', 'Tajikistan'),
(212, 'TZ', 'Tanzania, United Republic of'),
(213, 'TH', 'Thailand'),
(214, 'TG', 'Togo'),
(215, 'TK', 'Tokelau'),
(216, 'TO', 'Tonga'),
(217, 'TT', 'Trinidad and Tobago'),
(218, 'TN', 'Tunisia'),
(219, 'TR', 'Turkey'),
(220, 'TM', 'Turkmenistan'),
(221, 'TC', 'Turks and Caicos Islands'),
(222, 'TV', 'Tuvalu'),
(223, 'UG', 'Uganda'),
(224, 'UA', 'Ukraine'),
(225, 'AE', 'United Arab Emirates'),
(226, 'GB', 'United Kingdom'),
(227, 'UM', 'United States minor outlying islands'),
(228, 'UY', 'Uruguay'),
(229, 'UZ', 'Uzbekistan'),
(230, 'VU', 'Vanuatu'),
(231, 'VA', 'Vatican City State'),
(232, 'VE', 'Venezuela'),
(233, 'VN', 'Vietnam'),
(234, 'VG', 'Virigan Islands (British)'),
(235, 'VI', 'Virgin Islands (U.S.)'),
(236, 'WF', 'Wallis and Futuna Islands'),
(237, 'EH', 'Western Sahara'),
(238, 'YE', 'Yemen'),
(239, 'YU', 'Yugoslavia'),
(240, 'ZR', 'Zaire'),
(241, 'ZM', 'Zambia'),
(242, 'ZW', 'Zimbabwe');

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2013_11_14_004605_create_settings_table', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `pay_types`
--

CREATE TABLE `pay_types` (
  `id` int(11) NOT NULL,
  `type` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `days` int(11) NOT NULL,
  `cost` int(11) NOT NULL,
  `sale` int(64) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `pay_types`
--

INSERT INTO `pay_types` (`id`, `type`, `name`, `days`, `cost`, `sale`, `created_at`, `updated_at`) VALUES
(1, 'diamond', '7 дней', 7, 5, 0, '2016-09-03 06:47:38', '2016-09-03 07:56:56'),
(18, 'diamond', '14 дней', 14, 10, 5, '2016-09-03 07:58:30', '2016-09-03 07:58:30'),
(19, 'diamond', '1 месяц', 30, 20, 10, '2016-09-03 08:00:20', '2016-09-03 08:00:20'),
(20, 'diamond', '2 месяца', 60, 40, 10, '2016-09-03 08:01:06', '2016-09-03 08:01:42'),
(21, 'diamond', '3 месяца', 90, 60, 20, '2016-09-03 08:02:00', '2016-09-03 08:02:00'),
(22, 'diamond', '6 месяцев', 182, 120, 30, '2016-09-03 08:02:00', '2016-09-03 08:02:52'),
(23, 'banner', '1 месяц', 30, 200, 0, '2016-09-03 08:03:25', '2016-09-03 08:03:25'),
(24, 'banner', '3 месяца', 90, 600, 10, '2016-09-03 08:04:02', '2016-09-03 08:04:02'),
(25, 'banner', '6 месяцев', 182, 1200, 20, '2016-09-03 08:05:11', '2016-09-03 08:05:11'),
(26, 'banner', 'Год', 365, 2400, 30, '2016-09-03 08:05:37', '2016-09-03 08:05:37'),
(27, 'color', '1 месяц', 30, 100, 0, '2016-09-03 08:07:23', '2016-09-03 08:07:23'),
(28, 'color', '3 месяца', 90, 300, 10, '2016-09-03 08:07:48', '2016-09-03 08:07:48'),
(29, 'color', '6 месяцев', 182, 600, 20, '2016-09-03 08:08:27', '2016-09-03 08:09:44'),
(30, 'color', 'Год', 365, 1200, 30, '2016-09-03 08:10:14', '2016-09-03 08:10:14');

-- --------------------------------------------------------

--
-- Структура таблицы `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `alias` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `intro` text COLLATE utf8_unicode_ci NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8_unicode_ci,
  `meta_keywords` text COLLATE utf8_unicode_ci,
  `seo_text` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `posts`
--

INSERT INTO `posts` (`id`, `alias`, `title`, `intro`, `text`, `img`, `created_at`, `updated_at`, `meta_title`, `meta_description`, `meta_keywords`, `seo_text`) VALUES
(1, 'start_work', 'Старт ОБТ сервера CivCraft 1.8.9!', 'Сервер CivCraft запущен в режиме ОБТ. На сайт и в мониторинг будет добавлен чуть позже. Сервер CivCraft запущен в режиме ОБТ. На сайт и в мониторинг будет добавлен чуть позже.\r\n Сервер CivCraft запущен в режиме ОБТ. На сайт и в мониторинг будет добавлен \r\nasadfчуть позже', 'Сервер CivCraft запущен в режиме ОБТ. На сайт и в мониторинг будет добавлен чуть позже.\r\n\r\nПолезные ссылки: asdf\r\n - Описание сервера и его режима игры\r\n - Раздел форума\r\n - Слив багов/дюпов\r\n - Обсуждение', '/uploads/images/news/img-72426.jpg', '2016-07-15 00:00:00', '2016-07-20 00:00:00', NULL, NULL, NULL, NULL),
(2, 'official', 'Официальный фильм майнкрафт', 'Добрый день, с вами я - Vovan1955. Сегодня я вам расскажу удивительную новость о фильме Майнкрафт. Не обычный фильм, а от самих Mojang и WB! Начнем пожалуй наш рассказ', 'Информация\r\n\r\nКинокомпания WB подписала контракт с Mojang о создании официального фильма Minecraft. Действительно было бы интересно, увидеть кубики на экране в кино, но, полазив еще в интернете, увидел следующее Строки: Фильм майнкрафт не будет кубическим, его будут делать из реальной жизни Верить этом или нет, решать вам, но логически подумать нужно. Майнкрафт - не майнкрафт если в нем нет кубиков, да, воплотить фильм в кубиках будет сложно, ибо там нет сглаживания костей и т.д.\r\n\r\nБудет ли фильм успехом?\r\n\r\n\r\nФильм не будет таким прибыльным, так как он рассчитан на аудиторию 6+. В отличии от WarCraft, его аудитория 12+, посетило очень много человек, но на кубики не пойдут выше 16 лет, пойдут только 3+,6+ и т.д. Так что фильм будет не прибыльным, и не сможет окупиться на такой маленькой аудитории. Но если же все такие маленькие дети выпросят у родителей денежку на этот фильм, и на фильм пойдёт пол детская половина человечества, то фильм сможет окупиться, но не уйти в + на большие суммы.\r\n\r\nДата выхода\r\n\r\nКомпания WB и Mojang заявили официальную дату выхода фильма. Это 24 мая 2019 года.\r\n\r\nЕще один фильм\r\n\r\nПомимо этого фильма, выйдет еще Фан-Официальный фильм по майнкрафту от компании DiamondGame. Трейлер которого покажу зимой 2016-2017 года.\r\n\r\nВот и всё, не смею вас больше задерживать. Желаю удачи вам всем. Пока :)', '/uploads/images/news/img-64936.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL),
(3, 'm1.1', 'Релиз Minecraft 1.10 - Frostburn Update', 'Доброго времени суток, уважаемые читатели. Сегодня я хотел бы рассказать о вышедшем релизе 1.10, выпущенный 8 июня и получивший название - Frostburn Update.\r\nИтак, приступим!', 'Первое, что хотелось бы рассказать, это конечно же - мобы. Их целых 3!\r\n\r\nПолярный медведь - нейтральный моб, появляющийся только в снежных биомах. Если взрослая особь будет рядом со своими детёнышами, а вы подойдете слишком близко, то медведь вас атакует. При нападении, он встает на задние лапы и наносит критический удар, падая лапами вниз на игрока. С мишки дропается либо обычная рыба, либо лосось. \r\n Показать / Скрыть текст\r\n\r\nПесчаный зомби (Husk) - новый вид зомби, которого можно найти только в жарких биомах. Его особенность заключается в том, что он не горит днём. Опасен ещё и тем, что при ударе, наносит на игрока эффект голода. \r\n Показать / Скрыть текст\r\n\r\nБродячий скелет (Stray) - новый вид скелета, встречающийся в холодных биомах. От обычного скелета отличается тем, что ходит в рваной одежде. Также, стреляет не обычными стрелами, а с эффектом замедления I. \r\n Показать / Скрыть текст\r\n\r\nВсе три моба довольно опасны на ранних стадиях игры, так что будьте осторожны.\r\n\r\n\r\nВторое - это изменения в генерации мира.\r\n\r\n-Интересное изменение - теперь эндермены есть и в аду! Теперь, это единственные мобы, которые присутствуют во всех трёх мирах.\r\n Показать / Скрыть текст\r\n\r\n-В аду генерируются участки с блоками магмы, наступая на которые игрок получает урон. Но урон можно избежать, если зажать Shitft.\r\n Показать / Скрыть текст\r\n\r\n-В обычном мире деревни будут иметь свой вариант, в зависимости от биома, поскольку теперь они появляются в саванне и тайге. Также, вместо гравийных дорожек в деревнях генерируются тропинки. \r\n Показать / Скрыть текст\r\n\r\n-В новой версии от висячих в воздухе песка и гравия исходят частицы вниз, придавая вид, что они осыпаются.\r\n-Сокровищницы стали чаще генерироваться в мире.\r\n-Под землёй могут генерироваться останки животных, состоящие из костных блоков(из которых можно получить костную муку).\r\n-Огромные грибы могут расти ещё выше!\r\n\r\n\r\nТретье - новые блоки.\r\n\r\n1. Добавлен крафт блока адского нароста, требуется заполнить в верстаке все ячейки адским наростом. \r\n Показать / Скрыть текст\r\n\r\n2. Добавлен крафт красного адского кирпича, для этого нужно расположить в шахматном порядке адский нарост и адский кирпич на 4 ячейки. \r\n Показать / Скрыть текст\r\n\r\n3. Добавлен блок магмы, который крафтится из лавового крема, в кол-ве 4 штук, расположенных рядом друг с другом в сетке крафта. \r\n Показать / Скрыть текст\r\n\r\n4. Добавлен блок костной муки, который можно найти, но можно и скрафтить, заполнив все ячейки верстака костной мукой. Крафт можно обратить, и получить костную муку обратно. \r\n Показать / Скрыть текст\r\n Показать / Скрыть текст\r\n\r\n5. Добавлены структурные блоки для удобства создания пользовательских карт(их можно получить только командой:/give @p minecraft:structure_block).\r\n\r\nНу и последнее, довольно интересное обновление, портированное из Minecraft Pocket Edition - Авто-прыжок. По умолчанию включен. \r\n Показать / Скрыть текст\r\n\r\n\r\nЯ рассказал вам о самых главных и важных нововведениях в 1.10, хотя представлены здесь не все. На этом моя статья заканчивается, спасибо за внимание, дорогие читатели!', '/uploads/images/news/img-21806.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL),
(4, 'zdfsdfaf', 'sfdfbf', 'fgsdfgs', 'dfgsdfg', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'sdgad11', 'fsdfg22', 'bsfdgsdf33', 'gsadfsd44');

-- --------------------------------------------------------

--
-- Структура таблицы `purchases`
--

CREATE TABLE `purchases` (
  `id` int(11) NOT NULL,
  `server_id` int(11) NOT NULL,
  `type` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `days` int(11) NOT NULL,
  `cost` int(11) NOT NULL,
  `diamonds` int(11) NOT NULL,
  `color` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `end_period` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `purchases`
--

INSERT INTO `purchases` (`id`, `server_id`, `type`, `days`, `cost`, `diamonds`, `color`, `end_period`, `created_at`, `updated_at`) VALUES
(1, 13, 'banner', 30, 1800, 0, 'red', '2016-10-02 15:34:40', '2016-09-02 15:34:40', '2016-09-03 08:52:54'),
(7, 13, 'diamond', 65, 3000, 500, 'red', '2016-11-07 10:22:06', '2016-09-03 10:22:06', '2016-09-03 10:25:22'),
(8, 13, 'color', 365, 2000, 0, 'blue', '2015-09-03 10:26:14', '2014-09-03 10:26:14', '2016-09-03 10:26:14'),
(9, 14, 'color', 100, 400, 0, 'purple', '2016-12-13 16:34:36', '2016-09-04 16:34:36', '2016-09-04 16:47:09'),
(10, 21, 'color', 300, 4000, 0, 'yellow', '2017-07-01 16:48:10', '2016-09-04 16:48:10', '2016-09-04 16:48:23'),
(11, 18, 'diamond', 30, 20000, 20000, 'red', '2016-10-04 16:51:17', '2016-09-04 16:51:17', '2016-09-04 16:51:20'),
(12, 13, 'diamond', 100, 400000, 50000, 'red', '2016-12-14 06:44:53', '2016-09-05 06:44:53', '2016-09-05 06:44:53');

-- --------------------------------------------------------

--
-- Структура таблицы `servers`
--

CREATE TABLE `servers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci,
  `short_description` tinytext CHARACTER SET utf8 NOT NULL,
  `address` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `port` int(4) NOT NULL,
  `website` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `version_id` int(11) NOT NULL,
  `banner` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `youtube` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `votifier_active` enum('false','true') COLLATE utf8_unicode_ci NOT NULL,
  `votifier_address` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `votifier_port` int(4) NOT NULL,
  `votifier_key` mediumtext COLLATE utf8_unicode_ci,
  `status` enum('online','offline') COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `online_time` int(11) NOT NULL,
  `verified_time` int(11) NOT NULL,
  `votes` int(11) NOT NULL,
  `points` int(11) NOT NULL,
  `rank` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8_unicode_ci,
  `meta_keywords` text COLLATE utf8_unicode_ci,
  `seo_text` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `servers`
--

INSERT INTO `servers` (`id`, `name`, `description`, `short_description`, `address`, `port`, `website`, `country_id`, `version_id`, `banner`, `youtube`, `votifier_active`, `votifier_address`, `votifier_port`, `votifier_key`, `status`, `user_id`, `online_time`, `verified_time`, `votes`, `points`, `rank`, `created_at`, `updated_at`, `meta_title`, `meta_description`, `meta_keywords`, `seo_text`) VALUES
(13, 'BigCraft Выживание 1.8-1.10', '✑✑ Наш Сайт для покупки привилегий - bigcraft.ru\r\n✒✒ Наш Уникальный IP - mc.bigserv.ru:25565\r\n✑✑ Наша группа в вк - http://vk.com/minecraft_bigcraft\r\n▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧\r\n✏ Мини игры\r\n-ʘ Sky Wars\r\n-ʘ Bed Wars\r\n-ʘ SkyBlock\r\n-ʘ TNT Run\r\n-ʘ Spleef\r\n✏ Игровая Валюта - $\r\n✏ Деньги за проведенное время - $\r\n✏ Кланы\r\n✏ Хп над головой\r\n✏ Кейсы с донатом\r\n✏ Рандомный телепорт\r\n✏ Магазин Интерфейсом - /shop\r\n✏ Красивый спавн, построенный собственными руками!\r\n✏ Моб Арена - /warp mob\r\n✏ А также Уникальное удобное меню - /menu\r\n✏ Свадьбы\r\n✏ Паркур\r\n✏ Наркотики\r\n▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧\r\n☑ Высокие гарантии в покупке доната\r\n☑ Лучшая Тех.Поддержка\r\n☑ Низкие цены на Донат\r\n▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧', 'Кланы.SkyWars.BedWars.АвтоШахта.Донат дешевый сейчас скидки!Кейсы...', 'mc.bigserv.ru', 25565, 'http://bigcraft.ru', NULL, 32, 'banner-44059.gif', '', 'true', 'mc.bigserv.ru', 25565, '', 'online', 1, 50, 51, 1, 49, 1, '2016-09-03 19:11:39', '2016-09-03 19:11:39', 'BigCraft Выживание 1.8-1.10', 'BigCraft Выживание 1.8-1.10', 'BigCraft Выживание 1.8-1.10', 'BigCraft Выживание 1.8-1.10'),
(14, 'MineLegacy 1.7.2-1.8.9 MiniGame', 'Нет дюпа, кланы, сплиф, HG, SkyWars, BedWars, \r\nбатуты, конкурсы, наркотики \r\nIP: 46.105.77.64:25565', 'Нет дюпа, кланы, сплиф, HG, SkyWars, BedWars, батуты, конкурсы, наркотики IP: 46.105.77.64:25565', '46.105.77.64', 25565, 'http://minelegacy.ru', NULL, 36, 'banner-97118.gif', '', 'true', '46.105.77.64', 25565, '', 'online', 6, 2, 2, 0, 1, 2, '2016-09-03 19:11:40', '2016-09-03 19:11:40', 'MineLegacy 1.7.2-1.8.9 MiniGame', 'MineLegacy 1.7.2-1.8.9 MiniGame', 'MineLegacy 1.7.2-1.8.9 MiniGame', 'MineLegacy 1.7.2-1.8.9 MiniGame'),
(18, 'MixCraft SURVIVAL 1.8 - 1.10', '▒ &raquo;&gt; Наш проект MixCraft | IP: mw-play.ru &laquo;&lt;▒\r\n⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯\r\n▒ ➥ Кланы\r\n▒ ➥ Уникальная система навыков!!!\r\n▒ ➥ Свадьбы\r\n▒ ➥ SkyWars,BedWars,KitPvP\r\n▒ ➥ Эффекты для игроков!\r\n▒ ➥ Питомцы,Гаджеты,Превращения,Лошадки!\r\n▒ ➥ Магазин - /warp shop\r\n⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯\r\n⏺ Самопостроеный красивый спавн!\r\n⏺ Дешевые донат-услуги\r\n⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯\r\nНаш IP: mw-play.ru.\r\nНаш сайт: mixdonate.ru', 'Marvel/DC,Magic,Survival,MiniGames,Боевые питомц для всех|Уникальные', '87.98.139.109', 25565, 'https://mixdonate.ru', NULL, 31, '', '', 'true', '87.98.139.109', 25565, '', 'online', 7, 1, 1, 1, 1, 3, '2016-09-04 14:33:57', '2016-09-04 14:33:57', 'MixCraft SURVIVAL 1.8 - 1.10', 'MixCraft SURVIVAL 1.8 - 1.10', 'MixCraft SURVIVAL 1.8 - 1.10', ''),
(19, 'Bullaran 1.9-1.10 MiniGames 1', 'ЗАХОДИ!!!! IP: play.blrn.ru\r\n\r\nBullaran- это очень интересный сервер выживания со своими изюминками!\r\nУ нас есть:\r\n+Мини-игры.\r\n+Боссы, навыки.\r\n+Большой мир с новыми видами мобов!\r\n+Автошахта, прокачка навыка шахтерства.\r\n+У нас можно пожениться.\r\n+У нас ты можешь создать непобедимый клан!\r\n+Красивый спавн с удобным Магазином\r\n--\r\nОписывать много, просто заходи и сам увидишь)))\r\nplay.blrn.ru', 'BedWars, No-Dupe, Survival, Spleef, Казино, Кейсы, Кланы', '164.132.204.207', 25565, 'http://vk.com/bullaran', NULL, 32, '', '', 'true', '164.132.204.207', 25565, '', 'online', 1, 1, 1, 0, 0, 4, '2016-09-04 14:18:19', '2016-09-04 14:18:19', 'Bullaran 1.9-1.10 MiniGames', 'Bullaran 1.9-1.10 MiniGames', 'Bullaran 1.9-1.10 MiniGames', 'Bullaran 1.9-1.10 MiniGames'),
(20, 'NeverCraft 1.9-1.10 NODUPE', '════════[NeverCraft]═════════ஜ \r\n&bull;► Бесплатные кейсы! &gt; /dcase\r\n&bull;► Версия сервера от 1.9 до 1.10\r\n&bull;► IP: mc.ncraft.ru\r\n&bull;► Survival Only, Каждые 10 минут игры дают 1000$\r\n&bull;► Кейсы из которых выпадает донат!!!\r\n&bull;► Батуты, Моб-арены, Экономика\r\n&bull;► Дешевые донат-услуги www.ncraft.ru\r\n&bull;► Группа ВК- http://vk.com/neverc\r\nஜ════════[NeverCraft]═════════ஜ', 'Хочешь поиграть, но плохие сервера повсюду? NeverCraft - хороший!', '186.2.164.97', 25565, 'http://ncraft.ru', NULL, 35, 'banner-75499.gif', '', 'true', '186.2.164.97', 25565, '', 'online', 8, 1, 1, 0, 0, 5, '2016-09-04 14:20:46', '2016-09-04 14:20:46', 'NeverCraft 1.9-1.10 NODUPE', 'NeverCraft 1.9-1.10 NODUPE', 'NeverCraft 1.9-1.10 NODUPE', 'NeverCraft 1.9-1.10 NODUPE'),
(21, 'SweetWorld 1.8-1.10 Без Дюпа', 'ஜ═══════════════════════════ஜ \r\n1. SweetWorld версия 1.8-1.10+\r\n2. Ip: mc.sweetworld.su:25565 или 151.80.111.121:25565\r\n3. Наш сайт: http://www.sweetworld.su\r\n4. Группа http://vk.com/sworldsu\r\n5. Нет лагов, стабильная работа, раздачи, 1000lvl.\r\n6. Свадьбы, Авто-Раздачи, кланы, паркур, наркотики.\r\n7. Оружие, Питомцы, лавовый паркур и другое!\r\n8. Большой онлайн, есть с кем поиграть!:)\r\n9. Желаем хорошо провести у нас время!\r\nஜ═══════════════════════════ஜ', 'Без дюпа, Кланы, Свадьбы, Голодные Игры, Питомцы, Казино', '151.80.111.121', 25565, 'http://sweetworld.su', NULL, 31, 'banner-62217.gif', '', 'true', '151.80.111.121', 25565, '', 'online', 8, 1, 1, 0, 0, 6, '2016-09-04 14:22:40', '2016-09-04 14:22:40', 'SweetWorld 1.8-1.10 Без Дюпа', 'SweetWorld 1.8-1.10 Без Дюпа', 'SweetWorld 1.8-1.10 Без Дюпа', 'SweetWorld 1.8-1.10 Без Дюпа'),
(22, 'Magic Craft 1.8-1.9-1.10', 'Magic-Craft - проект серверов без модов, который включает в себя 2 грандиозных сервера соединенных качественным лобби. Первый сервер - Magic: на нем вы сможете изучать и использовать магию, оружие, есть кейсы с привилегиями и многое другое. Второй сервер - Survival. Здесь вам предстоит играть без креатива, гриферства, зарабатывать деньги, строить, развиваться, сражаться. Сервер находится на версиях 1.8-1.10+ Зови друзей:) У нас действуют скидки на донат - 45% спеши купить.\r\nДля тех, кому скучно у нас есть миниигры (BuildBattle, BedWars)', '◄magicmc.su:25565► 2 входящих сервера! Транспорт, Выживание,', '151.80.226.207', 25565, '', NULL, 31, 'banner-75510.gif', '', 'true', '151.80.226.207', 25565, '', 'online', 8, 1, 1, 0, 0, 7, '2016-09-04 14:28:57', '2016-09-04 14:28:57', 'Magic Craft 1.8-1.9-1.10', 'Magic Craft 1.8-1.9-1.10', 'Magic Craft 1.8-1.9-1.10', 'Magic Craft 1.8-1.9-1.10'),
(23, 'AeroCraft 1.8-1.10 MiniGames', 'ஜ════════[AeroCraft]═════════ஜ \r\n&bull;► Версия сервера от 1.8 до 1.10\r\n&bull;► IP: play.mcaero.ru\r\n&bull;► Survival, Наркотики, Свадьбы, Мини-Игры и т.д\r\n&bull;► Кейсы, из которых выпадает донат!!!\r\n&bull;► Батуты, Моб-арены, Экономика\r\n&bull;► Дешевые донат-услуги aero-craft.ru\r\n&bull;► Группа ВК- http://vk.com/aero_craft\r\nஜ════════[AeroCraft]═════════ஜ', 'Качественная сборка. Недорогой донат. Мы - AeroCraft, один из лучших', '87.98.182.9', 25565, 'http://aero-craft.ru', NULL, 31, '', '', 'true', '87.98.182.9', 25565, '', 'online', 8, 1, 1, 0, 0, 8, '2016-09-04 15:28:11', '2016-09-04 15:28:11', 'AeroCraft 1.8-1.10 MiniGames', 'AeroCraft 1.8-1.10 MiniGames', 'AeroCraft 1.8-1.10 MiniGames', 'AeroCraft 1.8-1.10 MiniGames'),
(24, 'AgeMagic 1.8-1.10 MiniGames', 'Самый крутой сервер Minecraft версии 1.8-1.10\r\nУ нас самый дешевый донат - magictime.su\r\nРаздачи, конкурсы на админки!\r\nЕсть SkyWars, SurvivalGames и BedWars\r\nIP: mc.agemagic.ru:25870 \r\nАдерс: mc.agemagic.ru:25870', 'P: mc.agemagic.ru:25870 Выживание | Кейсы | Мини-Игры', '190.115.21.38', 25870, 'http://agemagic.net', NULL, 32, 'banner-14877.gif', '', 'true', '190.115.21.38', 25565, '', 'online', 8, 1, 1, 0, 0, 9, '2016-09-04 14:51:54', '2016-09-04 14:51:54', 'AgeMagic 1.8-1.10 MiniGames ', 'AgeMagic 1.8-1.10 MiniGames ', 'AgeMagic 1.8-1.10 MiniGames ', 'AgeMagic 1.8-1.10 MiniGames '),
(25, 'PSPAWN 1.8 - 1.10 NODUPE', '▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬\r\n✔IP: play.pspawn.ru\r\n✔Группа ВК: www.vk.com/pspawn\r\n✔Донат: pspawn.ru\r\n\r\n►Без лагов! Низкий PING \r\n►Можно зайти с любой версии с 1.8 до 1.10 !! :)\r\n\r\n►Радужная броня\r\n►Магия\r\n►Деньги за убийства игроков/мобов!\r\n►Кланы!\r\n►Уникальные PVP-кейсы!\r\n►Авто-Шахта!\r\n►Нет дюпа!\r\n►Огромный /kit start !\r\n►Клановые войны!\r\n►Постоянные ивенты на ранги!\r\n►Магазин!\r\n►Моб Арена!\r\n►Питомцы!\r\n►Свадьбы!\r\n►Наркотики!\r\n►Батуты!\r\n▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬\r\n', '&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;◅◄play.pspawn.ru►▻&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull; 1.8-.1.10 Выживание,STALKER', 'play.pspawn.ru', 25565, 'http://pspawn.ru/', NULL, 45, 'banner-55008.jpg', '', 'true', 'play.pspawn.ru', 25565, '', 'online', 8, 1, 1, 0, 0, 10, '2016-09-04 14:54:23', '2016-09-04 14:54:23', 'PSPAWN 1.8 - 1.10 NODUPE', 'PSPAWN 1.8 - 1.10 NODUPE', 'PSPAWN 1.8 - 1.10 NODUPE', 'PSPAWN 1.8 - 1.10 NODUPE');

-- --------------------------------------------------------

--
-- Структура таблицы `server_tag`
--

CREATE TABLE `server_tag` (
  `server_id` int(11) UNSIGNED DEFAULT NULL,
  `tag_id` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `server_tag`
--

INSERT INTO `server_tag` (`server_id`, `tag_id`) VALUES
(23, 31),
(23, 88),
(24, 32),
(24, 89),
(24, 90),
(24, 91),
(24, 92),
(24, 93),
(24, 94),
(24, 73),
(24, 74),
(24, 75),
(24, 76),
(24, 77),
(24, 78),
(24, 79),
(24, 80),
(24, 81),
(24, 85),
(24, 86),
(24, 87),
(24, 61),
(24, 63),
(24, 64),
(24, 66),
(13, 32),
(13, 88),
(13, 91),
(13, 92),
(13, 94),
(13, 95),
(13, 96),
(13, 97),
(13, 98),
(13, 100),
(13, 101),
(13, 103),
(13, 105),
(13, 106),
(13, 73),
(13, 66),
(13, 67),
(13, 68),
(13, 69),
(19, 32),
(22, 31),
(22, 88),
(22, 89),
(22, 90),
(22, 91),
(22, 92),
(22, 93),
(22, 94),
(22, 62),
(22, 63),
(22, 64),
(22, 65),
(22, 68),
(14, 36),
(18, 31),
(20, 35),
(20, 90),
(20, 91),
(20, 92),
(20, 94),
(20, 95),
(20, 78),
(20, 65),
(20, 68),
(20, 70),
(25, 45),
(25, 89),
(25, 90),
(25, 91),
(25, 93),
(25, 94),
(25, 96),
(25, 73),
(25, 76),
(25, 77),
(25, 78),
(25, 63),
(25, 66),
(21, 31),
(21, 89),
(21, 91),
(21, 74),
(21, 77),
(21, 62),
(21, 64);

-- --------------------------------------------------------

--
-- Структура таблицы `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `environment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `settings`
--

INSERT INTO `settings` (`id`, `environment`, `key`, `value`) VALUES
(804, 'production', '*::app.business_mail', '"yourpaypalemail@gmail.com"'),
(805, 'production', '*::app.amount_day', '"1"'),
(806, 'production', '*::app.vip_currency', '"USD"'),
(807, 'production', '*::app.ads_top', '""'),
(808, 'production', '*::app.ads_bottom', '""'),
(809, 'production', '*::app.cron_key', '"nzAHfP9i580wtrQ"'),
(810, 'production', '*::app.seo_meta_title', '"\\u0421\\u043f\\u0438\\u0441\\u043e\\u043a \\u0441\\u0435\\u0440\\u0432\\u0435\\u0440\\u043e\\u0432 Minecraft "'),
(811, 'production', '*::app.seo_meta_description', '"\\u0421\\u043f\\u0438\\u0441\\u043e\\u043a \\u0441\\u0435\\u0440\\u0432\\u0435\\u0440\\u043e\\u0432 Minecraft "'),
(812, 'production', '*::app.seo_profile_prefix', '""'),
(813, 'production', '*::app.seo_profile_suffix', '" - \\u0421\\u043f\\u0438\\u0441\\u043e\\u043a \\u0441\\u0435\\u0440\\u0432\\u0435\\u0440\\u043e\\u0432 Minecraft"'),
(814, 'production', '*::app.seo_server_prefix', '""'),
(815, 'production', '*::app.seo_server_suffix', '" - Minecraft Server List"'),
(816, 'production', '*::app.seo_tag_prefix', '""'),
(817, 'production', '*::app.seo_tag_suffix', '" - \\u0421\\u043f\\u0438\\u0441\\u043e\\u043a \\u0441\\u0435\\u0440\\u0432\\u0435\\u0440\\u043e\\u0432 Minecraft "'),
(818, 'production', '*::app.seo_country_prefix', '""'),
(819, 'production', '*::app.seo_country_suffix', '" - \\u0421\\u043f\\u0438\\u0441\\u043e\\u043a \\u0441\\u0435\\u0440\\u0432\\u0435\\u0440\\u043e\\u0432 Minecraft "'),
(820, 'production', '*::app.seo_version_prefix', '""'),
(821, 'production', '*::app.seo_version_suffix', '" - \\u0421\\u043f\\u0438\\u0441\\u043e\\u043a \\u0441\\u0435\\u0440\\u0432\\u0435\\u0440\\u043e\\u0432 Minecraft "'),
(1085, 'production', '*::app.sitename', '"Minecraft Server List"'),
(1086, 'production', '*::app.language', '"en"'),
(1087, 'production', '*::app.facebook_link', '""'),
(1088, 'production', '*::app.twitter_link', '""'),
(1089, 'production', '*::app.googleplus_link', '""'),
(1090, 'production', '*::app.tracking', '""'),
(1091, 'production', '*::app.copyright', '"\\u00a9 2014 <a href=\\"http:\\/\\/codecanyon.net\\/item\\/minecraft-server-list\\/8972853?ref=blackburn1911\\">Minecraft Server List<\\/a>"'),
(1092, 'production', '*::app.servers_per_home', '"5"'),
(1093, 'production', '*::app.servers_per_tags', '"5"'),
(1094, 'production', '*::app.servers_per_manage', '"5"'),
(1095, 'production', '*::app.servers_per_search', '"5"'),
(1096, 'production', '*::app.servers_per_country', '"5"'),
(1097, 'production', '*::app.servers_per_version', '"5"'),
(1098, 'production', '*::app.allowed_tags', '"10"'),
(1099, 'production', '*::app.confirm_account', '"true"'),
(1100, 'production', '*::app.owner_email', '"demo@demo.com"'),
(1101, 'production', '*::app.news_message', '"Thank you for purchasing Minecraft Server List software. "'),
(1102, 'production', '*::app.recaptcha_public_key', '"6LdIoSQTAAAAAMGLFw5L1ty0UtavMqTCX4tNdOwL"'),
(1103, 'production', '*::app.recaptcha_private_key', '"6LdIoSQTAAAAAIXFHEtV5B10w3ZxgGZ3WZtQ1R5v"'),
(1104, 'production', '*::app.text_adverts', '"<p>Lorem Ipsum - \\u044d\\u0442\\u043e \\u0442\\u0435\\u043a\\u0441\\u0442-\\"\\u0440\\u044b\\u0431\\u0430\\", \\u0447\\u0430\\u0441\\u0442\\u043e \\u0438\\u0441\\u043f\\u043e\\u043b\\u044c\\u0437\\u0443\\u0435\\u043c\\u044b\\u0439 \\u0432 \\u043f\\u0435\\u0447\\u0430\\u0442\\u0438 \\u0438 \\u0432\\u044d\\u0431-\\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435. Lorem Ipsum \\u044f\\u0432\\u043b\\u044f\\u0435\\u0442\\u0441\\u044f \\u0441\\u0442\\u0430\\u043d\\u0434\\u0430\\u0440\\u0442\\u043d\\u043e\\u0439 \\"\\u0440\\u044b\\u0431\\u043e\\u0439\\" \\u0434\\u043b\\u044f \\u0442\\u0435\\u043a\\u0441\\u0442\\u043e\\u0432 \\u043d\\u0430 \\u043b\\u0430\\u0442\\u0438\\u043d\\u0438\\u0446\\u0435 \\u0441 \\u043d\\u0430\\u0447\\u0430\\u043b\\u0430 XVI \\u0432\\u0435\\u043a\\u0430. \\u0412 \\u0442\\u043e \\u0432\\u0440\\u0435\\u043c\\u044f \\u043d\\u0435\\u043a\\u0438\\u0439 \\u0431\\u0435\\u0437\\u044b\\u043c\\u044f\\u043d\\u043d\\u044b\\u0439 \\u043f\\u0435\\u0447\\u0430\\u0442\\u043d\\u0438\\u043a \\u0441\\u043e\\u0437\\u0434\\u0430\\u043b \\u0431\\u043e\\u043b\\u044c\\u0448\\u0443\\u044e \\u043a\\u043e\\u043b\\u043b\\u0435\\u043a\\u0446\\u0438\\u044e \\u0440\\u0430\\u0437\\u043c\\u0435\\u0440\\u043e\\u0432 \\u0438 \\u0444\\u043e\\u0440\\u043c \\u0448\\u0440\\u0438\\u0444\\u0442\\u043e\\u0432, \\u0438\\u0441\\u043f\\u043e\\u043b\\u044c\\u0437\\u0443\\u044f Lorem Ipsum \\u0434\\u043b\\u044f \\u0440\\u0430\\u0441\\u043f\\u0435\\u0447\\u0430\\u0442\\u043a\\u0438 \\u043e\\u0431\\u0440\\u0430\\u0437\\u0446\\u043e\\u0432. Lorem Ipsum \\u043d\\u0435 \\u0442\\u043e\\u043b\\u044c\\u043a\\u043e \\u0443\\u0441\\u043f\\u0435\\u0448\\u043d\\u043e \\u043f\\u0435\\u0440\\u0435\\u0436\\u0438\\u043b \\u0431\\u0435\\u0437 \\u0437\\u0430\\u043c\\u0435\\u0442\\u043d\\u044b\\u0445 \\u0438\\u0437\\u043c\\u0435\\u043d\\u0435\\u043d\\u0438\\u0439 \\u043f\\u044f\\u0442\\u044c \\u0432\\u0435\\u043a\\u043e\\u0432, \\u043d\\u043e \\u0438 \\u043f\\u0435\\u0440\\u0435\\u0448\\u0430\\u0433\\u043d\\u0443\\u043b \\u0432 \\u044d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u043d\\u044b\\u0439 \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d. \\u0415\\u0433\\u043e \\u043f\\u043e\\u043f\\u0443\\u043b\\u044f\\u0440\\u0438\\u0437\\u0430\\u0446\\u0438\\u0438 \\u0432 \\u043d\\u043e\\u0432\\u043e\\u0435 \\u0432\\u0440\\u0435\\u043c\\u044f \\u043f\\u043e\\u0441\\u043b\\u0443\\u0436\\u0438\\u043b\\u0438 \\u043f\\u0443\\u0431\\u043b\\u0438\\u043a\\u0430\\u0446\\u0438\\u044f \\u043b\\u0438\\u0441\\u0442\\u043e\\u0432 Letraset \\u0441 \\u043e\\u0431\\u0440\\u0430\\u0437\\u0446\\u0430\\u043c\\u0438 Lorem Ipsum \\u0432 60-\\u0445 \\u0433\\u043e\\u0434\\u0430\\u0445 \\u0438, \\u0432 \\u0431\\u043e\\u043b\\u0435\\u0435 \\u043d\\u0435\\u0434\\u0430\\u0432\\u043d\\u0435\\u0435 \\u0432\\u0440\\u0435\\u043c\\u044f, \\u043f\\u0440\\u043e\\u0433\\u0440\\u0430\\u043c\\u043c\\u044b \\u044d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u043d\\u043e\\u0439 \\u0432\\u0451\\u0440\\u0441\\u0442\\u043a\\u0438 \\u0442\\u0438\\u043f\\u0430 Aldus PageMaker, \\u0432 \\u0448\\u0430\\u0431\\u043b\\u043e\\u043d\\u0430\\u0445 \\u043a\\u043e\\u0442\\u043e\\u0440\\u044b\\u0445 \\u0438\\u0441\\u043f\\u043e\\u043b\\u044c\\u0437\\u0443\\u0435\\u0442\\u0441\\u044f Lorem Ipsum.<\\/p>"'),
(1105, 'production', '*::app.faq', '"<p>[b]Lorem Ipsum[\\/b] - \\u044d\\u0442\\u043e \\u0442\\u0435\\u043a\\u0441\\u0442-\\"\\u0440\\u044b\\u0431\\u0430\\", \\u0447\\u0430\\u0441\\u0442\\u043e \\u0438\\u0441\\u043f\\u043e\\u043b\\u044c\\u0437\\u0443\\u0435\\u043c\\u044b\\u0439 \\u0432 \\u043f\\u0435\\u0447\\u0430\\u0442\\u0438 \\u0438 \\u0432\\u044d\\u0431-\\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435. Lorem Ipsum \\u044f\\u0432\\u043b\\u044f\\u0435\\u0442\\u0441\\u044f \\u0441\\u0442\\u0430\\u043d\\u0434\\u0430\\u0440\\u0442\\u043d\\u043e\\u0439 \\"\\u0440\\u044b\\u0431\\u043e\\u0439\\" \\u0434\\u043b\\u044f \\u0442\\u0435\\u043a\\u0441\\u0442\\u043e\\u0432 \\u043d\\u0430 \\u043b\\u0430\\u0442\\u0438\\u043d\\u0438\\u0446\\u0435 \\u0441 \\u043d\\u0430\\u0447\\u0430\\u043b\\u0430 XVI \\u0432\\u0435\\u043a\\u0430. \\u0412 \\u0442\\u043e \\u0432\\u0440\\u0435\\u043c\\u044f \\u043d\\u0435\\u043a\\u0438\\u0439 \\u0431\\u0435\\u0437\\u044b\\u043c\\u044f\\u043d\\u043d\\u044b\\u0439 \\u043f\\u0435\\u0447\\u0430\\u0442\\u043d\\u0438\\u043a \\u0441\\u043e\\u0437\\u0434\\u0430\\u043b \\u0431\\u043e\\u043b\\u044c\\u0448\\u0443\\u044e \\u043a\\u043e\\u043b\\u043b\\u0435\\u043a\\u0446\\u0438\\u044e \\u0440\\u0430\\u0437\\u043c\\u0435\\u0440\\u043e\\u0432 \\u0438 \\u0444\\u043e\\u0440\\u043c \\u0448\\u0440\\u0438\\u0444\\u0442\\u043e\\u0432, \\u0438\\u0441\\u043f\\u043e\\u043b\\u044c\\u0437\\u0443\\u044f Lorem Ipsum \\u0434\\u043b\\u044f \\u0440\\u0430\\u0441\\u043f\\u0435\\u0447\\u0430\\u0442\\u043a\\u0438 \\u043e\\u0431\\u0440\\u0430\\u0437\\u0446\\u043e\\u0432. Lorem Ipsum \\u043d\\u0435 \\u0442\\u043e\\u043b\\u044c\\u043a\\u043e \\u0443\\u0441\\u043f\\u0435\\u0448\\u043d\\u043e \\u043f\\u0435\\u0440\\u0435\\u0436\\u0438\\u043b \\u0431\\u0435\\u0437 \\u0437\\u0430\\u043c\\u0435\\u0442\\u043d\\u044b\\u0445 \\u0438\\u0437\\u043c\\u0435\\u043d\\u0435\\u043d\\u0438\\u0439 \\u043f\\u044f\\u0442\\u044c \\u0432\\u0435\\u043a\\u043e\\u0432, \\u043d\\u043e \\u0438 \\u043f\\u0435\\u0440\\u0435\\u0448\\u0430\\u0433\\u043d\\u0443\\u043b \\u0432 \\u044d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u043d\\u044b\\u0439 \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d. \\u0415\\u0433\\u043e \\u043f\\u043e\\u043f\\u0443\\u043b\\u044f\\u0440\\u0438\\u0437\\u0430\\u0446\\u0438\\u0438 \\u0432 \\u043d\\u043e\\u0432\\u043e\\u0435 \\u0432\\u0440\\u0435\\u043c\\u044f \\u043f\\u043e\\u0441\\u043b\\u0443\\u0436\\u0438\\u043b\\u0438 \\u043f\\u0443\\u0431\\u043b\\u0438\\u043a\\u0430\\u0446\\u0438\\u044f \\u043b\\u0438\\u0441\\u0442\\u043e\\u0432 Letraset \\u0441 \\u043e\\u0431\\u0440\\u0430\\u0437\\u0446\\u0430\\u043c\\u0438 Lorem Ipsum \\u0432 60-\\u0445 \\u0433\\u043e\\u0434\\u0430\\u0445 \\u0438, \\u0432 \\u0431\\u043e\\u043b\\u0435\\u0435 \\u043d\\u0435\\u0434\\u0430\\u0432\\u043d\\u0435\\u0435 \\u0432\\u0440\\u0435\\u043c\\u044f, \\u043f\\u0440\\u043e\\u0433\\u0440\\u0430\\u043c\\u043c\\u044b \\u044d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u043d\\u043e\\u0439 \\u0432\\u0451\\u0440\\u0441\\u0442\\u043a\\u0438 \\u0442\\u0438\\u043f\\u0430 Aldus PageMaker, \\u0432 \\u0448\\u0430\\u0431\\u043b\\u043e\\u043d\\u0430\\u0445 \\u043a\\u043e\\u0442\\u043e\\u0440\\u044b\\u0445 \\u0438\\u0441\\u043f\\u043e\\u043b\\u044c\\u0437\\u0443\\u0435\\u0442\\u0441\\u044f Lorem Ipsum. <\\/p>\\r\\n<p>\\r\\n[b]Lorem Ipsum[\\/b] - \\u044d\\u0442\\u043e \\u0442\\u0435\\u043a\\u0441\\u0442-\\"\\u0440\\u044b\\u0431\\u0430\\", \\u0447\\u0430\\u0441\\u0442\\u043e \\u0438\\u0441\\u043f\\u043e\\u043b\\u044c\\u0437\\u0443\\u0435\\u043c\\u044b\\u0439 \\u0432 \\u043f\\u0435\\u0447\\u0430\\u0442\\u0438 \\u0438 \\u0432\\u044d\\u0431-\\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435. Lorem Ipsum \\u044f\\u0432\\u043b\\u044f\\u0435\\u0442\\u0441\\u044f \\u0441\\u0442\\u0430\\u043d\\u0434\\u0430\\u0440\\u0442\\u043d\\u043e\\u0439 \\"\\u0440\\u044b\\u0431\\u043e\\u0439\\" \\u0434\\u043b\\u044f \\u0442\\u0435\\u043a\\u0441\\u0442\\u043e\\u0432 \\u043d\\u0430 \\u043b\\u0430\\u0442\\u0438\\u043d\\u0438\\u0446\\u0435 \\u0441 \\u043d\\u0430\\u0447\\u0430\\u043b\\u0430 XVI \\u0432\\u0435\\u043a\\u0430. \\u0412 \\u0442\\u043e \\u0432\\u0440\\u0435\\u043c\\u044f \\u043d\\u0435\\u043a\\u0438\\u0439 \\u0431\\u0435\\u0437\\u044b\\u043c\\u044f\\u043d\\u043d\\u044b\\u0439 \\u043f\\u0435\\u0447\\u0430\\u0442\\u043d\\u0438\\u043a \\u0441\\u043e\\u0437\\u0434\\u0430\\u043b \\u0431\\u043e\\u043b\\u044c\\u0448\\u0443\\u044e \\u043a\\u043e\\u043b\\u043b\\u0435\\u043a\\u0446\\u0438\\u044e \\u0440\\u0430\\u0437\\u043c\\u0435\\u0440\\u043e\\u0432 \\u0438 \\u0444\\u043e\\u0440\\u043c \\u0448\\u0440\\u0438\\u0444\\u0442\\u043e\\u0432, \\u0438\\u0441\\u043f\\u043e\\u043b\\u044c\\u0437\\u0443\\u044f Lorem Ipsum \\u0434\\u043b\\u044f \\u0440\\u0430\\u0441\\u043f\\u0435\\u0447\\u0430\\u0442\\u043a\\u0438 \\u043e\\u0431\\u0440\\u0430\\u0437\\u0446\\u043e\\u0432. Lorem Ipsum \\u043d\\u0435 \\u0442\\u043e\\u043b\\u044c\\u043a\\u043e \\u0443\\u0441\\u043f\\u0435\\u0448\\u043d\\u043e \\u043f\\u0435\\u0440\\u0435\\u0436\\u0438\\u043b \\u0431\\u0435\\u0437 \\u0437\\u0430\\u043c\\u0435\\u0442\\u043d\\u044b\\u0445 \\u0438\\u0437\\u043c\\u0435\\u043d\\u0435\\u043d\\u0438\\u0439 \\u043f\\u044f\\u0442\\u044c \\u0432\\u0435\\u043a\\u043e\\u0432, \\u043d\\u043e \\u0438 \\u043f\\u0435\\u0440\\u0435\\u0448\\u0430\\u0433\\u043d\\u0443\\u043b \\u0432 \\u044d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u043d\\u044b\\u0439 \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d. \\u0415\\u0433\\u043e \\u043f\\u043e\\u043f\\u0443\\u043b\\u044f\\u0440\\u0438\\u0437\\u0430\\u0446\\u0438\\u0438 \\u0432 \\u043d\\u043e\\u0432\\u043e\\u0435 \\u0432\\u0440\\u0435\\u043c\\u044f \\u043f\\u043e\\u0441\\u043b\\u0443\\u0436\\u0438\\u043b\\u0438 \\u043f\\u0443\\u0431\\u043b\\u0438\\u043a\\u0430\\u0446\\u0438\\u044f \\u043b\\u0438\\u0441\\u0442\\u043e\\u0432 Letraset \\u0441 \\u043e\\u0431\\u0440\\u0430\\u0437\\u0446\\u0430\\u043c\\u0438 Lorem Ipsum \\u0432 60-\\u0445 \\u0433\\u043e\\u0434\\u0430\\u0445 \\u0438, \\u0432 \\u0431\\u043e\\u043b\\u0435\\u0435 \\u043d\\u0435\\u0434\\u0430\\u0432\\u043d\\u0435\\u0435 \\u0432\\u0440\\u0435\\u043c\\u044f, \\u043f\\u0440\\u043e\\u0433\\u0440\\u0430\\u043c\\u043c\\u044b \\u044d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u043d\\u043e\\u0439 \\u0432\\u0451\\u0440\\u0441\\u0442\\u043a\\u0438 \\u0442\\u0438\\u043f\\u0430 Aldus PageMaker, \\u0432 \\u0448\\u0430\\u0431\\u043b\\u043e\\u043d\\u0430\\u0445 \\u043a\\u043e\\u0442\\u043e\\u0440\\u044b\\u0445 \\u0438\\u0441\\u043f\\u043e\\u043b\\u044c\\u0437\\u0443\\u0435\\u0442\\u0441\\u044f Lorem Ipsum.adsf<\\/p>\\r\\n[b]Lorem Ipsum[\\/b] - \\u044d\\u0442\\u043e \\u0442\\u0435\\u043a\\u0441\\u0442-\\"\\u0440\\u044b\\u0431\\u0430\\", \\u0447\\u0430\\u0441\\u0442\\u043e \\u0438\\u0441\\u043f\\u043e\\u043b\\u044c\\u0437\\u0443\\u0435\\u043c\\u044b\\u0439 \\u0432 \\u043f\\u0435\\u0447\\u0430\\u0442\\u0438 \\u0438 \\u0432\\u044d\\u0431-\\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435. Lorem Ipsum \\u044f\\u0432\\u043b\\u044f\\u0435\\u0442\\u0441\\u044f \\u0441\\u0442\\u0430\\u043d\\u0434\\u0430\\u0440\\u0442\\u043d\\u043e\\u0439 \\"\\u0440\\u044b\\u0431\\u043e\\u0439\\" \\u0434\\u043b\\u044f \\u0442\\u0435\\u043a\\u0441\\u0442\\u043e\\u0432 \\u043d\\u0430 \\u043b\\u0430\\u0442\\u0438\\u043d\\u0438\\u0446\\u0435 \\u0441 \\u043d\\u0430\\u0447\\u0430\\u043b\\u0430 XVI \\u0432\\u0435\\u043a\\u0430. \\u0412 \\u0442\\u043e \\u0432\\u0440\\u0435\\u043c\\u044f \\u043d\\u0435\\u043a\\u0438\\u0439 \\u0431\\u0435\\u0437\\u044b\\u043c\\u044f\\u043d\\u043d\\u044b\\u0439 \\u043f\\u0435\\u0447\\u0430\\u0442\\u043d\\u0438\\u043a \\u0441\\u043e\\u0437\\u0434\\u0430\\u043b \\u0431\\u043e\\u043b\\u044c\\u0448\\u0443\\u044e \\u043a\\u043e\\u043b\\u043b\\u0435\\u043a\\u0446\\u0438\\u044e \\u0440\\u0430\\u0437\\u043c\\u0435\\u0440\\u043e\\u0432 \\u0438 \\u0444\\u043e\\u0440\\u043c \\u0448\\u0440\\u0438\\u0444\\u0442\\u043e\\u0432, \\u0438\\u0441\\u043f\\u043e\\u043b\\u044c\\u0437\\u0443\\u044f Lorem Ipsum \\u0434\\u043b\\u044f \\u0440\\u0430\\u0441\\u043f\\u0435\\u0447\\u0430\\u0442\\u043a\\u0438 \\u043e\\u0431\\u0440\\u0430\\u0437\\u0446\\u043e\\u0432. Lorem Ipsum \\u043d\\u0435 \\u0442\\u043e\\u043b\\u044c\\u043a\\u043e \\u0443\\u0441\\u043f\\u0435\\u0448\\u043d\\u043e \\u043f\\u0435\\u0440\\u0435\\u0436\\u0438\\u043b \\u0431\\u0435\\u0437 \\u0437\\u0430\\u043c\\u0435\\u0442\\u043d\\u044b\\u0445 \\u0438\\u0437\\u043c\\u0435\\u043d\\u0435\\u043d\\u0438\\u0439 \\u043f\\u044f\\u0442\\u044c \\u0432\\u0435\\u043a\\u043e\\u0432, \\u043d\\u043e \\u0438 \\u043f\\u0435\\u0440\\u0435\\u0448\\u0430\\u0433\\u043d\\u0443\\u043b \\u0432 \\u044d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u043d\\u044b\\u0439 \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d. \\u0415\\u0433\\u043e \\u043f\\u043e\\u043f\\u0443\\u043b\\u044f\\u0440\\u0438\\u0437\\u0430\\u0446\\u0438\\u0438 \\u0432 \\u043d\\u043e\\u0432\\u043e\\u0435 \\u0432\\u0440\\u0435\\u043c\\u044f \\u043f\\u043e\\u0441\\u043b\\u0443\\u0436\\u0438\\u043b\\u0438 \\u043f\\u0443\\u0431\\u043b\\u0438\\u043a\\u0430\\u0446\\u0438\\u044f \\u043b\\u0438\\u0441\\u0442\\u043e\\u0432 Letraset \\u0441 \\u043e\\u0431\\u0440\\u0430\\u0437\\u0446\\u0430\\u043c\\u0438 Lorem Ipsum \\u0432 60-\\u0445 \\u0433\\u043e\\u0434\\u0430\\u0445 \\u0438, \\u0432 \\u0431\\u043e\\u043b\\u0435\\u0435 \\u043d\\u0435\\u0434\\u0430\\u0432\\u043d\\u0435\\u0435 \\u0432\\u0440\\u0435\\u043c\\u044f, \\u043f\\u0440\\u043e\\u0433\\u0440\\u0430\\u043c\\u043c\\u044b \\u044d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u043d\\u043e\\u0439 \\u0432\\u0451\\u0440\\u0441\\u0442\\u043a\\u0438 \\u0442\\u0438\\u043f\\u0430 Aldus PageMaker, \\u0432 \\u0448\\u0430\\u0431\\u043b\\u043e\\u043d\\u0430\\u0445 \\u043a\\u043e\\u0442\\u043e\\u0440\\u044b\\u0445 \\u0438\\u0441\\u043f\\u043e\\u043b\\u044c\\u0437\\u0443\\u0435\\u0442\\u0441\\u044f Lorem Ipsum.adsf\\r\\n\\r\\n[b]Lorem Ipsum[\\/b] - \\u044d\\u0442\\u043e \\u0442\\u0435\\u043a\\u0441\\u0442-\\"\\u0440\\u044b\\u0431\\u0430\\", \\u0447\\u0430\\u0441\\u0442\\u043e \\u0438\\u0441\\u043f\\u043e\\u043b\\u044c\\u0437\\u0443\\u0435\\u043c\\u044b\\u0439 \\u0432 \\u043f\\u0435\\u0447\\u0430\\u0442\\u0438 \\u0438 \\u0432\\u044d\\u0431-\\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435. Lorem Ipsum \\u044f\\u0432\\u043b\\u044f\\u0435\\u0442\\u0441\\u044f \\u0441\\u0442\\u0430\\u043d\\u0434\\u0430\\u0440\\u0442\\u043d\\u043e\\u0439 \\"\\u0440\\u044b\\u0431\\u043e\\u0439\\" \\u0434\\u043b\\u044f \\u0442\\u0435\\u043a\\u0441\\u0442\\u043e\\u0432 \\u043d\\u0430 \\u043b\\u0430\\u0442\\u0438\\u043d\\u0438\\u0446\\u0435 \\u0441 \\u043d\\u0430\\u0447\\u0430\\u043b\\u0430 XVI \\u0432\\u0435\\u043a\\u0430. \\u0412 \\u0442\\u043e \\u0432\\u0440\\u0435\\u043c\\u044f \\u043d\\u0435\\u043a\\u0438\\u0439 \\u0431\\u0435\\u0437\\u044b\\u043c\\u044f\\u043d\\u043d\\u044b\\u0439 \\u043f\\u0435\\u0447\\u0430\\u0442\\u043d\\u0438\\u043a \\u0441\\u043e\\u0437\\u0434\\u0430\\u043b \\u0431\\u043e\\u043b\\u044c\\u0448\\u0443\\u044e \\u043a\\u043e\\u043b\\u043b\\u0435\\u043a\\u0446\\u0438\\u044e \\u0440\\u0430\\u0437\\u043c\\u0435\\u0440\\u043e\\u0432 \\u0438 \\u0444\\u043e\\u0440\\u043c \\u0448\\u0440\\u0438\\u0444\\u0442\\u043e\\u0432, \\u0438\\u0441\\u043f\\u043e\\u043b\\u044c\\u0437\\u0443\\u044f Lorem Ipsum \\u0434\\u043b\\u044f \\u0440\\u0430\\u0441\\u043f\\u0435\\u0447\\u0430\\u0442\\u043a\\u0438 \\u043e\\u0431\\u0440\\u0430\\u0437\\u0446\\u043e\\u0432. Lorem Ipsum \\u043d\\u0435 \\u0442\\u043e\\u043b\\u044c\\u043a\\u043e \\u0443\\u0441\\u043f\\u0435\\u0448\\u043d\\u043e \\u043f\\u0435\\u0440\\u0435\\u0436\\u0438\\u043b \\u0431\\u0435\\u0437 \\u0437\\u0430\\u043c\\u0435\\u0442\\u043d\\u044b\\u0445 \\u0438\\u0437\\u043c\\u0435\\u043d\\u0435\\u043d\\u0438\\u0439 \\u043f\\u044f\\u0442\\u044c \\u0432\\u0435\\u043a\\u043e\\u0432, \\u043d\\u043e \\u0438 \\u043f\\u0435\\u0440\\u0435\\u0448\\u0430\\u0433\\u043d\\u0443\\u043b \\u0432 \\u044d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u043d\\u044b\\u0439 \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d. \\u0415\\u0433\\u043e \\u043f\\u043e\\u043f\\u0443\\u043b\\u044f\\u0440\\u0438\\u0437\\u0430\\u0446\\u0438\\u0438 \\u0432 \\u043d\\u043e\\u0432\\u043e\\u0435 \\u0432\\u0440\\u0435\\u043c\\u044f \\u043f\\u043e\\u0441\\u043b\\u0443\\u0436\\u0438\\u043b\\u0438 \\u043f\\u0443\\u0431\\u043b\\u0438\\u043a\\u0430\\u0446\\u0438\\u044f \\u043b\\u0438\\u0441\\u0442\\u043e\\u0432 Letraset \\u0441 \\u043e\\u0431\\u0440\\u0430\\u0437\\u0446\\u0430\\u043c\\u0438 Lorem Ipsum \\u0432 60-\\u0445 \\u0433\\u043e\\u0434\\u0430\\u0445 \\u0438, \\u0432 \\u0431\\u043e\\u043b\\u0435\\u0435 \\u043d\\u0435\\u0434\\u0430\\u0432\\u043d\\u0435\\u0435 \\u0432\\u0440\\u0435\\u043c\\u044f, \\u043f\\u0440\\u043e\\u0433\\u0440\\u0430\\u043c\\u043c\\u044b \\u044d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u043d\\u043e\\u0439 \\u0432\\u0451\\u0440\\u0441\\u0442\\u043a\\u0438 \\u0442\\u0438\\u043f\\u0430 Aldus PageMaker, \\u0432 \\u0448\\u0430\\u0431\\u043b\\u043e\\u043d\\u0430\\u0445 \\u043a\\u043e\\u0442\\u043e\\u0440\\u044b\\u0445 \\u0438\\u0441\\u043f\\u043e\\u043b\\u044c\\u0437\\u0443\\u0435\\u0442\\u0441\\u044f Lorem Ipsum.adsf"');

-- --------------------------------------------------------

--
-- Структура таблицы `statistics`
--

CREATE TABLE `statistics` (
  `id` int(11) NOT NULL,
  `server_id` int(11) NOT NULL,
  `players` int(11) NOT NULL,
  `max_players` int(11) NOT NULL,
  `motd` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `favicon` mediumtext COLLATE utf8_unicode_ci,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `statistics`
--

INSERT INTO `statistics` (`id`, `server_id`, `players`, `max_players`, `motd`, `favicon`, `updated_at`) VALUES
(27, 13, 200, 2000, 'BungeeCord 1.7.x, 1.8.x, 1.9.x, 1.10.x', '0', '2016-09-01 08:22:40'),
(28, 13, 200, 2000, 'BungeeCord 1.7.x, 1.8.x, 1.9.x, 1.10.x', '0', '2016-09-01 09:22:40'),
(29, 13, 454, 2000, 'BungeeCord 1.7.x, 1.8.x, 1.9.x, 1.10.x', '0', '2016-09-01 10:24:44'),
(31, 13, 454, 2000, 'BungeeCord 1.7.x, 1.8.x, 1.9.x, 1.10.x', '0', '2016-09-01 12:24:47'),
(32, 13, 454, 2000, 'BungeeCord 1.7.x, 1.8.x, 1.9.x, 1.10.x', '0', '2016-09-01 13:24:48'),
(36, 13, 458, 2000, 'BungeeCord 1.7.x, 1.8.x, 1.9.x, 1.10.x', '0', '2016-09-01 17:24:52'),
(37, 13, 457, 2000, 'BungeeCord 1.7.x, 1.8.x, 1.9.x, 1.10.x', '0', '2016-09-01 18:24:53'),
(38, 13, 800, 2000, 'BungeeCord 1.7.x, 1.8.x, 1.9.x, 1.10.x', '0', '2016-09-02 19:31:02'),
(40, 13, 457, 2000, 'BungeeCord 1.7.x, 1.8.x, 1.9.x, 1.10.x', '0', '2016-09-01 21:24:55'),
(41, 13, 457, 2000, 'BungeeCord 1.7.x, 1.8.x, 1.9.x, 1.10.x', '0', '2016-09-01 22:24:56'),
(42, 13, 456, 2000, 'BungeeCord 1.7.x, 1.8.x, 1.9.x, 1.10.x', '0', '2016-09-01 23:24:56'),
(43, 13, 456, 2000, 'BungeeCord 1.7.x, 1.8.x, 1.9.x, 1.10.x', '0', '2016-09-02 00:24:57'),
(44, 13, 456, 2000, 'BungeeCord 1.7.x, 1.8.x, 1.9.x, 1.10.x', '0', '2016-09-02 01:24:58'),
(47, 13, 456, 2000, 'BungeeCord 1.7.x, 1.8.x, 1.9.x, 1.10.x', '0', '2016-09-02 04:25:00'),
(48, 13, 456, 2000, 'BungeeCord 1.7.x, 1.8.x, 1.9.x, 1.10.x', '0', '2016-09-02 05:25:00'),
(49, 13, 457, 2000, 'BungeeCord 1.7.x, 1.8.x, 1.9.x, 1.10.x', '0', '2016-09-02 06:25:01'),
(50, 13, 456, 2000, 'BungeeCord 1.7.x, 1.8.x, 1.9.x, 1.10.x', '0', '2016-09-02 07:25:02'),
(51, 13, 456, 2000, 'BungeeCord 1.7.x, 1.8.x, 1.9.x, 1.10.x', '0', '2016-09-02 08:25:02'),
(54, 13, 455, 2000, 'BungeeCord 1.7.x, 1.8.x, 1.9.x, 1.10.x', '0', '2016-09-02 09:25:04'),
(55, 13, 716, 2000, 'BungeeCord 1.7.x, 1.8.x, 1.9.x, 1.10.x', '0', '2016-09-03 19:11:39'),
(56, 14, 310, 1000, '-]-- MineLegacy [1.7.2-1.8.9] --[-NoDupe,Clans,Spleef,Casino,BedWars,SkyWars,HungerGames', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAgrklEQVR42u2bB1SV57a1dxJTNMYSCyK9SO8oRZRmwwJWUFBB6V1AAenSVECkKEWxi1IsWLDFbhI19paoSTSJLRq7wa6Zd74f5UgM53jHuOf/zx03jLEGsOu3nnetuebaqAyA7P9yyP4G8DcAyD76+OO3YujQoTJDQ0NZt27dZFZWVjIPDw9Z4bx5MvGlrq4u8/HxkX7u3KWLzMHBQfr5ww8/lJ7j7e0t/a6rqyu9Tl5Rkaxz584yO0dHmXfsdFnc8hXS/dq838zSUjZm0iSZT2qauKlNJ0VF+44KCsGG/fuv6O/jc1BeX79aUVuntexPX8r6+jJza2tZ6zZtpN979u4ta/Ppp/XXpKQkfe+ooy37uG1b6ecPPvmk2fP/EwC0JQBHAggkgGIC2ONbVHw5ZuvWG2nHjt+ee/787znbtj+dXF5eZzR0WBkf/97/dgAqBDCCAJIIYE1wUdHhmTt3fZd96NDP+WfO3im4+P3TeVeuYP7NWyi6cxcr7j9ARlUVEs+eQ2DtlrtdtLQiPmnfXvZh69ay9/k+/xsAfEQAlnwdHwIoTV+8eFfe7j2n5p86fWXBpct1S67fwMqHD7Gyrg7Lfq/Dwrv3UHL7DkqYeCF/XvfgASJLFyDtl1+QeOkShhcUXGrTuXP/j5jQB7y+/0QA7xGAEp8zmAAiAoKCFhUuLNuxtLb2bNWZMw9rRFJPn6Hy2XOUM+kynnT64W8QsWkTfCor4bZsGTxKSlHI088jiM23bsGHAFKvXkUiIUScOoVegYH7CKDHfxKAjwnAmABGDRgwYHpAYODCnHnz9izasPFy7Xff4cCDh9j/4gU2MPGlv93miT/Cav4ctX49xi5YgCGLF8N2/nyYz5kDvZRk2EREYC4fl3vrNyw9dgyBNRuQ9MsVTP/pJ0z78UdM2rXrD1UH+yICsCCAz/5/AfiEAEwIYOSQIUOmhYaHL8/Oy/umeseOugMs1VM84f3PX6Cap1hw7lukHzqM+H374L96NfJ++BFVLPc+U6ZAYbwH1OPjoJk6A5rJSVCfGgXnnDnIYfJzGUmsiqnHTyDup58x7dJlRHz/PUIuXMDQsrJHXQwNKy0HD44igIEEoMprev/fCaAdAagTQE8CcLJzcJg8KSBgWXZx8bfbvzmC8/fu4bsnT7CLF73ut99Q8/IVyggipLIKo4tL4JidjV5JiegxfjzSTp5CxbVrMJk+Hd0nT4aCjzcUJk2CwsQJUBg5EgE1NZh18yYKfv0VPkVFiP3xEqKZfCTBhV28iEBWlc/p0+iTlv7HiOjo30cGB39lOXDQTE0DAxcCMCaAzqIV/ycAfEwAGgzrgU5OXl4BAbnRqWlbF69bd3XXuXM49+QpjrKUa3+9iSW8wJknTyJ8yxYkbtuOdQSQyNNW8/aGemQktLNmQ4MnbDBxIopu/Ir5e/dBPzsL3fm7yrSpUCccERoeHkg8fx7pN24gn6c+YcUKRF/+CZEs/TCefhBP3+/bbzH5zBl4fPMN+rFtYvie6QcOwL+46Haf8eOruxsbh7SVk+tNACpCk98ZwIcffdQs2rVvb+rr57dibW3t3VMUoOOPfsdOqvNalnXl3bvY/Oo1CjmefBYuhENyMrQjpsCUiU7kqW148RIhc/NgzAQ62dpCnokph4XCYdo0VLD8w+cXwWDJYnQZNgxqiQn1APjdIiQEM65dRxIrJIcgwgk1khUgkg9m8v7i9M+ehdep0xh/4gTcDh+GWUwMBtduxvAvv8So/fsxbNUq9AwOvtnd1HTexx07WhJANwJo/S8B/OnrPXsHh/IHPOGveZqVVOPMg4fgu2QpBmZmSKW5j7c7R0ZBeZIXNNnH3Vnexl5eSNq5C1vZ/x7p6TBYthRdnYfVJ5gQj7G5uah5/BhDExKgV7YQXUaMaDp98RqDZ89m8tcRTy+Qc/06ItlG4T/8gGCWfsB35+HDypvE05/AaeB+/DjGHj2KYdu3wZhgh+7Zg8GMATt3wnHbVvSprIDx9NjXnUyMq1t//vnQ91q1ak8AH74rgDZ5eXmPbvwBLKICG/KkeqSkQJ+nprdwATzmzsXXj59Af0o4FAP8oRwaArW46dBkL89j6e7gKdtFR0M7Pw9ybq71SUZPQ8DqCmxhFRny8QKIop9fEwD1kGB4cjLEMfmYn39GzOXLTP5HKflAqfS/w2Sevufp+tN3P34MrkeOYBSrwH7JElgXFsBJAPjiCzgQgO3mTei9fh3Mly97rRIY8LKzpeVxAnAV3uQvAbThKHkjnC9cvPjHidd/IKqqGlqzZkKOEFQ8J6IbTy2C6rz5+x9gkJYK+QnjpeRFEsZMaDHH3g4KoVlsDNQodIpBgdJ9qvyeRMGs5qmqs10UKICqMdFNAFT5OhE83eiff0EoT9iPiYWw9KXkefrenCaTCGCiOH0CGHusAcChQ3Devw9mM2agH5Pu/8UOOGxtAFCzHpZ0kqaLyqCdkf5CcZLX7baGhnuZe7u3ALz//vtNYWlpWfyAqr6DfT4iKwv6opRFv/IUu40ZjVxexMIdO2C0oBTdRo+WEhC93CcqCstYGSuPHkOv2bNYHQFQ4XOkJH18UHDlKrJoerR4X/dx46QqaOx/w8BAadRNZQWFUtgms6eDLn4Pf2qBD3t/EsvfUyr/kxgn+p/lP4ZCOPLQQbh89RX6rVsHPQpuv23bYL91C2w3iQoggOpq9FxVDuPFi9BjRopo1afMXfOftUCrmJiYC7+9fo0qKn3P2FjoMlG5UaOki9XmuCqno8ug2OiXlkCuAYDq9Fi4zMzE8qdPkVO9BtZ8QwWqvBp7W9yvHxyMxRTPQLaPPl9PwdX1H+XPx/SlkIrkY1n+/hRWX87/wAsX60+fyu8lTl+UP4VxHPtfABhNACMOCgBfYvDevTAlWOPUVNgTYN9NG6UWsKyuQs/Vq2C6Yjm0xaH4+opk9d8CIN/ps8aw3759+7MzLP+88xegT5OiJvqVvS5Oyp7Ct4UiF1xSAk06t8Y+Firvx9ZYRWhTCwol6oru46TniPut4+JQfv8+BsfHwyBvLpQmT2oCoMbnji4vRyRHnvD9bqw6IXr+fH9x+pMJwPPMWUwgAI8GAK5vAHBmBQxhG9ixCgw4bs1y56DvxrcB6ORkQ5G+g7kbvQUgeIi+FKMH9Fp4hSZkK8s/bM0a6ZS7U91VRU+zZ8czub3PntGxZUOVUFQbSlyRp53M8VRV9xjuLDXj8pVQcnNrKvFBTKqcBscsKhK6HF2q1IGmCqAe+DGZcLZAJk96GKskgL3v23D6ovwnsvzHv9H/YxoEcPjXX2MY22UwneZATgCr5cugQ+0x5Wv0XrcWFtQAc1arqbidYBoAmL4FoCLKTsRHaVN9f/n15Uus5CkPnTULBkxElL/oceXAAEznmNt9+zYsWe6qtLJqjX3MMZhDD7+GIuhAc2PEaaFEYZMMDh8zesFClPAE9fg6WizDxtYQcPT9/RHKcRchNIB9O4Yn6X+h/vQnNZa/MD8EIPV/A4CRfwawa5c0Ak04ETRZVea8hhYAvF0B5RG2ItSL8rJfn3n1CiVcWsx5UvpU0Mb+V2bC8zgWq44dZ5nlQnns2KYkzNjjuXxOBee3GT29fmYmVIKC6gEQiNfatUhkYoYF+dBmRTW2hhq1w5ptEcaxF8uFxyMnB94nTsKPALwJwEuI39k3y/8EXAlgtADACeByUAA40ASg347t6LuhBjrUlB68DnMKuDnb0YQAdOfmUgN80LbdJyqM5gCKA6xEjNlA9dxOJ5fJ/tPhhfVIT4OC9+T6k6KSizV2Fomaz58niZyUBMdgP75hLt1iGfvTjONRMzxMahkJnJ8vQnhKPvn5MKGX0Gx4nggVzv9B8+YhhOMxlRCGZWTATyg/Q/S+V2P5E4B7s/4/ghF/UQECgN2WWlhV0IoHB0kgzNj/EgD6EgUC0DOXb81oDiDe1URExgmSX/HsBXxpWgyLi6DA8hSjTJSsY2IS1lPlA7ncGFBRVZiklERkBNwWliGPFZDBsWNBlVcW/qChPbrTDEXT0DgnJcGU5Sk/jpXDk9ckOCVW1RiWvQCQxiQH5uXVn/75+vL3lNS/of8JQOr/BgGUKkAA4C7gtG8vBuyiC2wA0GfjBphxEnV3d4f+nByYsBJ0WX2KPAxHN9VWjOYAYtwsZWEjem6+8OgR5j+hyLH/DamecmPGSIkI0zKOfbyFLs+JVlY/MbGpjxU8PRG0ezcKCCCMpywUVxgmcb8I5bFumMYRZxkeDh22hbyzM/Q4981oYXvTYfowoSn0/BFskdGbN8P3/F+Uv+j/kxTAhgoYJSZAA4ChBCDGoGSDtxNALQGwDazWruHEyYP86FEwopjrFRZC0d8PDqNVPmE0B3BqZbjM32P41a/p/+eylG2opIYs9S68WOmUqfjhfJNN7PGePLkeND2NfSzn4oxEakMWbe44zmFDbnpiCepOjyBWXKPQUMRevwG/r77G5P0HMJn9GyLWW469EAqfMDxJdHpDuD94s9yl8m8AMPHsmX8AaBTABgDDhQvkCJQqoBmAzU0AxAjU5HUqUfz0WH1inNs4K7ZlNAdwujy8W0Zq/KsKAkikATHjKNOiaspPmFDf5zRAM5lENjXComg+1NhLopzU+b0XFXcGt8S0e/fgt30HPEXwYoJ4weH08FNY/tF87hSutb5HjsJ1wwYMoX93LC1Fb1aMFQW1lzBIfE9zVt5A3jeRiXq+0f9NAFgBY44eeaMCvpIqwGlvwyLE5UhUgA0BWFN4e4mliNNAjF0l2nElTjKLgd07MJoDOLo8rP+yqkoUcI57VlRK/aPIclWlkgrL2os2M4sjzoc7eE8SteayM5TbYShVeSrndcqde4w7mH71KmK50AhPH8GTnbBtO4aUlMKGm54JTZXR7CyYLVoEc45XM56O8cqVMF62DAacNobUDgO+rwnDiI+1KyjAWCbp0dj/jQLYMAJdGk3Qgf3SItS/cRFiBdjU1FeAAGDK1+9BYe7KaSYgmNjKdWE0B1AzxydkN99oNjVgEE/BmH3cleUv1lwDbm9jl6/AzIcPEcvHhPF0Ik6eQjT7Npknn8z5H3f1OgIPHcZYmqfBCxbANi8fFhxp5iw7M66+xgw90YcMfd6vw9ulD0miIzFweih8kgIxMWI8nAI8oMOWMaNwiZFpzHFqwz52YXJuYgNs7H8BQAig5AL3Y9CbAKgjvQWANQ0AOAG0udB1HjZUmgz6Vp3lGc0BlCROyDvGLS6Dpzy8uBhmfEIfqvYo6oDvrt2I5Wmm875U7gEpfFw0f/fj2Bm3dh1cVpZLZWvLk7VimPEkDdkmukxSr6RYSlozIx260VGwiwnFuPggRGUEIa9gCirKorB9WRQOr4pCbrgTZgX0g7KhPjScnGAQFgYdnpyBAMHJY8FWGcTEhAOsL/+DTSNw0J7d6MdN0J4A+koAuAgRQE+OQzF6tWjcOg8dAlUeppbZ50qM5gASInw2HuaMT6OQTWFZe3HbC2OpxVPcEq5e489H4c3eHlezAcMrKjBoxQrYL10KK/aXGZMVp2XAsSkWHR0aDm1euNWMOAxNnYbJmZFIyArH/LkhqJgfgp0LQ3ByVQSub03A7/vS8ORABp5+mYElsUNRMa0/tFW6QV5BAeo9e0LZmVsox6yYJlpsC2PqkuW8QjjwYJwPfImhjSZot/AAO6RNsO+mhlV4TbUEQFyXJvWl64jhUBb+RL+9GqM5AC8fn9O1VP+kX29iKoUrmHR9WVYe3KyGc7sbwGT7cgEyp5kwyJoNXV6IoShpJq43eyasMlMwcGY83GfHInhWBBJmh6FgTiAqCgKxd1EILm2Yjt/3pjLZdCnZv4rlCSNQGWUPa30lKCnVh1z37pAzM4PC8OFQpdiqcSfR4BQynDkTlsUlsF25AgNZ+i0BMGtYgpT86z+4URafRWi27cFoDsDG0fHbJKrpOO7TLtzqHGlsrClCRlxitFm+eqSuT5LCTxvPzkCfzGRYTA2DTeBkuIZPQHqqN8qyvLGtOBDnqqbi3u4ZqNvfcrICxOP99dFYARVpbqiI6IN+JipQVVWFhoYG1NXVIU8Inbp2RVcjIxiEhFAXMqQVWnyMrj9jBnqxZW1ERdIA2TUC4CbYiyJrQMeqRgFXpP3W4aRR4TImp9JWl9EcQMfOnbPMJ026piXWVT7JkH2jT+EyKS1G7+J89JuTjiEzpmLEND94R46Hv/9oRIZ6Izd1KjaVxuFsVaxUzi0nnIE7O5NxuSYG5ysjcXZVOE6vDJPiu4oI3NgSj6oMdwmAs4WalLiysjJ0dXUlEApsCTk5OTjQjk9gJZpyvdYWPoQTSjhS7dhYmPN2Syq+NU/eQiTPalXnHqI4yUsSQX3er8rfuyp8ZshoDqBD+/at5HR0ZmhGRl7Szsy4Q6v6VJkjw8ZnImKTg1CWPgF7FgSzb5Nwcd10LCtMxU9cXp4/f46DXEjmpoTje97eEoCrm+Owe74vSmOcMcvfkWGPgrABmBc+ECVRg3Gg1A+VGeOwakofeNhqQFNTE926dYO2trYEQUtLS4JgRXNV+/gJfDjjdcVOwWvskZIs/WFFwXMi1Pi7EUvegKEmyt3DA3qcKEbiQCnIYj/pKPexKeMtADICkH3SunX/dmpqc7rY2+6TG+L0a1iwx/OX59fg2cHZjFl4+tUsLJjhiwf37kB8vXjxAhdpdJbyDQoTvFHXQhUsThiNWA9r5Ic4Ytl0Z2zOmcBTj8OjPSm4uz0eD3YlY+1MD6wItUHwYB0p4c8//1w6/UYIOjo6UNXTw/67dzGHpqovPUk/en4TKrw29xVNWmsNlruyhzs0WBX6hGC6nIsQdUBUtJhGomI+bf+hBaNFADICkBGArFWrVnIu/a32vfiuGs8JQMSjfZkozs1A49cZOjVvb2+0a9cOibFRuMj+/ysA93cm4c62ONzeOh3XaqJwfjVLvyqq2WM2ZU/EwgBLTHPRkwB06NBBqgLRDiJ5EYpsi0UUu1XCrrNdBwkLvFuMwC/Y//UjULhAS2GD6QHMysthTB8gDJYuAWhwGnzc5oPejHcC8L6JrvryulOrmXw2Iwt3d6Vh1fJFTQDO07e3b98elpa8cI6r79bEsd8z/1L0mkISv7S3RLI21xMl/pZIGmNIAD0kAF0pfqL3hR6ISlBR7IZkCvK2Z89hx6VMrMD9xQchYgukAPZp/DRY8gA0QeUr6DSX1gNgCwg/8uHH79sy/jmAgQP7yvrZmMuGOFqVPPu2Gi8OE8ChHNzfk4FlZfOl5J8/rcONqz9hTdVqnDl1HGkpibhcK1pgJiOzRT1oKTZmjUeJnwVyJppJGiBaQF5evglCd06DYRYqGD1uOHbSlY6m+jvQGPXnDuDAJciWU8Bm00ZYcQJYcC03FyaIY9Jo6RLJautyimnQn7Ru28qR0TIAQyMdma2lSWPMffbtGgLIkaLuy9lIj/bB8uIcrMyJRO38SKzNCUJ5dgRGDhuIX7Yx+a8a4r8JoZa6UOJvIbWB6P1OnTpJ41BAEAA6d/ocU1304WClh923bmEqbbBYokT523MJ6ssdoDc1wYprda+qKmnXEAAMCUBMNF0aNg0KZusObQYy3gagqa0pU1GUl5no93gzMp+cW4OX3+Qy5uDq1hQsShyHfQvCcWLFFFysjsIFjrVji0MQ5t4P13bm/APAV5n/LQg7CyejlMkvDektVUCXLl3Qo0cPqKmpSRBU5Tsh3d0YbnZayFtfgzxumGZsA0dqgp1kgTdJO4AlJ0RPLnZm4u8BjQJIADoEIDbbjz7vOITRHICKYneZnpb6X0Xi47NrmfxcKe7szsDFtbG4vzsNz77O4mRgfD2bPZ2J3MiRuLWvgL//awB11IAHe2bg7o4EPOQEENpwqCwACwOtUMoQwicACDEUoaKiAkP1rpg1wRQRznoIpgES/6ymT2IiF6BtsBX9z/K3Zv9b0AeYiyWIG6dRgwCKP+np0N+IzzA+6trVhdEcgI6mWksxtU4AOJJHAHkShBeshueH50iaUB9ZkkAWxrjizqGydwIgTNM1mp9Di4JwaKGfBEGYIwFgnq+lBKBz587S6UsQ1FVgrSuP1LHGmOFmBL+p4dj49BlGz58PG562LS27zUb2fyMA9v9bACieYgx20lVwYzQHoKmq0lIEPjxDAEfzCSG/HsSReggvCKE+6idEGVvjyJpZOLs6AudWR+KnDbEtQnjME/91WwIOLg5FZdpYXKwMx6X10VgSYoMifytJAwQAMQa11JUxpJcafPprIcRJGxFDdeDmPhwHWAFT9x+AKee9GH/WYvytW8v+r6zvf5a/kZgAFEC9BQ0AWAFq9lrTLF2VmwNQV1FuKTxvHa/Gq6OFjHxGngThxTdvQsjGi0PZOMAtb92cANRQzTfMHo/Di4LfANAcgtgBbm5LxI/rorGtwAfHlgRJdnh1lC0WUwN6aDYA6NIJDkYKmM3SXxlmg7Ah2vC0V8cga23svXQZ+bfvSH8TlE5/ff3pi/lvuqp+/hsuaRDA0lJoixagCKoMs9owLkXxvT8BUGkp3C5/XY5Xx4qYfIEEob4a8uoBSBByJACiCh7ty8BvXyTjt21JeLgntR4AQ3iDh9wGb3AFvlKbhEubU/HVynhU53OKZE3C/V0p0uM3xffDuml2sDHRghpnvnmPbhQ+E1pkG2ye7oDqqbbIm2yOSE6DpZs2YxFtsfThqhA/of7VVfXjT5S/+KRpcf2f9HXoASQANELKE0bedk9VbNcMgLKSUkvhfHbnYrw+UUIIBVI8PZyH50fqW+KFCGrDi8MEcWiOJIpPKIq/UxQf7M3EXYrlHZqnm1+k4du1CVibG4glOdOwoqwQh7hD3OI4i/IZSTFMlCpjd/oQKdF0d1OMt9dCGpNfESaSt0dtnIMUlVF9kOFhgukZqah49gy2TMpKLEDC/bH8TVetkv51ijT+WP46nP/ahQXQypsr+QCViFCMS1Md1AyAgrx8SzHgm41FeH1qAZMvlOLihhRc31eIuqML8eTkMjw+sRyPjy1C3ZFS3P+6EDf35uB4ZTy2LojB8uwwlGSGo2ROMqorV+PKlSvS/tD49erVK4T6jscPq0OkSbB/tktToiLpmhg7CUjjbY2353iawWfSGGwigBElpTBnsr2qG8WvvEn8dAryoZk247VGchIj8ZUI9aSE13qDzcreFUCfPZVz8er0oiYAZ6rikBnqjDgfJxTnJKF0bjpK8zJRmj8TS0ryUbFyCfbu/gLfX/gO9+/eRt3vD/Hs2VO84Ob4+vXrpuTFz1vp35W6y+PEQm/JHh+YPbxZsi1FGaeF+0BjbL53D9FHjkIvM6P+9BvMjwH7vgdvU/CaCM2xDrvk7YwWd7M3K+4+yKZMY4TFckXrnrOaAVBVVm4pzGrKMvDqzBK8Ol4P4Mmhubi1eyYWp3jg2tVfcJ+b4f17d6V4cP8+HtKmPn78WDppkeQff/whJfzy5Utcv34d22lbc3Nz4enpKal920/b4JtiTwnA/ncEsIZaEDLMAGmbNiHzlyvQio+T/pKlwa3QwHMsbFwcMGpYLzj100ZChZ1yNxvdrnJ9DLvI97PoquFsKqdg1bNzMwAa9P8thO7KggS8PrsUr4/Pa4Lw6lg+agsCUL2sCHtrK3Bg2xrsZ+zYsBrz5+UjOztbivT0dMRzawsMDMSIESNgbW0NSzMz2Jubw5LLTcdPP0XHdp/iJCtAtEBl7MC3Sv6vQjymyM8Sod5D4Bc1CU5jHOAxwRERk+yR6tMHc/16Y2GQNWJHG2DOFodWESU2MgKQEYCMAGQE0HwMWpuZtxQqOUkheEUA9cnXC6GYBjd2pOPi5pn4+Ys5+GVnrvT9x21Z2FQUiV6mhk2f6qgoKcHO2BjaXGYG6Osjy9UVy3x8sHD8eKQPGIChplq4uDJQAlAUbIeNsfbvVAXro+1QzFaY42uNXIYwUaun9EFFVF8UcqlK8jK/4u5iWkUA7zH+OYAhjv1aii7ZiQ0ApMQL/jEOJXcoRiFd4WHhCLMlV3hzewq8h1vh/ffeQ7vWreHGVXk2kw5x4BgLCsKplBScYxwKD8eiUSOR5WmPm5ujpSlQEGjLxGzfCYAIAUs8fu00WywJ7o3Z3j1vhI3T2+LlrpnhO8bQvb+TmYVI/l8CGD7IqaVomxHj/04AxPiTRt72ZCxOcsOHrVphiJERKpn0gdhYbKVhOcPE90VEYN3kyVgwZjgKJw7CwSJv/L53hgSgKNQRa6a9O4BKGqc5Xr2eBDnpHHK2UCkOHKUXaufY3d7BtXsXApARgOydAIxwcmopWnmPdTl5aHXKw1+/yGSiefWOsMkS19thAUB8JnhgQSD2FvliWcIo2BqrIHPcYKyP8sbGKB9sjQnE+nAvlPu7YomvCxYEDcPefC882JXU5BAXRA6UzM4/S3pDjD3KgqyfZnqYXfAbqLvVzkAxX12+o2vr1p/IE4CMAGQEIPufAiBztOk9YbqvS2lRnPvObUWhp05UTP/lx03Jdbd2pqPuqyw8FZ8Wsfy/WRaOrble2D7XE7XZHqjJdENNuitqMlyxNnUMqpJHoXrGaOzMnYDTS/xxrWYq6ugO37TIi6YNRhV7+M9Jb2Kpr47o87zYz+LnZFfjg242GuV2hoohHdt/ZtqmdesPCEBGALJ/FwAZAci01dWUzAx1RoR6DEgsTRy/piIn4Mj2kuBzh8ujL51cPfXGhbXT713eEP/kWm3i61vbk3Bv1ww82C0iBY+4+ooPQOv2pUrj7kkLfzNYGe+Mysi+TUrPani5KLj3rULvXt9GOuttc7VWiVPu2s6yfdtPPyAAGQHICED2/wqAjABkBCAjANmgYS6fK6h0t7LsbTG+v5VeUsDoPmWzw4Z9sWLGuHOr0sf/tLvY/8rJVRHXT6+OuvnDupjbVzfH3f11a8ID7gOP+L3utx1JT+7uTH52d2fKC/7+6t6ulNerkkf+Qev7jBDuLQm2vpY4xuj4sF6qs3pqdrFVl+/wAQHICEBGALL/BAAyApARgIwAZAQgIwAZAfDCOrTr0O4zTfkuHWy6de08oqeukp/3MLP4lMl2c+K97BYnTnZcN2+ay66qTI8jlTMnXkjy6Xd9Q47nw9wpQ58nuJocce+rPsNcvVMvDfn2rQhARgBMsoPs3wbg7/85+jeAvwH8DeD/avwXxG4kD5Cwxp0AAAAASUVORK5CYII=', '2016-09-03 19:11:40'),
(57, 18, 197, 1337, 'BungeeCord 1.8.x, 1.9.x, 1.10.x', '0', '2016-09-03 19:19:00'),
(58, 19, 224, 400, 'BungeeCord 1.7.x, 1.8.x, 1.9.x, 1.10.x', '0', '2016-09-04 14:11:44'),
(59, 20, 120, 200, 'BungeeCord 1.8.x, 1.9.x, 1.10.x', '0', '2016-09-04 14:20:46'),
(60, 21, 149, 400, 'BungeeCord 1.8.x, 1.9.x, 1.10.x', '0', '2016-09-04 14:22:40'),
(61, 22, 324, 999, 'BungeeCord 1.7.x, 1.8.x, 1.9.x, 1.10.x', '0', '2016-09-04 14:28:58'),
(62, 23, 116, 250, 'BungeeCord 1.8.x, 1.9.x, 1.10.x', '0', '2016-09-04 14:31:13'),
(63, 24, 190, 1337, 'BungeeCord 1.8.x, 1.9.x, 1.10.x', '0', '2016-09-04 14:51:56'),
(64, 25, 218, 400, '', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAwsklEQVR42pWaBVRWWRf3n7FFsLuDEgNUQEUBkUYpAek0QUpBBES6u0xMUMTCDiwUu2PUMSbsGBsL1Jnfd+7jyBrf7/3etT7X2p579t3n3LP/Z+ezkOkbW2Bj78QkFzcmTLTG3dqNeI8lrMm8iZdtMbpGUxiga4Wl8wKsPQqZ4laOv8t+QlyrifA4Q3LwBbKjj5EVe4iCtKNkZZ0jP/8aeQW3xXib7MKTpCZsJjNqPRkRZeREbCJ37l6K5lexNPEwJRnVLEsUFHuUpdG7WRazlWXzt7Fs7k5WC5nyhMNsTjnH1vQL7MiW6CJbUq9TtuAi+bNuEO58HFeLMkz089EaPo/uA70YNSSImQa5rJq6l0OxJzgRd5SLKUe5IOhYzH5KPFczSTMQLQ1PZIN0J6A7zpOxpg5M8ZlMSlA4J1bsY1NhFTkFxzCx9sfaJoyczFP8WvGRlUGHmGGejMuY6diOtMd8uBn6g/TQUR3OwL4D6delO30790K7rxZWGuZ46nowyyCISNMYEqxSybTOIdumgKyJOWRZZpNtlUOmpXi2yibNMoMEsxTmj48lZOwcpo70x0nLFQMVI5S7KNOhTVs6d2yLWt/ejFDTwGDIaOx0JuE/fh5xdosp8t5NeeAtDoQ+Ye7YtSy3288Oj2McCjvD0ZhjnF1wiJ9Ta9gStJFQ62jG6c1A1kvbjuEm0xhi6IDTJG+SI2I5c+go5UvKiF2xC2PPZJycC3m4/jPWY2ywM7LAaqwRJrp6GAzTRWOAKm3bdUS52xBcRk4h3Xkh5bN2Uhm6h02zdrB5eiUbfDeyzmMdpS5rWOm0gpJJS1lqu4iFNgspmlhEsXUxxeJ5kVCixHE5q11Ws95rnVi3gU3Tt7DJfxsVM7dTInjzzBMYr25Jl/bd6Na1E1pq6owZqinOMhyTUaOxMjDEfaIbF6PfMc94HQXWmzgw5TCvj7yjvPgwpZllJM1IwsnEFX19G2Qj3cKYHJyB3eR5TLSdRVBoJkmZxSTnFeKetRC9GaUkx1xmTdh6PGwccDC2xHTkaAYpK9NcQUHcuBZRFqms8t3MqumbWD61ggKPlSTa5RNiHIeXXgiTRkzBcog7ZoMmYzzQASMVWwxVbDBQtsZYjOPF3FjVTjzbY6rmgO0QDzx0/JktLCHFJpdFLitY4b2WlT7lrPLZwGqfLSz33EiAfgS9OqjQUlERtX59GaetzUQjIxytrDiccBVf0zK8hStEjyzgesbPaA/yQM/MBxtzG6wnWDLWxAxZUGwIS1PSOXfiKnEZG3APK8IvrwjvwnyMI9MwCd/HyXWfmesWgb2ZOeNH6dKtW0daK/XCWTeUxa5rWexRRrH7Gha5rybHaQkJE/OIN88k1jSVuPHJJBunkWaSQapxBonjkpivP595YyKYqzeXeXrhRI+JZIHBfJLHJZBlmkmuRR6Z5tksME4kfFwM4SaJpFgLno2wFscSFgsLWeRaSrGwqMLJa/DSDUGl6zi69+jKyGGDsDDSZ1X4RqI9jmCpHUfA0FSuJV9Dp58bw0d5McFhBk6Ojji4uiFbMC+Dxz/f5taR0+w+eByP7B245CzBK78Yz7jFpK64zJOaj0wV7jFOT5tevXuh1c+WCOGzi91KKXJdw0L3UjkIJR5rKXEuY7mDuDHbJSyxKiTXJJN4wzjmCEVn6s7Cb8Q0PLV8cBvqweShbjgNdsV5iKuYe+M7fKqQCSRcgBNnFEe2AHGxdSErHZaxynkliycvJ2NiPjEmKaRY5ZJjv5TsScvIsl9GmHEyRhq+9OrXHd0RA4n0jGFJwC3Ga8/HW4BQNHktplqzMNKZxUyXYlbFlzLRwg5Z5NRFLA5YQfWaIxw8fpHZFdUEZG4mqWw/hUv2cuTAA64duIO9uSXDNHWw051DprjlRe5lFDivpshN3LxbmbiVNSx1WslS60UUW+QQpx+Fv8507AfaMbKnLsod+tNJsR0KzZvTsnljWrZsjIJCIxr/JKNJExlN/6HmTRvRprkS/dsrM7aPAd7DfYg0iCBvQpYAYQWr3deySHxnvgAh3DCeRMs80kQ8SRZxJFoE0Ul6UShr9MJlwmT2xP6J4egonI1jmTRmLha6QRgNnI6fXhL5czbgYOmCzMY1nXlTyrhy4jXlG/eRdWAf4bmVLF69k+pTt3n96W92LBWmZGTPDKsElk4plZt7pv1iskQwy3cqodBxGYU2xRRYZBNrEInfcC9Gdtehi2JrOir9RJe2TenUthmDerRmjEon1Lu3QaWLEn3bNMJ8gheTfeNw8olhkmekoAiGaI2ge/smtGvTmCbNZLRX6Ii5iiVzDeZQbJvHamF1JS6lws0yCNZfQJRJKskTCsmYtJAkp1xmiHPojhrO2axXTLLJY9Q4P4aNcMVMcybFHhV4a85jishGU6dEIlOfGE1QcAXlC/eTW7SEospiFu48wZHLP3Pm8i1O1zzi6sq7LJ5RxZrZ5Sz0XUOSda6gfNLtFwozXETuhFzh3/EEaPuh32M0Co1kdFaUCSUV6dehFSP7tsNhSDdmjFHGsI8S6u2bot5JAbUOzQmMyiNh8SbiF5aTsHA98zJWYmvjiLqSDFPlNpiqtsVQrT29OjWlbYt2OA2ZRKJIqSsE8EsnryBODsJ8IowS5QAs8ltDts8iZk8qZvuCK9haZjFyvAujDf2ZaZzCeVFTZJouZXPGRfzCCpApaHujMsqTcTa+WLm4EJ48hejFyyhaV0rlvpNcvvqM56UfWB+9QdQA5STa5xA7QWQKKY/bFpFtmUmiwTy8Bzswors2Yw3M0Rtrgo72OFQ6Nhd5vCfWGl1w0uyO6YDWZKYkc/DIcXbsF3TwFDUnz/LwySMePn7M73fvs/PQcYKjM5gaFImHbwBeU0QdoqOO58heqPZQpHFTGTo9hxFpNFukzCUUikuINk3CXSdQZI0YFvqsZnXIBpYHb+B6Th2eTnmYWvkwyTScytRzHEo+yZ7Q/ZTGHMZuWjYyfadAbj+4Sc2ZGuanL2DYhNGMCg7GNyeewQGBxC8/yY0lt9goKrRU11xibNKJm5hJhjDFHIt0kvTD8Rxoy4heI3ByDcDewR83r3ARYCYwskcr7Id2w0WQ37BuTOirSNWeXbyofcubj+95/f4dbwW9+/CBus+feS/G17WvefzsGfcfP+XA8XMsyFiMzkAVpo/tg51Wd4b2aUvzFjIGdlImQn+WqCVEdrDKFCDEM7C3OYm2mawJ2cjm6B0cy7hGblg17p6J+IuLu7XxPiXTq7hd9gtTjVLQtpqFLD5/Od/+fZH/f+/lXbSmhNLScTLNnbzZeOQix9POUx5ewZgRjoRbxpJmky3MPoN0w7l4DbJGt88InN38sbP1xd0jiBkhSZibWIjg14qQkb0JFDRnTD/8xmpw9NRpHr98ybPXr3nx5i21Qun6L5+pq68XYLzn+etXvP/0SQ7M69o33L7/AAM9Aa6eKqPVumCh0Rnlroq0aC5jrAiucQbBIvaINGueiKv2NHQ07Mn3WkhF1DYq445xbvUjUeHmkCpc92rxDVImrOKPrbdxMQxk+ISpyDZuWsYn7vDm+UPq6v4UEHxm34X9dLG252HdVTkoO+cdINA5Bn1dV9Lss8ixziDPdAHThzqgLQohe0dfHCf54uEWiN+UKNJzV7EoLxfzrjK8VBXwUldkuk53Zlrr8/vTP3leWyusoJbajx/58vUrn79+4ZMA4MOnjwKMLwKMOl6+ecOTF895++kDRemxLLDUwkVXGU3hBn1FfOndU1SfmqOw7mtAtiiFCyxSSLdIYKSGNSY6bhTNXMrWuOOcW/IKU5NwFniuJH9yCVtSqtmbsYPJttMx81uArDgqjg+vHlD7+j6vH9/n6qlrbFizjpUlJVz9/QLUQlFAKY7WAaIU9qbAMU+Ur6kk6Plj1GMkZuMm4u46C2/3QKaJqLpxw05ev33J7ZvXuHL5PJVlK1i/YhHbtmzg5u+/8eDpM95+/MCHujqh/F/fABBK1wsX+PrXX3wV87q6el6+fcuLt2/kdO/RA67+fIET1XspzM9nqp8/jo4eGFpY0rOTKnO0nVlolkCeZSJB+gGM1pmMvbEvayK2sTf+EUNGBjNmVDAexvNYk7SPtISNzAhMwm5WDrLYaQt4c/cZL/94SO2z55zZdYx1xasJmxHN+RNn4QVE+uRibzWdGabBLHEpYKH40IyhdsLvtfDzCcXPMwT/6fNZX76N12+e8+TpI2Her+Xm/VHc7HPh85K/vxM3LJl33XdlBX2WABDzv+TKS4B8kctJayV6/a6WdwKwT5+/8EFYhuQuNSdOMzskFvtJnugYjGdcVy1yxoWJcyVTYJeE3lArrExE/eCVzf6kZwwxmI3ehPm4uQvwfIrxi96ImUsImjaByKqXj6E0LotFieuoXr+PC1WnWJy4mhlesZzYVQNvIMqzkPEG9hR5FojUk0++iegbeo/BRG+CUD4Y/2mRpCQVc+/+XUH3ePLnM7kPS8pINyjd5ut37+Q+Likv3bykvHTrX758lSv/11/fLOHDpzoRFN8L9/ggdxEJDGmfDwK4D4L358sXvBHgLl9WhqfLdMwn2qHeS4P5outcIlxgmWMWXvo+jNd3JnRyKqcWv8HaZxkmPoVY+xVg4xWDhX8SE119GOsSjGyqfjvRvAxizXQzFvgEEDE1mbTIElyEz6/PruD9iafMdstg5qRQVs1cQYFtMukGgQxrN5BpPrPxcw9mqm84+/Yd5vovN/nt7j3hv2/lvvz4xUsevnjBk5ev+fP1G6HYR2ERdcLHPwsXqOeLuNWvf/9j9sJS3gmFJZDeCLBeCar9+EkOlCQruczD5y+49/Axv/5xl+rqGvw8AnCwc0Nr+ChcBxixyHw+q8UFZTqnYDjSmrnO2Zxe9xLnmeUY+qQzenIoFl6zcU3MxN5zOpNCY5ENUuiCptIAurbsiE6vNiKdKDNx+EC0NDTxHaHFndhLhPtmsDJM1OJ+RWSYRxKr58uwbpqEhWWKCBtJaGgKV67f4uqN2zwUbvRWmO6TV6+5//wl9/+UAHjFq1qhkKSclPqkyC+3hG8BsFYAIyn8QvJ7EfyeibWv3r2XK/5WWEStoKeva7kn9vrt4ROu3BAF2tmLzJw6F0eHKdjYuzOu23AKTeZSYp/KIu98ZjvOY+rE+fxS+Z7xLgsZZj1bXnH6+acTml0mut5sZqbnI7Nr0gQDWVNmt+nK7O4q2LTshIrsJ1RErh0m68+1iOeUxW4U3dUaUhwWkG4WzhwdD0z69SdikiFT7SYyxc2LnIwsdu/azcGjxzlw6iy7jp+h6oxIoVevc+nOXW48eMKdx38KQESef/VWWMQ7uVISSPcEaL8+esqtB4+4df+ReH7CPREs7z55xu+PHnPjj3ucEUpXn7/I1qoDrClbS256Cq4TJ+Bhpk+ojS6mvdTINQ6n2CqGnMnJFAYsImfGSi4urcVm8hJ8PdJImFkiAvwJzpy9y/Uj11i1bx+y/AEDKWzbjVhZY/JlMmbq6DG+Zh+zVy5m9v4qfq15xtb4XSR7JBBjFUaGRQTTNV1wFlaS6mHCkpnmXF3my9k8L9YFmLIhwprVwVZke+uzPMKO/asi2LUykq0rY6lcnczO9dns2ZTH7nUZbFsTz7bVcWwVJD1vXRUrn28vjWfPuiT2rU9h79pE9pbGsl/Q0bIozpbO5srq6dwrn0ZNmq2o/ceS42mA9QBlMozmUGgZTZxFEImeKayOquSPtR9wc1vJjrLTvHhYz6M/P3Lr9GMQPc7RK9eQVekYUTHMiH2a+qxtrECTaYGoFuWhvWklowpW8+v2D2T7Z6M/cjyzTWeK0jcat8F2+BgMJcHbgpocL+6tmsaEnopo92yNVnclJgzqRKHbCKrDxrMr0IC3h3Ph7jb4ZaOgLXB7K9wR8zvb4Vcx/rr92/y2oFvi/S+b4YaQvbYerpbDlbVwuRQurIBzi+BEOhwV2WubP3m+40nzNMJaVZkEUdwsso7Da9QkJujbkR+0ilvlbwmO2E5q2hYSEsso2nqW7RvP8aX+KxV7jiILUtEiRHUUSeqj2dh3CG2cxe3evUPnmYFMqNrPvppa5rlHoT/KSLSboWRZRWGrOoFpltokT7Ph1tpQ1s6zIS3cm1W5c1mcHkrkLBf0NJVR76xAgo0Gu2bocDR7Kvyxk6/Xt/D1xuZv4/XN/6JNfLkm6OcNfL5aQf2V9dRfXseXS2X8JSl/TYBwdTV/nSnmy/FMuJgjQIhmwzxrkryMsR2iTPSY6SyxF22yyXSsDG2ZZhvJo1PvSC7aQ2hiCam7qsmv+YWFy3Zx/d5L8ldtQ7Zf34qTFvb8bu7J0m4D6CjcoLnKEDrPCmDsrkpmaiSTGVzMJCNvFtiEk2oZiXFfY4ImjSU90JFfN0Vyf086FyvTOSZM9ogw2cOlcexZESNA8aRvJyXMBndljdcwtoVPEDctWcB2oWgFX65u+Kaw9PwPfRaKf75czlfp5q+V8+X8Um6uDGRflCXLPYeLvn8gQQb9CBrTi5slUzi/0JtEYYkuowRfx5uljpkUT85k3CgzQp2S+OPXt0SLoJe7cw+5F68RuLKMmKVbOfv+bwLmr0BWoGvCYcsJxPUdxvjGrZE16k5zHQP6Z2cxYE0JMwwLyQpaQpBdMvG2EcSazkGvx1giPE3In+vC492JPNufw/UdWZzbksHPYnx0II93NcW8PVrIsbI4xo9QR7WbEsFmamRNHsqhXA+5addfKBVUxueL3+ib2VfAzQpe7k3hUr47a72HkzRBA4fhPRnWpx3aat2wHKOBm/kwYu2H8aQyhOQpVkwx08RrqJOoU7JZ5lGAl2kgMVNyuXTuPksr97Dk8lUy95aQvzObqRt2E76uivjcrchGqavhot2f8T170L5bbxw0vOnbaiSNVIbRoocmWZaVJAWsI8IpkVT7BYQazUS3xwhiAyZSmuxLbXWmXNE3R4p4X50tzh7F/vxZpPuZkTXTkpXzJrM6eRoxMxwYPrAbgd7mvKhZwpczS6k/vYy6MyVy4upaFgUYcnlJKBuCjIUS2gQaDkCnXwcUWzZhaN/uhE2ZRHn6dC5viOXilnieH0rjz+1hpPlbM8dFD1t1CxaKQqjEZyHzHfOJm7GCmzfusfHaJbKr17PyYCq5J1aTeXo/aaWVxKasQqZr6cSo8dr4Oc9kgfCZBKtp2A53onUffbp3HEaZz1HyA/cQMimI3MlpeI/yYZz6UBJDHahZHsLnk/lwZhGnRZS2dp+BjiiYBpl6IROu9J+0PHEaz44K5U8tpv7EYj6fWipomXzkwnK25ocybmAHPA1VUe3ZlsZNGv2w3thzAWMdg3B08eLw4iCe7o3n/cE4CsIciA+wZLyqDhk2iZSJ3iVGpMKyeWe5/edTFp7aw1JhkYtuVbL2+RnKj28nvmQL0RlFyPScffELzqBoXhn2Fv64j7bE32ycaEHtcZ+Qz5agG2SIfDrfNYZslwwsBzvgbjacdGH+d3fE8depAr4ey8bGbw4eCavIrKxhRc1JwhbnyjvJ3sp9admiuejhFbAbpSIivDD7k4uoP7VEWIBkBSXieRlfTy/h6ZHFeDkY0a6dEh3ad6Zx4yZ0GdCPU3duYzndh9TNx5ietQnL0CLcvbx4u28+fx1PozzFl9TZLvIfTkLGzWF98HoKpi1nYcBmLj5/RO653cRc2UHysiLy5kSQtG0POfdekSJafVnBb3eYV55JWIwjM6OLGDNyivAfa4xH6mBhF8+hhc8ImhxN9tRc4h0S6NleBVP9IZSmTeWdMP/Px/P5cjSDqKJSDt5+xGuh9MvXL4iJn46FpzP9e3Wlf9eOqPTqhaWuAOB6KXUnFlEnzL/+zHI5SS4ggfFZgHBzVw79urRFvU9P1ETLO1hDheDESOILEuWAnn7wJ8sPXCEzJ5MP+2P5+0QWJ1fNJTdyKsPUejBW1Zxl09awInQj4dbFnHnznoiNi8SF5TFrghc+GlMYNmI44z18mPP7c2RpV0+RKjZe/uZXoratxn7uVHSMrHH2tcAjMoGaNbcJ94xjof8SAixmy03RykBbmP983tfkUF+TKxTKo/LAER59+sJfYq8PdZ9EQ9KPYs/RmA7szpgBHZk8RoUJugNEoCvj0ylJ+RXUn11J/blV8lECQbKCegGO+ZCuGGt0ZXD3NlgN7sXaWcbMcjORA/BJ0I3ntVRtFxmiOkVYXx4PdqVTFCP6E3VllBQ6iuIon3Vzd5DkvZYDq34hNWQh1loz6dlZDa12Guh3NECvtxWGQ3WRha/KZNrObeS8vs5mPrOi7hUhG5ZjmSLy+82LlCfvJdovjbypRYzXskXWWIatkTZXK9J5czCLj0ezqTuWz63b13n1+Stv//l9acG0SYSN6cq+4FFsnTaCOaZ9sdfsJFxgnVB2OZ/PruLLudUiza3msxjrzq34FgvOlTC2208Uug5j/2xDtgWNZaZeD9KTY/kq9n31t7Aw0SNcPrJVmH8On44X8+JgPqXpEQxVUZFfkKdBCKUhleTN2sSe+NvMNVmIWRsfPBr7MbedE+5NJ9CsVQcRlFWRTS9MI7F8FWtvX2bZnRoWvfuTwAtXGSpy5bFbH0iduYwU/0KinRMZ2HcUMtE7OJmM4LetWbw9kMGnwxnUV6dz8/xRXr19zZsPb3j77hV/CzDv/nKeA5tXsaGkgMq1Kzi5SxQ0Z5ZQd1YoK25eUvzz+TVyqj+3Uh4X6k4upKZqK+uW5LBjZRHH927kyf3b1Ne95aXw5xfPn/D86SOuV63hq7C+TzWFfKguZF9RHMo9e6OgIKrRfqaiPF7G0pAtVIRdIVdnOwPaGNLqp84CoGb81LQ5nVt2YHAHAUBqeTnrd1cJEz7K5lP7WXC4Ep9tFRgkL+HAhsekhy4jYUo2vuaB9O0hTKbpTzgYDObpzjw+VKXx6UAqnw9lc29bKreO7uDBL1d4/ugPXjy5x+uXT3n75jkvXjzl7q+3+HlLGl8FAJ8kn5cDIOj8NxDqJDcQFvD1ZDHnqip4/PghL8W6l0LhP5/eFyD8xqPfb3H7wimu7innaeUC6kQarDuQRr2wxHNLEunVqSstFFrRv9sYQi1iKAncyOq5J6meeINhivb0a6ZOi2ZtcW3kxLAWwwQAfZFllu9i4fI9nK75g1MnLpGzrxKvLRWMi15O9eo/yJqzgmCnGMYMNkdL3VIUSiIG6KiICFzA+90pfNyVyEcx1gswvojD1O5O4k9RGT6pyuPJviwe7k7n/rYkXu5J4q+TIv1JJCJ/3VnJDVZ+A0GKBVJAlDKDSI9fjhXwbG8m93dn8mBfLg/35fF4ZxrPtibwflc8Xw9I30ulbncyH8W+n3YlcXt5Eh3E7Tdt3gxtZSsRR5zI815KaXg1ewPu4KI4i2mKnnRr34VuLbswU+ZH5+bdkblGrSYtYw8XDj/h9NGfSd26Fcd16xk3r5xTK++SFrwUb9EDDOw3Gk31CXIfMxrUnY9VhbzdJg4k6IOgj9sT+bAjmU+7xcGqpFvJoe5gNvVH8qk/Vkz9cRH5jy2k7rigk8IKxG1LmeAblXybS9lByNULmXrh259F7q6vKaKuukDsJ2JNlXC5PcLqdiTxcVuc+G68fHy3JY57yxJp/FMTFFu1wXSoM4N6jSbaJo41c3dzNOgZfTtao6CkjLFsggCpFRNkVhg3Go/MctpiooI2cnb3H+xdf5j5FZVYFJfilraHWxsfETMtn4ljvVDrPxYNZTN+aqJIv44K4tYLeL05jveV8XyoFECIyuzDNmENO1L4sDtNACRSZHWe6NwWwlmh3DGhiPBXOYmg+elY4TeeUFQKZHWSomL+SYD06Zg0L+aj4H04KngiyH2sypaD+2mb9L0FfNg8X1CM+G4Mb9fP527RAvnldGnbD6OB1gzqOZqpRgEsCVzPibCXuHSNYajydIZ3H0G3Zt1o2bQ9rcUoG+8cR7R3MTcO3qVi+XrSDxzEd9Uu/BIOcqviKeE+WYzQMGCouhkD+hjQuaMOKh378mpbFs83LqBWoP9+WzLvJQvYkyriQgYfD2TzWdz8jQ1xTHPUY12Cj4juxSKJF4GoG/4SwevLEck6xK2K8rlOGo+KdHpU3LQAQaLPx78B8UkColoAdiBTmLvYf6f4zhaRgjfOExTFuw1RPF8dwaWUOSg16sKgHvqMUTVDp/8YTLWsSZ+ymCOhjzDpMg+drgFo68xirMo4NNsIINr0R2ZoP5MUv0Je3nvL5h1bid93nBFT09BzXMatsj+Z451Bv5690BCL+vceS9cuevRpPYinm5J4timBV5vF7W9PEb6ZIlf+gwiIH6u/RedaYc53KmLlNxOWlE5BQhCV2TM4v3QWdzeEC0uZz5eDCfxdLSqRYxmCskRkz5LXFn+K+FErTF8C4P0RMQoX+FiVLvf591tj+LApSg7Aq7VzebQsgl2hXnRWGIRa99GMVB7P8H7aDOyjTrRrFjVRd3EYmESfNmb06m2Pum4A4zVtMOilj8zdK4F04ef37j5lQWohY2bm0tPEDxv35dwpf46/8wL69u4mXECPfr316dZ5FEotNLi0MIzXovN7UZnMGxGgavem8/5AlgAgl/fiJt+L2/soRn5eiZlGLzIWreLQyUus2XGEmOL1zIgtxDs8Fa/QeHzC4pgRmYR/TCpB6UuYMTeCioJQ3hzOEwAUiX2EK1Tn8OGA+MYeAfjOWGoro3mzPpIX6yJ5JfqQRZ6WIggORqWLNroDjBjcZzA9u3Ug0D6RI1l3me2RhY6mk2j4RtBDVLs+ftmUuEcgm2Qbho9dsEAqDNepKWjZzUfVyAvvKWvFLb3C3Xom6mrK9O41mAG9DenaSQdFxWGE21vxaHMxL3dl82ZvDrX783l3uFDcVtE/lC+6wzw5CH+dKGBR2jy2HTrNw+ffS6Uf/0kV5McP77l59ijvq5L5ekL4/tF8oXiefJ/3h3N4tz+dd3uTqd2ZIAJwHG/lcSeZv3fmYDdIi65tNBnQZTgjVYzp37M/vXp1wM00iMvbHpK5eDmBYf4YObgw0XQGhnHbmfn7I2QDtcxxHOtJmnMsIZ4FWJguwMEuhSDfrfxe/gpHC3cGDx5Ip46dUe1rIixgJB3aaqHUSoNgazeurMrl3cGl4oDF1B4ooPaQAEJkgHfCZKVROvgH4RII0/6jMo6zVeWcP3uKX27d4tdff+fCzzc4deESx48d4cjOcq6Vx4hY8F1pYU2Hs6mV7yVcYr9ovfdmCCsQxdd+4Sp7s/llYSJhxnYoNRsoUtwQVLqPRF/Dik6dW9Ovf1ds9F24evwp8+/cJ/7qEZJEYxR36TIu5x5hdPUTsn7Ct71HO7I5tYJjh+4TaZrDVIN0ioPOcHvLS2xMHNAcPJi2HVuh3MeQvt3G0bPTWBRa9hK+3ZLGsjY465mwOV6Y4+5FItCJ3v7UQv4Wkf7zkVzqhTKfa/LEKAKfKJslP+e4oJpMnohu8ubaOTzYukBUk4J3TOotRMATVlMnRum5XuxRJ8y/7ojYR/C/iNjye3kSxTP9MFQeI77fVpxDgbaKAxjQbSya/cYzqI82nbsq0W9AVyzGWHPuwBt8d9wi79xp1lyoYkX1LuLyV+EZtByZ7hAz0eVNpTBkERe2PGT95HUcSf6Dy5vf8suax5gZWjJ0yBB69OiColIb8aHeohj6iUZNm4i+4Kd/9es/8ZMoM7V6dWe6mTZF0004muPDxZJAzi4O5FJJKHfWRXG7PIoHInM8FlnjiaD725O5uSme3yqT+E2Y9B2RWa6URnJ0cQg70qezI3cmy2LcmDt5LC56QxnQoaMceJmsVcO3f2rciEZNGtGkSTP6dFKjbTsFuvZoS1/lrowfNZ7TW94zd2E1abuusmX/Pm78fJGy9MVcOfsGmZ2hMwucvEjzSGWnz05+ybvHxaxbAuEqdsfex0BvLIMHDaJnrx6079CWFq1aiFrg24dbyTqhp+1KG8UuNPtJkf4dR6Haz5xuKmPoPVKYodqI//rDyP8v9dW3o+9oa9r1HSrKXCMGtNeT84f1dRIBNhIF6ac8MW/Rshlt2irQsbMSXQQAfZQ7M2rEaC5VviE2dRuL1x5hy87TvHzwkM2x5ayMWCcKIX0zpgsz35V6knPhN7g4/2dWuO7ETqeEbdHv0RkxEo2BA+nVo4dA9hvqur2dKPD8VTQskJhwiKlORVhYhTHS2pvhNu6MtPNFw9BaLtuoaTOaiMqrcXMFGrdQoEmLVjRpKZEiTRSUBP1rlN61UKRpS0m+lXwua9xEbl26dn7o2PigY+uJlpk9BmOnEDhpG/Om3ibK6zHzbFbSrElzfmomo0NHRbqIVrp3/05YjnfkwXaoKr3DisyDrCs+z8NHb9k+bxvrsrYiszA0JMoukg1Op7k4/TdOzL3Oz+tEE3L5Dc+v1TJs2Aj69++HYrumaPawZO2MJ2xMuUxBai5ZuXEkZmXhFxqFjU8w5q4BmEyegaVHEMqaI7/dimhOmrdUoJlQqoGEws1b/UOKrUVrqiR4inJ+0+9ACLmmAiRJXtpnkKHI204zMHaega13MNNCIklIT2PjphVc3HOUMyt/57qos8KtVghLaI5i+5/o0qsVObOXUbJuI2d2n6P25Fvuln7gwbG/OL35GtU7TiMzMRiHm0EsNzLe8uXXWj7/9UqkpGe8q7/Jki2bWRe7H7U+I4hz3MKB4lvs276BP+4c58Xja7x5eoO3gp7fv8L1S8eo2redNWVlLC8tx9BykvzgzVq0RKGVIkqKSiJ9KtFSPEtzBfHcSsQUpTZt5aTYWoytxVyMrcW8fbt2gt9Gvl7aZ/PWjZw4foibV0/y4uE1Pr78lY+vfqP2+W3RfV7lnmjlz5/eyuENh6nOfEWobT7aqkacKP+N4m1lQqenfKx/yPMnt3ly/jZvj35g2ewyZDpDjdiT8Sv8Lf11SK30e46g13z5co8VG1dRsKaCW/vfcfHcMV69usmrP3/j5ZM7/9AtXj76hefiQK8fXaNeHOjapRpCwmfTX0WNzu070EkELSUlJXErLenYRhGVrp3oL6fOKPfoimqP7qj27IaKeJZoQPfOdBOxprVY06ZtO1F5dqVj2/Z4e7gI8Muof3OX+ncPqX1xV9A9al894K14fvX0V57cvcK9385w9+5xTlYd4dqB1wRnp3L7wVnphzpB7//5U6B66uvuidhwG9kYDX9eH5FevvpHeUnoBfXvBbIPzpG2tJDN5Ud5+ew2L57d5c3z++KD96l9+VA+vnn+O/W197h86TgT7Oxo166T6B0GoammRk9xc5pNZfRXaEHPtm3o0K69UKYNXdoooSxAUBUKqwvl1Xp0Q1ko3kFJUdQY7USsaU8npdb0at4UdbFH6yZNGDZEk249+tGj3wDSU2P58vEJn98/48Pbp7x//Zja1w95IwB5/vQ3Hj+8wf2b19i15Szlu9bw95c7cp3g3T86fuFvcclvz75AFmtfxcnyfayL2sCiKctJtcsj2iSJaNNk0ifnUThtGfleq7l1/hIHVoggMm8j6yI3Uh61SawRNG8DaU6FBGtFM0c7Bjd9e2aNDmb2iPkED55HypBIEofMI0o8zxUULOaBgtS7dSZUK0pQtJyCNaMIGholXzNn0DwixBik7IOVADBwcCRzdBYwd5RornSDaNe5N31U1LlwrprHt27Lz1IeKc4i6OzOU9y++DMrQ8pFoF7LwpnLyAhLxcffibkhs8h0yWVZ0Ao2xFXy4ORNZOvCL1BdsZttdlWc9b70X2mFaQU3Tl789oH/h8x3Ob/xrkSMjv2fckm6hQKATvLxf8llaUayun3zH+Tm6cVjPm4cAwcPR6lrL2p2H5Z/V3on6XBswzFunLryA89Nezp2LtYEus9s4EvjrfMiCG6N/J0Sz3IqbfdyN/C+nC74XuXatF8a5stN1nP73FVx25s45XWhgf+fJMn5m3sQqRf3g5z0fHvmbw1zSaEuigryscb9NGFacUQOS5E/x2jGkjkiRb4mTyuS/d1byeW+7xMzLhEnawt8ne1QHaiJr8sc+Xel95IONRU13DhxpYF3YHINM3TDiPUPJD1obgNfGndGb0e2IWgPeW45bLHZzW8Bv3PE9SRp5tkkG+VyZco1Oa/EpJy7l66zNkJEYo+zDXIS/z9pjqgBovXjG+QqJmwnf2gk6dqibp9xS86TFGongpw0Smv0uhsyaYC7/Dmx2xhWq7rJz1MoWUDHZnI5aZ20Z6JZMrM8HQif6oKthQmmOi7yddJ7aU31umqu1lxq4ElU5XSEeLNU0gMjGvjSWBGwDFnKlAWscalgk/VObooDLjNeR1FwDOkesT/wfj9/ldK5FRx1O9nAy80sxUjDmpBJ0wi1n0qY4xRmTXZgvgDgu5x0+G6tlQgRfv9vnq3wbWk85FxDrLj1SOHz3r2dmaMZR5SwBklWihUtRVCU5KR1Ek8CICXUmwAPe3ycrDHXdZafRXovnbdqZRXn9p9u4H1fF2uaQsqMuQ18adw4pQLZFE9vCuwL2CBu6trUaywdv5ac6RGkusT8wLt95gJr5pRz2KWmgec7OQSToXbEegcx13k6sx2mEOLuxIJxCQ1y0uENNAcRLgLkv3lV//i2NC+z2EzZAHsCWzbCd1Aguybtl8uGj4zFfOyoBjmJF2ucSI5vFCGmwcy3m4u+sqX8LNJ76by7luzkyPYjDTyJLvleJmF0HgnGGex3qm44f7ZTOrKE0DCm6QVSblUpBC+y2KiURM8QEh2jfuDdPnWOsrD1wqcOy3l7HQ7I+f9Jofa+xBknNchJhzfRGUGEUObfvP2dW8rH7/MdYh7Z8icM+pnK95FkI0bHYTp2dIOcxJtvmEC8aZL8bJLcRF0n+Si9l3hL4kUzt626gfedznqd5YgA8Ptcej/NcqYAIDCUWWahrLUUEd7rNDvt9xJnmEqCfhZHXY/KeYuMVnNq9zFyPEuE4vvlvP9GktwMK095oPout9qsgpBh0cSKGHDC/bicJym05x8AvsusEH4vza1FLJDOIq0PGTafoYMGNshJ6xNG55IwKlf+LJ01flyafJTeS+tmTUwkLqpUfpb/1zm/n3WO21xkkTP8iZkUyRrzDRx3PyanvY5VAu2DDfPicasIcI5itnacXO7ftNN+zw9y/ja+hI2K/YEvPR9xPdIwl/v+P9Fdmh90PiTf69+jtCZQ1ANKLRUa5CSS9vn3XtJZvz9La10GzWDiEA/5Wb7z/xtJ7yNcw5GF+Pky33Yeq8zKqXY5JKed9rvYarujgYoMV6DfywLX4dPw0QnCW3cWs8YHE2YZSrS4bUlGWifJ+Vp4ES5M9ztPon/vJZGk0N5/APgu858kyc0XBZReY9n/lPs3STq4afpjquIgP8t3/nqrzXKd/i0rvZ/tEIJsmutk5lqGscKkTASIvSJtbSJ77BLmD88kRCteVGmJRGhnYaMeyBC1UYwfY8FkS2ucbSxxtLFgqn5Aw9pCg+U46E0mXBQrm6y3yHnLjNeQLNJgtsjpuYKyNb/R6n9cQJL5LifNpVGaS+vTxDqPVo1+kPtfJJ3De8Qsxve3kZ9F4q21rMB7YCixunkC1G0NstL7ABt/ZE62E0VEDaVEfHjPpB0U6JfgrObPtKEJTNSJx1gsHtjJCrVO+oSLuXSYf9OS8avYZlvZsNZOABAqytaKCRvkPEkmtmNLUgVldmpJihiT2zUjr9M3ACQZSVZSdmGHFj/w0gVg0R2aN/C+k/RNiSd9r1wo+J0v6eA9PAALtUnyd995niLNRuvkUmq+rkFWej/VajqyCWbmBBgFyzfdbruFPP2lTFL1oWurEfTvOA717qb06TKc9m17E6YdzzqLcrncfyNprbWIyiEi5X2Xkw4a0KYps9s2Y7ZQfHKrxgxq1YJ2Ct9cQJKRZKV6f0TLZj/w0gQACZ0UGnjf+XNERhk0ZDAOY1zk3/z+TtLBU3Mmpsp2DXyJ56gSgFUfTwJFUF1lWtpwVk+TKchMjEyYKtLgYqMVVFpvIFeYv0FPC9GxDaZPx5Gij+9Om+696d57sByAMvMyudx/I2mtpbYjwcPnN8hJh/dSaopP66ZyINSUWtFIsQ0tmn2zAElGkp0lAGjcrtMPvAThKl5i3XfepgnrhSnnCDMPQHXQEMYNMSNCJ0lkkVL5e0kHDy1/jPtZy8/yneeo6k+XtoMZ09OsgS+NToZeyEaOHIu3jv8PZj26t5mowDqj2KkHHXqp0am3Op16qDFneOz/5QL/SSZa9gKAmB94TkpN8GvbnOAOLemt0FL+q09HpbY/yEhVX9P/4C0QbmHYstEPvBDtBSgPHISaAGCA6kDMh9r88N518DTG9bX6gWen5kWT1u3RV534A99mlCuywZq6DBqqjYaGFmoDhwrSom3H7rTu0l/QANp2U6Z9T1Xa91BmcH8tRg/WR3uoHkNEN6Yq+n61oWKN5jD6awxBVXM4fdQ06KkykA59B9C8XRf6iijuIm7erU0zHFo1QUWYeeNWbejcph19O3SSU7+OnVCUfids054RLZowqU0LXNs1x6l1Mzo3a4ZCp+507a9Mf6F0P6G8dPt9VdXpr65BnwEqAgh1VNUH0X+ABn17qtOxfU/a9lAX3eIAWndVpXUPVVp17CPWDEZt8AjUh+igPlQXDa2R/B/TRcoMt6lsmAAAAABJRU5ErkJggg==', '2016-09-04 14:54:24'),
(65, 13, 830, 2000, 'BungeeCord 1.7.x, 1.8.x, 1.9.x, 1.10.x', '0', '2016-09-05 17:01:37'),
(66, 14, 390, 1000, '-]-- MineLegacy [1.7.2-1.8.9] --[- |  !  SkyWars  BedWars ', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAgrklEQVR42u2bB1SV57a1dxJTNMYSCyK9SO8oRZRmwwJWUFBB6V1AAenSVECkKEWxi1IsWLDFbhI19paoSTSJLRq7wa6Zd74f5UgM53jHuOf/zx03jLEGsOu3nnetuebaqAyA7P9yyP4G8DcAyD76+OO3YujQoTJDQ0NZt27dZFZWVjIPDw9Z4bx5MvGlrq4u8/HxkX7u3KWLzMHBQfr5ww8/lJ7j7e0t/a6rqyu9Tl5Rkaxz584yO0dHmXfsdFnc8hXS/dq838zSUjZm0iSZT2qauKlNJ0VF+44KCsGG/fuv6O/jc1BeX79aUVuntexPX8r6+jJza2tZ6zZtpN979u4ta/Ppp/XXpKQkfe+ooy37uG1b6ecPPvmk2fP/EwC0JQBHAggkgGIC2ONbVHw5ZuvWG2nHjt+ee/787znbtj+dXF5eZzR0WBkf/97/dgAqBDCCAJIIYE1wUdHhmTt3fZd96NDP+WfO3im4+P3TeVeuYP7NWyi6cxcr7j9ARlUVEs+eQ2DtlrtdtLQiPmnfXvZh69ay9/k+/xsAfEQAlnwdHwIoTV+8eFfe7j2n5p86fWXBpct1S67fwMqHD7Gyrg7Lfq/Dwrv3UHL7DkqYeCF/XvfgASJLFyDtl1+QeOkShhcUXGrTuXP/j5jQB7y+/0QA7xGAEp8zmAAiAoKCFhUuLNuxtLb2bNWZMw9rRFJPn6Hy2XOUM+kynnT64W8QsWkTfCor4bZsGTxKSlHI088jiM23bsGHAFKvXkUiIUScOoVegYH7CKDHfxKAjwnAmABGDRgwYHpAYODCnHnz9izasPFy7Xff4cCDh9j/4gU2MPGlv93miT/Cav4ctX49xi5YgCGLF8N2/nyYz5kDvZRk2EREYC4fl3vrNyw9dgyBNRuQ9MsVTP/pJ0z78UdM2rXrD1UH+yICsCCAz/5/AfiEAEwIYOSQIUOmhYaHL8/Oy/umeseOugMs1VM84f3PX6Cap1hw7lukHzqM+H374L96NfJ++BFVLPc+U6ZAYbwH1OPjoJk6A5rJSVCfGgXnnDnIYfJzGUmsiqnHTyDup58x7dJlRHz/PUIuXMDQsrJHXQwNKy0HD44igIEEoMprev/fCaAdAagTQE8CcLJzcJg8KSBgWXZx8bfbvzmC8/fu4bsnT7CLF73ut99Q8/IVyggipLIKo4tL4JidjV5JiegxfjzSTp5CxbVrMJk+Hd0nT4aCjzcUJk2CwsQJUBg5EgE1NZh18yYKfv0VPkVFiP3xEqKZfCTBhV28iEBWlc/p0+iTlv7HiOjo30cGB39lOXDQTE0DAxcCMCaAzqIV/ycAfEwAGgzrgU5OXl4BAbnRqWlbF69bd3XXuXM49+QpjrKUa3+9iSW8wJknTyJ8yxYkbtuOdQSQyNNW8/aGemQktLNmQ4MnbDBxIopu/Ir5e/dBPzsL3fm7yrSpUCccERoeHkg8fx7pN24gn6c+YcUKRF/+CZEs/TCefhBP3+/bbzH5zBl4fPMN+rFtYvie6QcOwL+46Haf8eOruxsbh7SVk+tNACpCk98ZwIcffdQs2rVvb+rr57dibW3t3VMUoOOPfsdOqvNalnXl3bvY/Oo1CjmefBYuhENyMrQjpsCUiU7kqW148RIhc/NgzAQ62dpCnokph4XCYdo0VLD8w+cXwWDJYnQZNgxqiQn1APjdIiQEM65dRxIrJIcgwgk1khUgkg9m8v7i9M+ehdep0xh/4gTcDh+GWUwMBtduxvAvv8So/fsxbNUq9AwOvtnd1HTexx07WhJANwJo/S8B/OnrPXsHh/IHPOGveZqVVOPMg4fgu2QpBmZmSKW5j7c7R0ZBeZIXNNnH3Vnexl5eSNq5C1vZ/x7p6TBYthRdnYfVJ5gQj7G5uah5/BhDExKgV7YQXUaMaDp98RqDZ89m8tcRTy+Qc/06ItlG4T/8gGCWfsB35+HDypvE05/AaeB+/DjGHj2KYdu3wZhgh+7Zg8GMATt3wnHbVvSprIDx9NjXnUyMq1t//vnQ91q1ak8AH74rgDZ5eXmPbvwBLKICG/KkeqSkQJ+nprdwATzmzsXXj59Af0o4FAP8oRwaArW46dBkL89j6e7gKdtFR0M7Pw9ybq71SUZPQ8DqCmxhFRny8QKIop9fEwD1kGB4cjLEMfmYn39GzOXLTP5HKflAqfS/w2Sevufp+tN3P34MrkeOYBSrwH7JElgXFsBJAPjiCzgQgO3mTei9fh3Mly97rRIY8LKzpeVxAnAV3uQvAbThKHkjnC9cvPjHidd/IKqqGlqzZkKOEFQ8J6IbTy2C6rz5+x9gkJYK+QnjpeRFEsZMaDHH3g4KoVlsDNQodIpBgdJ9qvyeRMGs5qmqs10UKICqMdFNAFT5OhE83eiff0EoT9iPiYWw9KXkefrenCaTCGCiOH0CGHusAcChQ3Devw9mM2agH5Pu/8UOOGxtAFCzHpZ0kqaLyqCdkf5CcZLX7baGhnuZe7u3ALz//vtNYWlpWfyAqr6DfT4iKwv6opRFv/IUu40ZjVxexMIdO2C0oBTdRo+WEhC93CcqCstYGSuPHkOv2bNYHQFQ4XOkJH18UHDlKrJoerR4X/dx46QqaOx/w8BAadRNZQWFUtgms6eDLn4Pf2qBD3t/EsvfUyr/kxgn+p/lP4ZCOPLQQbh89RX6rVsHPQpuv23bYL91C2w3iQoggOpq9FxVDuPFi9BjRopo1afMXfOftUCrmJiYC7+9fo0qKn3P2FjoMlG5UaOki9XmuCqno8ug2OiXlkCuAYDq9Fi4zMzE8qdPkVO9BtZ8QwWqvBp7W9yvHxyMxRTPQLaPPl9PwdX1H+XPx/SlkIrkY1n+/hRWX87/wAsX60+fyu8lTl+UP4VxHPtfABhNACMOCgBfYvDevTAlWOPUVNgTYN9NG6UWsKyuQs/Vq2C6Yjm0xaH4+opk9d8CIN/ps8aw3759+7MzLP+88xegT5OiJvqVvS5Oyp7Ct4UiF1xSAk06t8Y+Firvx9ZYRWhTCwol6oru46TniPut4+JQfv8+BsfHwyBvLpQmT2oCoMbnji4vRyRHnvD9bqw6IXr+fH9x+pMJwPPMWUwgAI8GAK5vAHBmBQxhG9ixCgw4bs1y56DvxrcB6ORkQ5G+g7kbvQUgeIi+FKMH9Fp4hSZkK8s/bM0a6ZS7U91VRU+zZ8czub3PntGxZUOVUFQbSlyRp53M8VRV9xjuLDXj8pVQcnNrKvFBTKqcBscsKhK6HF2q1IGmCqAe+DGZcLZAJk96GKskgL3v23D6ovwnsvzHv9H/YxoEcPjXX2MY22UwneZATgCr5cugQ+0x5Wv0XrcWFtQAc1arqbidYBoAmL4FoCLKTsRHaVN9f/n15Uus5CkPnTULBkxElL/oceXAAEznmNt9+zYsWe6qtLJqjX3MMZhDD7+GIuhAc2PEaaFEYZMMDh8zesFClPAE9fg6WizDxtYQcPT9/RHKcRchNIB9O4Yn6X+h/vQnNZa/MD8EIPV/A4CRfwawa5c0Ak04ETRZVea8hhYAvF0B5RG2ItSL8rJfn3n1CiVcWsx5UvpU0Mb+V2bC8zgWq44dZ5nlQnns2KYkzNjjuXxOBee3GT29fmYmVIKC6gEQiNfatUhkYoYF+dBmRTW2hhq1w5ptEcaxF8uFxyMnB94nTsKPALwJwEuI39k3y/8EXAlgtADACeByUAA40ASg347t6LuhBjrUlB68DnMKuDnb0YQAdOfmUgN80LbdJyqM5gCKA6xEjNlA9dxOJ5fJ/tPhhfVIT4OC9+T6k6KSizV2Fomaz58niZyUBMdgP75hLt1iGfvTjONRMzxMahkJnJ8vQnhKPvn5MKGX0Gx4nggVzv9B8+YhhOMxlRCGZWTATyg/Q/S+V2P5E4B7s/4/ghF/UQECgN2WWlhV0IoHB0kgzNj/EgD6EgUC0DOXb81oDiDe1URExgmSX/HsBXxpWgyLi6DA8hSjTJSsY2IS1lPlA7ncGFBRVZiklERkBNwWliGPFZDBsWNBlVcW/qChPbrTDEXT0DgnJcGU5Sk/jpXDk9ckOCVW1RiWvQCQxiQH5uXVn/75+vL3lNS/of8JQOr/BgGUKkAA4C7gtG8vBuyiC2wA0GfjBphxEnV3d4f+nByYsBJ0WX2KPAxHN9VWjOYAYtwsZWEjem6+8OgR5j+hyLH/DamecmPGSIkI0zKOfbyFLs+JVlY/MbGpjxU8PRG0ezcKCCCMpywUVxgmcb8I5bFumMYRZxkeDh22hbyzM/Q4981oYXvTYfowoSn0/BFskdGbN8P3/F+Uv+j/kxTAhgoYJSZAA4ChBCDGoGSDtxNALQGwDazWruHEyYP86FEwopjrFRZC0d8PDqNVPmE0B3BqZbjM32P41a/p/+eylG2opIYs9S68WOmUqfjhfJNN7PGePLkeND2NfSzn4oxEakMWbe44zmFDbnpiCepOjyBWXKPQUMRevwG/r77G5P0HMJn9GyLWW469EAqfMDxJdHpDuD94s9yl8m8AMPHsmX8AaBTABgDDhQvkCJQqoBmAzU0AxAjU5HUqUfz0WH1inNs4K7ZlNAdwujy8W0Zq/KsKAkikATHjKNOiaspPmFDf5zRAM5lENjXComg+1NhLopzU+b0XFXcGt8S0e/fgt30HPEXwYoJ4weH08FNY/tF87hSutb5HjsJ1wwYMoX93LC1Fb1aMFQW1lzBIfE9zVt5A3jeRiXq+0f9NAFgBY44eeaMCvpIqwGlvwyLE5UhUgA0BWFN4e4mliNNAjF0l2nElTjKLgd07MJoDOLo8rP+yqkoUcI57VlRK/aPIclWlkgrL2os2M4sjzoc7eE8SteayM5TbYShVeSrndcqde4w7mH71KmK50AhPH8GTnbBtO4aUlMKGm54JTZXR7CyYLVoEc45XM56O8cqVMF62DAacNobUDgO+rwnDiI+1KyjAWCbp0dj/jQLYMAJdGk3Qgf3SItS/cRFiBdjU1FeAAGDK1+9BYe7KaSYgmNjKdWE0B1AzxydkN99oNjVgEE/BmH3cleUv1lwDbm9jl6/AzIcPEcvHhPF0Ik6eQjT7Npknn8z5H3f1OgIPHcZYmqfBCxbANi8fFhxp5iw7M66+xgw90YcMfd6vw9ulD0miIzFweih8kgIxMWI8nAI8oMOWMaNwiZFpzHFqwz52YXJuYgNs7H8BQAig5AL3Y9CbAKgjvQWANQ0AOAG0udB1HjZUmgz6Vp3lGc0BlCROyDvGLS6Dpzy8uBhmfEIfqvYo6oDvrt2I5Wmm875U7gEpfFw0f/fj2Bm3dh1cVpZLZWvLk7VimPEkDdkmukxSr6RYSlozIx260VGwiwnFuPggRGUEIa9gCirKorB9WRQOr4pCbrgTZgX0g7KhPjScnGAQFgYdnpyBAMHJY8FWGcTEhAOsL/+DTSNw0J7d6MdN0J4A+koAuAgRQE+OQzF6tWjcOg8dAlUeppbZ50qM5gASInw2HuaMT6OQTWFZe3HbC2OpxVPcEq5e489H4c3eHlezAcMrKjBoxQrYL10KK/aXGZMVp2XAsSkWHR0aDm1euNWMOAxNnYbJmZFIyArH/LkhqJgfgp0LQ3ByVQSub03A7/vS8ORABp5+mYElsUNRMa0/tFW6QV5BAeo9e0LZmVsox6yYJlpsC2PqkuW8QjjwYJwPfImhjSZot/AAO6RNsO+mhlV4TbUEQFyXJvWl64jhUBb+RL+9GqM5AC8fn9O1VP+kX29iKoUrmHR9WVYe3KyGc7sbwGT7cgEyp5kwyJoNXV6IoShpJq43eyasMlMwcGY83GfHInhWBBJmh6FgTiAqCgKxd1EILm2Yjt/3pjLZdCnZv4rlCSNQGWUPa30lKCnVh1z37pAzM4PC8OFQpdiqcSfR4BQynDkTlsUlsF25AgNZ+i0BMGtYgpT86z+4URafRWi27cFoDsDG0fHbJKrpOO7TLtzqHGlsrClCRlxitFm+eqSuT5LCTxvPzkCfzGRYTA2DTeBkuIZPQHqqN8qyvLGtOBDnqqbi3u4ZqNvfcrICxOP99dFYARVpbqiI6IN+JipQVVWFhoYG1NXVIU8Inbp2RVcjIxiEhFAXMqQVWnyMrj9jBnqxZW1ERdIA2TUC4CbYiyJrQMeqRgFXpP3W4aRR4TImp9JWl9EcQMfOnbPMJ026piXWVT7JkH2jT+EyKS1G7+J89JuTjiEzpmLEND94R46Hv/9oRIZ6Izd1KjaVxuFsVaxUzi0nnIE7O5NxuSYG5ysjcXZVOE6vDJPiu4oI3NgSj6oMdwmAs4WalLiysjJ0dXUlEApsCTk5OTjQjk9gJZpyvdYWPoQTSjhS7dhYmPN2Syq+NU/eQiTPalXnHqI4yUsSQX3er8rfuyp8ZshoDqBD+/at5HR0ZmhGRl7Szsy4Q6v6VJkjw8ZnImKTg1CWPgF7FgSzb5Nwcd10LCtMxU9cXp4/f46DXEjmpoTje97eEoCrm+Owe74vSmOcMcvfkWGPgrABmBc+ECVRg3Gg1A+VGeOwakofeNhqQFNTE926dYO2trYEQUtLS4JgRXNV+/gJfDjjdcVOwWvskZIs/WFFwXMi1Pi7EUvegKEmyt3DA3qcKEbiQCnIYj/pKPexKeMtADICkH3SunX/dmpqc7rY2+6TG+L0a1iwx/OX59fg2cHZjFl4+tUsLJjhiwf37kB8vXjxAhdpdJbyDQoTvFHXQhUsThiNWA9r5Ic4Ytl0Z2zOmcBTj8OjPSm4uz0eD3YlY+1MD6wItUHwYB0p4c8//1w6/UYIOjo6UNXTw/67dzGHpqovPUk/en4TKrw29xVNWmsNlruyhzs0WBX6hGC6nIsQdUBUtJhGomI+bf+hBaNFADICkBGArFWrVnIu/a32vfiuGs8JQMSjfZkozs1A49cZOjVvb2+0a9cOibFRuMj+/ysA93cm4c62ONzeOh3XaqJwfjVLvyqq2WM2ZU/EwgBLTHPRkwB06NBBqgLRDiJ5EYpsi0UUu1XCrrNdBwkLvFuMwC/Y//UjULhAS2GD6QHMysthTB8gDJYuAWhwGnzc5oPejHcC8L6JrvryulOrmXw2Iwt3d6Vh1fJFTQDO07e3b98elpa8cI6r79bEsd8z/1L0mkISv7S3RLI21xMl/pZIGmNIAD0kAF0pfqL3hR6ISlBR7IZkCvK2Z89hx6VMrMD9xQchYgukAPZp/DRY8gA0QeUr6DSX1gNgCwg/8uHH79sy/jmAgQP7yvrZmMuGOFqVPPu2Gi8OE8ChHNzfk4FlZfOl5J8/rcONqz9hTdVqnDl1HGkpibhcK1pgJiOzRT1oKTZmjUeJnwVyJppJGiBaQF5evglCd06DYRYqGD1uOHbSlY6m+jvQGPXnDuDAJciWU8Bm00ZYcQJYcC03FyaIY9Jo6RLJautyimnQn7Ru28qR0TIAQyMdma2lSWPMffbtGgLIkaLuy9lIj/bB8uIcrMyJRO38SKzNCUJ5dgRGDhuIX7Yx+a8a4r8JoZa6UOJvIbWB6P1OnTpJ41BAEAA6d/ocU1304WClh923bmEqbbBYokT523MJ6ssdoDc1wYprda+qKmnXEAAMCUBMNF0aNg0KZusObQYy3gagqa0pU1GUl5no93gzMp+cW4OX3+Qy5uDq1hQsShyHfQvCcWLFFFysjsIFjrVji0MQ5t4P13bm/APAV5n/LQg7CyejlMkvDektVUCXLl3Qo0cPqKmpSRBU5Tsh3d0YbnZayFtfgzxumGZsA0dqgp1kgTdJO4AlJ0RPLnZm4u8BjQJIADoEIDbbjz7vOITRHICKYneZnpb6X0Xi47NrmfxcKe7szsDFtbG4vzsNz77O4mRgfD2bPZ2J3MiRuLWvgL//awB11IAHe2bg7o4EPOQEENpwqCwACwOtUMoQwicACDEUoaKiAkP1rpg1wRQRznoIpgES/6ymT2IiF6BtsBX9z/K3Zv9b0AeYiyWIG6dRgwCKP+np0N+IzzA+6trVhdEcgI6mWksxtU4AOJJHAHkShBeshueH50iaUB9ZkkAWxrjizqGydwIgTNM1mp9Di4JwaKGfBEGYIwFgnq+lBKBz587S6UsQ1FVgrSuP1LHGmOFmBL+p4dj49BlGz58PG562LS27zUb2fyMA9v9bACieYgx20lVwYzQHoKmq0lIEPjxDAEfzCSG/HsSReggvCKE+6idEGVvjyJpZOLs6AudWR+KnDbEtQnjME/91WwIOLg5FZdpYXKwMx6X10VgSYoMifytJAwQAMQa11JUxpJcafPprIcRJGxFDdeDmPhwHWAFT9x+AKee9GH/WYvytW8v+r6zvf5a/kZgAFEC9BQ0AWAFq9lrTLF2VmwNQV1FuKTxvHa/Gq6OFjHxGngThxTdvQsjGi0PZOMAtb92cANRQzTfMHo/Di4LfANAcgtgBbm5LxI/rorGtwAfHlgRJdnh1lC0WUwN6aDYA6NIJDkYKmM3SXxlmg7Ah2vC0V8cga23svXQZ+bfvSH8TlE5/ff3pi/lvuqp+/hsuaRDA0lJoixagCKoMs9owLkXxvT8BUGkp3C5/XY5Xx4qYfIEEob4a8uoBSBByJACiCh7ty8BvXyTjt21JeLgntR4AQ3iDh9wGb3AFvlKbhEubU/HVynhU53OKZE3C/V0p0uM3xffDuml2sDHRghpnvnmPbhQ+E1pkG2ye7oDqqbbIm2yOSE6DpZs2YxFtsfThqhA/of7VVfXjT5S/+KRpcf2f9HXoASQANELKE0bedk9VbNcMgLKSUkvhfHbnYrw+UUIIBVI8PZyH50fqW+KFCGrDi8MEcWiOJIpPKIq/UxQf7M3EXYrlHZqnm1+k4du1CVibG4glOdOwoqwQh7hD3OI4i/IZSTFMlCpjd/oQKdF0d1OMt9dCGpNfESaSt0dtnIMUlVF9kOFhgukZqah49gy2TMpKLEDC/bH8TVetkv51ijT+WP46nP/ahQXQypsr+QCViFCMS1Md1AyAgrx8SzHgm41FeH1qAZMvlOLihhRc31eIuqML8eTkMjw+sRyPjy1C3ZFS3P+6EDf35uB4ZTy2LojB8uwwlGSGo2ROMqorV+PKlSvS/tD49erVK4T6jscPq0OkSbB/tktToiLpmhg7CUjjbY2353iawWfSGGwigBElpTBnsr2qG8WvvEn8dAryoZk247VGchIj8ZUI9aSE13qDzcreFUCfPZVz8er0oiYAZ6rikBnqjDgfJxTnJKF0bjpK8zJRmj8TS0ryUbFyCfbu/gLfX/gO9+/eRt3vD/Hs2VO84Ob4+vXrpuTFz1vp35W6y+PEQm/JHh+YPbxZsi1FGaeF+0BjbL53D9FHjkIvM6P+9BvMjwH7vgdvU/CaCM2xDrvk7YwWd7M3K+4+yKZMY4TFckXrnrOaAVBVVm4pzGrKMvDqzBK8Ol4P4Mmhubi1eyYWp3jg2tVfcJ+b4f17d6V4cP8+HtKmPn78WDppkeQff/whJfzy5Utcv34d22lbc3Nz4enpKal920/b4JtiTwnA/ncEsIZaEDLMAGmbNiHzlyvQio+T/pKlwa3QwHMsbFwcMGpYLzj100ZChZ1yNxvdrnJ9DLvI97PoquFsKqdg1bNzMwAa9P8thO7KggS8PrsUr4/Pa4Lw6lg+agsCUL2sCHtrK3Bg2xrsZ+zYsBrz5+UjOztbivT0dMRzawsMDMSIESNgbW0NSzMz2Jubw5LLTcdPP0XHdp/iJCtAtEBl7MC3Sv6vQjymyM8Sod5D4Bc1CU5jHOAxwRERk+yR6tMHc/16Y2GQNWJHG2DOFodWESU2MgKQEYCMAGQE0HwMWpuZtxQqOUkheEUA9cnXC6GYBjd2pOPi5pn4+Ys5+GVnrvT9x21Z2FQUiV6mhk2f6qgoKcHO2BjaXGYG6Osjy9UVy3x8sHD8eKQPGIChplq4uDJQAlAUbIeNsfbvVAXro+1QzFaY42uNXIYwUaun9EFFVF8UcqlK8jK/4u5iWkUA7zH+OYAhjv1aii7ZiQ0ApMQL/jEOJXcoRiFd4WHhCLMlV3hzewq8h1vh/ffeQ7vWreHGVXk2kw5x4BgLCsKplBScYxwKD8eiUSOR5WmPm5ujpSlQEGjLxGzfCYAIAUs8fu00WywJ7o3Z3j1vhI3T2+LlrpnhO8bQvb+TmYVI/l8CGD7IqaVomxHj/04AxPiTRt72ZCxOcsOHrVphiJERKpn0gdhYbKVhOcPE90VEYN3kyVgwZjgKJw7CwSJv/L53hgSgKNQRa6a9O4BKGqc5Xr2eBDnpHHK2UCkOHKUXaufY3d7BtXsXApARgOydAIxwcmopWnmPdTl5aHXKw1+/yGSiefWOsMkS19thAUB8JnhgQSD2FvliWcIo2BqrIHPcYKyP8sbGKB9sjQnE+nAvlPu7YomvCxYEDcPefC882JXU5BAXRA6UzM4/S3pDjD3KgqyfZnqYXfAbqLvVzkAxX12+o2vr1p/IE4CMAGQEIPufAiBztOk9YbqvS2lRnPvObUWhp05UTP/lx03Jdbd2pqPuqyw8FZ8Wsfy/WRaOrble2D7XE7XZHqjJdENNuitqMlyxNnUMqpJHoXrGaOzMnYDTS/xxrWYq6ugO37TIi6YNRhV7+M9Jb2Kpr47o87zYz+LnZFfjg242GuV2hoohHdt/ZtqmdesPCEBGALJ/FwAZAci01dWUzAx1RoR6DEgsTRy/piIn4Mj2kuBzh8ujL51cPfXGhbXT713eEP/kWm3i61vbk3Bv1ww82C0iBY+4+ooPQOv2pUrj7kkLfzNYGe+Mysi+TUrPani5KLj3rULvXt9GOuttc7VWiVPu2s6yfdtPPyAAGQHICED2/wqAjABkBCAjANmgYS6fK6h0t7LsbTG+v5VeUsDoPmWzw4Z9sWLGuHOr0sf/tLvY/8rJVRHXT6+OuvnDupjbVzfH3f11a8ID7gOP+L3utx1JT+7uTH52d2fKC/7+6t6ulNerkkf+Qev7jBDuLQm2vpY4xuj4sF6qs3pqdrFVl+/wAQHICEBGALL/BAAyApARgIwAZAQgIwAZAfDCOrTr0O4zTfkuHWy6de08oqeukp/3MLP4lMl2c+K97BYnTnZcN2+ay66qTI8jlTMnXkjy6Xd9Q47nw9wpQ58nuJocce+rPsNcvVMvDfn2rQhARgBMsoPs3wbg7/85+jeAvwH8DeD/avwXxG4kD5Cwxp0AAAAASUVORK5CYII=', '2016-09-05 17:01:37'),
(67, 18, 268, 1337, 'BungeeCord 1.8.x, 1.9.x, 1.10.x', '0', '2016-09-05 17:01:38'),
(68, 19, 193, 400, 'BungeeCord 1.7.x, 1.8.x, 1.9.x, 1.10.x', '0', '2016-09-05 17:01:38'),
(69, 20, 76, 200, 'BungeeCord 1.8.x, 1.9.x, 1.10.x', '0', '2016-09-05 17:01:38'),
(70, 21, 120, 400, 'HexaCord 1.7.x, 1.8.x, 1.9.x, 1.10.x', '0', '2016-09-05 17:01:38'),
(71, 22, 246, 999, 'BungeeCord 1.7.x, 1.8.x, 1.9.x, 1.10.x', '0', '2016-09-05 17:01:39'),
(72, 23, 118, 250, 'BungeeCord 1.8.x, 1.9.x, 1.10.x', '0', '2016-09-05 17:01:39'),
(73, 24, 148, 1337, 'BungeeCord 1.8.x, 1.9.x, 1.10.x', '0', '2016-09-05 17:01:39');
INSERT INTO `statistics` (`id`, `server_id`, `players`, `max_players`, `motd`, `favicon`, `updated_at`) VALUES
(74, 25, 153, 400, '', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAwsklEQVR42pWaBVRWWRf3n7FFsLuDEgNUQEUBkUYpAek0QUpBBES6u0xMUMTCDiwUu2PUMSbsGBsL1Jnfd+7jyBrf7/3etT7X2p579t3n3LP/Z+ezkOkbW2Bj78QkFzcmTLTG3dqNeI8lrMm8iZdtMbpGUxiga4Wl8wKsPQqZ4laOv8t+QlyrifA4Q3LwBbKjj5EVe4iCtKNkZZ0jP/8aeQW3xXib7MKTpCZsJjNqPRkRZeREbCJ37l6K5lexNPEwJRnVLEsUFHuUpdG7WRazlWXzt7Fs7k5WC5nyhMNsTjnH1vQL7MiW6CJbUq9TtuAi+bNuEO58HFeLMkz089EaPo/uA70YNSSImQa5rJq6l0OxJzgRd5SLKUe5IOhYzH5KPFczSTMQLQ1PZIN0J6A7zpOxpg5M8ZlMSlA4J1bsY1NhFTkFxzCx9sfaJoyczFP8WvGRlUGHmGGejMuY6diOtMd8uBn6g/TQUR3OwL4D6delO30790K7rxZWGuZ46nowyyCISNMYEqxSybTOIdumgKyJOWRZZpNtlUOmpXi2yibNMoMEsxTmj48lZOwcpo70x0nLFQMVI5S7KNOhTVs6d2yLWt/ejFDTwGDIaOx0JuE/fh5xdosp8t5NeeAtDoQ+Ye7YtSy3288Oj2McCjvD0ZhjnF1wiJ9Ta9gStJFQ62jG6c1A1kvbjuEm0xhi6IDTJG+SI2I5c+go5UvKiF2xC2PPZJycC3m4/jPWY2ywM7LAaqwRJrp6GAzTRWOAKm3bdUS52xBcRk4h3Xkh5bN2Uhm6h02zdrB5eiUbfDeyzmMdpS5rWOm0gpJJS1lqu4iFNgspmlhEsXUxxeJ5kVCixHE5q11Ws95rnVi3gU3Tt7DJfxsVM7dTInjzzBMYr25Jl/bd6Na1E1pq6owZqinOMhyTUaOxMjDEfaIbF6PfMc94HQXWmzgw5TCvj7yjvPgwpZllJM1IwsnEFX19G2Qj3cKYHJyB3eR5TLSdRVBoJkmZxSTnFeKetRC9GaUkx1xmTdh6PGwccDC2xHTkaAYpK9NcQUHcuBZRFqms8t3MqumbWD61ggKPlSTa5RNiHIeXXgiTRkzBcog7ZoMmYzzQASMVWwxVbDBQtsZYjOPF3FjVTjzbY6rmgO0QDzx0/JktLCHFJpdFLitY4b2WlT7lrPLZwGqfLSz33EiAfgS9OqjQUlERtX59GaetzUQjIxytrDiccBVf0zK8hStEjyzgesbPaA/yQM/MBxtzG6wnWDLWxAxZUGwIS1PSOXfiKnEZG3APK8IvrwjvwnyMI9MwCd/HyXWfmesWgb2ZOeNH6dKtW0daK/XCWTeUxa5rWexRRrH7Gha5rybHaQkJE/OIN88k1jSVuPHJJBunkWaSQapxBonjkpivP595YyKYqzeXeXrhRI+JZIHBfJLHJZBlmkmuRR6Z5tksME4kfFwM4SaJpFgLno2wFscSFgsLWeRaSrGwqMLJa/DSDUGl6zi69+jKyGGDsDDSZ1X4RqI9jmCpHUfA0FSuJV9Dp58bw0d5McFhBk6Ojji4uiFbMC+Dxz/f5taR0+w+eByP7B245CzBK78Yz7jFpK64zJOaj0wV7jFOT5tevXuh1c+WCOGzi91KKXJdw0L3UjkIJR5rKXEuY7mDuDHbJSyxKiTXJJN4wzjmCEVn6s7Cb8Q0PLV8cBvqweShbjgNdsV5iKuYe+M7fKqQCSRcgBNnFEe2AHGxdSErHZaxynkliycvJ2NiPjEmKaRY5ZJjv5TsScvIsl9GmHEyRhq+9OrXHd0RA4n0jGFJwC3Ga8/HW4BQNHktplqzMNKZxUyXYlbFlzLRwg5Z5NRFLA5YQfWaIxw8fpHZFdUEZG4mqWw/hUv2cuTAA64duIO9uSXDNHWw051DprjlRe5lFDivpshN3LxbmbiVNSx1WslS60UUW+QQpx+Fv8507AfaMbKnLsod+tNJsR0KzZvTsnljWrZsjIJCIxr/JKNJExlN/6HmTRvRprkS/dsrM7aPAd7DfYg0iCBvQpYAYQWr3deySHxnvgAh3DCeRMs80kQ8SRZxJFoE0Ul6UShr9MJlwmT2xP6J4egonI1jmTRmLha6QRgNnI6fXhL5czbgYOmCzMY1nXlTyrhy4jXlG/eRdWAf4bmVLF69k+pTt3n96W92LBWmZGTPDKsElk4plZt7pv1iskQwy3cqodBxGYU2xRRYZBNrEInfcC9Gdtehi2JrOir9RJe2TenUthmDerRmjEon1Lu3QaWLEn3bNMJ8gheTfeNw8olhkmekoAiGaI2ge/smtGvTmCbNZLRX6Ii5iiVzDeZQbJvHamF1JS6lws0yCNZfQJRJKskTCsmYtJAkp1xmiHPojhrO2axXTLLJY9Q4P4aNcMVMcybFHhV4a85jishGU6dEIlOfGE1QcAXlC/eTW7SEospiFu48wZHLP3Pm8i1O1zzi6sq7LJ5RxZrZ5Sz0XUOSda6gfNLtFwozXETuhFzh3/EEaPuh32M0Co1kdFaUCSUV6dehFSP7tsNhSDdmjFHGsI8S6u2bot5JAbUOzQmMyiNh8SbiF5aTsHA98zJWYmvjiLqSDFPlNpiqtsVQrT29OjWlbYt2OA2ZRKJIqSsE8EsnryBODsJ8IowS5QAs8ltDts8iZk8qZvuCK9haZjFyvAujDf2ZaZzCeVFTZJouZXPGRfzCCpApaHujMsqTcTa+WLm4EJ48hejFyyhaV0rlvpNcvvqM56UfWB+9QdQA5STa5xA7QWQKKY/bFpFtmUmiwTy8Bzswors2Yw3M0Rtrgo72OFQ6Nhd5vCfWGl1w0uyO6YDWZKYkc/DIcXbsF3TwFDUnz/LwySMePn7M73fvs/PQcYKjM5gaFImHbwBeU0QdoqOO58heqPZQpHFTGTo9hxFpNFukzCUUikuINk3CXSdQZI0YFvqsZnXIBpYHb+B6Th2eTnmYWvkwyTScytRzHEo+yZ7Q/ZTGHMZuWjYyfadAbj+4Sc2ZGuanL2DYhNGMCg7GNyeewQGBxC8/yY0lt9goKrRU11xibNKJm5hJhjDFHIt0kvTD8Rxoy4heI3ByDcDewR83r3ARYCYwskcr7Id2w0WQ37BuTOirSNWeXbyofcubj+95/f4dbwW9+/CBus+feS/G17WvefzsGfcfP+XA8XMsyFiMzkAVpo/tg51Wd4b2aUvzFjIGdlImQn+WqCVEdrDKFCDEM7C3OYm2mawJ2cjm6B0cy7hGblg17p6J+IuLu7XxPiXTq7hd9gtTjVLQtpqFLD5/Od/+fZH/f+/lXbSmhNLScTLNnbzZeOQix9POUx5ewZgRjoRbxpJmky3MPoN0w7l4DbJGt88InN38sbP1xd0jiBkhSZibWIjg14qQkb0JFDRnTD/8xmpw9NRpHr98ybPXr3nx5i21Qun6L5+pq68XYLzn+etXvP/0SQ7M69o33L7/AAM9Aa6eKqPVumCh0Rnlroq0aC5jrAiucQbBIvaINGueiKv2NHQ07Mn3WkhF1DYq445xbvUjUeHmkCpc92rxDVImrOKPrbdxMQxk+ISpyDZuWsYn7vDm+UPq6v4UEHxm34X9dLG252HdVTkoO+cdINA5Bn1dV9Lss8ixziDPdAHThzqgLQohe0dfHCf54uEWiN+UKNJzV7EoLxfzrjK8VBXwUldkuk53Zlrr8/vTP3leWyusoJbajx/58vUrn79+4ZMA4MOnjwKMLwKMOl6+ecOTF895++kDRemxLLDUwkVXGU3hBn1FfOndU1SfmqOw7mtAtiiFCyxSSLdIYKSGNSY6bhTNXMrWuOOcW/IKU5NwFniuJH9yCVtSqtmbsYPJttMx81uArDgqjg+vHlD7+j6vH9/n6qlrbFizjpUlJVz9/QLUQlFAKY7WAaIU9qbAMU+Ur6kk6Plj1GMkZuMm4u46C2/3QKaJqLpxw05ev33J7ZvXuHL5PJVlK1i/YhHbtmzg5u+/8eDpM95+/MCHujqh/F/fABBK1wsX+PrXX3wV87q6el6+fcuLt2/kdO/RA67+fIET1XspzM9nqp8/jo4eGFpY0rOTKnO0nVlolkCeZSJB+gGM1pmMvbEvayK2sTf+EUNGBjNmVDAexvNYk7SPtISNzAhMwm5WDrLYaQt4c/cZL/94SO2z55zZdYx1xasJmxHN+RNn4QVE+uRibzWdGabBLHEpYKH40IyhdsLvtfDzCcXPMwT/6fNZX76N12+e8+TpI2Her+Xm/VHc7HPh85K/vxM3LJl33XdlBX2WABDzv+TKS4B8kctJayV6/a6WdwKwT5+/8EFYhuQuNSdOMzskFvtJnugYjGdcVy1yxoWJcyVTYJeE3lArrExE/eCVzf6kZwwxmI3ehPm4uQvwfIrxi96ImUsImjaByKqXj6E0LotFieuoXr+PC1WnWJy4mhlesZzYVQNvIMqzkPEG9hR5FojUk0++iegbeo/BRG+CUD4Y/2mRpCQVc+/+XUH3ePLnM7kPS8pINyjd5ut37+Q+Likv3bykvHTrX758lSv/11/fLOHDpzoRFN8L9/ggdxEJDGmfDwK4D4L358sXvBHgLl9WhqfLdMwn2qHeS4P5outcIlxgmWMWXvo+jNd3JnRyKqcWv8HaZxkmPoVY+xVg4xWDhX8SE119GOsSjGyqfjvRvAxizXQzFvgEEDE1mbTIElyEz6/PruD9iafMdstg5qRQVs1cQYFtMukGgQxrN5BpPrPxcw9mqm84+/Yd5vovN/nt7j3hv2/lvvz4xUsevnjBk5ev+fP1G6HYR2ERdcLHPwsXqOeLuNWvf/9j9sJS3gmFJZDeCLBeCar9+EkOlCQruczD5y+49/Axv/5xl+rqGvw8AnCwc0Nr+ChcBxixyHw+q8UFZTqnYDjSmrnO2Zxe9xLnmeUY+qQzenIoFl6zcU3MxN5zOpNCY5ENUuiCptIAurbsiE6vNiKdKDNx+EC0NDTxHaHFndhLhPtmsDJM1OJ+RWSYRxKr58uwbpqEhWWKCBtJaGgKV67f4uqN2zwUbvRWmO6TV6+5//wl9/+UAHjFq1qhkKSclPqkyC+3hG8BsFYAIyn8QvJ7EfyeibWv3r2XK/5WWEStoKeva7kn9vrt4ROu3BAF2tmLzJw6F0eHKdjYuzOu23AKTeZSYp/KIu98ZjvOY+rE+fxS+Z7xLgsZZj1bXnH6+acTml0mut5sZqbnI7Nr0gQDWVNmt+nK7O4q2LTshIrsJ1RErh0m68+1iOeUxW4U3dUaUhwWkG4WzhwdD0z69SdikiFT7SYyxc2LnIwsdu/azcGjxzlw6iy7jp+h6oxIoVevc+nOXW48eMKdx38KQESef/VWWMQ7uVISSPcEaL8+esqtB4+4df+ReH7CPREs7z55xu+PHnPjj3ucEUpXn7/I1qoDrClbS256Cq4TJ+Bhpk+ojS6mvdTINQ6n2CqGnMnJFAYsImfGSi4urcVm8hJ8PdJImFkiAvwJzpy9y/Uj11i1bx+y/AEDKWzbjVhZY/JlMmbq6DG+Zh+zVy5m9v4qfq15xtb4XSR7JBBjFUaGRQTTNV1wFlaS6mHCkpnmXF3my9k8L9YFmLIhwprVwVZke+uzPMKO/asi2LUykq0rY6lcnczO9dns2ZTH7nUZbFsTz7bVcWwVJD1vXRUrn28vjWfPuiT2rU9h79pE9pbGsl/Q0bIozpbO5srq6dwrn0ZNmq2o/ceS42mA9QBlMozmUGgZTZxFEImeKayOquSPtR9wc1vJjrLTvHhYz6M/P3Lr9GMQPc7RK9eQVekYUTHMiH2a+qxtrECTaYGoFuWhvWklowpW8+v2D2T7Z6M/cjyzTWeK0jcat8F2+BgMJcHbgpocL+6tmsaEnopo92yNVnclJgzqRKHbCKrDxrMr0IC3h3Ph7jb4ZaOgLXB7K9wR8zvb4Vcx/rr92/y2oFvi/S+b4YaQvbYerpbDlbVwuRQurIBzi+BEOhwV2WubP3m+40nzNMJaVZkEUdwsso7Da9QkJujbkR+0ilvlbwmO2E5q2hYSEsso2nqW7RvP8aX+KxV7jiILUtEiRHUUSeqj2dh3CG2cxe3evUPnmYFMqNrPvppa5rlHoT/KSLSboWRZRWGrOoFpltokT7Ph1tpQ1s6zIS3cm1W5c1mcHkrkLBf0NJVR76xAgo0Gu2bocDR7Kvyxk6/Xt/D1xuZv4/XN/6JNfLkm6OcNfL5aQf2V9dRfXseXS2X8JSl/TYBwdTV/nSnmy/FMuJgjQIhmwzxrkryMsR2iTPSY6SyxF22yyXSsDG2ZZhvJo1PvSC7aQ2hiCam7qsmv+YWFy3Zx/d5L8ldtQ7Zf34qTFvb8bu7J0m4D6CjcoLnKEDrPCmDsrkpmaiSTGVzMJCNvFtiEk2oZiXFfY4ImjSU90JFfN0Vyf086FyvTOSZM9ogw2cOlcexZESNA8aRvJyXMBndljdcwtoVPEDctWcB2oWgFX65u+Kaw9PwPfRaKf75czlfp5q+V8+X8Um6uDGRflCXLPYeLvn8gQQb9CBrTi5slUzi/0JtEYYkuowRfx5uljpkUT85k3CgzQp2S+OPXt0SLoJe7cw+5F68RuLKMmKVbOfv+bwLmr0BWoGvCYcsJxPUdxvjGrZE16k5zHQP6Z2cxYE0JMwwLyQpaQpBdMvG2EcSazkGvx1giPE3In+vC492JPNufw/UdWZzbksHPYnx0II93NcW8PVrIsbI4xo9QR7WbEsFmamRNHsqhXA+5addfKBVUxueL3+ib2VfAzQpe7k3hUr47a72HkzRBA4fhPRnWpx3aat2wHKOBm/kwYu2H8aQyhOQpVkwx08RrqJOoU7JZ5lGAl2kgMVNyuXTuPksr97Dk8lUy95aQvzObqRt2E76uivjcrchGqavhot2f8T170L5bbxw0vOnbaiSNVIbRoocmWZaVJAWsI8IpkVT7BYQazUS3xwhiAyZSmuxLbXWmXNE3R4p4X50tzh7F/vxZpPuZkTXTkpXzJrM6eRoxMxwYPrAbgd7mvKhZwpczS6k/vYy6MyVy4upaFgUYcnlJKBuCjIUS2gQaDkCnXwcUWzZhaN/uhE2ZRHn6dC5viOXilnieH0rjz+1hpPlbM8dFD1t1CxaKQqjEZyHzHfOJm7GCmzfusfHaJbKr17PyYCq5J1aTeXo/aaWVxKasQqZr6cSo8dr4Oc9kgfCZBKtp2A53onUffbp3HEaZz1HyA/cQMimI3MlpeI/yYZz6UBJDHahZHsLnk/lwZhGnRZS2dp+BjiiYBpl6IROu9J+0PHEaz44K5U8tpv7EYj6fWipomXzkwnK25ocybmAHPA1VUe3ZlsZNGv2w3thzAWMdg3B08eLw4iCe7o3n/cE4CsIciA+wZLyqDhk2iZSJ3iVGpMKyeWe5/edTFp7aw1JhkYtuVbL2+RnKj28nvmQL0RlFyPScffELzqBoXhn2Fv64j7bE32ycaEHtcZ+Qz5agG2SIfDrfNYZslwwsBzvgbjacdGH+d3fE8depAr4ey8bGbw4eCavIrKxhRc1JwhbnyjvJ3sp9admiuejhFbAbpSIivDD7k4uoP7VEWIBkBSXieRlfTy/h6ZHFeDkY0a6dEh3ad6Zx4yZ0GdCPU3duYzndh9TNx5ietQnL0CLcvbx4u28+fx1PozzFl9TZLvIfTkLGzWF98HoKpi1nYcBmLj5/RO653cRc2UHysiLy5kSQtG0POfdekSJafVnBb3eYV55JWIwjM6OLGDNyivAfa4xH6mBhF8+hhc8ImhxN9tRc4h0S6NleBVP9IZSmTeWdMP/Px/P5cjSDqKJSDt5+xGuh9MvXL4iJn46FpzP9e3Wlf9eOqPTqhaWuAOB6KXUnFlEnzL/+zHI5SS4ggfFZgHBzVw79urRFvU9P1ETLO1hDheDESOILEuWAnn7wJ8sPXCEzJ5MP+2P5+0QWJ1fNJTdyKsPUejBW1Zxl09awInQj4dbFnHnznoiNi8SF5TFrghc+GlMYNmI44z18mPP7c2RpV0+RKjZe/uZXoratxn7uVHSMrHH2tcAjMoGaNbcJ94xjof8SAixmy03RykBbmP983tfkUF+TKxTKo/LAER59+sJfYq8PdZ9EQ9KPYs/RmA7szpgBHZk8RoUJugNEoCvj0ylJ+RXUn11J/blV8lECQbKCegGO+ZCuGGt0ZXD3NlgN7sXaWcbMcjORA/BJ0I3ntVRtFxmiOkVYXx4PdqVTFCP6E3VllBQ6iuIon3Vzd5DkvZYDq34hNWQh1loz6dlZDa12Guh3NECvtxWGQ3WRha/KZNrObeS8vs5mPrOi7hUhG5ZjmSLy+82LlCfvJdovjbypRYzXskXWWIatkTZXK9J5czCLj0ezqTuWz63b13n1+Stv//l9acG0SYSN6cq+4FFsnTaCOaZ9sdfsJFxgnVB2OZ/PruLLudUiza3msxjrzq34FgvOlTC2208Uug5j/2xDtgWNZaZeD9KTY/kq9n31t7Aw0SNcPrJVmH8On44X8+JgPqXpEQxVUZFfkKdBCKUhleTN2sSe+NvMNVmIWRsfPBr7MbedE+5NJ9CsVQcRlFWRTS9MI7F8FWtvX2bZnRoWvfuTwAtXGSpy5bFbH0iduYwU/0KinRMZ2HcUMtE7OJmM4LetWbw9kMGnwxnUV6dz8/xRXr19zZsPb3j77hV/CzDv/nKeA5tXsaGkgMq1Kzi5SxQ0Z5ZQd1YoK25eUvzz+TVyqj+3Uh4X6k4upKZqK+uW5LBjZRHH927kyf3b1Ne95aXw5xfPn/D86SOuV63hq7C+TzWFfKguZF9RHMo9e6OgIKrRfqaiPF7G0pAtVIRdIVdnOwPaGNLqp84CoGb81LQ5nVt2YHAHAUBqeTnrd1cJEz7K5lP7WXC4Ep9tFRgkL+HAhsekhy4jYUo2vuaB9O0hTKbpTzgYDObpzjw+VKXx6UAqnw9lc29bKreO7uDBL1d4/ugPXjy5x+uXT3n75jkvXjzl7q+3+HlLGl8FAJ8kn5cDIOj8NxDqJDcQFvD1ZDHnqip4/PghL8W6l0LhP5/eFyD8xqPfb3H7wimu7innaeUC6kQarDuQRr2wxHNLEunVqSstFFrRv9sYQi1iKAncyOq5J6meeINhivb0a6ZOi2ZtcW3kxLAWwwQAfZFllu9i4fI9nK75g1MnLpGzrxKvLRWMi15O9eo/yJqzgmCnGMYMNkdL3VIUSiIG6KiICFzA+90pfNyVyEcx1gswvojD1O5O4k9RGT6pyuPJviwe7k7n/rYkXu5J4q+TIv1JJCJ/3VnJDVZ+A0GKBVJAlDKDSI9fjhXwbG8m93dn8mBfLg/35fF4ZxrPtibwflc8Xw9I30ulbncyH8W+n3YlcXt5Eh3E7Tdt3gxtZSsRR5zI815KaXg1ewPu4KI4i2mKnnRr34VuLbswU+ZH5+bdkblGrSYtYw8XDj/h9NGfSd26Fcd16xk3r5xTK++SFrwUb9EDDOw3Gk31CXIfMxrUnY9VhbzdJg4k6IOgj9sT+bAjmU+7xcGqpFvJoe5gNvVH8qk/Vkz9cRH5jy2k7rigk8IKxG1LmeAblXybS9lByNULmXrh259F7q6vKaKuukDsJ2JNlXC5PcLqdiTxcVuc+G68fHy3JY57yxJp/FMTFFu1wXSoM4N6jSbaJo41c3dzNOgZfTtao6CkjLFsggCpFRNkVhg3Go/MctpiooI2cnb3H+xdf5j5FZVYFJfilraHWxsfETMtn4ljvVDrPxYNZTN+aqJIv44K4tYLeL05jveV8XyoFECIyuzDNmENO1L4sDtNACRSZHWe6NwWwlmh3DGhiPBXOYmg+elY4TeeUFQKZHWSomL+SYD06Zg0L+aj4H04KngiyH2sypaD+2mb9L0FfNg8X1CM+G4Mb9fP527RAvnldGnbD6OB1gzqOZqpRgEsCVzPibCXuHSNYajydIZ3H0G3Zt1o2bQ9rcUoG+8cR7R3MTcO3qVi+XrSDxzEd9Uu/BIOcqviKeE+WYzQMGCouhkD+hjQuaMOKh378mpbFs83LqBWoP9+WzLvJQvYkyriQgYfD2TzWdz8jQ1xTHPUY12Cj4juxSKJF4GoG/4SwevLEck6xK2K8rlOGo+KdHpU3LQAQaLPx78B8UkColoAdiBTmLvYf6f4zhaRgjfOExTFuw1RPF8dwaWUOSg16sKgHvqMUTVDp/8YTLWsSZ+ymCOhjzDpMg+drgFo68xirMo4NNsIINr0R2ZoP5MUv0Je3nvL5h1bid93nBFT09BzXMatsj+Z451Bv5690BCL+vceS9cuevRpPYinm5J4timBV5vF7W9PEb6ZIlf+gwiIH6u/RedaYc53KmLlNxOWlE5BQhCV2TM4v3QWdzeEC0uZz5eDCfxdLSqRYxmCskRkz5LXFn+K+FErTF8C4P0RMQoX+FiVLvf591tj+LApSg7Aq7VzebQsgl2hXnRWGIRa99GMVB7P8H7aDOyjTrRrFjVRd3EYmESfNmb06m2Pum4A4zVtMOilj8zdK4F04ef37j5lQWohY2bm0tPEDxv35dwpf46/8wL69u4mXECPfr316dZ5FEotNLi0MIzXovN7UZnMGxGgavem8/5AlgAgl/fiJt+L2/soRn5eiZlGLzIWreLQyUus2XGEmOL1zIgtxDs8Fa/QeHzC4pgRmYR/TCpB6UuYMTeCioJQ3hzOEwAUiX2EK1Tn8OGA+MYeAfjOWGoro3mzPpIX6yJ5JfqQRZ6WIggORqWLNroDjBjcZzA9u3Ug0D6RI1l3me2RhY6mk2j4RtBDVLs+ftmUuEcgm2Qbho9dsEAqDNepKWjZzUfVyAvvKWvFLb3C3Xom6mrK9O41mAG9DenaSQdFxWGE21vxaHMxL3dl82ZvDrX783l3uFDcVtE/lC+6wzw5CH+dKGBR2jy2HTrNw+ffS6Uf/0kV5McP77l59ijvq5L5ekL4/tF8oXiefJ/3h3N4tz+dd3uTqd2ZIAJwHG/lcSeZv3fmYDdIi65tNBnQZTgjVYzp37M/vXp1wM00iMvbHpK5eDmBYf4YObgw0XQGhnHbmfn7I2QDtcxxHOtJmnMsIZ4FWJguwMEuhSDfrfxe/gpHC3cGDx5Ip46dUe1rIixgJB3aaqHUSoNgazeurMrl3cGl4oDF1B4ooPaQAEJkgHfCZKVROvgH4RII0/6jMo6zVeWcP3uKX27d4tdff+fCzzc4deESx48d4cjOcq6Vx4hY8F1pYU2Hs6mV7yVcYr9ovfdmCCsQxdd+4Sp7s/llYSJhxnYoNRsoUtwQVLqPRF/Dik6dW9Ovf1ds9F24evwp8+/cJ/7qEZJEYxR36TIu5x5hdPUTsn7Ct71HO7I5tYJjh+4TaZrDVIN0ioPOcHvLS2xMHNAcPJi2HVuh3MeQvt3G0bPTWBRa9hK+3ZLGsjY465mwOV6Y4+5FItCJ3v7UQv4Wkf7zkVzqhTKfa/LEKAKfKJslP+e4oJpMnohu8ubaOTzYukBUk4J3TOotRMATVlMnRum5XuxRJ8y/7ojYR/C/iNjye3kSxTP9MFQeI77fVpxDgbaKAxjQbSya/cYzqI82nbsq0W9AVyzGWHPuwBt8d9wi79xp1lyoYkX1LuLyV+EZtByZ7hAz0eVNpTBkERe2PGT95HUcSf6Dy5vf8suax5gZWjJ0yBB69OiColIb8aHeohj6iUZNm4i+4Kd/9es/8ZMoM7V6dWe6mTZF0004muPDxZJAzi4O5FJJKHfWRXG7PIoHInM8FlnjiaD725O5uSme3yqT+E2Y9B2RWa6URnJ0cQg70qezI3cmy2LcmDt5LC56QxnQoaMceJmsVcO3f2rciEZNGtGkSTP6dFKjbTsFuvZoS1/lrowfNZ7TW94zd2E1abuusmX/Pm78fJGy9MVcOfsGmZ2hMwucvEjzSGWnz05+ybvHxaxbAuEqdsfex0BvLIMHDaJnrx6079CWFq1aiFrg24dbyTqhp+1KG8UuNPtJkf4dR6Haz5xuKmPoPVKYodqI//rDyP8v9dW3o+9oa9r1HSrKXCMGtNeT84f1dRIBNhIF6ac8MW/Rshlt2irQsbMSXQQAfZQ7M2rEaC5VviE2dRuL1x5hy87TvHzwkM2x5ayMWCcKIX0zpgsz35V6knPhN7g4/2dWuO7ETqeEbdHv0RkxEo2BA+nVo4dA9hvqur2dKPD8VTQskJhwiKlORVhYhTHS2pvhNu6MtPNFw9BaLtuoaTOaiMqrcXMFGrdQoEmLVjRpKZEiTRSUBP1rlN61UKRpS0m+lXwua9xEbl26dn7o2PigY+uJlpk9BmOnEDhpG/Om3ibK6zHzbFbSrElzfmomo0NHRbqIVrp3/05YjnfkwXaoKr3DisyDrCs+z8NHb9k+bxvrsrYiszA0JMoukg1Op7k4/TdOzL3Oz+tEE3L5Dc+v1TJs2Aj69++HYrumaPawZO2MJ2xMuUxBai5ZuXEkZmXhFxqFjU8w5q4BmEyegaVHEMqaI7/dimhOmrdUoJlQqoGEws1b/UOKrUVrqiR4inJ+0+9ACLmmAiRJXtpnkKHI204zMHaega13MNNCIklIT2PjphVc3HOUMyt/57qos8KtVghLaI5i+5/o0qsVObOXUbJuI2d2n6P25Fvuln7gwbG/OL35GtU7TiMzMRiHm0EsNzLe8uXXWj7/9UqkpGe8q7/Jki2bWRe7H7U+I4hz3MKB4lvs276BP+4c58Xja7x5eoO3gp7fv8L1S8eo2redNWVlLC8tx9BykvzgzVq0RKGVIkqKSiJ9KtFSPEtzBfHcSsQUpTZt5aTYWoytxVyMrcW8fbt2gt9Gvl7aZ/PWjZw4foibV0/y4uE1Pr78lY+vfqP2+W3RfV7lnmjlz5/eyuENh6nOfEWobT7aqkacKP+N4m1lQqenfKx/yPMnt3ly/jZvj35g2ewyZDpDjdiT8Sv8Lf11SK30e46g13z5co8VG1dRsKaCW/vfcfHcMV69usmrP3/j5ZM7/9AtXj76hefiQK8fXaNeHOjapRpCwmfTX0WNzu070EkELSUlJXErLenYRhGVrp3oL6fOKPfoimqP7qj27IaKeJZoQPfOdBOxprVY06ZtO1F5dqVj2/Z4e7gI8Muof3OX+ncPqX1xV9A9al894K14fvX0V57cvcK9385w9+5xTlYd4dqB1wRnp3L7wVnphzpB7//5U6B66uvuidhwG9kYDX9eH5FevvpHeUnoBfXvBbIPzpG2tJDN5Ud5+ew2L57d5c3z++KD96l9+VA+vnn+O/W197h86TgT7Oxo166T6B0GoammRk9xc5pNZfRXaEHPtm3o0K69UKYNXdoooSxAUBUKqwvl1Xp0Q1ko3kFJUdQY7USsaU8npdb0at4UdbFH6yZNGDZEk249+tGj3wDSU2P58vEJn98/48Pbp7x//Zja1w95IwB5/vQ3Hj+8wf2b19i15Szlu9bw95c7cp3g3T86fuFvcclvz75AFmtfxcnyfayL2sCiKctJtcsj2iSJaNNk0ifnUThtGfleq7l1/hIHVoggMm8j6yI3Uh61SawRNG8DaU6FBGtFM0c7Bjd9e2aNDmb2iPkED55HypBIEofMI0o8zxUULOaBgtS7dSZUK0pQtJyCNaMIGholXzNn0DwixBik7IOVADBwcCRzdBYwd5RornSDaNe5N31U1LlwrprHt27Lz1IeKc4i6OzOU9y++DMrQ8pFoF7LwpnLyAhLxcffibkhs8h0yWVZ0Ao2xFXy4ORNZOvCL1BdsZttdlWc9b70X2mFaQU3Tl789oH/h8x3Ob/xrkSMjv2fckm6hQKATvLxf8llaUayun3zH+Tm6cVjPm4cAwcPR6lrL2p2H5Z/V3on6XBswzFunLryA89Nezp2LtYEus9s4EvjrfMiCG6N/J0Sz3IqbfdyN/C+nC74XuXatF8a5stN1nP73FVx25s45XWhgf+fJMn5m3sQqRf3g5z0fHvmbw1zSaEuigryscb9NGFacUQOS5E/x2jGkjkiRb4mTyuS/d1byeW+7xMzLhEnawt8ne1QHaiJr8sc+Xel95IONRU13DhxpYF3YHINM3TDiPUPJD1obgNfGndGb0e2IWgPeW45bLHZzW8Bv3PE9SRp5tkkG+VyZco1Oa/EpJy7l66zNkJEYo+zDXIS/z9pjqgBovXjG+QqJmwnf2gk6dqibp9xS86TFGongpw0Smv0uhsyaYC7/Dmx2xhWq7rJz1MoWUDHZnI5aZ20Z6JZMrM8HQif6oKthQmmOi7yddJ7aU31umqu1lxq4ElU5XSEeLNU0gMjGvjSWBGwDFnKlAWscalgk/VObooDLjNeR1FwDOkesT/wfj9/ldK5FRx1O9nAy80sxUjDmpBJ0wi1n0qY4xRmTXZgvgDgu5x0+G6tlQgRfv9vnq3wbWk85FxDrLj1SOHz3r2dmaMZR5SwBklWihUtRVCU5KR1Ek8CICXUmwAPe3ycrDHXdZafRXovnbdqZRXn9p9u4H1fF2uaQsqMuQ18adw4pQLZFE9vCuwL2CBu6trUaywdv5ac6RGkusT8wLt95gJr5pRz2KWmgec7OQSToXbEegcx13k6sx2mEOLuxIJxCQ1y0uENNAcRLgLkv3lV//i2NC+z2EzZAHsCWzbCd1Aguybtl8uGj4zFfOyoBjmJF2ucSI5vFCGmwcy3m4u+sqX8LNJ76by7luzkyPYjDTyJLvleJmF0HgnGGex3qm44f7ZTOrKE0DCm6QVSblUpBC+y2KiURM8QEh2jfuDdPnWOsrD1wqcOy3l7HQ7I+f9Jofa+xBknNchJhzfRGUGEUObfvP2dW8rH7/MdYh7Z8icM+pnK95FkI0bHYTp2dIOcxJtvmEC8aZL8bJLcRF0n+Si9l3hL4kUzt626gfedznqd5YgA8Ptcej/NcqYAIDCUWWahrLUUEd7rNDvt9xJnmEqCfhZHXY/KeYuMVnNq9zFyPEuE4vvlvP9GktwMK095oPout9qsgpBh0cSKGHDC/bicJym05x8AvsusEH4vza1FLJDOIq0PGTafoYMGNshJ6xNG55IwKlf+LJ01flyafJTeS+tmTUwkLqpUfpb/1zm/n3WO21xkkTP8iZkUyRrzDRx3PyanvY5VAu2DDfPicasIcI5itnacXO7ftNN+zw9y/ja+hI2K/YEvPR9xPdIwl/v+P9Fdmh90PiTf69+jtCZQ1ANKLRUa5CSS9vn3XtJZvz9La10GzWDiEA/5Wb7z/xtJ7yNcw5GF+Pky33Yeq8zKqXY5JKed9rvYarujgYoMV6DfywLX4dPw0QnCW3cWs8YHE2YZSrS4bUlGWifJ+Vp4ES5M9ztPon/vJZGk0N5/APgu858kyc0XBZReY9n/lPs3STq4afpjquIgP8t3/nqrzXKd/i0rvZ/tEIJsmutk5lqGscKkTASIvSJtbSJ77BLmD88kRCteVGmJRGhnYaMeyBC1UYwfY8FkS2ucbSxxtLFgqn5Aw9pCg+U46E0mXBQrm6y3yHnLjNeQLNJgtsjpuYKyNb/R6n9cQJL5LifNpVGaS+vTxDqPVo1+kPtfJJ3De8Qsxve3kZ9F4q21rMB7YCixunkC1G0NstL7ABt/ZE62E0VEDaVEfHjPpB0U6JfgrObPtKEJTNSJx1gsHtjJCrVO+oSLuXSYf9OS8avYZlvZsNZOABAqytaKCRvkPEkmtmNLUgVldmpJihiT2zUjr9M3ACQZSVZSdmGHFj/w0gVg0R2aN/C+k/RNiSd9r1wo+J0v6eA9PAALtUnyd995niLNRuvkUmq+rkFWej/VajqyCWbmBBgFyzfdbruFPP2lTFL1oWurEfTvOA717qb06TKc9m17E6YdzzqLcrncfyNprbWIyiEi5X2Xkw4a0KYps9s2Y7ZQfHKrxgxq1YJ2Ct9cQJKRZKV6f0TLZj/w0gQACZ0UGnjf+XNERhk0ZDAOY1zk3/z+TtLBU3Mmpsp2DXyJ56gSgFUfTwJFUF1lWtpwVk+TKchMjEyYKtLgYqMVVFpvIFeYv0FPC9GxDaZPx5Gij+9Om+696d57sByAMvMyudx/I2mtpbYjwcPnN8hJh/dSaopP66ZyINSUWtFIsQ0tmn2zAElGkp0lAGjcrtMPvAThKl5i3XfepgnrhSnnCDMPQHXQEMYNMSNCJ0lkkVL5e0kHDy1/jPtZy8/yneeo6k+XtoMZ09OsgS+NToZeyEaOHIu3jv8PZj26t5mowDqj2KkHHXqp0am3Op16qDFneOz/5QL/SSZa9gKAmB94TkpN8GvbnOAOLemt0FL+q09HpbY/yEhVX9P/4C0QbmHYstEPvBDtBSgPHISaAGCA6kDMh9r88N518DTG9bX6gWen5kWT1u3RV534A99mlCuywZq6DBqqjYaGFmoDhwrSom3H7rTu0l/QANp2U6Z9T1Xa91BmcH8tRg/WR3uoHkNEN6Yq+n61oWKN5jD6awxBVXM4fdQ06KkykA59B9C8XRf6iijuIm7erU0zHFo1QUWYeeNWbejcph19O3SSU7+OnVCUfids054RLZowqU0LXNs1x6l1Mzo3a4ZCp+507a9Mf6F0P6G8dPt9VdXpr65BnwEqAgh1VNUH0X+ABn17qtOxfU/a9lAX3eIAWndVpXUPVVp17CPWDEZt8AjUh+igPlQXDa2R/B/TRcoMt6lsmAAAAABJRU5ErkJggg==', '2016-09-05 17:01:39');

-- --------------------------------------------------------

--
-- Структура таблицы `tags`
--

CREATE TABLE `tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8_unicode_ci,
  `meta_keywords` text COLLATE utf8_unicode_ci,
  `h1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seo_text` text COLLATE utf8_unicode_ci,
  `type` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `tags`
--

INSERT INTO `tags` (`id`, `name`, `slug`, `meta_title`, `meta_description`, `meta_keywords`, `h1`, `seo_text`, `type`) VALUES
(31, '1.10.2', '1.10.2', '1.10.2', '1.10.2', '1.10.2', '1.10.2', '1.10.2', 4),
(32, '1.10', '1.10', '1.10', '1.10', '1.10', '1.10', '1.10', 4),
(33, '1.9.4', '1.9.4', '1.9.4', '1.9.4', '1.9.4', '1.9.4', '1.9.4', 4),
(34, '1.9.2', '1.9.2', '1.9.2', '1.9.2', '1.9.2', '1.9.2', '1.9.2', 4),
(35, '1.9', '1.9', '1.9', '1.9', '1.9', '1.9', '1.9', 4),
(36, '1.8.9', '1.8.9', '1.8.9', '1.8.9', '1.8.9', '1.8.9', '1.8.9', 4),
(37, '1.8.8', '1.8.8', '1.8.8', '1.8.8', '1.8.8', '1.8.8', '1.8.8', 4),
(38, '1.8.7', '1.8.7', '1.8.7', '1.8.7', '1.8.7', '1.8.7', '1.8.7', 4),
(39, '1.8.6', '1.8.6', '1.8.6', '1.8.6', '1.8.6', '1.8.6', '1.8.6', 4),
(40, '1.8.5', '1.8.5', '1.8.5', '1.8.5', '1.8.5', '1.8.5', '1.8.5', 4),
(41, '1.8.4', '1.8.4', '1.8.4', '1.8.4', '1.8.4', '1.8.4', '1.8.4', 4),
(42, '1.8.3', '1.8.3', '1.8.3', '1.8.3', '1.8.3', '1.8.3', '1.8.3', 4),
(43, '1.8.2', '1.8.2', '1.8.2', '1.8.2', '1.8.2', '1.8.2', '1.8.2', 4),
(44, '1.8.1', '1.8.1', '1.8.1', '1.8.1', '1.8.1', '1.8.1', '1.8.1', 4),
(45, '1.8', '1.8', '1.8', '1.8', '1.8', '1.8', '1.8', 4),
(46, '1.7.9', '1.7.9', '1.7.9', '1.7.9', '1.7.9', '1.7.9', '1.7.9', 4),
(47, '1.7.8', '1.7.8', '1.7.8', '1.7.8', '1.7.8', '1.7.8', '1.7.8', 4),
(48, '1.7.5', '1.7.5', '1.7.5', '1.7.5', '1.7.5', '1.7.5', '1.7.5', 4),
(49, '1.7.4', '1.7.4', '1.7.4', '1.7.4', '1.7.4', '1.7.4', '1.7.4', 4),
(50, '1.7.2', '1.7.2', '1.7.2', '1.7.2', '1.7.2', '1.7.2', '1.7.2', 4),
(51, '1.7.10', '1.7.10', '1.7.10', '1.7.10', '1.7.10', '1.7.10', '1.7.10', 4),
(52, '1.7', '1.7', '1.7', '1.7', '1.7', '1.7', '1.7', 4),
(53, '1.6.4', '1.6.4', '1.6.4', '1.6.4', '1.6.4', '1.6.4', '1.6.4', 4),
(54, '1.6.2', '1.6.2', '1.6.2', '1.6.2', '1.6.2', '1.6.2', '1.6.2', 4),
(55, '1.6', '1.6', '1.6', '1.6', '1.6', '1.6', '1.6', 4),
(56, '1.5.2', '1.5.2', '1.5.2', '1.5.2', '1.5.2', '1.5.2', '1.5.2', 4),
(57, '1.5', '1.5', '1.5', '1.5', '1.5', '1.5', '1.5', 4),
(58, '1.4.7', '1.4.7', '1.4.7', '1.4.7', '1.4.7', '1.4.7', '1.4.7', 4),
(59, '1.4', '1.4', '1.4', '1.4', '1.4', '1.4', '1.4', 4),
(60, 'Pocket Edition', 'pocket_edition', 'Pocket Edition', 'Pocket Edition', 'Pocket Edition', 'Pocket Edition', 'Pocket Edition', 4),
(61, 'Лаки блоки', 'lucky_block', 'Лаки блоки', 'Лаки блоки', 'Лаки блоки', 'Лаки блоки', 'Лаки блоки', 2),
(62, 'Сплиф арена', 'spleef_arena', 'Сплиф арена', 'Сплиф арена', 'Сплиф арена', 'Сплиф арена', 'Сплиф арена', 2),
(63, 'Моб Арена', 'mob_arena', 'Моб Арена', 'Моб Арена', 'Моб Арена', 'Моб Арена', 'Моб Арена', 2),
(64, 'PaintBall', 'paintball', 'PaintBall', 'PaintBall', 'PaintBall', 'PaintBall', 'PaintBall', 2),
(65, 'Голодные игры', 'hunger_games', 'Голодные игры', 'Голодные игры', 'Голодные игры', 'Голодные игры', 'Голодные игры', 2),
(66, 'Паркур', 'parkur', 'Паркур', 'Паркур', 'Паркур', 'Паркур', 'Паркур', 2),
(67, 'Bed Wars', 'bed_wars', 'Bed Wars', 'Bed Wars', 'Bed Wars', 'Bed Wars', 'Bed Wars', 2),
(68, 'Sky Wars', 'sky_wars', 'Sky Wars', 'Sky Wars', 'Sky Wars', 'Sky Wars', 'Sky Wars', 2),
(69, 'Прятки', 'pryatki', 'Прятки', 'Прятки', 'Прятки', 'Прятки', 'Прятки', 2),
(70, 'TNT Run', 'tnt_run', 'TNT Run', 'TNT Run', 'TNT Run', 'TNT Run', 'TNT Run', 2),
(71, 'Build Battle', 'build_battle', 'Build Battle', 'Build Battle', 'Build Battle', 'Build Battle', 'Build Battle', 2),
(73, 'BuildCraft', 'buildcraft', 'BuildCraft', 'BuildCraft', 'BuildCraft', 'BuildCraft', 'BuildCraft', 3),
(74, 'Rail Craft', 'rail_craft', 'Rail Craft', 'Rail Craft', 'Rail Craft', 'Rail Craft', 'Rail Craft', 3),
(75, 'RedPower', 'redpower', 'RedPower', 'RedPower', 'RedPower', 'RedPower', 'RedPower', 3),
(76, 'Forestry', 'forestry', 'Forestry', 'Forestry', 'Forestry', 'Forestry', 'Forestry', 3),
(77, 'DayZ', 'dayz', 'DayZ', 'DayZ', 'DayZ', 'DayZ', 'DayZ', 3),
(78, 'Terra Firma Cra', 'terrafirmacraft', 'Terra Firma Craft', 'Terra Firma Craft', 'Terra Firma Craft', 'Terra Firma Cra', 'Terra Firma Craft', 3),
(79, 'Millenaire', 'millenaire', 'Millenaire', 'Millenaire', 'Millenaire', 'Millenaire', 'Millenaire', 3),
(80, 'MineZ', 'minez', 'MineZ', 'MineZ', 'MineZ', 'MineZ', 'MineZ', 3),
(81, 'Flan''s', 'flans', 'Flan''s', 'Flan''s', 'Flan''s', 'Flan''s', 'Flan''s', 3),
(82, 'Divine RPG', 'divine_rpg', 'Divine RPG', 'Divine RPG', 'Divine RPG', 'Divine RPG', 'Divine RPG', 3),
(83, 'GregTech', 'gregtech', 'GregTech', 'GregTech', 'GregTech', 'GregTech', 'GregTech', 3),
(84, 'Pixelmon', 'pixelmon', 'Pixelmon', 'Pixelmon', 'Pixelmon', 'Pixelmon', 'Pixelmon', 3),
(85, 'Сумеречный Лес', 'twinlight_forest', 'Сумеречный Лес', 'Сумеречный Лес', 'Сумеречный Лес', 'Сумеречный Лес', 'Сумеречный Лес', 3),
(86, 'Thaumcraft', 'thaumcraft', 'Thaumcraft', 'Thaumcraft', 'Thaumcraft', 'Thaumcraft', 'Thaumcraft', 3),
(87, 'Mo''Creatures', 'mo_creatures', 'Mo''Creatures', 'Mo''Creatures', 'Mo''Creatures', 'Mo''Creatures', 'Mo''Creatures', 3),
(88, 'Выживание', 'smp', 'Выживание', 'Выживание', 'Выживание', 'Выживание', 'Выживание', 1),
(89, 'Креатив', 'creative', 'Креатив', 'Креатив', 'Креатив', 'Креатив', 'Креатив', 1),
(90, 'PVP', 'pvp', 'PVP', 'PVP', 'PVP', 'PVP', 'PVP', 1),
(91, 'Whitelist', 'whitelist', 'Whitelist', 'Whitelist', 'Whitelist', 'Whitelist', 'Whitelist', 1),
(92, 'Пиратские', 'piratskie', 'Пиратские', 'Пиратские ', 'Пиратские', 'Пиратские', 'Пиратские', 1),
(93, 'Лицензия', 'licence', 'Лицензия', 'Лицензия', 'Лицензия', 'Лицензия', 'Лицензия', 1),
(94, 'Кейсы', 'cases', 'Кейсы', 'Кейсы', 'Кейсы', 'Кейсы', 'Кейсы', 1),
(95, 'Кланы', 'clans', 'Кланы', 'Кланы', 'Кланы', 'Кланы', 'Кланы', 1),
(96, 'Оружие', 'oruzhie', 'Оружие', 'Оружие', 'Оружие', 'Оружие', 'Оружие', 1),
(97, 'Херобрин', 'herobrine', 'Херобрин', 'Херобрин', 'Херобрин', 'Херобрин', 'Херобрин', 1),
(98, 'Ивенты', 'events', 'Ивенты', 'Ивенты', 'Ивенты', 'Ивенты', 'Ивенты', 1),
(99, 'RPG', 'rpg', 'RPG', 'RPG', 'RPG', 'RPG', 'RPG', 1),
(100, 'Экономика', 'ekonomika', 'Экономика', 'Экономика', 'Экономика', 'Экономика', 'Экономика', 1),
(101, 'Приват', 'privat', 'Приват', 'Приват', 'Приват', 'Приват', 'Приват', 1),
(102, 'PVE', 'pve', 'PVE', 'PVE', 'PVE', 'PVE', 'PVE', 1),
(103, 'Тюрьма', 'jail', 'Тюрьма', 'Тюрьма', 'Тюрьма', 'Тюрьма', 'Тюрьма', 1),
(104, 'Лаунчер', 'launcher', 'Лаунчер', 'Лаунчер', 'Лаунчер', 'Лаунчер', 'Лаунчер', 1),
(105, 'Дюп', 'dup', 'Дюп', 'Дюп', 'Дюп', 'Дюп', 'Дюп', 1),
(106, '1000 лвл', 'thousand', '1000 лвл', '1000 лвл', '1000 лвл', '1000 лвл', '1000 лвл', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(68) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `website` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `facebook_page` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `google_page` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `youtube_url` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `steam_url` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `twitter_username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `skype_username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `minecraft_username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password_temp` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `role` enum('member','admin') COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `active` enum('0','1') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `email`, `username`, `password`, `website`, `facebook_page`, `google_page`, `youtube_url`, `steam_url`, `twitter_username`, `skype_username`, `minecraft_username`, `password_temp`, `role`, `remember_token`, `code`, `active`, `created_at`, `updated_at`) VALUES
(1, 'demo@demo.com', 'demo', '$2y$10$nm2XAWQ8aP8Xi4pXuazv1e.JdeSnTwqUZ0rTQrXIrVEj9fR4hqDSq', 'http://asdfqr.wef', 'http://asdfqr.wef', 'http://asdfqr.wef', 'http://asdfqr.wef', 'http://asdfqr.wef', 'tooollll', 'tooollll33', 'tooollll', '', 'admin', 'YtSfiTpEbJDbMXReUzKKEobGfYCEkpiHlutCA0n5BOb9Do4B2r3mrknVkLwq', '', '1', '2016-09-05 08:47:42', '2016-09-05 08:47:42'),
(2, 'olegblud@gmail.com', 'olegblud', '$2y$10$I.kOxv21eOSsmaHJNoQ3iOs0Rf2NlX4O0HualeQd1B/t5dkVVSo.2', '', '', '', '', '', '', '', '', '$2y$10$Z/u/6aCKOXxQ4r908ZgaJuiYDUT3e/5dXaG7EYz6onNRQXrsiOttO', 'member', 'S8wM9gC9IIhmSx9sLkLx7hsTGBqPvXtpADEYZ2YlUozi3lmEovqipyLFORid', 'EB6vOVKcQbGoR6De0BVPIefNofw17fGWwYfMLMFOu1cps6aIrXwFahy0tmTG', '1', '2016-07-12 15:46:30', '2016-07-12 12:46:30'),
(3, 'fszd@adsf.df', 'asdfghdgfs', '$2y$10$LjlNuXIw1CJGCncpDYTeeea1XZbWtL7WGUSTMhQGAX4KPwjQIlrgm', '', '', '', '', '', '', '', '', '', 'member', '', '', '1', '2016-07-12 08:14:59', '2016-07-12 08:14:59'),
(4, 'qwrqd@adsf.sd', 'qwrqd@adsf.sd', '$2y$10$nQ3aj5XQFfXhpcy0SLEjhOP8xuVU40ddP3O3eqGs0BXMLlGi3dTBG', '', '', '', '', '', '', '', '', '', 'member', '', '', '1', '2016-07-12 08:19:40', '2016-07-12 08:19:40'),
(5, '235432@asdf.sd', '2543324', '$2y$10$OpACK2Y0U9GKy1kr3Psvt.VIHIgoIM/mnnvTC3Rq2czX2u7e5Ey42', '', '', '', '', '', '', '', '', '', 'member', 'j4Y6RoaozFBn120VXEZV78FPtxrHS7cNkSAh3aZQoJk4xDPnP0D7zISvladg', '', '1', '2016-07-12 11:31:09', '2016-07-12 08:31:09'),
(6, 'chenged@gmail.com', 'chenged', '$2y$10$vgy57nqkWkY58kybSzGsheUlcrSkANxGXNFE0XIcFM/iMXnBKIqVa', '', '', '', '', '', '', '', '', '', 'member', '5BMkHUjJ2DKmltgveNnztS4ZPKbMGUPZNPl0ouPHZtamcbaPIy12ankmXp8Q', '', '1', '2016-09-03 19:11:08', '2016-09-03 19:11:08'),
(7, 'chup@gmail.com', 'chup', '$2y$10$nCqH13Lp/28hDaiVuRYU8eb.ks2sJmN.ntjz2wHdevLbdyQZq59si', '', '', '', '', '', '', '', '', '', 'member', '', '', '1', '2016-09-03 19:12:29', '2016-09-03 19:12:29'),
(8, 'minecraft@gmail.com', 'minecraft', '$2y$10$wt3X/f5RZ0rw64O/f5FNVODWr42IUqPu3fB/G3KVZKlNZepd6IAcG', '', '', '', '', '', '', '', '', '', 'member', 'Yd6mVk1Vac4GZdeMeH3m3r6368hYvZqHsOgMudClc5ZUQfhuBGazU1xlgmS9', '', '1', '2016-09-05 08:50:00', '2016-09-05 08:50:00');

-- --------------------------------------------------------

--
-- Структура таблицы `vips`
--

CREATE TABLE `vips` (
  `id` int(11) NOT NULL,
  `server_id` int(11) NOT NULL,
  `start` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `days` int(11) NOT NULL,
  `txn_id` varchar(22) COLLATE utf8_unicode_ci NOT NULL,
  `mc_gross` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `payment_status` enum('pending','completed','refunded') COLLATE utf8_unicode_ci NOT NULL,
  `payer_first_name` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `payer_last_name` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `payer_email` varchar(80) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `votes`
--

CREATE TABLE `votes` (
  `id` int(11) NOT NULL,
  `server_id` int(11) NOT NULL,
  `mc_username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ip` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `votes`
--

INSERT INTO `votes` (`id`, `server_id`, `mc_username`, `ip`, `created_at`) VALUES
(6, 6, 'gdsfndgsfds', '127.0.0.1', '2016-07-23 07:00:22'),
(7, 13, 'root', '127.0.0.1', '2016-09-02 09:23:56'),
(8, 18, 'doom', '127.0.0.1', '2016-09-04 14:33:57');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `pay_types`
--
ALTER TABLE `pay_types`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique` (`alias`);

--
-- Индексы таблицы `purchases`
--
ALTER TABLE `purchases`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `servers`
--
ALTER TABLE `servers`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `server_tag`
--
ALTER TABLE `server_tag`
  ADD KEY `tag_server_id` (`server_id`),
  ADD KEY `tag_tag_id` (`tag_id`);

--
-- Индексы таблицы `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_environment_key_unique` (`environment`,`key`),
  ADD KEY `settings_key_index` (`key`);

--
-- Индексы таблицы `statistics`
--
ALTER TABLE `statistics`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `vips`
--
ALTER TABLE `vips`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `votes`
--
ALTER TABLE `votes`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=243;
--
-- AUTO_INCREMENT для таблицы `pay_types`
--
ALTER TABLE `pay_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT для таблицы `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `purchases`
--
ALTER TABLE `purchases`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT для таблицы `servers`
--
ALTER TABLE `servers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT для таблицы `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1106;
--
-- AUTO_INCREMENT для таблицы `statistics`
--
ALTER TABLE `statistics`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;
--
-- AUTO_INCREMENT для таблицы `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=107;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `vips`
--
ALTER TABLE `vips`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `votes`
--
ALTER TABLE `votes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `server_tag`
--
ALTER TABLE `server_tag`
  ADD CONSTRAINT `server_server_id` FOREIGN KEY (`server_id`) REFERENCES `servers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tag_tag_id` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
