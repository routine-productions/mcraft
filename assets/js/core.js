function votifier() {
    if ($('#votifier_active').is(':checked')) {
        document.getElementById("votifier_div").style.display = "block";
    } else {
        document.getElementById("votifier_div").style.display = "none";
    }
}

function checkConnection() {

    if ($('#address').val().length > 1) {
        $('#connection_notice').html('<p>Please wait...</p>');
        $('#connection_status').show();

        $.ajax({
            type: 'POST',
            url: '/json/check-connection',
            data: {address: $('#address').val(), port: $('#port').val(), version: $('#version option:selected').text()},
            success: function (data) {

                if (data.state == "good") {
                    notice = '<span class="label label-success"><i class="glyphicon glyphicon-ok"></i> Good</span>';
                    $('#register-server').prop('disabled', false);
                } else if (data.state == "error") {
                    notice = '<span class="label label-danger"><i class="glyphicon glyphicon-remove"></i> ' + data.message + '</span>';
                    $('#register-server').prop('disabled', true);
                }

                $('#connection_notice').html(notice);

            },
            complete: function (xhr, textStatus) {
                if (textStatus != "success") {
                    notice = '<span class="label label-danger"><i class="glyphicon glyphicon-remove"></i> An error occurred</span>';
                    $('#connection_notice').html(notice);
                    $('#register-server').prop('disabled', true);
                }
            },
            dataType: 'json',
            async: true
        });
    }
}

$(document).ready(function () {
    $("body").tooltip({selector: '[data-toggle=tooltip]'});
});
