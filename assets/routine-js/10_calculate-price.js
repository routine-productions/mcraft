$('[name=pay_type]').change(function () {
    Count_Price(this);
});
$('[name=count]').keyup(function () {
    Count_Price(this);
});

function Count_Price(This) {
    var $Parent = $(This).parents('.Pay-Popup');
    var $Price_Span = $Parent.find('.Full-Cost');
    var $Sale_Span = $Parent.find('.Full-Sale');

    var Price = $Parent.find('#pay_type option:selected').attr('data-value');
    var Sale = $Parent.find('#pay_type option:selected').attr('data-sale');

    if ($Parent.find('#count').val()) {
        Price = Price * $Parent.find('#count').val();
        Sale = Sale * $Parent.find('#count').val();
    }

    if (Sale == 0) {
        $Sale_Span.html('');
    } else {
        $Sale_Span.html('Общая скидка: <span>' + Sale + '</span> р.');
    }

    $Price_Span.html('Общая стоимость: <span>' + Price + '</span> р.');
}

$('.Move-To-Payment').click(function () {
    var $Parent = $(this).parents('.Pay-Popup');
    var Price = $Parent.find('.Full-Cost span').text();

    $(this).parents('.JS-Modal-Blackout').removeClass('Visible');
    $('#Modal-Pay').addClass('Visible');

    return false;
});