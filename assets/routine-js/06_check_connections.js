function Check_Connection() {

    $('#connection_notice').html('<p>Ожидание соединения...</p>');
    $('#connection_status').show();

    $.ajax({
        type: 'POST',
        url: '/json/check-connection',
        data: {address: $('#address').val(), port: $('#port').val(), version: $('#version option:selected').text()},
        success: function (data) {
            if (data.state == "good") {
                Notice = 'Подключение установлено!';
                $('#register-server').prop('disabled', false);
            } else if (data.state == "error") {
                Notice = 'Ошибка подключения!   ';
                $('#register-server').prop('disabled', true);
            }

            $('#connection_notice').html(Notice);
        },

        complete: function (xhr, textStatus) {
            if (textStatus != "success") {
                notice = 'Ошибка подключения';
                $('#connection_notice').html(notice);
                $('#register-server').prop('disabled', true);
            }
        },
        dataType: 'json',
        async: true
    });
}

$('#check').click(function () {
    Check_Connection();
});