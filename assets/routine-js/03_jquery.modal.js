/*
 * Copyright (c) 2015
 * Routine JS - Modal
 * Version 0.4.0
 * Created 2015.12.03
 * Author Bunker Labs
 *
 *
 * button: class="JS-Modal-Button" href="#Modal-1"
 *
 *modal: class="JS-Modal-Blackout" id="Modal-1"
 */


$.fn.JS_Modal = function (Object) {
    var Methods = {};
    Object = $.extend({
        Buttons: this,
        Box: '.JS-Modal',
        Close: '.JS-Modal-Close',
        Blackout: '.JS-Modal-Blackout',
        Callback: {
            Before_Show: function (Event) {
                console.log($(Event.currentTarget));
                if ($(Event.currentTarget).hasClass('Move-To-Payment')) {

                    var $Parent = $(Event.currentTarget).parents('.Pay-Popup');
                    var Price = $Parent.find('.Full-Cost span').text();
                    $(Event.currentTarget).parents('.JS-Modal-Blackout').removeClass('Visible');

                    var Type = $Parent.find('[name=full_price] option:selected').attr('data-type');

                    var Title = 'MinecraftRating: "Сервер ' + $('.Full-Title').text().trim() +
                        '" (id=' + $('.Card-Full').attr('data-page-id') + ') - ' +
                        Type;

                    if ($Parent.find('[name=color]').length > 0) {
                        Title += ' (' + $Parent.find('[name=color] option:selected').text() + ')';
                    }

                    if ($Parent.find('[name=count]').length > 0) {
                        Title += ': ' + $Parent.find('[name=count]').val() + 'шт.';
                    }

                    Title += ' Срок действия: ' + $Parent.find('[name=full_price] option:selected').text();


                    // Yandex
                    $('.Pay-Popup .Yandex-Money iframe').attr('src',
                        $('.Pay-Popup .Yandex-Money iframe').attr('src') +
                        '&default-sum=' + Price +
                        '&targets=' + Title
                    );

                    // Web Money
                    $('.Pay-Popup .Web-Money input[name=LMI_PAYMENT_DESC]').val(Title);
                    $('.Pay-Popup .Web-Money input[name=LMI_PAYMENT_AMOUNT]').val(Price);
                }
            },
            After_Show: function () {
            },
            Before_Hide: function () {
            },
            After_Hide: function () {
            }
        }
    }, Object);

    Methods.Data_Params = function (Element) {
        $.each(Object, function (Index, Value) {
            if ((typeof Value == 'string' || typeof Value == 'number') && Element.attr('data-modal-' + Index.toLowerCase())) {
                Object[Index] = Element.attr('data-modal-' + Index.toLowerCase());
            }
        });
    };


    Methods.Show = function (Event) {
        Object.Target = $($(Event.currentTarget).attr('href'));

        Methods.Data_Params(Object.Target);

        Object.Callback.Before_Show(Event);
        Object.Target
            .addClass('Visible')
            .delay(parseFloat(Object.Target.css('transition-duration')) * 1000)
            .promise().done(Object.Callback.After_Show);

        Object.Target.click(Methods.Hide);
    };

    Methods.Hide = function (Event) {

        if ($(Event.target).is(Object.Close) || $(Event.target).is(Object.Blackout)) {
            Object.Callback.Before_Hide();
            Object.Target
                .removeClass('Visible')
                .delay(parseFloat(Object.Target.css('transition-duration')) * 1000).promise()
                .done(Object.Callback.After_Hide);

        }
    };

    Object.Buttons.click(Methods.Show);

    return this;
};

$('.JS-Modal-Button').JS_Modal();



 

