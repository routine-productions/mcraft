$(document).on('click', '.Login .Cabinet-Enter, .Reset-Password .Cabinet-Enter, .Registration .Cabinet-Enter', function () {
    var Form = $(this).parents('form');
    var Data = Form.serialize();
    var Action = Form.attr('action');

    $.ajax({
        type: 'POST',
        url: Action,
        data: Data,
        dataType: 'json',
        success: function (Responce) {
            if (Responce['status'] == 'success') {
                location.reload();
            } else if (Responce['status'] == 'success-password') {
                var Message = '';
                $.each(Responce['messages'], function (Index, Value) {
                    Message += Value + ' ';
                });
                Form.find('.Cabinet-Message').text(Message);
                Form.find('input[type=text]').val('');
            }
            else {
                var Message = '';
                $.each(Responce['messages'], function (Index, Value) {
                    Message += Value + ' ';
                });
                Form.find('.Cabinet-Message').text(Message);
            }
        }
    });
    return false;
});