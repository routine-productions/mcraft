(function () {
    var $Content = $('main'),
        $Header = $('body>header'),
        $Footer = $('body>footer');

    Sticky_Footer();
    $(window).on('resize', function () {
        Sticky_Footer();
    });

    function Sticky_Footer() {
        $Content.css('min-height', $(window).height() - $Header.outerHeight() - $Footer.height());
    }
})();