/*
 * Copyright (c) 2015
 * Routine JS -Page Class
 * Version 0.1.0
 * Create 2015.12.18
 * Author Routine.Productions

 * Usage:
 * work with tag <html>

 * /news/article#prologue
 * to
 * <html class="Page-News-Article Hash-Prologue">
 */
(function ($) {
    $(document).ready(function () {

        if (location.pathname != '/') {
            var Path = location.pathname.split('/').slice(1),
                Class = 'Page';

            $.each(Path, function (Key, Value) {
                Class += '-' + Value.charAt(0).toUpperCase() + Value.slice(1);
            });
            $('html').addClass(Class);
        } else {
            $('html').addClass('Page-Home');
        }

        if (location.hash) {
            $('html').addClass('Hash-' + location.hash.charAt(1).toUpperCase() + location.hash.slice(2));
        }else{
            $('html').addClass('No-Hash');
        }
    });
})(jQuery);
