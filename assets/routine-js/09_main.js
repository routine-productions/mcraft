// Filter Toggle
// $('.Filter-Button').click(function () {
//     if ($('.Filter-Menu').is(':hidden')) {
//         $('.Filter-Menu').slideDown(200).addClass('Active');
//         $('.Filter-Open').addClass('DisActive');
//         $('.Filter-Close').addClass('Active');
//         $('.Header-Filters').addClass('Active');
//     } else {
//         $('.Filter-Menu').slideUp(200).removeClass('Active');
//         $('.Filter-Open').removeClass('DisActive');
//         $('.Filter-Close').removeClass('Active');
//         $('.Header-Filters').removeClass('Active');
//     }
//     return false;
// });

$('.JS-Scroll-Top').click(function () {

    if ($(window).scrollTop() > 0) {
        var Scroll_Duration_String = $(this).attr('data-scroll-duration') ? $(this).attr('data-scroll-duration') : 400,
            Scroll_Duration = parseInt(Scroll_Duration_String);

        $('body, html').animate({
            scrollTop: 0
        }, Scroll_Duration);
    }

    return false;
});


$('.Header-Hamburger').click(function () {
    if ($(window).width() <= 1024) {
        if ($(this).hasClass('is-active')) {
            $(this).removeClass('is-active');
            $('.Header-Menu nav').removeClass('Active');

        } else {
            $(this).addClass('is-active');
            $('.Header-Menu nav').addClass('Active');
        }
    }
    return false;
});

// Search
$('.Header-Search svg').click(function () {
    var Search = $('.Header-Search input').val();
    if (Search.length > 1) {
        location.href = '/search/' + Search;
    }
});

$('.Header-Search input').click(function () {
    var Search = $('.Header-Search input').val();
    if (Search.length > 1) {
        location.href = '/search/' + Search;
    }
});

$('.Header-Search input').keypress(function (Event) {
    if (Event.which == 13) {
        var Search = $('.Header-Search input').val();
        if (Search.length > 1) {
            location.href = '/search/' + Search;
        }
    }
});


// Filter
// var Location = location.pathname.split('/');
// Location = '/' + Location[1] + '/' + Location[2];
// $('.Filter-Menu a').each(function () {
//     if ($(this).attr('href') == Location) {
//         $(this).parent().addClass('Active');
//         $('.Filter-Open').trigger('click');
//         return false;
//     }
// });

var Location = location.pathname.split('-');

if (Location[0] == '/servera') {
    $('.Filter-Open').trigger('click');
}


