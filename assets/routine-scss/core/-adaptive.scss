// @charset 'UTF-8';

$unit-intervals: (
        'px': 1,
        'em': 0.01,
        'rem': 0.1
) !default;

$im-media-support: true !default;

$im-no-media-breakpoint: 'desktop' !default;

$im-no-media-expressions: ('screen', 'portrait', 'landscape') !default;

@function log($message) {
    @if feature-exists('at-error') {
        @error $message;
    } @else {
        @warn $message;
        $_: noop();
    }

    @return $message;
}


@mixin log($message) {
    @if log($message) {}
}


@function noop() {}

@function im-intercepts-static-breakpoint($conditions...) {
    $no-media-breakpoint-value: map-get($Viewports, $im-no-media-breakpoint);

    @if not $no-media-breakpoint-value {
        @if log('`#{$im-no-media-breakpoint}` is not a valid breakpoint.') {}
    }

    @each $condition in $conditions {
        @if not map-has-key($Viewport-Expressions, $condition) {
            $operator: get-expression-operator($condition);
            $prefix: get-expression-prefix($operator);
            $value: get-expression-value($condition, $operator);

            // scss-lint:disable SpaceAroundOperator
            @if ($prefix == 'max' and $value <= $no-media-breakpoint-value) or
          ($prefix == 'min' and $value > $no-media-breakpoint-value) {
                @return false;
            }
        } @else if not index($im-no-media-expressions, $condition) {
            @return false;
        }
    }

    @return true;
}

@function get-expression-operator($expression) {
    @each $operator in ('>=', '>', '<=', '<', '≥', '≤') {
        @if str-index($expression, $operator) {
            @return $operator;
        }
    }

    // It is not possible to include a mixin inside a function, so we have to
    // rely on the `log(..)` function rather than the `log(..)` mixin. Because
    // functions cannot be called anywhere in Sass, we need to hack the call in
    // a dummy variable, such as `$_`. If anybody ever raise a scoping issue with
    // Sass 3.3, change this line in `@if log(..) {}` instead.
    $_: log('No operator found in `#{$expression}`.');
}

@function get-expression-dimension($expression, $operator) {
    $operator-index: str-index($expression, $operator);
    $parsed-dimension: str-slice($expression, 0, $operator-index - 1);
    $dimension: 'width';

    @if str-length($parsed-dimension) > 0 {
        $dimension: $parsed-dimension;
    }

    @return $dimension;
}

@function get-expression-prefix($operator) {
    @return if(index(('<', '<=', '≤'), $operator), 'max', 'min');
}

@function get-expression-value($expression, $operator) {
    $operator-index: str-index($expression, $operator);
    $value: str-slice($expression, $operator-index + str-length($operator));

    @if map-has-key($Viewports, $value) {
        $value: map-get($Viewports, $value);
    } @else {
        $value: to-number($value);
    }

    $interval: map-get($unit-intervals, unit($value));

    @if not $interval {
        $_: log('Unknown unit `#{unit($value)}`.');
    }

    @if $operator == '>' {
        $value: $value + $interval;
    } @else if $operator == '<' {
        $value: $value - $interval;
    }

    @return $value;
}


@function parse-expression($expression) {
    // If it is part of $Viewport-Expressions, it has no operator
    // then there is no need to go any further, just return the value
    @if map-has-key($Viewport-Expressions, $expression) {
        @return map-get($Viewport-Expressions, $expression);
    }

    $operator: get-expression-operator($expression);
    $dimension: get-expression-dimension($expression, $operator);
    $prefix: get-expression-prefix($operator);
    $value: get-expression-value($expression, $operator);

    @return '(#{$prefix}-#{$dimension}: #{$value})';
}

@function slice($list, $start: 1, $end: length($list)) {
    @if length($list) < 1 or $start > $end {
        @return ();
    }

    $result: ();

    @for $i from $start through $end {
        $result: append($result, nth($list, $i));
    }

    @return $result;
}

@function to-number($value) {
    @if type-of($value) == 'number' {
        @return $value;
    } @else if type-of($value) != 'string' {
        $_: log('Value for `to-number` should be a number or a string.');
    }

    $result: 0;
    $digits: 0;
    $minus: str-slice($value, 1, 1) == '-';
    $numbers: ('0': 0, '1': 1, '2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8, '9': 9);

    @for $i from if($minus, 2, 1) through str-length($value) {
        $character: str-slice($value, $i, $i);

        @if not (index(map-keys($numbers), $character) or $character == '.') {
            @return to-length(if($minus, -$result, $result), str-slice($value, $i))
        }

        @if $character == '.' {
            $digits: 1;
        } @else if $digits == 0 {
            $result: $result * 10 + map-get($numbers, $character);
        } @else {
            $digits: $digits * 10;
            $result: $result + map-get($numbers, $character) / $digits;
        }
    }

    @return if($minus, -$result, $result);
}


@function to-length($value, $unit) {
    $units: ('px': 1px, 'cm': 1cm, 'mm': 1mm, '%': 1%, 'ch': 1ch, 'pc': 1pc, 'in': 1in, 'em': 1em, 'rem': 1px, 'pt': 1pt, 'ex': 1ex, 'vw': 1vw, 'vh': 1vh, 'vmin': 1vmin, 'vmax': 1vmax);

    @if not index(map-keys($units), $unit) {
        $_: log('Invalid unit `#{$unit}`.');
    }

    @return $value * map-get($units, $unit);
}

///
/// This mixin aims at redefining the configuration just for the scope of
/// the call. It is helpful when having a component needing an extended
/// configuration such as custom breakpoints (referred to as tweakpoints)
/// for instance.
///
/// @author Hugo Giraudel
///
/// @param {Map} $tweakpoints [()] - Map of tweakpoints to be merged with `$Viewports`
/// @param {Map} $tweak-media-expressions [()] - Map of tweaked media expressions to be merged with `$media-expression`
///
/// @example scss - Extend the global breakpoints with a tweakpoint
///  @include media-context(('custom': 678px)) {
///    .foo {
///      @include media('>phone', '<=custom') {
///       // ...
///      }
///    }
///  }
///
/// @example scss - Extend the global media expressions with a custom one
///  @include media-context($tweak-media-expressions: ('all': 'all')) {
///    .foo {
///      @include media('all', '>phone') {
///       // ...
///      }
///    }
///  }
///
/// @example scss - Extend both configuration maps
///  @include media-context(('custom': 678px), ('all': 'all')) {
///    .foo {
///      @include media('all', '>phone', '<=custom') {
///       // ...
///      }
///    }
///  }
///

@mixin Adaptive-Context($tweakpoints: (), $tweak-media-expressions: ()) {
    // Save global configuration
    $global-breakpoints: $Viewports;
    $global-media-expressions: $Viewport-Expressions;

    // Update global configuration
    $Viewports: map-merge($Viewports, $tweakpoints) !global;
    $Viewport-Expressions: map-merge($Viewport-Expressions, $tweak-media-expressions) !global;

    @content;

    // Restore global configuration
    $Viewports: $global-breakpoints !global;
    $Viewport-Expressions: $global-media-expressions !global;
}
////
/// include-media public exposed API
/// @author Eduardo Boucas
/// @access public
////


///
/// Generates a media query based on a list of conditions
///
/// @param {Arglist}   $conditions  - Media query conditions
///
/// @example scss - With a single set breakpoint
///  @include media('>phone') { }
///
/// @example scss - With two set breakpoints
///  @include media('>phone', '<=tablet') { }
///
/// @example scss - With custom values
///  @include media('>=358px', '<850px') { }
///
/// @example scss - With set breakpoints with custom values
///  @include media('>desktop', '<=1350px') { }
///
/// @example scss - With a static expression
///  @include media('retina2x') { }
///
/// @example scss - Mixing everything
///  @include media('>=350px', '<tablet', 'retina3x') { }
///

@mixin Adaptive($conditions...) {
    // scss-lint:disable SpaceAroundOperator
    @if ($im-media-support and length($conditions) == 0) or
      (not $im-media-support and im-intercepts-static-breakpoint($conditions...)) {
        @content;
    } @else if ($im-media-support and length($conditions) > 0) {
        @media #{unquote(parse-expression(nth($conditions, 1)))} {
            // Recursive call
            @include Adaptive(slice($conditions, 2)...) {
            @content;
        }
        }
    }
}