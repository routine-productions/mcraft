"use strict";

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Require packages
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    minifyCss = require('gulp-minify-css'),
    uglify = require('gulp-uglify'),
    autoprefixer = require('gulp-autoprefixer'),
    rename = require("gulp-rename"),
    concat = require("gulp-concat"),
    combineMq = require('gulp-combine-mq');

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Watch changes in files
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

gulp.task('watch', function () {
    gulp.watch('./assets/routine-scss/**/*.scss', ['scss']);
    gulp.watch('./assets/routine-js/**/*.js', ['js']);
});

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// scss
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

gulp.task('scss', function () {
    gulp.src('./assets/routine-scss/index.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(combineMq({
            beautify: false
        }))
        .pipe(autoprefixer({browsers: ['> 1%', 'last 2 version', 'IE 9-11'], cascade: false}))
        .pipe(minifyCss())
        .pipe(rename('index.min.css'))
        .pipe(gulp.dest('./assets/css'));
});

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// js
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

gulp.task('js', function () {

    gulp.src('./assets/routine-js/**/*.js')
        .pipe(concat('index.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./assets/js'));
});

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////