<?php

function view($page, $data = array())
{    
    $path = BASEPATH . 'views' . DS . $page . '.php';
    if (is_file($path)) {
        extract($data);
        require $path;
    }
}