<?php
error_reporting(0);
session_start();
session_name();
session_name('install');

define('DS', DIRECTORY_SEPARATOR);
define('BASEPATH', realpath(dirname(__FILE__)) . DS);

$step = isset($_SESSION['step']) ? (int)$_SESSION['step'] : 1;

require 'lib.php';

require BASEPATH . 'steps' . DS . 'step_' . $step . '.php';

exit;

?>