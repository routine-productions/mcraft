<h3>Step 1 - Check compatibility.</h3>
<p>We will check if your system is compatible with Minecraft Server List.</p>
<br><br>

<h4>PHP Version:</h4>
Version required: <?php echo $php_version['needed']; ?><br />
Your version: <?php echo $php_version['current']; ?><br />
Status: <?php if ($php_version['status'] == false): ?> <span class="not_ok">FAIL</span> <?php else: ?> <span class="ok">PASSED</span> <?php endif; ?>
<br>
<br>
<h4>PDO</h4>
PDO in this momment is<b><?php if ($pdo === false) :  ?> uninstalled <?php else: ?> installed <?php endif; ?></b>.<br />
Status: <?php if ($pdo === false): ?> <span class="not_ok">FAIL</span> <?php else: ?> <span class="ok">PASSED</span> <?php endif; ?>
<br>
<br>
<h4>Sockets</h4>
Sockets in this momment are<b><?php if ($sockets === false) :  ?> uninstalled <?php else: ?> installed <?php endif; ?></b>.<br />
Status: <?php if ($sockets === false): ?> <span class="not_ok">FAIL</span> <?php else: ?> <span class="ok">PASSED</span> <?php endif; ?>
<br>
 <br>
<h4>GD Library</h4>
GD library in this momment is<b><?php if ($gd === false) :  ?> uninstalled <?php else: ?> installed <?php endif; ?></b>.<br />
Status: <?php if ($gd === false): ?> <span class="not_ok">FAIL</span> <?php else: ?> <span class="ok">PASSED</span> <?php endif; ?>
<br>
<br>

<hr>
<br>
<h4>Check if <i>msq/app/config/database.php</i> is writable</h4>
<i>config/database.php</i> in this momment is<b><?php if ($config_database === false) :  ?> not writable <?php else: ?> writable <?php endif; ?></b>.<br />
Status: <?php if ($config_database === false): ?> <span class="not_ok">FAIL</span> <?php else: ?> <span class="ok">PASSED</span> <?php endif; ?>

<br>
<br>
<h4>Check if <i>uploads/images</i> folder is writable</h4>
<i>uploads/images</i> folder in this momment is<b><?php if ($uploads === false) :  ?> not writable <?php else: ?> writable <?php endif; ?></b>.<br />
Status: <?php if ($uploads === false): ?> <span class="not_ok">FAIL</span> <?php else: ?> <span class="ok">PASSED</span> <?php endif; ?>

<br> <br>
<?php if ($final === false): ?>
    <h3 class="not_ok">Your system does not meet minimum requirements to be perform installation.</h3>
<?php else: ?>
    <h3 class="ok">Your system is compatible. Please proceed to next step:</h3>
    <form method="post"><input type="submit" name="submit"  class="button" value="Send" /></form>
<?php endif; ?>