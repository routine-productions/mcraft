<?php
session_start();
session_destroy();
?>
<h3>This script was installed successfully.</h3>
<p>Please delete <b>install</b> folder.</p>
<p>Please set chomd <b>0644</b> to <b>msq/app/config/database.php</b> file.</p>
<p>Login with email <b>demo@demo.com</b> and password <b>demo</b></p>
<pre>
    <a href="http://<?php echo $_SERVER["SERVER_NAME"] . '/account/login';  ?>" class="button">Login</a>
</pre>
