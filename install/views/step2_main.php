<h3>Step 2 - Database settings.</h3>
<p>For the website to connect to database you must be configured.</p>
<br /><br />
<form id="contactform" class="rounded" method="post">
<h3>Database data:</h3>
<div class="field">
	<label for="server">Server:</label>
  	<input type="text" class="input" name="server" id="server" value="localhost"/>
	<p class="hint">Your database server.</p>
</div>
<div class="field">
	<label for="username">Username:</label>
  	<input type="text" class="input" name="username" id="username" value="root"/>
	<p class="hint">Your database username.</p>
</div>
<div class="field">
	<label for="password">Password:</label>
  	<input type="text" class="input" name="password" id="password" value="password" />
	<p class="hint">Your database password.</p>
</div>
<div class="field">
    <label for="name">Name:</label>
    <input type="text" class="input" name="name" id="name" value="databasename" />
    <p class="hint">Your database name.</p>
</div>
<input type="submit" name="submit"  class="button" value="Proceed" />
</form>
<?php if (isset($status)): if ($status === false):?>
<br />  
    <h3 class="not_ok">Could not connect to the database.</h3>
    Check the data better.
<?php else: ?>
<br />
    <h3 class="ok">The connection to the database has been successfully performed.</h3>
    <?php if(isset($in_config)): ?>
        <p>The file <b>database.php</b> is not writable. Please copy this code in <b>msq/app/config/database.php</b>:</p>
        <p>Do not continue to the next step until you make changes.</p>
        <div class="field">
            <textarea cols="55" rows="12"><?php echo $in_config; ?></textarea>
        </div>
    <?php endif; ?>
    <form method="post"><input type="submit" name="Submit"  class="button" value="Next step" /></form>
<?php endif; endif; ?>