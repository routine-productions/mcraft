<?php

view('header', array('site_title' => 'Step 2 - Database settings'));

$check = false;

$data = array();

if ( isset($_POST['server']) && isset($_POST['username']) && isset($_POST['name'])) {
    try {
        $dbh = new PDO(sprintf('mysql:host=%s;dbname=%s', $_POST['server'], $_POST['name']), $_POST['username'], $_POST['password']);

        if ($dbh !== false)
        {
            $_SESSION['mysql'] = array(
                'host'     => $_POST['server'],
                'username' => $_POST['username'],
                'password' => $_POST['password'],
                'name'     => $_POST['name']
            );
            $status = true;
        }
    } catch (PDOException $e) {
        $status = false;
    }
}

if (isset($status)) {
    $data['status'] = $status;

    if ($status === true) {        
        $file_data = "<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | PDO Fetch Style
    |--------------------------------------------------------------------------
    |
    | By default, database results will be returned as instances of the PHP
    | stdClass object; however, you may desire to retrieve records in an
    | array format for simplicity. Here you can tweak the fetch style.
    |
    */

    'fetch' => PDO::FETCH_CLASS,

    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */

    'default' => 'mysql',

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
    */

    'connections' => array(

        'sqlite' => array(
            'driver'   => 'sqlite',
            'database' => __DIR__.'/../database/production.sqlite',
            'prefix'   => '',
        ),

        'mysql' => array(
            'driver'    => 'mysql',
            'host'      => '{$_POST['server']}',
            'database'  => '{$_POST['name']}',
            'username'  => '{$_POST['username']}',
            'password'  => '{$_POST['password']}',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ),

        'pgsql' => array(
            'driver'   => 'pgsql',
            'host'     => 'localhost',
            'database' => 'forge',
            'username' => 'forge',
            'password' => '',
            'charset'  => 'utf8',
            'prefix'   => '',
            'schema'   => 'public',
        ),

        'sqlsrv' => array(
            'driver'   => 'sqlsrv',
            'host'     => 'localhost',
            'database' => 'database',
            'username' => 'root',
            'password' => '',
            'prefix'   => '',
        ),

    ),

    /*
    |--------------------------------------------------------------------------
    | Migration Repository Table
    |--------------------------------------------------------------------------
    |
    | This table keeps track of all the migrations that have already run for
    | your application. Using this information, we can determine which of
    | the migrations on disk haven't actually been run in the database.
    |
    */

    'migrations' => 'migrations',

    /*
    |--------------------------------------------------------------------------
    | Redis Databases
    |--------------------------------------------------------------------------
    |
    | Redis is an open source, fast, and advanced key-value store that also
    | provides a richer set of commands than a typical key-value systems
    | such as APC or Memcached. Laravel makes it easy to dig right in.
    |
    */

    'redis' => array(

        'cluster' => false,

        'default' => array(
            'host'     => '127.0.0.1',
            'port'     => 6379,
            'database' => 0,
        ),

    ),

);";
        $file = '..' . DS . 'msq' . DS . 'app' . DS . 'config' . DS . 'database.php';

        if (is_file($file))
        {
            if (is_writable($file))
            {
                file_put_contents($file, $file_data);                
            }
            else
            {
                $data['in_config'] = $file_data;
            }
        }
        if (isset($_POST['submit']))
        {
            $_SESSION['step'] = 3;
            header('location: index.php');
        }
    }
}

view('step2_main', $data);

view('footer');