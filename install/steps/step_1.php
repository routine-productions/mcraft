<?php

view('header', array('site_title' => 'Step 1 - Compatibility'));

$check = true;

if (version_compare(PHP_VERSION, '5.4.0', '>=')) {  
    $is_php = true;
} else {
    $check = false;   
    $is_php = false;
}

$pdo = true;

if (class_exists('pdo') === false) {
    $pdo = false;
    $check = false;
}

$sockets = true;

if (extension_loaded('sockets') === false) {
    $sockets = false;
    $check = false;
}

$gd = true;

if (extension_loaded('gd') === false) {
    $gd = false;
    $check = false;
}

$config_database = true;

if (is_writable('..' . DS . 'msq' . DS . 'app' . DS . 'config' . DS . 'database.php') === false) {
    $config_database = false;
    $check = false;
}

$uploads = true;

if (is_writable('..' . DS . 'uploads' . DS . 'images') === false) {
    $uploads = false;
    $check = false;
}

$data = array(
    'php_version' => array(
        'needed'  => '5.4.0',
        'current' => PHP_VERSION,
        'pdo'   => $pdo,
        'status'  => $is_php
    ),
    'sockets' => $sockets,
    'gd' => $gd,
    'config_database' => $config_database,
    'uploads' => $uploads,
    'final'   => $check
);
view('step1_main', $data);

if (isset($_POST['submit']))
{
    $_SESSION['step'] = 2;
    header('location: index.php');
}

view('footer');