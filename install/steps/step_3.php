<?php

view('header', array('site_title' => 'Step 3 - Preparation database'));

$dbh = new PDO(sprintf('mysql:host=%s;dbname=%s', $_SESSION['mysql']['host'], $_SESSION['mysql']['name']), $_SESSION['mysql']['username'], $_SESSION['mysql']['password']);

$data = array();

if (isset($_POST['Submit'])) {
    $exec = $dbh->exec(file_get_contents(BASEPATH . 'sql' . DS . 'sql.sql'));

    if ($exec !== false) {
        $data['status'] = true;
    } else {
        $data['status'] = false;
    }

    $_SESSION['step'] = 4;
  
}

view('step3_main', $data);
view('footer');